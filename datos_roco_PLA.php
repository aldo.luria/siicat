<?php 
    include "procesossesiones/seguridad.php";
    IF($_SESSION['tipo'] == "PLANT"){
?>  
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ver Examen ROCO</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
	<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<?php 
		require_once "php/connect.php";
		require_once "procesosroco/actualizarroco_PLA.php";
	 ?>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>