<!DOCTYPE html>

<?php 
error_reporting( ~E_NOTICE );
 require_once "php/connect.php";

 if(isset($_GET['id_alumno']) && !empty($_GET['id_alumno']))
 {
  $id = $_GET['id_alumno'];
  $stmt_edit = $pdo->prepare('SELECT AA.*,M.nombre_mun FROM alta_alumnos AS AA LEFT JOIN municipios AS M ON AA.municipio = M.folio_mun WHERE id_alumno=:id_alumno');
  $stmt_edit->execute(array(':id_alumno'=>$id));
  $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
  extract($edit_row);
 }
 else
 {
  header("Location: alta_alumnos.php");
 }

if(isset($_POST['btn_save_updates']))
{
            $id_alumno=$_POST['id_alumno'];
            $apellidop=$_POST['apellidop'];
            //$foto=$_POST['foto'];
            $apellidom=$_POST['apellidom'];
            $nombre=$_POST['nombre'];
            $curp=$_POST['curp'];
            $domicilio=$_POST['domicilio'];
            $localidad=$_POST['localidad'];
            $cp=$_POST['cp'];
            $municipio=$_POST['municipio'];
            $estado=$_POST['estado'];
            $civil=$_POST['civil'];
            $discapacidad=$_POST['discapacidad'];
            $tel=$_POST['tel'];
            $sexo=$_POST['sexo'];
            $edad=$_POST['edad'];
            $correo=$_POST['correo'];

            $imgFile = $_FILES['foto']['name'];
            $tmp_dir = $_FILES['foto']['tmp_name'];
            $imgSize = $_FILES['foto']['size'];
                 
            if($imgFile)
            {
                $upload_dir = 'img/alumno/'; // upload directory 
                $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
                $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
                $nombre_img = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt;
                if(in_array($imgExt, $valid_extensions))
                {   
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['foto_alum']);
                    move_uploaded_file($tmp_dir,$upload_dir.$nombre_img);
                }
                else
                {
                    $errMSG = "El archivo debe pesar menos de 5MB.";
                }
               }
               else
               {
                $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
               } 
              }
              else
              {
               // if no image selected the old image remain as it is.
                $nombre_img = $edit_row['foto_alum']; // old image from database
              } 


            $imgFile2 = $_FILES['imgcurp']['name'];
            $tmp_dir2 = $_FILES['imgcurp']['tmp_name'];
            $imgSize2 = $_FILES['imgcurp']['size'];
                 
            if($imgFile2)
            {
                $upload_dir2 = 'img/alumno/'; // upload directory 
                $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension
                $valid_extensions2 = array('pdf'); // valid extensions
                $nombre_curp = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt2;
                if(in_array($imgExt2, $valid_extensions2))
                {   
                if($imgSize2 < 5000000)
                {
                    unlink($upload_dir2.$edit_row['imagen_curp']);
                    move_uploaded_file($tmp_dir2,$upload_dir2.$nombre_curp);
                }
                else
                {
                    $errMSG = "El archivo debe pesar menos de 5MB.";
                }
               }
               else
               {
                $errMSG = "Inserte un archivo PNG.";  
               } 
              }
              else
              {
               // if no image selected the old image remain as it is.
                $nombre_curp = $edit_row['imagen_curp']; // old image from database
              } 


            $imgFile3 = $_FILES['imgacta']['name'];
            $tmp_dir3 = $_FILES['imgacta']['tmp_name'];
            $imgSize3 = $_FILES['imgacta']['size'];
                 
            if($imgFile3)
            {
                $upload_dir3 = 'img/alumno/'; // upload directory 
                $imgExt3 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION)); // get image extension
                $valid_extensions3 = array('pdf'); // valid extensions
                $nombre_acta = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt3;
                if(in_array($imgExt3, $valid_extensions3))
                {   
                if($imgSize3 < 5000000)
                {
                    unlink($upload_dir3.$edit_row['imagen_actan']);
                    move_uploaded_file($tmp_dir3,$upload_dir3.$nombre_acta);
                }
                else
                {
                    $errMSG = "El archivo debe pesar menos de 5MB.";
                }
               }
               else
               {
                $errMSG = "Inserte un archivo PNG.";  
               } 
              }
              else
              {
               // if no image selected the old image remain as it is.
                $nombre_acta = $edit_row['imagen_actan']; // old image from database
              } 


            $imgFile4 = $_FILES['imgdomi']['name'];
            $tmp_dir4 = $_FILES['imgdomi']['tmp_name'];
            $imgSize4 = $_FILES['imgdomi']['size'];
                 
            if($imgFile4)
            {
                $upload_dir4 = 'img/alumno/'; // upload directory 
                $imgExt4 = strtolower(pathinfo($imgFile4,PATHINFO_EXTENSION)); // get image extension
                $valid_extensions4 = array('pdf'); // valid extensions
                $nombre_domi = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt4;
                if(in_array($imgExt4, $valid_extensions4))
                {   
                if($imgSize4 < 5000000)
                {
                    unlink($upload_dir4.$edit_row['imagen_compdom']);
                    move_uploaded_file($tmp_dir4,$upload_dir4.$nombre_domi);
                }
                else
                {
                    $errMSG = "El archivo debe pesar menos de 5MB.";
                }
               }
               else
               {
                $errMSG = "Inserte un archivo PNG.";  
               } 
              }
              else
              {
               // if no image selected the old image remain as it is.
                $nombre_domi = $edit_row['imagen_compdom']; // old image from database
              } 


            $imgFile5 = $_FILES['imgdomi']['name'];
            $tmp_dir5 = $_FILES['imgdomi']['tmp_name'];
            $imgSize5 = $_FILES['imgdomi']['size'];
                 
            if($imgFile5)
            {
                $upload_dir5 = 'img/alumno/'; // upload directory 
                $imgExt5 = strtolower(pathinfo($imgFile5,PATHINFO_EXTENSION)); // get image extension
                $valid_extensions5 = array('pdf'); // valid extensions
                $nombre_estu = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt5;
                if(in_array($imgExt5, $valid_extensions5))
                {   
                if($imgSize5 < 5000000)
                {
                    unlink($upload_dir5.$edit_row['imagen_compest']);
                    move_uploaded_file($tmp_dir5,$upload_dir5.$nombre_estu);
                }
                else
                {
                    $errMSG = "El archivo debe pesar menos de 5MB.";
                }
               }
               else
               {
                $errMSG = "Inserte un archivo PNG.";  
               } 
              }
              else
              {
               // if no image selected the old image remain as it is.
                $nombre_estu = $edit_row['imagen_compest']; // old image from database
              } 

                // if no error occured, continue ....
            if(!isset($errMSG))
            {

                $consulta2=$pdo->prepare("UPDATE alta_alumnos SET apellido_p=:apellidop, apellido_m=:apellidom, nombre_a=:nombre, sexo=:sexo, curp=:curp, edad=:edad, num_tel=:tel, domicilio=:domicilio, localidad=:localidad, CP=:cp, municipio=:municipio, estado=:estado, estado_civil=:civil, discapacidad=:discapacidad, imagen_actan=:imgacta, imagen_curp=:imgcurp, imagen_compdom=:imgdomi, imagen_compest=:imgestu, foto_alum=:foto, email=:correo WHERE id_alumno=:id_alumno");

                $consulta2->bindParam(':apellidop',$apellidop);
                $consulta2->bindParam(':apellidom',$apellidom);
                $consulta2->bindParam(':nombre',$nombre);
                $consulta2->bindParam(':sexo',$sexo);
                $consulta2->bindParam(':curp',$curp);
                $consulta2->bindParam(':edad',$edad);
                $consulta2->bindParam(':tel',$tel);
                $consulta2->bindParam(':domicilio',$domicilio);
                $consulta2->bindParam(':localidad',$localidad);
                $consulta2->bindParam(':cp',$cp);
                $consulta2->bindParam(':municipio',$municipio);
                $consulta2->bindParam(':estado',$estado);
                $consulta2->bindParam(':civil',$civil);
                $consulta2->bindParam(':discapacidad',$discapacidad);
                $consulta2->bindParam(':imgacta',$nombre_acta);
                $consulta2->bindParam(':imgcurp',$nombre_curp);
                $consulta2->bindParam(':imgdomi',$nombre_domi);
                $consulta2->bindParam(':imgestu',$nombre_estu);
                $consulta2->bindParam(':foto',$nombre_img);
                $consulta2->bindParam(':correo',$correo);
                $consulta2->bindParam(':id_alumno',$id_alumno);

                if($consulta2->execute()){
                header("refresh:0");
                }else{
                    echo '
                        <a href="alta_alumnos.php" style="text-decoration: none">
                            <div class="container-fluid text-center alert alert-danger" style="margin-bottom: 0px">
                                <h4>Error: No se Actualizaron los datos</h4>
                            </div>
                        </a>
                    ';
                }
            }
 
}

?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro Instructores</title>
        <style type="text/css">
        #formulario{
            box-shadow: 5px 5px 20px #9f9f9f;
            background-color: #e7e7e7;
        }
        a{
            color: #186d41 !important;
        }
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #9f282f !important;
        }
    </style>
</head>
<header>
    <?php require_once "partes/nav_PLA.php"; ?>
</header>
<body>
    <div class="page-header text-center">
        <h4><strong>ACTUALIZACIÓN - ALUMNOS</strong></h4>
    </div>

    <div class="container">
        <form method="post" enctype="multipart/form-data">

        <input type="hidden" name="id_alumno" value="<?php echo $edit_row['id_alumno'] ?>">

        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos Personales</div>
                <div class="row" id="titulo"><strong>Datos Generales</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-2">Apellido Paterno</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="apellidop" required="" value="<?php echo $edit_row['apellido_p'] ?>">
                    </div>

                    <label class="col-sm-2">Apelido Materno</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="apellidom" required="" value="<?php echo $edit_row['apellido_m'] ?>">
                    </div>
                </div>

                <div class="row text-left">
                    <label  class="col-sm-2">Nombre(s)</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="nombre" required="" value="<?php echo $edit_row['nombre_a'] ?>">
                    </div>

                    <label class="col-sm-2">Foto</label>
                    <div class="col-sm-4">
                        <img src="img/alumno/<?php echo $edit_row['foto_alum'] ?>" width="200px"><br>
                        <input class="form-control input-sm" type="file" name="foto" accept="image/*">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CURP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="curp" required="" value="<?php echo $edit_row['curp'] ?>">
                    </div>

                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Dirección</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Domicilio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="domicilio" required="" value="<?php echo $edit_row['domicilio'] ?>">
                    </div>

                    <label class="col-sm-2">Colonia o Localidad</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="localidad" required="" value="<?php echo $edit_row['localidad'] ?>">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" name="cp" min="0" required="" value="<?php echo $edit_row['CP'] ?>">
                    </div>
                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" type="text" name="municipio" required="" id="sel1">
                            <option value="<?php echo $edit_row['municipio'] ?>"><?php echo $edit_row['nombre_mun'] ?> </option>
                            <?php 
                                $sql = "SELECT folio_mun, nombre_mun FROM municipios ORDER BY nombre_mun ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['folio_mun'].'">'.$row['nombre_mun'].'</option>';
                                }

                            ?>
                        </select>

                    </div>
                    
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Estado</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="estado" value="<?php echo $edit_row['estado'] ?>">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Información adicional</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Estado Civil</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="civil" value="<?php echo $edit_row['estado_civil'] ?>">
                        </div>
                        <label class="col-sm-2">Discapacidad</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="discapacidad" value="<?php echo $edit_row['discapacidad'] ?>">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">No Teléfono</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="tel" min="0" value="<?php echo $edit_row['num_tel'] ?>">
                        </div>

                        <label class="col-sm-2">Sexo</label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="sexo" id="sel1">
                                <option value="<?php echo $edit_row['sexo'] ?>"><?php echo $edit_row['sexo'] ?></option>
                                <option>--- Seleccionar sexo ---</option>
                                <option value="FEMENINO">FEMENINO</option>
                                <option value="MASCULINO">MASCULINO</option>
                            </select>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Edad</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="edad" min="0" value="<?php echo $edit_row['edad'] ?>">
                        </div>

                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="email" name="correo" value="<?php echo $edit_row['email'] ?>">
                        </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Documentos</div>
                <div class="row" id="titulo"><strong>Ingresa los documentos escaneados en la casilla que correspondan.</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">CURP</label>
                        <div class="col-sm-4">
                            <!--<iframe src="img/alumno/<?php echo $edit_row['imagen_curp'] ?>" style="width:200px; height:200px;" frameborder="0"></iframe><br>-->
                            <input class="form-control input-sm" type="file" name="imgcurp" accept="application/pdf">
                            <a href="img/alumno/<?php echo $edit_row['imagen_curp'] ?>" target="_blank"><i class="fa fa-eye"> </i> CURP</a>
                        </div>

                        <label class="col-sm-2">Acta de Nacimiento</label>
                        <div class="col-sm-4">
                            <!--<iframe src="img/alumno/<?php echo $edit_row['imagen_actan'] ?>" style="width:200px; height:200px;" frameborder="0"></iframe><br>-->
                            <input class="form-control input-sm" type="file" name="imgacta" accept="application/pdf">
                            <a href="img/alumno/<?php echo $edit_row['imagen_actan'] ?>" target="_blank"><i class="fa fa-eye"> </i> Acta de Nacimiento</a>
                        </div>
                </div>
                <div class="row text-left">
                        <label class="col-sm-2">Comprobante de Domicilio</label>
                        <div class="col-sm-4">
                            <!--<iframe src="img/alumno/<?php echo $edit_row['imagen_compdom'] ?>" style="width:200px; height:200px;" frameborder="0"></iframe><br>-->
                            <input class="form-control input-sm" type="file" name="imgdomi" accept="application/pdf">
                            <a href="img/alumno/<?php echo $edit_row['imagen_compdom'] ?>" target="_blank"><i class="fa fa-eye"> </i> Comprobante de Domicilio</a>
                        </div>

                        <label class="col-sm-2">Certificado de Estudios</label>
                        <div class="col-sm-4">
                            <!--<iframe src="img/alumno/<?php echo $edit_row['imagen_compest'] ?>" style="width:200px; height:200px;" frameborder="0"></iframe><br>-->
                            <input class="form-control input-sm" type="file" name="imgestu" accept="application/pdf">
                            <a href="img/alumno/<?php echo $edit_row['imagen_compest'] ?>" target="_blank"><i class="fa fa-eye"> </i> Certificado de Estudios</a>
                        </div>
                </div>
            </div>

            
            

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btn_save_updates" value="Guardar">
            <a class="btn btn-danger" href="alta_alumnos.php" style="color: white !important">Cancelar</a>
            </div> 
        </form>
    </div>
    <br>
</body>
</html>