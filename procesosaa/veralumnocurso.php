<!DOCTYPE html>

<?php  
error_reporting( ~E_NOTICE );
 require_once "php/connect.php";
 
 if(isset($_GET['id_alumno']) && !empty($_GET['id_alumno']))
 {
  $id = $_GET['id_alumno'];
  //Información Alumno
  $consulta = $pdo->prepare('SELECT A.*,M.nombre_mun FROM alta_alumnos AS A LEFT JOIN municipios AS M ON A.municipio = M.folio_mun WHERE id_alumno=:id');
  $consulta->execute(array(':id'=>$id));
  $fila = $consulta->fetch(PDO::FETCH_ASSOC);
  extract($fila);

  //Cursos Regular
  $registros=$pdo->prepare("
    SELECT SQL_CALC_FOUND_ROWS ACR.*,CR.*,M.nombre_mun,P.nombre,I.Nombre,O.* FROM alumno_curso2018reg AS ACR LEFT JOIN cursos2018_regular AS CR ON ACR.id_curso = CR.folio LEFT JOIN municipios AS M ON CR.id_mun_c = M.folio_mun LEFT JOIN plantel AS P ON CR.id_plantel = P.id LEFT JOIN instructores AS I ON CR.id_instructor = I.id LEFT JOIN oferta AS O ON CR.id_modulo_c = O.id WHERE id_alumno_c=".$id." ORDER BY id_ac DESC
    ");
  $registros->execute();
  $registros=$registros->fetchAll();

  $totalregistros=count($registros);

  //Cursos EBC
  $registros2=$pdo->prepare("
    SELECT SQL_CALC_FOUND_ROWS ACE.*,EBC.*,M.nombre_mun,P.nombre,I.Nombre,O.* FROM alumno_curso2018ebc AS ACE LEFT JOIN cursos2018_ebc AS EBC ON ACE.id_curso = EBC.folio LEFT JOIN municipios AS M ON EBC.id_mun_c = M.folio_mun LEFT JOIN plantel AS P ON EBC.id_plantel = P.id LEFT JOIN instructores AS I ON EBC.id_instructor = I.id LEFT JOIN oferta AS O ON EBC.id_modulo_c = O.id WHERE id_alumno_c=".$id." ORDER BY id_ac DESC
    ");
  $registros2->execute();
  $registros2=$registros2->fetchAll();

  $totalregistros2=count($registros2);

  //Cursos Extensión
  $registros3=$pdo->prepare("
    SELECT SQL_CALC_FOUND_ROWS ACEX.*,EX.*,M.nombre_mun,P.nombre,I.Nombre FROM alumno_curso2018ext AS ACEX LEFT JOIN cursos2018_extension AS EX ON ACEX.id_curso = EX.folio LEFT JOIN municipios AS M ON EX.id_mun_c = M.folio_mun LEFT JOIN plantel AS P ON EX.id_plantel = P.id LEFT JOIN instructores AS I ON EX.id_instructor = I.id WHERE id_alumno_c=".$id." ORDER BY id_ac DESC
    ");
  $registros3->execute();
  $registros3=$registros3->fetchAll();

  $totalregistros3=count($registros3);

  //Cursos CAE
  $registros4=$pdo->prepare("
    SELECT SQL_CALC_FOUND_ROWS ACC.*,CAE.*,M.nombre_mun,P.nombre,I.Nombre FROM alumno_curso2018cae AS ACC LEFT JOIN cursos2018_cae AS CAE ON ACC.id_curso = CAE.folio LEFT JOIN municipios AS M ON CAE.id_mun_c = M.folio_mun LEFT JOIN plantel AS P ON CAE.id_plantel = P.id LEFT JOIN instructores AS I ON CAE.id_instructor = I.id WHERE id_alumno_c=".$id." ORDER BY id_ac DESC
    ");
  $registros4->execute();
  $registros4=$registros4->fetchAll();

  $totalregistros4=count($registros4);

  //Examen ROCO
  $registros5=$pdo->prepare("
    SELECT SQL_CALC_FOUND_ROWS ACRO.*,ROC.*,M.nombre_mun,P.nombre,I.Nombre,O.* FROM alumno_curso2018roco AS ACRO LEFT JOIN cursos2018_roco AS ROC ON ACRO.id_curso = ROC.folio LEFT JOIN municipios AS M ON ROC.id_mun_c = M.folio_mun LEFT JOIN plantel AS P ON ROC.id_plantel = P.id LEFT JOIN instructores AS I ON ROC.id_instructor = I.id LEFT JOIN oferta AS O ON ROC.id_modulo_c = O.id WHERE id_alumno_c=".$id." ORDER BY id_ac DESC
    ");
  $registros5->execute();
  $registros5=$registros5->fetchAll();

  $totalregistros5=count($registros5);

 }
 else
 {
  header("Location: instructores_vistaPLANE.php");
 }


?>
<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	#formato-cred{
    		box-shadow: 5px 5px 20px #9f9f9f;
			border: 2px solid #7a1315;
			border-radius:10px 10px 10px 10px; 
    	}
        .btn-success{
            background-color: #186d41 !important;
        }
        label{
        	font-size: 15px;
        }
        .btn-rounded {
          border-radius: 10px 10px 10px 10px !important;
        }
        .btn-primary {
          font-size: 20px !important;
          width: 50px;
          height: 50px;
          background-color: #7a1315 !important;
          border: solid 2px #7a1315!important;
          color: #ffffff;
        }
        .btn-primary:hover {
          font-size: 20px !important;
          width: 50px;
          height: 50px;
          background-color: #18181c !important;
          color: #ffffff !important;
          border: solid 2px #18181c !important;
        }
    </style>
</head>
<header>
	    <?php require_once "partes/nav_PLA.php"; ?>
</header>
<body>
    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNO</strong></h4>
    </div>
    <div class="container">

      <div id="formato-cred">
        <div class="container">
          <div class="row" id="titulo" style="border-radius:7px 7px 0px 0px; background-color: #560f11; font-size: 15px;"><strong>Datos del Alumno</strong></div>
          <div class="row"></div>
		    	<div class="row">
		    		<img class="col-sm-2" src="img/alumno/<?php echo $fila['foto_alum'] ?>" width="200px" height="200px">
		        	<label class="col-sm-10">
		        		<p style="font-size: ">
		        		 <strong>NOMBRE: </strong> <?php echo $fila['nombre_a'] ?> <?php echo $fila['apellido_p'] ?> <?php echo $fila['apellido_m'] ?> <br>
		        		 <strong>DOMICILIO: </strong> <?php echo $fila['domicilio'] ?>, <?php echo $fila['localidad'] ?>, <?php echo $fila['CP'] ?>, <?php echo $fila['nombre_mun'] ?>, <?php echo $fila['estado'] ?>.<br>
		        		 <strong>CURP: </strong> <?php echo $fila['curp'] ?> <br> 
		        		 <strong>SEXO: </strong><?php echo $fila['sexo'] ?> <br> 
		        		 <strong>DISCAPACIDAD: </strong><?php echo $fila['discapacidad'] ?><br>
		        		 <strong>TEL: </strong><?php echo $fila['num_tel'] ?>
		        		 <strong>EMAIL: </strong><?php echo $fila['email'] ?><br>
		        		</p>
		        	</label>
		    	</div>
                <div class="row"></div>				
      	</div>
      </div>
      <br><br><br>
      <div class="page-header text-center">
          <h4><strong>ESCOGER MODALIDAD</strong></h4>
      </div>
      <div id="formato-cred" class="container">
        <div class="row">
          <a href="#ventana1" class="btn btn-primary btn-rounded  justify-content-end col-md" data-toggle="modal" style="margin-top: 0px; margin-left: 7px; margin-right: 7px;">REGULAR</a>
          <a href="#ventana2" class="btn btn-primary btn-rounded  justify-content-end col-md" data-toggle="modal" style="margin-top: 0px; margin-left: 7px; margin-right: 7px;">EBC</a>
          <a href="#ventana3" class="btn btn-primary btn-rounded  justify-content-end col-md" data-toggle="modal" style="margin-top: 0px; margin-left: 7px; margin-right: 7px;">EXTENSIÓN</a>
          <a href="#ventana4" class="btn btn-primary btn-rounded  justify-content-end col-md" data-toggle="modal" style="margin-top: 0px; margin-left: 7px; margin-right: 7px;">CAE</a>
          <a href="#ventana5" class="btn btn-primary btn-rounded  justify-content-end col-md" data-toggle="modal" style="margin-top: 0px; margin-left: 7px; margin-right: 7px;">ROCO</a>
        </div>
      </div>
        <div class="text-right">
            <div class="modal fade" id="ventana1">
                <div class="modal-dialog modal-lg">   
                    <div class="modal-content">
                            <div class="modal-header">
                                <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- contenido-->
                            <div class="modal-body">

                                <?php require_once "formularios/inscribir_alumreg.php"; ?>

                            </div>

                            <div class="modal-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <div class="modal fade" id="ventana2">
                <div class="modal-dialog modal-lg">   
                    <div class="modal-content">
                            <div class="modal-header">
                                <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- contenido-->
                            <div class="modal-body">

                                <?php require_once "formularios/inscribir_alumebc.php"; ?>

                            </div>

                            <div class="modal-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <div class="modal fade" id="ventana3">
                <div class="modal-dialog modal-lg">   
                    <div class="modal-content">
                            <div class="modal-header">
                                <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- contenido-->
                            <div class="modal-body">

                                <?php require_once "formularios/inscribir_alumext.php"; ?>

                            </div>

                            <div class="modal-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <div class="modal fade" id="ventana4">
                <div class="modal-dialog modal-lg">   
                    <div class="modal-content">
                            <div class="modal-header">
                                <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- contenido-->
                            <div class="modal-body">

                                <?php require_once "formularios/inscribir_alumcae.php"; ?>

                            </div>

                            <div class="modal-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <div class="modal fade" id="ventana5">
                <div class="modal-dialog modal-lg">   
                    <div class="modal-content">
                            <div class="modal-header">
                                <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- contenido-->
                            <div class="modal-body">

                                <?php require_once "formularios/inscribir_alumroco.php"; ?>

                            </div>

                            <div class="modal-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>

       <br><br><br>

    <div class="page-header text-center">
        <h4><strong>CURSOS INSCRITOS</strong></h4>
    </div>
      
      <div class="container">
        <div class="row" id="titulo" style="border-radius:7px 7px 0px 0px; background-color: #560f11; font-size: 15px;"><strong>Cursos Regular</strong></div>
      </div>
      <table class="table table-bordered table-striped table-hover table-condensed responsive" style="border: 2px solid #7a1315; margin-bottom: 0px">
          <thead>
              <tr>        
                  <th class="text-center" style="border: 2px solid #7a1315;">folio</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Módulo</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Instructor</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Plantel</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Dirección</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Fecha</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Status</th>
              </tr>

          </thead>
          <tbody>
                      <?php
                      if($totalregistros>=1): 
                      foreach($registros as $reg): 
                      ?>
              
             <tr>
                  <td class="text-center"><strong><?php echo $reg['id_curso'] ?></strong></td>
                  <td><?php echo $reg['nombreModulo'] ?></td>
                  <td><?php echo $reg['Nombre'] ?></td>
                  <td><?php echo $reg['nombre'] ?></td>
                  <td><?php echo $reg['calle_c'] ?>, <?php echo $reg['colonia_c'] ?>, <?php echo $reg['nombre_mun'] ?>.</td>
                  <td><?php echo $reg['fecha_inicio'] ?> al <?php echo $reg['fecha_fin'] ?></td>
                  <td><?php echo $reg['status'] ?></td>
              </tr>

                      <?php 
                      endforeach; 
                      else:
                      ?>
                  <tr>
                      <td colspan='11' class="text-center"><strong>No se ha inscrito al cursos</strong></td>
                  </tr>
                      <?php 
                      endif; 
                      ?>
          </tbody>
        </table>

      <div class="container">
        <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;"><strong>Cursos EBC</strong></div>
      </div>
      <table class="table table-bordered table-striped table-hover table-condensed responsive" style="border: 2px solid #7a1315; margin-bottom: 0px">
          <thead>
              <tr>        
                  <th class="text-center" style="border: 2px solid #7a1315;">folio</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Módulo</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Instructor</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Plantel</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Dirección</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Fecha</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Status</th>
              </tr>

          </thead>
          <tbody>
                      <?php
                      if($totalregistros2>=1): 
                      foreach($registros2 as $reg2): 
                      ?>
              
             <tr>
                  <td class="text-center"><strong><?php echo $reg2['id_curso'] ?></strong></td>
                  <td><?php echo $reg2['nombreModulo'] ?></td>
                  <td><?php echo $reg2['Nombre'] ?></td>
                  <td><?php echo $reg2['nombre'] ?></td>
                  <td><?php echo $reg2['calle_c'] ?>, <?php echo $reg2['colonia_c'] ?>, <?php echo $reg2['nombre_mun'] ?>.</td>
                  <td><?php echo $reg2['fecha_inicio'] ?> al <?php echo $reg2['fecha_fin'] ?></td>
                  <td><?php echo $reg2['status'] ?></td>
              </tr>

                      <?php 
                      endforeach; 
                      else:
                      ?>
                  <tr>
                      <td colspan='11' class="text-center"><strong>No se ha inscrito al cursos</strong></td>
                  </tr>
                      <?php 
                      endif; 
                      ?>
          </tbody>
        </table>

      <div class="container">
        <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;"><strong>Cursos Extensión</strong></div>
      </div>
      <table class="table table-bordered table-striped table-hover table-condensed responsive" style="border: 2px solid #7a1315; margin-bottom: 0px">
          <thead>
              <tr>        
                  <th class="text-center" style="border: 2px solid #7a1315;">folio</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Curso</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Instructor</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Plantel</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Dirección</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Fecha</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Status</th>
              </tr>

          </thead>
          <tbody>
                      <?php
                      if($totalregistros3>=1): 
                      foreach($registros3 as $reg3): 
                      ?>
              
             <tr>
                  <td class="text-center"><strong><?php echo $reg3['id_curso'] ?></strong></td>
                  <td><?php echo $reg3['curso'] ?></td>
                  <td><?php echo $reg3['Nombre'] ?></td>
                  <td><?php echo $reg3['nombre'] ?></td>
                  <td><?php echo $reg3['calle_c'] ?>, <?php echo $reg3['colonia_c'] ?>, <?php echo $reg3['nombre_mun'] ?>.</td>
                  <td><?php echo $reg3['fecha_inicio'] ?> al <?php echo $reg3['fecha_fin'] ?></td>
                  <td><?php echo $reg3['status'] ?></td>
              </tr>

                      <?php 
                      endforeach; 
                      else:
                      ?>
                  <tr>
                      <td colspan='11' class="text-center"><strong>No se ha inscrito al cursos</strong></td>
                  </tr>
                      <?php 
                      endif; 
                      ?>
          </tbody>
        </table>

        <div class="container">
        <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;"><strong>Cursos CAE</strong></div>
      </div>
      <table class="table table-bordered table-striped table-hover table-condensed responsive" style="border: 2px solid #7a1315; margin-bottom: 0px">
          <thead>
              <tr>        
                  <th class="text-center" style="border: 2px solid #7a1315;">folio</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Módulo</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Instructor</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Plantel</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Dirección</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Fecha</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Status</th>
              </tr>

          </thead>
          <tbody>
                      <?php
                      if($totalregistros4>=1): 
                      foreach($registros4 as $reg4): 
                      ?>
              
             <tr>
                  <td class="text-center"><strong><?php echo $reg4['id_curso'] ?></strong></td>
                  <td><?php echo $reg4['curso'] ?></td>
                  <td><?php echo $reg4['Nombre'] ?></td>
                  <td><?php echo $reg4['nombre'] ?></td>
                  <td><?php echo $reg4['calle_c'] ?>, <?php echo $reg4['colonia_c'] ?>, <?php echo $reg4['nombre_mun'] ?>.</td>
                  <td><?php echo $reg4['fecha_inicio'] ?> al <?php echo $reg4['fecha_fin'] ?></td>
                  <td><?php echo $reg4['status'] ?></td>
              </tr>

                      <?php 
                      endforeach; 
                      else:
                      ?>
                  <tr>
                      <td colspan='11' class="text-center"><strong>No se ha inscrito al cursos</strong></td>
                  </tr>
                      <?php 
                      endif; 
                      ?>
          </tbody>
        </table>

      <div class="container">
        <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;"><strong>Exámenes ROCO</strong></div>
      </div>
      <table class="table table-bordered table-striped table-hover table-condensed responsive" style="border: 2px solid #7a1315; margin-bottom: 0px">
          <thead>
              <tr>        
                  <th class="text-center" style="border: 2px solid #7a1315;">folio</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Módulo</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Instructor</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Plantel</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Dirección</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Fecha</th>
                  <th class="text-center" style="border: 2px solid #7a1315;">Status</th>
              </tr>

          </thead>
          <tbody>
                      <?php
                      if($totalregistros5>=1): 
                      foreach($registros5 as $reg5): 
                      ?>
              
             <tr>
                  <td class="text-center"><strong><?php echo $reg5['id_curso'] ?></strong></td>
                  <td><?php echo $reg5['nombreModulo'] ?></td>
                  <td><?php echo $reg5['Nombre'] ?></td>
                  <td><?php echo $reg5['nombre'] ?></td>
                  <td><?php echo $reg5['calle_c'] ?>, <?php echo $reg5['colonia_c'] ?>, <?php echo $reg5['nombre_mun'] ?>.</td>
                  <td><?php echo $reg5['fecha_inicio'] ?> al <?php echo $reg5['fecha_fin'] ?></td>
                  <td><?php echo $reg5['status'] ?></td>
              </tr>

                      <?php 
                      endforeach; 
                      else:
                      ?>
                  <tr>
                      <td colspan='11' class="text-center"><strong>No se ha inscrito a un curso.</strong></td>
                  </tr>
                      <?php 
                      endif; 
                      ?>
          </tbody>
        </table>

    </div>
</body>
</html>