<?php 
  include "php/connect.php";  
  $sql = "SELECT * FROM modulosespecialidades WHERE id_modulo=".$_GET['id'];
  $result = $pdo->query($sql);
  $rows = $result->fetchAll();
  /*foreach ($rows as $row) {
      echo '<option value="'.$row['id_especialidad'].'">'.$row['especialidad'].'</option>';
  }*/
?>
<div id="cargaresp">
    <div class="row text-left">
      <label class="col-sm-2">Especialidad</label>
      <div class="col-sm-10">
        <select class="form-control input-sm" type="number" name="idespecialidad" onchange="from(document.formebc.idespecialidad.value,'cargarmod','cargar_datos2_cmbo.php');" required="">
          <option value="0"> --- Seleccione Especialidad --- </option>
        <?php
          foreach ($rows as $row) {
            echo '<option value="'.$row['id_especialidad'].'">'.$row['especialidad'].'</option>';
          }
        ?>
        </select>
      </div>
    </div>

  <div  id="cargarmod">
    <div class="row text-left">
      <label class="col-sm-2">Módulo</label>
      <div class="col-sm-10">
          <select class="form-control input-sm" type="number" name="idmodulo" required="">
              <option value="0"> --- Seleccione Módulo --- </option>
          </select>
      </div>
    </div>

    <div class="row text-left">
        <label class="col-sm-2">Cve Especialidad</label>
        <div class="col-sm-4">
            <select class="form-control input-sm" type="text" disabled="">
                <option value="0"> Cargando... </option>
            </select>
        </div>

        <label class="col-sm-2">Cve Módulo</label>
        <div class="col-sm-4">
            <select class="form-control input-sm" type="text" disabled="">
                <option value="0"> Cargando... </option>
            </select>
        </div>
    </div>

    <div class="row text-left">
        <label class="col-sm-2">Duración</label>
        <div class="col-sm-4">
            <select class="form-control input-sm" type="text" disabled="">
                <option value="0"> Cargando... </option>
            </select>
        </div>
    </div>
  </div>
</div>