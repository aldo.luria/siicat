<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave']))
 {
    $id = $_GET['id_alumno'];
    $idcurso=$_POST['idcurso4'];  

    

    $consulta4=$pdo->prepare("INSERT INTO alumno_curso2018cae(id_alumno_c,id_curso) VALUES(:id_alumno, :idcurso) ");

    $consulta4->bindParam(':id_alumno',$id);
    $consulta4->bindParam(':idcurso',$idcurso);

    if($consulta4->execute()){
        echo "Ya se registroooooooooooooooooooooooooooooó";
    }else{
        echo "Error no se pudo almacenar la información";
    }

 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNOS - CURSOS2018_CAE</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="POST" enctype="multipart/form-data" name="formebc4">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos del Curso</div>
                <div class="row" id="titulo"><strong>Curso CAE</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-4">Seleccionar Curso</label>
                          <div class="col-sm-8">
                            <select class="form-control input-sm" type="number" name="idcurso4" onchange="from(document.formebc4.idcurso4.value,'cargarcurso','cargar_datos_curso.php');" required="">
                            <option value="0"> --- Seleccione Curso --- </option>
                            <?php 
                                $inscribir4=$pdo->prepare("
                            SELECT SQL_CALC_FOUND_ROWS CAE.*,P.nombre,I.Expediente,I.Nombre,I.curp,M.nombre_mun FROM cursos2018_cae AS CAE LEFT JOIN plantel AS P ON CAE.id_plantel = P.id LEFT JOIN instructores AS I ON CAE.id_instructor = I.id LEFT JOIN municipios AS M ON CAE.id_mun_c = M.folio_mun ORDER BY folio DESC
                            ");

                                  $inscribir4->execute();
                                  $rows4=$inscribir4->fetchAll();
                                    foreach ($rows4 as $row4) {
                                      echo '<option value="'.$row4['folio'].'">'.$row4['curso'].'</option>';
                                      }

                                      ?>
                            </select>

                          </div>
                </div>
              <div id="cargarcurso">
                <div class="row text-left">
                    <label  class="col-sm-4">Instructor</label>
                    <div class="col-md-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-4">Duración</label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>

                </div>
              </div>
                
            </div>
        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave" value="Inscribir">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white; background-color: #7a1315">Cancelar</a>
            </div> 
        </form>
    </div>

</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>