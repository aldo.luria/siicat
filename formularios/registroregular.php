<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro Curso Regular</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>
    <div class="page-header text-center">
        <h4><strong>NUEVO REGISTRO - CURSO2018_REGULAR</strong></h4>
    </div>

    <div class="container-fluid">
        <form action="guardarregular.php" method="POST" enctype="multipart/form-data" name="formebc">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos del Instructor</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Instructor</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" name="id_inst" required="">
                            <option>--- Seleccionar Instructor ---</option>
                            <?php 
                                $sql = "SELECT * FROM instructores WHERE NOT (condicion = 'No Competente') ORDER BY Nombre ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['id'].'">'.$row['Nombre'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

                    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos del Curso</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Plantel</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="id_plan" required="">
                            <?php 
                                $sql = 'SELECT * FROM plantel WHERE nombre = "'.$_SESSION['nombre'].'" ';
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['id'].'">'.$row['nombre'].'</option>';
                                }
                            ?>
                        </select>
                    </div>

                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" type="text" name="id_mun_c" required="" id="sel1">
                            <option>--- Seleccionar municipio ---</option>
                            <?php 

                                $sql = "SELECT folio_mun, nombre_mun FROM municipios ORDER BY nombre_mun ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['folio_mun'].'">'.$row['nombre_mun'].'</option>';
                                }

                            ?>
                        </select>

                    </div>
                </div>

                <div class=" row text-left">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="calle" required="">
                    </div>

                    <label class="col-sm-2">Colonia</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="colonia" required="">
                    </div>
                </div>

                <div class=" row text-left">
                    <label class="col-sm-2">Campo</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" type="number" name="idcampo" onchange="from(document.formebc.idcampo.value,'cargaresp','cargar_datos_cmbo.php');" required="">
                            <option value="0"> --- Seleccione Campo --- </option>
                            <?php 
                                $sql = "SELECT * FROM modulosformacion ORDER BY idmodulo ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['idmodulo'].'">'.$row['modulo'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>

            <div id="cargaresp">

                <div class="row text-left">
                    <label class="col-sm-2">Especialidad</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" type="number" name="idespecialidad" required="">
                            <option value="0"> --- Seleccione Especialidad --- </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Módulo</label>
                    <div class="col-sm-10" id="cargarmod">
                        <select class="form-control input-sm" type="number" name="idmodulo" required="">
                            <option value="0"> --- Seleccione Modulo --- </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Cve Especialidad</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Especialidad </option>
                        </select>
                    </div>
              
                    <label class="col-sm-2">Cve Módulo</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Módulo </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Duración</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Módulo </option>
                        </select>
                    </div>
                </div>
                
            </div>

                <div class="row text-left">
                    <label class="col-sm-2">Fecha Inicio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="fechainicio" required="">
                    </div>
              
                    <label class="col-sm-2">Fecha Fin</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="fechafin" required="">
                    </div>
                </div>
            </div>
                
                            
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Horarios</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Lunes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="lunesinicio" >
                        </div>
                        <label class="col-sm-2">Lunes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="lunesfin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Martes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="martesinicio" >
                        </div>
                        <label class="col-sm-2">Martes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="martesfin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Miércoles Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="miercolesinicio" >
                        </div>
                        <label class="col-sm-2">Miércoles Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="miercolesfin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Jueves Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="juevesinicio" >
                        </div>
                        <label class="col-sm-2">Jueves Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="juevesfin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Viernes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="viernesinicio" >
                        </div>
                        <label class="col-sm-2">Viernes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="viernesfin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Sábado Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sabadoinicio" >
                        </div>
                        <label class="col-sm-2">Sábado Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sabadofin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Domingo Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="domingoinicio" >
                        </div>
                        <label class="col-sm-2">Domingo Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="domingofin" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="status" value="Sugerido" disabled>
                        </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Observaciones</strong></div>
                <div class="row text-left">
                        <div class="col-sm-12">
                            <textarea class="form-control input-sm" name="observa" style="height:150px;">(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.)</textarea>
                        </div>
                </div>
            </div>

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" value="Guardar">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white;">Cancelar</a>
            </div>
        </form>
    </div>
                    
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>