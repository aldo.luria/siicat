<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave5']))
 {
    $id = $_GET['id_alumno'];
    $idcurso=$_POST['idcurso5'];  

    

    $consulta5=$pdo->prepare("INSERT INTO alumno_curso2018roco(id_alumno_c,id_curso) VALUES(:id_alumno, :idcurso) ");

    $consulta5->bindParam(':id_alumno',$id);
    $consulta5->bindParam(':idcurso',$idcurso);

    if($consulta5->execute()){
        echo "Ya se registroooooooooooooooooooooooooooooó";
    }else{
        echo "Error no se pudo almacenar la información";
    }

 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNOS - CURSO2018_ROCO</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="POST" enctype="multipart/form-data" name="formebc5">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos del Curso</div>
                <div class="row" id="titulo"><strong>Curso ROCO</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-4">Seleccionar Curso</label>
                          <div class="col-sm-8">
                            <select class="form-control input-sm" type="number" name="idcurso5" onchange="from(document.formebc5.idcurso5.value,'cargarcursoroco','cargar_datos_cursoroco.php');" required="">
                            <option value="0"> --- Seleccione Curso --- </option>
                            <?php 
                                $inscribir5=$pdo->prepare("
                            SELECT SQL_CALC_FOUND_ROWS ROC.*,P.nombre,I.*,O.nombreModulo,E.especialidad,M.nombre_mun FROM cursos2018_roco AS ROC LEFT JOIN plantel AS P ON ROC.id_plantel = P.id LEFT JOIN instructores AS I ON ROC.id_instructor = I.id LEFT JOIN oferta AS O ON ROC.id_modulo_c = O.id LEFT JOIN modulosespecialidades AS E ON O.idEspecialidad = E.id_especialidad LEFT JOIN municipios AS M ON ROC.id_mun_c = M.folio_mun ORDER BY folio DESC
                            ");
                                  $inscribir5->execute();
                                  $rows5=$inscribir5->fetchAll();
                                    foreach ($rows5 as $row5) {
                                      echo '<option value="'.$row5['folio'].'">'.$row5['nombreModulo'].'</option>';
                                      }

                                      ?>
                            </select>

                          </div>
                </div>
              <div id="cargarcursoroco">
                <div class="row text-left">
                    <label  class="col-sm-4">Instructor</label>
                    <div class="col-md-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-4">Duración</label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>

                </div>
              </div>
                
            </div>
        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave5" value="Inscribir">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white; background-color: #7a1315">Cancelar</a>
            </div> 
        </form>
    </div>

</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>