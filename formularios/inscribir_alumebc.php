<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave2']))
 {
    $id = $_GET['id_alumno'];
    $idcurso=$_POST['idcurso2'];  

    

    $consulta2=$pdo->prepare("INSERT INTO alumno_curso2018ebc(id_alumno_c,id_curso) VALUES(:id_alumno, :idcurso) ");

    $consulta2->bindParam(':id_alumno',$id);
    $consulta2->bindParam(':idcurso',$idcurso);

    if($consulta2->execute()){
        echo "Ya se registroooooooooooooooooooooooooooooó";
    }else{
        echo "Error no se pudo almacenar la información";
    }

 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNOS - CURSOS2018_EBC</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="POST" enctype="multipart/form-data" name="formebc2">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos del Curso</div>
                <div class="row" id="titulo"><strong>Curso EBC</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-4">Seleccionar Curso</label>
                          <div class="col-sm-8">
                            <select class="form-control input-sm" type="number" name="idcurso2" onchange="from(document.formebc2.idcurso2.value,'cargarcursoebc','cargar_datos_cursoebc.php');" required="">
                            <option value="0"> --- Seleccione Curso --- </option>
                            <?php 
                                $inscribir2=$pdo->prepare("
                            SELECT SQL_CALC_FOUND_ROWS EBC.*,P.nombre,I.*,O.nombreModulo,E.especialidad,M.nombre_mun FROM cursos2018_ebc AS EBC LEFT JOIN plantel AS P ON EBC.id_plantel = P.id LEFT JOIN instructores AS I ON EBC.id_instructor = I.id LEFT JOIN oferta AS O ON EBC.id_modulo_c = O.id LEFT JOIN modulosespecialidades AS E ON O.idEspecialidad = E.id_especialidad LEFT JOIN municipios AS M ON EBC.id_mun_c = M.folio_mun ORDER BY EBC.folio DESC
                            ");


                                  $inscribir2->execute();
                                  $rows2=$inscribir2->fetchAll();
                                    foreach ($rows2 as $row2) {
                                      echo '<option value="'.$row2['folio'].'">'.$row2['nombreModulo'].'</option>';
                                      }

                                      ?>
                            </select>

                          </div>
                </div>
              <div id="cargarcursoebc">
                <div class="row text-left">
                    <label  class="col-sm-4">Instructor</label>
                    <div class="col-md-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-4">Duración</label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>

                </div>
              </div>
                
            </div>
        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave2" value="Inscribir">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white; background-color: #7a1315">Cancelar</a>
            </div> 
        </form>
    </div>

</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>