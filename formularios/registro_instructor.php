<?php 
    include "php/connect.php"; 
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave']))
 {

    foreach($pdo->query('SELECT MAX(Expediente) FROM instructores') as $row) {
    }
    $result = $row['MAX(Expediente)'] + 1;

    $expediente=$result;
    //$foto=$_POST['foto'];
    $curp=$_POST['curp'];
    $nombre=$_POST['nombre'];
    $calle=$_POST['calle'];
    $colonia=$_POST['colonia'];
    $localidad=$_POST['localidad'];
    $cp=$_POST['cp'];
    $seccion=$_POST['seccion'];
    $civil=$_POST['civil'];
    $sexo=$_POST['sexo'];
    $nacimiento=$_POST['nacimiento'];
    $alta=$_POST['alta'];
    $baja=$_POST['baja'];
    $perfil=$_POST['perfil'];
    $nivel=$_POST['nivel'];
    $especialidad=$_POST['especialidad'];
    $evaluacion=$_POST['evaluacion'];
    $puntaje=$_POST['puntaje'];
    $cel=$_POST['cel'];
    $tel=$_POST['tel'];
    $correo=$_POST['correo'];
    $observaciones=$_POST['observaciones'];
    $calidad="Sin calidad";
    $id_mun=$_POST['id_mun'];
  
  $imgFile = $_FILES['user_image']['name'];
  $tmp_dir = $_FILES['user_image']['tmp_name'];
  $imgSize = $_FILES['user_image']['size'];
  
  
  if(empty($imgFile)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir = 'img/inst/'; // upload directory
 
   $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
  
   // rename uploading image
   $userpic = $expediente."_".rand(1000,1000000).".".$imgExt;
    
   // allow valid image file formats
   if(in_array($imgExt, $valid_extensions)){   
    // Check file size '5MB'
    if($imgSize < 5000000)    {
     move_uploaded_file($tmp_dir,$upload_dir.$userpic);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

  $imgFile2 = $_FILES['acta']['name'];
  $tmp_dir2 = $_FILES['acta']['tmp_name'];
  $imgSize2 = $_FILES['acta']['size'];
  if(empty($imgFile2)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir2 = 'img/pdfinst/'; // upload directory
   $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION));
   $valid_extensions2 = array( 'pdf'); 
   $userpic2 = $expediente."_".rand(1000,1000000).".".$imgExt2;
   if(in_array($imgExt2, $valid_extensions2)){   
    if($imgSize2 < 5000000)    {
     move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

$imgFile3 = $_FILES['cv']['name'];
  $tmp_dir3 = $_FILES['cv']['tmp_name'];
  $imgSize3 = $_FILES['cv']['size'];
  if(empty($imgFile3)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir3 = 'img/pdfinst/'; // upload directory
   $imgExt3 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION));
   $valid_extensions3 = array('pdf'); 
   $userpic3 = $expediente."_".rand(1000,1000000).".".$imgExt3;
   if(in_array($imgExt3, $valid_extensions3)){   
    if($imgSize3 < 5000000)    {
     move_uploaded_file($tmp_dir3,$upload_dir3.$userpic3);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }
  
  $imgFile4 = $_FILES['curp2']['name'];
  $tmp_dir4 = $_FILES['curp2']['tmp_name'];
  $imgSize4 = $_FILES['curp2']['size'];
  if(empty($imgFile4)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir4 = 'img/pdfinst/'; // upload directory
   $imgExt4 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION));
   $valid_extensions4 = array('pdf'); 
   $userpic4 = $expediente."_".rand(1000,1000000).".".$imgExt4;
   if(in_array($imgExt4, $valid_extensions4)){   
    if($imgSize4 < 5000000)    {
     move_uploaded_file($tmp_dir4,$upload_dir4.$userpic4);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

  $imgFile5 = $_FILES['expl']['name'];
  $tmp_dir5 = $_FILES['expl']['tmp_name'];
  $imgSize5 = $_FILES['expl']['size'];
  if(empty($imgFile5)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir5 = 'img/pdfinst/'; // upload directory
   $imgExt5 = strtolower(pathinfo($imgFile5,PATHINFO_EXTENSION));
   $valid_extensions5 = array('jpeg', 'jpg', 'png', 'gif','pdf'); 
   $userpic5 = $expediente."_".rand(1000,1000000).".".$imgExt5;
   if(in_array($imgExt5, $valid_extensions5)){   
    if($imgSize5 < 5000000)    {
     move_uploaded_file($tmp_dir5,$upload_dir5.$userpic5);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

   $imgFile6 = $_FILES['domi']['name'];
  $tmp_dir6 = $_FILES['domi']['tmp_name'];
  $imgSize6 = $_FILES['domi']['size'];
  if(empty($imgFile6)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir6 = 'img/pdfinst/'; // upload directory
   $imgExt6 = strtolower(pathinfo($imgFile6,PATHINFO_EXTENSION));
   $valid_extensions6 = array('pdf'); 
   $userpic6 = $expediente."_".rand(1000,1000000).".".$imgExt6;
   if(in_array($imgExt6, $valid_extensions6)){   
    if($imgSize6 < 5000000)    {
     move_uploaded_file($tmp_dir6,$upload_dir6.$userpic6);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

   $imgFile7 = $_FILES['expfg']['name'];
  $tmp_dir7 = $_FILES['expfg']['tmp_name'];
  $imgSize7 = $_FILES['expfg']['size'];
  if(empty($imgFile7)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir7 = 'img/pdfinst/'; // upload directory
   $imgExt7 = strtolower(pathinfo($imgFile7,PATHINFO_EXTENSION));
   $valid_extensions7 = array('jpeg', 'jpg', 'png', 'gif','pdf'); 
   $userpic7 = $expediente."_".rand(1000,1000000).".".$imgExt7;
   if(in_array($imgExt7, $valid_extensions7)){   
    if($imgSize7 < 5000000)    {
     move_uploaded_file($tmp_dir7,$upload_dir7.$userpic7);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

  $imgFile8 = $_FILES['ine']['name'];
  $tmp_dir8 = $_FILES['ine']['tmp_name'];
  $imgSize8 = $_FILES['ine']['size'];
  if(empty($imgFile8)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir8 = 'img/pdfinst/'; // upload directory
   $imgExt8 = strtolower(pathinfo($imgFile8,PATHINFO_EXTENSION));
   $valid_extensions8 = array('pdf'); 
   $userpic8 = $expediente."_".rand(1000,1000000).".".$imgExt8;
   if(in_array($imgExt8, $valid_extensions8)){   
    if($imgSize8 < 5000000)    {
     move_uploaded_file($tmp_dir8,$upload_dir8.$userpic8);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }
  

  
  // if no error occured, continue ....
  if(!isset($errMSG))
  {
   $consulta = $pdo->prepare("INSERT INTO instructores(Expediente,imagen,Curp,Nombre,Calle,Colonia_barrio,Localidad,CP,Seccion_elec,Estado_civil,Sexo,F_Nacimiento,F_Alta,F_Baja,Pefil,Nivel_academico,Especialidad,Fecha_evaluacion,Puntaje,No_celular,No_telefono,Email,Observaciones,calidad,id_municipios,condicion,acta_doc,curp_doc,dom_doc,ine_doc,expfg_doc,expl_doc,cv_doc,actualizacion_doc) VALUES(:expediente, :foto, :curp, :nombre, :calle, :colonia, :localidad, :cp, :seccion, :civil, :sexo, :nacimiento, :alta, :baja, :perfil, :nivel, :especialidad, :evaluacion, :puntaje, :cel, :tel, :correo, :observaciones, :calidad, :id_mun, '', :acta,:curp2, :domi, :ine, :expfg, :expl,:cv, '') ");

    $consulta->bindParam(':expediente',$expediente);
    $consulta->bindParam(':foto',$userpic);
    $consulta->bindParam(':acta',$userpic2);
    $consulta->bindParam(':cv',$userpic3);
    $consulta->bindParam(':curp2',$userpic4);
    $consulta->bindParam(':expl',$userpic5);
    $consulta->bindParam(':domi',$userpic6);
    $consulta->bindParam(':expfg',$userpic7);
    $consulta->bindParam(':ine',$userpic8);
    $consulta->bindParam(':curp',$curp);
    $consulta->bindParam(':nombre',$nombre);
    $consulta->bindParam(':calle',$calle);
    $consulta->bindParam(':colonia',$colonia);
    $consulta->bindParam(':localidad',$localidad);
    $consulta->bindParam(':cp',$cp);
    $consulta->bindParam(':seccion',$seccion);
    $consulta->bindParam(':civil',$civil);
    $consulta->bindParam(':sexo',$sexo);
    $consulta->bindParam(':nacimiento',$nacimiento);
    $consulta->bindParam(':alta',$alta);
    $consulta->bindParam(':baja',$baja);
    $consulta->bindParam(':perfil',$perfil);
    $consulta->bindParam(':nivel',$nivel);
    $consulta->bindParam(':especialidad',$especialidad);
    $consulta->bindParam(':evaluacion',$evaluacion);
    $consulta->bindParam(':puntaje',$puntaje);
    $consulta->bindParam(':cel',$cel);
    $consulta->bindParam(':tel',$tel);
    $consulta->bindParam(':correo',$correo);
    $consulta->bindParam(':observaciones',$observaciones);
    $consulta->bindParam(':calidad',$calidad);
    $consulta->bindParam(':id_mun',$id_mun);
   
    if($consulta->execute()){
        ?>
        <script>
        window.location.href='instructores_vistaPLANE.php';
        </script>
        <?php
    }else{
        echo "Error no se pudo almacenar la información";
    }

  }
 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro Instructores</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>NUEVO REGISTRO - INSTRUCTORES</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="post" enctype="multipart/form-data">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos Personales</div>
                <div class="row" id="titulo"><strong>Datos Generales</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-2">Expediente</label>
                    <div class="col-sm-4">
                        <?php  
                            foreach($pdo->query('SELECT MAX(Expediente) FROM instructores') as $row) {
                            }
                            $result = $row['MAX(Expediente)'] + 1;
                        ?>
                        <input class="form-control input-sm" type="text" <?php echo 'value="'.$result.'"'?> min="0" disabled>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Nombre</label>
                    <div class="col-sm-10">
                        <input class="form-control input-sm" type="text" name="nombre" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CURP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="curp" required="">
                    </div>

                    <label class="col-sm-2">Foto</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="user_image" accept="image/*" required="">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Dirección</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="calle" required="">
                    </div>

                    <label class="col-sm-2">Localidad</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="localidad" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" name="cp" min="0" required="">
                    </div>

                    <label class="col-sm-2">Colonia Barrio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="colonia" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" type="text" name="id_mun" required="" id="sel1">
                            <option>--- Seleccionar municipio ---</option>
                            <?php 

                                $sql = "SELECT folio_mun, nombre_mun FROM municipios ORDER BY nombre_mun ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['folio_mun'].'">'.$row['nombre_mun'].'</option>';
                                }

                            ?>
                        </select>

                    </div>

                    <label class="col-sm-2">Sección Elec</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" name="seccion" min="0">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Informción adicional</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Estado Civil</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="civil">
                        </div>
                        <label class="col-sm-2">F Nacimiento</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="date" name="nacimiento" required="">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">No Teléfono</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="tel" min="0">
                        </div>

                        <label class="col-sm-2">Sexo</label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="sexo" required="" id="sel1">
                                <option>--- Seleccionar sexo ---</option>
                                <option value="FEMENINO">FEMENINO</option>
                                <option value="MASCULINO">MASCULINO</option>
                            </select>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">No Celular</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="cel" min="0" required="">
                        </div>

                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="email" name="correo">
                        </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Perfil Académico</div>
                <div class="row" id="titulo"><strong>Formación</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Perfil</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="perfil" required="">
                        </div>

                        <label class="col-sm-2">Nivel Académico</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="nivel" required="">
                        </div>
                </div>
            </div>

            
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Docencia</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Especialidad</label>
                        <div class="col-sm-10">
                            <textarea class="form-control input-sm" type="text" name="especialidad" required="" rows="3"></textarea> 
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Fecha Evaluación</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="date" name="evaluacion">
                        </div>

                        <label class="col-sm-2">Puntaje</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="puntaje" min="0">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Fecha Alta</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="date" name="alta">
                        </div>

                        <label class="col-sm-2">Fecha Baja</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="date" name="baja">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Observaciones</label>
                        <div class="col-sm-10">
                            <textarea class="form-control input-sm" type="text" name="observaciones" rows="3"></textarea>
                        </div>
                </div>

            </div>

            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Expediente Digital</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Acta de Nacimiento</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="acta" accept="application/pdf" required="">
                    </div>

                    <label class="col-sm-2">Curriculum Vitae </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="cv" accept="application/pdf" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Curp</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="curp2" accept="application/pdf" required="">
                    </div>

                    <label class="col-sm-2">Experiencia Laboral </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="expl" accept="" required="">
                    </div>
                </div>

                   <div class="row text-left">
                    <label class="col-sm-2">Domicilio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="domi" accept="application/pdf" required="">
                    </div>

                    <label class="col-sm-2">Experiencia Frente Grupo </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="expfg" accept="" required="">
                    </div>
                </div>

                   <div class="row text-left"> 
                    <label class="col-sm-2">INE</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="ine" accept="application/pdf" required="">
                    </div>
                </div>

            </div>

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave" value="Guardar">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white;">Cancelar</a>
            </div> 
        </form>
    </div>
    
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>