<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave3']))
 {
    $id = $_GET['id_alumno'];
    $idcurso=$_POST['idcurso3'];  

    

    $consulta3=$pdo->prepare("INSERT INTO alumno_curso2018ext(id_alumno_c,id_curso) VALUES(:id_alumno, :idcurso) ");

    $consulta3->bindParam(':id_alumno',$id);
    $consulta3->bindParam(':idcurso',$idcurso);

    if($consulta3->execute()){
        echo "Ya se registroooooooooooooooooooooooooooooó";
    }else{
        echo "Error no se pudo almacenar la información";
    }

 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNOS - CURSOS2018_EXTENSIÓN</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="POST" enctype="multipart/form-data" name="formebc3">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos del Curso</div>
                <div class="row" id="titulo"><strong>Curso Extensión</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-4">Seleccionar Curso</label>
                          <div class="col-sm-8">
                            <select class="form-control input-sm" type="number" name="idcurso3" onchange="from(document.formebc3.idcurso3.value,'cargarcursoext','cargar_datos_cursoext.php');" required="">
                            <option value="0"> --- Seleccione Curso --- </option>
                            <?php 
                                $inscribir3=$pdo->prepare("
                            SELECT SQL_CALC_FOUND_ROWS CE.*, P.nombre, I.Expediente, I.Nombre, I.curp, M.nombre_mun FROM cursos2018_extension AS CE LEFT JOIN plantel AS P ON CE.id_plantel = P.id LEFT JOIN instructores AS I ON CE.id_instructor = I.id LEFT JOIN municipios AS M ON CE.id_mun_c = M.folio_mun ORDER BY folio DESC
                            ");
                                  $inscribir3->execute();
                                  $rows3=$inscribir3->fetchAll();
                                    foreach ($rows3 as $row3) {
                                      echo '<option value="'.$row3['folio'].'">'.$row3['curso'].'</option>';
                                      }

                                      ?>
                            </select>

                          </div>
                </div>
              <div id="cargarcursoext">
                <div class="row text-left">
                    <label  class="col-sm-4">Instructor</label>
                    <div class="col-md-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-4">Duración</label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>

                </div>
              </div>
                
            </div>
        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave3" value="Inscribir">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white; background-color: #7a1315">Cancelar</a>
            </div> 
        </form>
    </div>

</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>