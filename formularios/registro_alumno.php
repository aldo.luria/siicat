<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave']))
 {
    $apellidop=$_POST['apellidop'];
    //$foto=$_POST['foto'];
    $apellidom=$_POST['apellidom'];
    $nombre=$_POST['nombre'];
    $curp=$_POST['curp'];
    $domicilio=$_POST['domicilio'];
    $localidad=$_POST['localidad'];
    $cp=$_POST['cp'];
    $municipio=$_POST['municipio'];
    $estado=$_POST['estado'];
    $civil=$_POST['civil'];
    $discapacidad=$_POST['discapacidad'];
    $tel=$_POST['tel'];
    $sexo=$_POST['sexo'];
    $edad=$_POST['edad'];
    $correo=$_POST['correo'];
  
  //Recibo los datos de la foto del alumno
  $imgFile = $_FILES['foto']['name'];
  $tmp_dir = $_FILES['foto']['tmp_name'];
  $imgSize = $_FILES['foto']['size'];
  
  
  if(empty($imgFile)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir = 'img/alumno/'; // upload directory
 
   $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
  
   // rename uploading image
   $nombre_img = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt;
    
   // allow valid image file formats
   if(in_array($imgExt, $valid_extensions)){   
    // Check file size '5MB'
    if($imgSize < 10000000)    {
     move_uploaded_file($tmp_dir,$upload_dir.$nombre_img);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   }
  }

  //Recibo los datos de la foto de la curp
  $imgFile2 = $_FILES['imgcurp']['name'];
  $tmp_dir2 = $_FILES['imgcurp']['tmp_name'];
  $imgSize2 = $_FILES['imgcurp']['size'];
  
  
  if(empty($imgFile2)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir2 = 'img/alumno/'; // upload directory
 
   $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions2 = array('pdf'); // valid extensions
  
   // rename uploading image
   $nombre_curp = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt2;
    
   // allow valid image file formats
   if(in_array($imgExt2, $valid_extensions2)){   
    // Check file size '5MB'
    if($imgSize2 < 10000000)    {
     move_uploaded_file($tmp_dir2,$upload_dir2.$nombre_curp);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo PDF.";  
   }
  }

  //Recibo los datos de la foto del acta de nacimiento
  $imgFile3 = $_FILES['imgacta']['name'];
  $tmp_dir3 = $_FILES['imgacta']['tmp_name'];
  $imgSize3 = $_FILES['imgacta']['size'];
  
  
  if(empty($imgFile3)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir3 = 'img/alumno/'; // upload directory
 
   $imgExt3 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions3 = array('pdf'); // valid extensions
  
   // rename uploading image
   $nombre_acta = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt3;
    
   // allow valid image file formats
   if(in_array($imgExt3, $valid_extensions3)){   
    // Check file size '5MB'
    if($imgSize3 < 10000000)    {
     move_uploaded_file($tmp_dir3,$upload_dir3.$nombre_acta);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo PDF.";  
   }
  }

  //Recibo los datos de la foto del comprobante de domicilio
  $imgFile4 = $_FILES['imgdomi']['name'];
  $tmp_dir4 = $_FILES['imgdomi']['tmp_name'];
  $imgSize4 = $_FILES['imgdomi']['size'];
  
  
  if(empty($imgFile4)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir4 = 'img/alumno/'; // upload directory
 
   $imgExt4 = strtolower(pathinfo($imgFile4,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions4 = array('pdf'); // valid extensions
  
   // rename uploading image
   $nombre_domi = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt4;
    
   // allow valid image file formats
   if(in_array($imgExt4, $valid_extensions4)){   
    // Check file size '5MB'
    if($imgSize4 < 10000000)    {
     move_uploaded_file($tmp_dir4,$upload_dir4.$nombre_domi);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo PDF.";  
   }
  }

  //Recibo los datos de la foto del certificado de estudios
  $imgFile5 = $_FILES['imgestu']['name'];
  $tmp_dir5 = $_FILES['imgestu']['tmp_name'];
  $imgSize5 = $_FILES['imgestu']['size'];
  
  
  if(empty($imgFile5)){
   $errMSG = "Seleccione una imagen.";
  }
  else
  {
   $upload_dir5 = 'img/alumno/'; // upload directory
 
   $imgExt5 = strtolower(pathinfo($imgFile5,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions5 = array('pdf'); // valid extensions
  
   // rename uploading image
   $nombre_estu = $apellidop."_".$apellidom."_".$nombre."_".rand(1000,1000000).".".$imgExt5;
    
   // allow valid image file formats
   if(in_array($imgExt5, $valid_extensions5)){   
    // Check file size '5MB'
    if($imgSize5 < 10000000)    {
     move_uploaded_file($tmp_dir5,$upload_dir5.$nombre_estu);
    }
    else{
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else{
    $errMSG = "Inserte un archivo PDF.";  
   }
  }
  
  
  // if no error occured, continue ....
  if(!isset($errMSG))
  {
   $consulta = $pdo->prepare("INSERT INTO alta_alumnos(apellido_p,apellido_m,nombre_a,sexo,curp,edad,num_tel,domicilio,localidad,CP,municipio,estado,estado_civil,discapacidad,imagen_actan,imagen_curp,imagen_compdom,imagen_compest,foto_alum,email) VALUES(:apellidop, :apellidom, :nombre, :sexo, :curp, :edad, :tel, :domicilio, :localidad, :cp, :municipio, :estado, :civil, :discapacidad, :imgacta, :imgcurp, :imgdomi, :imgestu, :foto, :correo) ");

    $consulta->bindParam(':apellidop',$apellidop);
    $consulta->bindParam(':apellidom',$apellidom);
    $consulta->bindParam(':nombre',$nombre);
    $consulta->bindParam(':sexo',$sexo);
    $consulta->bindParam(':curp',$curp);
    $consulta->bindParam(':edad',$edad);
    $consulta->bindParam(':tel',$tel);
    $consulta->bindParam(':domicilio',$domicilio);
    $consulta->bindParam(':localidad',$localidad);
    $consulta->bindParam(':cp',$cp);
    $consulta->bindParam(':municipio',$municipio);
    $consulta->bindParam(':estado',$estado);
    $consulta->bindParam(':civil',$civil);
    $consulta->bindParam(':discapacidad',$discapacidad);
    $consulta->bindParam(':imgacta',$nombre_acta);
    $consulta->bindParam(':imgcurp',$nombre_curp);
    $consulta->bindParam(':imgdomi',$nombre_domi);
    $consulta->bindParam(':imgestu',$nombre_estu);
    $consulta->bindParam(':foto',$nombre_img);
    $consulta->bindParam(':correo',$correo);
   
    if($consulta->execute()){
        ?>
        <script>
        window.location.href='alta_alumnos.php';
        </script>
        <?php
    }else{
        echo "Error no se pudo almacenar la información";
    }

  }
 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro Alumnos</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>NUEVO REGISTRO - ALUMNOS</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="post" enctype="multipart/form-data">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos Personales</div>
                <div class="row" id="titulo"><strong>Datos Generales</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-2">Apellido Paterno</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="apellidop" required="">
                    </div>

                    <label class="col-sm-2">Apelido Materno</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="apellidom" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label  class="col-sm-2">Nombre(s)</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="nombre" required="">
                    </div>

                    <label class="col-sm-2">Foto</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="foto" accept="image/*" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CURP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="curp" required="">
                    </div>

                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Dirección</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Domicilio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="domicilio" required="">
                    </div>

                    <label class="col-sm-2">Colonia o Localidad</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="localidad" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">CP</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" name="cp" min="0" required="">
                    </div>
                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" type="text" name="municipio" required="" id="sel1">
                            <option>--- Seleccionar municipio ---</option>
                            <?php 
                                $sql = "SELECT folio_mun, nombre_mun FROM municipios ORDER BY nombre_mun ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['folio_mun'].'">'.$row['nombre_mun'].'</option>';
                                }

                            ?>
                        </select>

                    </div>
                    
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Estado</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="estado" >
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Información adicional</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Estado Civil</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="civil">
                        </div>
                        <label class="col-sm-2">Discapacidad</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="discapacidad">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">No Teléfono</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="tel" min="0">
                        </div>

                        <label class="col-sm-2">Sexo</label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="sexo" id="sel1">
                                <option>--- Seleccionar sexo ---</option>
                                <option value="FEMENINO">FEMENINO</option>
                                <option value="MASCULINO">MASCULINO</option>
                            </select>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Edad</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="number" name="edad" min="0">
                        </div>

                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="email" name="correo">
                        </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Documentos</div>
                <div class="row" id="titulo"><strong>Ingresa los documentos escaneados en la casilla que correspondan.</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">CURP</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="file" name="imgcurp" accept="application/pdf" required="">
                        </div>

                        <label class="col-sm-2">Acta de Nacimiento</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="file" name="imgacta" accept="application/pdf" required="">
                        </div>
                </div>
                <div class="row text-left">
                        <label class="col-sm-2">Comprobante de Domicilio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="file" name="imgdomi" accept="application/pdf" required="">
                        </div>

                        <label class="col-sm-2">Certificado de Estudios</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="file" name="imgestu" accept="application/pdf" required="">
                        </div>
                </div>
            </div>

            
            

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave" value="Guardar">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white;">Cancelar</a>
            </div> 
        </form>
    </div>
    
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>