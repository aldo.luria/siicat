<?php 
    include "php/connect.php";
    IF(!empty($_SESSION['username']) || $_SESSION['tipo'] == "PLANT"){
?>
<!DOCTYPE html>
<?php  
error_reporting( ~E_NOTICE ); // avoid notice

 if(isset($_POST['btnsave1']))
 {
    $id = $_GET['id_alumno'];
    $idcurso=$_POST['idcurso'];  

    

    $consulta1=$pdo->prepare("INSERT INTO alumno_curso2018reg(id_alumno_c,id_curso) VALUES(:id_alumno, :idcurso) ");

    $consulta1->bindParam(':id_alumno',$id);
    $consulta1->bindParam(':idcurso',$idcurso);

    if($consulta1->execute()){
        echo "Ya se registroooooooooooooooooooooooooooooó";
    }else{
        echo "Error no se pudo almacenar la información";
    }

 }
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<body>

    <div class="page-header text-center">
        <h4><strong>INSCRIBIR ALUMNOS - CURSO2018_REGULAR</strong></h4>
    </div>

    <div class="container-fluid">
        <form method="POST" enctype="multipart/form-data" name="formebc1">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Datos del Curso</div>
                <div class="row" id="titulo"><strong>Curso Regular</strong></div>
                <div class="row text-left">
                    <label  class="col-sm-4">Seleccionar Curso</label>
                          <div class="col-sm-8">
                            <select class="form-control input-sm" type="number" name="idcurso" onchange="from(document.formebc1.idcurso.value,'cargarcursoreg','cargar_datos_cursoreg.php');" required="">
                            <option value="0"> --- Seleccione Curso --- </option>
                            <?php 
                                $inscribir1=$pdo->prepare("
                            SELECT SQL_CALC_FOUND_ROWS CR.*,P.nombre,I.*,O.nombreModulo,E.especialidad,M.nombre_mun FROM cursos2018_regular AS CR LEFT JOIN plantel AS P ON CR.id_plantel = P.id LEFT JOIN instructores AS I ON CR.id_instructor = I.id LEFT JOIN oferta AS O ON CR.id_modulo_c = O.id LEFT JOIN modulosespecialidades AS E ON O.idEspecialidad = E.id_especialidad LEFT JOIN municipios AS M ON CR.id_mun_c = M.folio_mun ORDER BY folio DESC 
                            ");
                                  $inscribir1->execute();
                                  $rows1=$inscribir1->fetchAll();
                                    foreach ($rows1 as $row1) {
                                      echo '<option value="'.$row1['folio'].'">'.$row1['nombreModulo'].'</option>';
                                      }

                                      ?>
                            </select>

                          </div>
                </div>
              <div id="cargarcursoreg">
                <div class="row text-left">
                    <label  class="col-sm-4">Instructor</label>
                    <div class="col-md-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-4">Duración</label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" type="text" disabled="">
                            <option value="0"> Esperando Curso </option>
                        </select>
                    </div>

                </div>
              </div>
                
            </div>
        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btnsave1" value="Inscribir">
            <a class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="color: white; background-color: #7a1315">Cancelar</a>
            </div> 
        </form>
    </div>

</body>
<?php
        }else{
        header("Location: ../index.php");
    }
?>
</html>