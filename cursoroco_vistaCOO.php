<?php 
    include "procesossesiones/seguridad.php";
    include "php/connect.php";
    include "php/paginadorocoasig.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Examen ROCO</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/popper.min.js"></script> 
    <script src="js/jquery-3.3.1.slim.min.js"></script> 
    <script src="js/aprobar.js"></script>
    <script src="js/noaprobar.js"></script>
    <style type="text/css">
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #7a1315 !important;
            color: white !important;
        }
        ul.pagination li{
            font-size: 15px;
        }
        table{
            box-shadow: 5px 5px 20px #9f9f9f;
        }
        ul.pagination li{
            box-shadow: 5px 5px 10px #9f9f9f;
        }
    </style>
</head>
<?php  
        IF($_SESSION['tipo'] == "COO"){
?>
<header>    
    <?php require_once "partes/nav_COO.php"; ?>
</header>
<body>

	<div id="tabla">

			<div class="page-header">
                <h4><strong>CONSULTA - EXAMEN ROCO</strong></h4>
            </div>

            <form action="buscar_roco_COO.php" method="GET" class="form_sear">
                        <div class="container-fluid">
                        <div class="row">
                            <input type="text" class="form-control col-md justify-content-start" name="busqueda" id="busqueda" placeholder="Búsqueda rápida">
                            <button type="submit" class="btn btn-default  justify-content-start"><i class="fa fa-search"></i></button>
                            <div class="col-md"></div>
        	                <!--<a href="#ventana1" class="btn btn-success btn-lage  justify-content-end" data-toggle="modal">Nuevo <i class="fa fa-plus-circle"></i></a>-->
                        </div>
                        </div>
            </form>
            <br>

            <div class="text-right">

            	<div class="modal fade" id="ventana1">
                    <div class="modal-dialog modal-lg">   
                        <div class="modal-content">
                                <div class="modal-header">
                                    <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <!-- contenido-->
                                <div class="modal-body">

                                    <?php require_once "formularios/registro_roco.php"; ?>

                                </div>

                                <div class="modal-footer">
                                </div>
                        </div>
                    </div>
                </div>

            </div>

			<table class="table table-bordered table-striped table-hover table-condensed responsive">
				<thead>
					<tr>		
                        <th class="text-center">Folio</th>
				        <th class="text-center">Expediente</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Plantel</th>
                        <th class="text-center">Especialidad</th>
                        <th class="text-center">Módulo</th>
                        <th class="text-center">Municipio</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Aprobar</th>
                        <th class="text-center">Rechazar</th>
                        
					</tr>

				</thead>
				<tbody>
               <?php
                            if($totalregistros5>=1): 
                            foreach($registros5 as $reg):
                ?>
                    
                               <tr>
                                    <td class='text-center'><strong><?php echo $reg['folio']  ?></strong></td>
                                    <td class='text-center'><strong><?php echo $reg['Expediente']  ?></strong></td>
                                    <td><?php echo $reg['Nombre'] ?></td>
                                    <td><?php echo $reg['nombre'] ?></td>
                                    <td><?php echo $reg['especialidad'] ?></td>
                                    <td><?php echo $reg['nombreModulo'] ?></td>
                                    <td class='text-nowrap'><?php echo $reg['nombre_mun'] ?></td>
                                    <td class='text-center text-nowrap'><?php echo $reg['fecha_inicio'] ?></td>
                                    <td class="text-center"><?php echo $reg['status'] ?></td>
                                    <td class='text-center'><?php echo "<a data-confirm4='¿Aprobar Curso?' href='aprobar_roco.php?folio=".$reg['folio']."'><i class='fa fa-check-square' style='font-size:20px; color:green'></i></a>"?></td>
                                    <td class='text-center'><?php echo "<a data-confirm5='¿Recahzar Curso?' href='rechazar_roco.php?folio=".$reg['folio']."'><i class='fa fa-remove' style='font-size:20px; color:red'></i></a>"?></td>
                                </tr>

                <?php
                            endforeach; 
                            else:
                ?>
                                <tr>
                                    <td colspan='11' class="text-center"><strong>No hay registros</strong></td>
                                </tr>
                <?php 
                            endif; 
                ?>
                </tbody>
            </table>

                <?php
                $primera5 = ($pagina5 - 5) > 1 ? $pagina5 - 5 : 1;
                $ultima5 = ($pagina5 + 5) < $numeropaginas5 ? $pagina5 + 5 : $numeropaginas5;

                if($numeropaginas5>=1): ?>
                    <br>
                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination justify-content-center">
                            <li class="page-item active">
                                <div class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">
                                    Página <?php echo $pagina5; ?> de <?php echo $numeropaginas5; ?>
                                </div>
                            </li>
                            <?php if($pagina5==1): ?>
                                <li class="disabled page-item" title="Prímera">
                                    <a class="page-link" href="#"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="disabled page-item" title="Anterior">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Prímera">
                                    <a class="page-link" href="cursoroco_vistaCOO.php?pagina5=1"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="page-item" title="Anterior">
                                    <a class="page-link" href="cursoroco_vistaCOO.php?pagina5=<?php echo $pagina5-1; ?>" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php endif;

                                for ($i = $primera5; $i <= $ultima5; $i++){
                                    if ($pagina5 == $i)
                                        echo '<li class="active page-item">
                                            <a class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">'.$pagina5.'</a>
                                        </li>';
                                    else
                                        echo '<li class="page-item">
                                            <a class="page-link" href="cursoroco_vistaCOO.php?pagina5='.$i.'">'.$i.'</a>
                                            </li>';
                                }

                            if($pagina5==$numeropaginas5):?>
                                <li class="disabled page-item" title="Siguiente">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="disabled page-item" title="Última">
                                    <a class="page-link" href="#"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Siguiente">
                                    <a class="page-link" href="cursoroco_vistaCOO.php?pagina5=<?php echo $pagina5+1; ?>" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="page-item" title="Última">
                                    <a class="page-link" href="cursoroco_vistaCOO.php?pagina5=<?php echo $numeropaginas5; ?>"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                
            
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>