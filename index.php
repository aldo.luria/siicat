<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Iniciar sesión</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style type="text/css">
    	#formulario{
    		box-shadow: 5px 5px 20px #9f9f9f;
    	}
        .btn-success{
            background-color: #186d41 !important;
        }
        label{
        	font-size: 15px;
        }
    </style>
</head>
<header>
    <nav id="inicio" class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.php"><img src="img/Tlaxcala-ICATLAX.png" width="200px" style="padding-left: 30px" title="Menú - Inicio"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </nav>
</header>
<body>
<?php  
    if (isset($_POST['username']) && isset($_POST['clave'])) {
        require_once "php/connect.php";
        require_once "procesossesiones/login.php";
    }
?>
    <br>
    <div class="page-header text-center">
        <h4><strong>INICIAR SESIÓN</strong></h4>
        <div class="text-center">
            <i class="fa fa-user-circle-o" style="font-size: 80px; color: #560f11"></i><br>
        </div> 
    </div>
    <div class="container">
    <div class="row"></div>
        <form action="" method="POST">
        <div id="formulario">
            <div class="container">
                <div class="row" id="titulo" style="background-color: #560f11; font-size: 15px;">Iniciar sesión</div>


                <div class="row" id="titulo"><strong>Datos de usuario</strong></div>
                <div class="row"></div>
                <div class="row text-left">
                    <label class="col-lg-2">Usuario</label>
                    <div class="col-lg-10">
                        <input class="form-control input-lg" type="text" name="username" placeholder="Username" required="">
                    </div>
                </div>	
                <div class="row text-left">
                    <label class="col-lg-2">Contraseña</label>
                    <div class="col-lg-10">
                        <input class="form-control input-lg" type ="password" name="clave" placeholder="Password" required="">
                    </div>
                </div>
                <div class="row"></div>				
        	</div>
    	</div>

        <div class="text-center"><br>
        	<input class="btn btn-success" type="submit" value="Ingresar" name="iniciar">
        </div> 
	    </form>

    </div>

</body>
<footer>
    <?php require_once "partes/footer.html" ?>
</footer>
</html>