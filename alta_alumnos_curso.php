<?php 
    include "procesossesiones/seguridad.php";
    include "php/connect.php";
    include "php/pagalumno.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Alumnos</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/popper.min.js"></script> 
    <script src="js/jquery-3.3.1.slim.min.js"></script> 
    <script src="js/personalizado.js"></script> 
    <style type="text/css">
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #7a1315 !important;
            color: white !important;
        }
        ul.pagination li{
            font-size: 15px;
        }
        table{
            box-shadow: 5px 5px 20px #9f9f9f;
        }
        ul.pagination li{
            box-shadow: 5px 5px 10px #9f9f9f;
        }
    </style>
</head>
<?php  
        IF($_SESSION['tipo'] == "PLANT"){
?>
<header>    
    <?php require_once "partes/nav_PLA.php"; ?>
</header>
<body>
    <?php
        require_once "php/pagalumno.php";
    ?>

    <div id="tabla">

            <div class="page-header">
                <h4><strong>CONSULTA - INSCRIBIR ALUMNOS</strong></h4>
            </div>

            <form action="buscar_alumno_curso.php" method="GET" class="form_sear">
                        <div class="container-fluid">
                        <div class="row">
                            <input type="text" class="form-control col-md justify-content-start" name="busqueda" id="busqueda" placeholder="Búsqueda rápida">
                            <button type="submit" class="btn btn-default  justify-content-start"><i class="fa fa-search"></i></button>
                            <div class="col-md"></div>
                           <!--<a href="#ventana1" class="btn btn-success btn-lage  justify-content-end" data-toggle="modal">Nuevo <i class="fa fa-plus-circle"></i></a>-->
                        </div>
                        </div>
            </form>
            <br>

            <div class="text-right">

                <div class="modal fade" id="ventana1">
                    <div class="modal-dialog modal-lg">   
                        <div class="modal-content">
                                <div class="modal-header">
                                    <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <!-- contenido-->
                                <div class="modal-body">

                                    <?php require_once "formularios/registro_alumno.php"; ?>

                                </div>

                                <div class="modal-footer">
                                </div>
                        </div>
                    </div>
                </div>

            </div>

            <table class="table table-bordered table-striped table-hover table-condensed responsive">
                <thead>
                    <tr>        
                        <th class="text-center">Inscribir</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">CURP</th>
                        <th class="text-center">Edad</th>
                        <th class="text-center">Teléfono</th>
                        <th class="text-center">Domicilio</th>
                        <th class="text-center">Estado Civil</th>
                        <th class="text-center">Discapacidad</th>
                    </tr>

                </thead>
                <tbody>
                            <?php
                            if($totalregistros2>=1): 
                            foreach($registros2 as $reg): 
                            ?>
                    
                   <tr>
                        <td class='text-center'><a href="datosalumnoscursos.php?id_alumno=<?php echo $reg['id_alumno'] ?>"><i class="fa fa-edit" style="font-size:20px; color: #186d41;"></i></a></td>
                        <td><?php echo $reg['nombre_a'] ?> <?php echo $reg['apellido_p']  ?> <?php echo $reg['apellido_m'] ?></td>
                        <td class='text-center text-nowrap'><?php echo $reg['curp'] ?></td>
                        <td class='text-center text-nowrap'><?php echo $reg['edad'] ?></td>
                        <td class='text-center text-nowrap'><?php echo $reg['num_tel'] ?></td>
                        <td><?php echo $reg['domicilio'] ?></td>
                        <td class='text-center text-nowrap'><?php echo $reg['estado_civil'] ?></td>
                        <td class='text-center'><?php echo $reg['discapacidad'] ?></td>
                    </tr>

                            <?php 
                            endforeach; 
                            else:
                            ?>
                        <tr>
                            <td colspan='11' class="text-center"><strong>No hay registros</strong></td>
                        </tr>
                            <?php 
                            endif; 
                            ?>
                </tbody>
                </table>

                <?php
                $primera = ($pagina2 - 5) > 1 ? $pagina2 - 5 : 1;
                $ultima = ($pagina2 + 5) < $numeropaginas2 ? $pagina2 + 5 : $numeropaginas2;

                if($numeropaginas2>=1): ?>
                    <br>
                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination justify-content-center">
                            <li class="page-item active">
                                <div class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">
                                    Página <?php echo $pagina2; ?> de <?php echo $numeropaginas2; ?>
                                </div>
                            </li>
                            <?php if($pagina2==1): ?>
                                <li class="disabled page-item" title="Prímera">
                                    <a class="page-link" href="#"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="disabled page-item" title="Anterior">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Prímera">
                                    <a class="page-link" href="alta_alumnos.php?pagina2=1"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="page-item" title="Anterior">
                                    <a class="page-link" href="alta_alumnos.php?pagina2=<?php echo $pagina2-1; ?>" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php endif;

                                for ($i = $primera; $i <= $ultima; $i++){
                                    if ($pagina2 == $i)
                                        echo '<li class="active page-item">
                                            <a class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">'.$pagina2.'</a>
                                        </li>';
                                    else
                                        echo '<li class="page-item">
                                            <a class="page-link" href="alta_alumnos.php?pagina2='.$i.'">'.$i.'</a>
                                            </li>';
                                }

                            if($pagina2==$numeropaginas2):?>
                                <li class="disabled page-item" title="Siguiente">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="disabled page-item" title="Última">
                                    <a class="page-link" href="#"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Siguiente">
                                    <a class="page-link" href="alta_alumnos?pagina2=<?php echo $pagina2+1; ?>" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="page-item" title="Última">
                                    <a class="page-link" href="alta_alumnos.php?pagina2=<?php echo $numeropaginas2; ?>"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                
            
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>