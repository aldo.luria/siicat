<?php 
	//Actualizar datos
	if (isset($_POST['id_inst']) && isset($_POST['id_plan'])) {
	$folio=$_POST['folio'];
    $curso=$_POST['curso'];
    $duracion=$_POST['duracion'];
    $calle=$_POST['calle'];                
    $colonia=$_POST['colonia'];
    $finicio=$_POST['finicio'];
    $ffin=$_POST['ffin'];
    //$totalhrs=$_POST['totalhrs'];
    $linicio=$_POST['linicio'];
    $lfin=$_POST['lfin'];
    $minicio=$_POST['minicio'];
    $mfin=$_POST['mfin'];
    $miinicio=$_POST['miinicio'];
    $mifin=$_POST['mifin'];
    $jinicio=$_POST['jinicio'];
    $jfin=$_POST['jfin'];
    $vinicio=$_POST['vinicio'];
    $vfin=$_POST['vfin'];
    $sinicio=$_POST['sinicio'];
    $sfin=$_POST['sfin'];
    $dinicio=$_POST['dinicio'];
    $dfin=$_POST['dfin'];
    $status='Asignado';
    $id_inst=$_POST['id_inst'];
    $id_plan=$_POST['id_plan'];
    $id_mun_c=$_POST['id_mun_c'];
    $observa=$_POST['observa'];

	$consulta2=$pdo->prepare("UPDATE cursos2018_extension SET curso=:curso, duracion=:duracion, calle_c=:calle, colonia_c=:colonia, fecha_inicio=:finicio, fecha_fin=:ffin, h_lunes_ini=:linicio, h_lunes_fin=:lfin, h_martes_ini=:minicio, h_martes_fin=:mfin, h_miercoles_ini=:miinicio, h_miercoles_fin=:mifin, h_jueves_ini=:jinicio, h_jueves_fin=:jfin, h_viernes_ini=:vinicio, h_viernes_fin=:vfin, h_sabado_ini=:sinicio, h_sabado_fin=:sfin, h_domingo_ini=:dinicio, h_domingo_fin=:dfin, status=:status, id_instructor=:id_inst, id_plantel=:id_plan, id_mun_c=:id_mun_c, observaciones_c=:observa WHERE folio=:folio");

    $consulta2->bindParam(':curso',$curso);
    $consulta2->bindParam(':duracion',$duracion);
    $consulta2->bindParam(':calle',$calle);
    $consulta2->bindParam(':colonia',$colonia);
    $consulta2->bindParam(':finicio',$finicio);
    $consulta2->bindParam(':ffin',$ffin);
    //$consulta2->bindParam(':totalhrs',$totalhrs);
    $consulta2->bindParam(':linicio',$linicio);
    $consulta2->bindParam(':lfin',$lfin);
    $consulta2->bindParam(':minicio',$minicio);
    $consulta2->bindParam(':mfin',$mfin);
    $consulta2->bindParam(':miinicio',$miinicio);
    $consulta2->bindParam(':mifin',$mifin);
    $consulta2->bindParam(':jinicio',$jinicio);
    $consulta2->bindParam(':jfin',$jfin);
    $consulta2->bindParam(':vinicio',$vinicio);
    $consulta2->bindParam(':vfin',$vfin);
    $consulta2->bindParam(':sinicio',$sinicio);
    $consulta2->bindParam(':sfin',$sfin);
    $consulta2->bindParam(':dinicio',$dinicio);
    $consulta2->bindParam(':dfin',$dfin);
    $consulta2->bindParam(':status',$status);
    $consulta2->bindParam(':id_inst',$id_inst);
    $consulta2->bindParam(':id_plan',$id_plan);
    $consulta2->bindParam(':id_mun_c',$id_mun_c); 
    $consulta2->bindParam(':observa',$observa); 
	$consulta2->bindParam(':folio',$folio);

			if($consulta2->execute()){
                echo '
                    <div class="container-fluid text-center alert alert-success" style="margin-bottom: 0px">
                        <h4>Datos Actualizados</h4>
                        <a class="btn btn-success" href="#inicio">Seguir editando</a>
                        <a class="btn btn-danger" href="cursoextension_vistaDTA.php">Atras</a>
                    </div>

                ';
			}else{
                echo '
                    <a href="cursoextension_vistaDTA.php" style="text-decoration: none">
                        <div class="container-fluid text-center alert alert-danger" style="margin-bottom: 0px">
                            <h4>Error: No se Actualizaron los datos</h4>
                        </div>
                    </a>

                ';
			}

	}

    //combobox instructor
    $sql = "SELECT * FROM instructores ORDER BY Nombre ASC";
    $result = $pdo->query($sql);
    $rows = $result->fetchAll();

    //combobox plantel
    $sql2 = "SELECT * FROM plantel ORDER BY nombre ASC";
    $result2 = $pdo->query($sql2);
    $rows2 = $result2->fetchAll();

    ///combobox municipio
    $sql3 = "SELECT folio_mun, nombre_mun FROM municipios ORDER BY nombre_mun ASC";
    $result3 = $pdo->query($sql3);
    $rows3 = $result3->fetchAll();

	//Recuperar datos
	if(isset($_GET['folio'])){
		$folio=$_GET['folio'];
		$consulta=$pdo->prepare("SELECT CE.*,P.nombre,I.*,M.* FROM cursos2018_extension AS CE LEFT JOIN plantel AS P ON CE.id_plantel = P.id LEFT JOIN instructores AS I ON CE.id_instructor = I.id LEFT JOIN municipios AS M ON CE.id_mun_c = M.folio_mun WHERE CE.folio=:folio");
		$consulta->bindParam(":folio",$folio);
		$consulta->execute();
		if($consulta->rowCount()>=1){
			$fila=$consulta->fetch();

			echo '

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
        <style type="text/css">
        #formulario{
            box-shadow: 5px 5px 20px #9f9f9f;
            background-color: #e7e7e7;
        }
        a{
            color: #186d41;
        }
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #9f282f !important;
        }
    </style>
</head>
<header>'; ?>
    <?php require_once "partes/nav_DTA.php"; ?>
<?php echo'
</header>
<body>
    <div class="page-header text-center">
        <h4><strong>ACTUALIZAR REGISTRO - CURSOS2018_EXTENSIÓN</strong></h4>
    </div>

    <div class="container">
        <form action="" method="POST">

        <input type="hidden" name="folio" value="'.$fila['folio'].'">

        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos del Instructor</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Instructor</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" name="id_inst" required="">
                                <option value="'.$fila['id_instructor'].'">'.$fila['Nombre'].'</option>
                                <option>--- Seleccionar Instructor ---</option>';?>
                            <?php 
                                 foreach ($rows as $row) {
                                    echo '<option value="'.$row['id'].'">'.$row['Nombre'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos del Curso</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Plantel</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="id_plan" required="">
                            <option value="'.$fila['id_plantel'].'">'.$fila['nombre'].'</option>';?>
                            <?php 
                                foreach ($rows2 as $row2) {
                                    echo '<option value="'.$row2['id'].'">'.$row2['nombre'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>                    
                    </div>

                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="id_mun_c" required="">
                            <option value="'.$fila['id_mun_c'].'">'.$fila['nombre_mun'].'</option>';?>
                            <?php 
                                foreach ($rows3 as $row3) {
                                    echo '<option value="'.$row3['folio_mun'].'">'.$row3['nombre_mun'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div> 
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="calle" value="'.$fila['calle_c'].'" required="">
                    </div>

                    <label class="col-sm-2">Colonia</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="colonia" value="'.$fila['colonia_c'].'" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Curso</label>
                    <div class="col-sm-10">
                        <input class="form-control input-sm" type="text" name="curso" value="'.$fila['curso'].'" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Duración</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" name="duracion" min="0" value="'.$fila['duracion'].'" required="">
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Fecha Inicio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="finicio" value="'.$fila['fecha_inicio'].'" required="">
                    </div>

                    <label class="col-sm-2">Fecha Fin</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="ffin" value="'.$fila['fecha_fin'].'" required="">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Horarios</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Lunes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="linicio" value="'.$fila['h_lunes_ini'].'" >
                        </div>
                        <label class="col-sm-2">Lunes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="lfin" value="'.$fila['h_lunes_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Martes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="minicio" value="'.$fila['h_martes_ini'].'" >
                        </div>
                        <label class="col-sm-2">Martes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="mfin" value="'.$fila['h_martes_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Miércoles Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="miinicio" value="'.$fila['h_miercoles_ini'].'" >
                        </div>
                        <label class="col-sm-2">Miércoles Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="mifin" value="'.$fila['h_miercoles_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Jueves Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="jinicio" value="'.$fila['h_jueves_ini'].'" >
                        </div>
                        <label class="col-sm-2">Jueves Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="jfin" value="'.$fila['h_jueves_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Viernes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="vinicio" value="'.$fila['h_viernes_ini'].'" >
                        </div>
                        <label class="col-sm-2">Viernes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="vfin" value="'.$fila['h_viernes_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Sábado Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sinicio" value="'.$fila['h_sabado_ini'].'" >
                        </div>
                        <label class="col-sm-2">Sábado Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sfin" value="'.$fila['h_sabado_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Domingo Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="dinicio" value="'.$fila['h_domingo_ini'].'" >
                        </div>
                        <label class="col-sm-2">Domingo Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="dfin" value="'.$fila['h_domingo_fin'].'" >
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="status" value="'.$fila['status'].'" disabled>
                        </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Observaciones</strong></div>
                <div class="row text-left">
                        <div class="col-sm-12">
                            <textarea class="form-control input-sm" name="observa" style="height:150px;">'.$fila['observaciones_c'].'</textarea>
                        </div>
                </div>
            </div>

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" value="Asignar">
            <a class="btn btn-danger" href="cursoextension_vistaDTA.php">Cancelar</a>
            </div> 
        </form>
    </div>
    <br>
</body>
</html>

			';

			}else{
			echo "Ocurrio un error";
		}
	}else{
		echo "Error no se pudo procesar la petición";
	}