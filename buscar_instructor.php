<?php 
    include "procesossesiones/seguridad.php";
    include "php/connect.php";
    include "phpbus/paginadorbuscainstdta.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Instructores</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <script src="js/popper.min.js"></script> 
    <script src="js/jquery-3.3.1.slim.min.js"></script> 
    <script src="js/personalizado.js"></script> 
    <style type="text/css">
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #7a1315 !important;
        }
        ul.pagination li{
            font-size: 15px;
        }
        table{
            box-shadow: 5px 5px 20px #9f9f9f;
        }
        table tbody tr td a{
            color: #186d41 !important;
        }
        ul.pagination li{
            box-shadow: 5px 5px 10px #9f9f9f;
        }
    </style>
</head>
<?php  
        IF($_SESSION['tipo'] == "DTA"){
?>
<header>    
    <?php require_once "partes/nav_DTA.php"; ?>
</header>
<body>
    <?php
        require_once "php/connect.php";
        if(isset($_GET['id'])){
            require_once "procesos/eliminar.php";
        }
         
        $busqueda = $_REQUEST['busqueda'];
        if(empty($busqueda))
        {
            header("location: instructores.php");
        }

        $busqueda = $_GET['busqueda'];

        $sql = $pdo->prepare("SELECT I.*,M.nombre_mun FROM instructores AS I LEFT JOIN municipios AS M on I.id_municipios = M.folio_mun WHERE NOT (I.condicion = 'No Competente') AND (calidad LIKE '%".$busqueda."%' OR Expediente LIKE '%".$busqueda."%' OR I.Nombre LIKE '%".$busqueda."%' OR M.nombre_mun LIKE '%".$busqueda."%' OR Pefil LIKE '%".$busqueda."%' OR Especialidad LIKE '%".$busqueda."%' OR Puntaje LIKE '%".$busqueda."%' OR No_celular LIKE '%".$busqueda."%' OR No_telefono LIKE '%".$busqueda."%' OR Email LIKE '%".$busqueda."%') ORDER BY id DESC LIMIT $inicio, $regpagina");
        $sql ->execute(array('busqueda' =>$busqueda));
        $resultado = $sql->fetchAll();
        
    ?>


    <div id="tabla">

            <div class="page-header">
                <h4><strong>CONSULTA - INSTRUCTORES</strong></h4>
            </div>

            <form action="buscar_instructor.php" method="GET" class="form_sear">
                        <div class="container-fluid">
                        <div class="row">
                            <input type="text" class="form-control col-md justify-content-start" name="busqueda" id="busqueda" placeholder="Búsqueda rápida" value="<?php echo $busqueda; ?>">
                            <button type="submit" class="btn btn-default  justify-content-start"><i class="fa fa-search"></i></button>
                            <div class="col-md"></div>
                           <!--<a href="#ventana1" class="btn btn-success btn-lage  justify-content-end" data-toggle="modal">Nuevo <i class="fa fa-plus-circle"></i></a>-->
                        </div>
                        </div>
            </form>
            <br>

            <div class="text-right">

                <div class="modal fade" id="ventana1">
                    <div class="modal-dialog modal-lg">   
                        <div class="modal-content">
                                <div class="modal-header">
                                    <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <!-- contenido-->
                                <div class="modal-body">

                                    <!-- Colocar formulario de registro de instrictor -->
                                        
                                </div>

                                <div class="modal-footer">
                                </div>
                        </div>
                    </div>
                </div>

            </div>

            <table class="table table-bordered  table-hover table-condensed responsive">
                <thead>
                    <tr>        
                                <th class="text-center">Editar</th>
                                <th class="text-center">Calidad</th>
                                <th class="text-center">Expediente</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Municipio</th>
                                <th class="text-center">Perfil</th>
                                <th class="text-center">Especialidad</th>
                                <th class="text-center">Puntaje</th>
                                <th class="text-center">Celular</th>
                                <th class="text-center">Teléfono</th>
                                <th class="text-center">Email</th>
                    </tr>

                </thead>
                <tbody>
                            <?php
                   if($totalregistros>=1):
                    foreach ($resultado as $row):
                        switch($row['calidad']){
                                    case 'REGULAR':?>
                    <tr class="alert alert-warning">
                        <td class='text-center'><?php echo "<a href='datos_DTA.php?id=".$row['id']."'><i class='fa fa-pencil' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                        <td><?php echo $row['calidad'] ?></td>
                        <td class='text-center'><strong><?php echo $row['Expediente']  ?></strong></td>
                        <td><?php echo $row['Nombre'] ?></td>
                        <td><?php echo $row['nombre_mun'] ?></td>
                        <td><?php echo $row['Pefil'] ?></td>
                        <td><?php echo $row['Especialidad'] ?></td>
                        <td class='text-right text-nowrap'><?php echo $row['Puntaje'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_celular'] ?>"><?php echo $row['No_celular'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_telefono'] ?>"><?php echo $row['No_telefono'] ?></td>
                        <td><a href="mailtp:<?php echo $row['Email'] ?>"><?php echo $row['Email'] ?></a></td>
                    </tr>
                                <?php break;
                                    case 'MALO':?>
                    <tr class="alert alert-danger">
                        <td class='text-center'><?php echo "<a href='datos_DTA.php?id=".$row['id']."'><i class='fa fa-pencil' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                        <td><?php echo $row['calidad'] ?></td>
                        <td class='text-center'><strong><?php echo $row['Expediente']  ?></strong></td>
                        <td><?php echo $row['Nombre'] ?></td>
                        <td><?php echo $row['nombre_mun'] ?></td>
                        <td><?php echo $row['Pefil'] ?></td>
                        <td><?php echo $row['Especialidad'] ?></td>
                        <td class='text-right text-nowrap'><?php echo $row['Puntaje'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_celular'] ?>"><?php echo $row['No_celular'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_telefono'] ?>"><?php echo $row['No_telefono'] ?></td>
                        <td><a href="mailtp:<?php echo $row['Email'] ?>"><?php echo $row['Email'] ?></a></td>
                    </tr>
                                <?php break;
                                case 'BUENO':?>
                    <tr class="alert alert-success">
                        <td class='text-center'><?php echo "<a href='datos_DTA.php?id=".$row['id']."'><i class='fa fa-pencil' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                        <td><?php echo $row['calidad'] ?></td>
                        <td class='text-center'><strong><?php echo $row['Expediente']  ?></strong></td>
                        <td><?php echo $row['Nombre'] ?></td>
                        <td><?php echo $row['nombre_mun'] ?></td>
                        <td><?php echo $row['Pefil'] ?></td>
                        <td><?php echo $row['Especialidad'] ?></td>
                        <td class='text-right text-nowrap'><?php echo $row['Puntaje'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_celular'] ?>"><?php echo $row['No_celular'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_telefono'] ?>"><?php echo $row['No_telefono'] ?></td>
                        <td><a href="mailtp:<?php echo $row['Email'] ?>"><?php echo $row['Email'] ?></a></td>
                    </tr>
                                <?php break;
                                    default:?>
                    <tr>
                        <td class='text-center'><?php echo "<a href='datos_DTA.php?id=".$row['id']."'><i class='fa fa-pencil' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                        <td><?php echo $row['calidad'] ?></td>
                        <td class='text-center'><strong><?php echo $row['Expediente']  ?></strong></td>
                        <td><?php echo $row['Nombre'] ?></td>
                        <td><?php echo $row['nombre_mun'] ?></td>
                        <td><?php echo $row['Pefil'] ?></td>
                        <td><?php echo $row['Especialidad'] ?></td>
                        <td class='text-right text-nowrap'><?php echo $row['Puntaje'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_celular'] ?>"><?php echo $row['No_celular'] ?></td>
                        <td class='text-center text-nowrap'><a href="tel:<?php echo $row['No_telefono'] ?>"><?php echo $row['No_telefono'] ?></td>
                        <td><a href="mailtp:<?php echo $row['Email'] ?>"><?php echo $row['Email'] ?></a></td>
                    </tr>
                                    <?php break;
                                }
                            ?>
                    
                    <?php    
                    endforeach; 
                        else:
                    ?>    

                        <tr>
                            <td colspan='11' class="text-center"><strong>No hay cursos</strong></td>
                        </tr>
                        
                        <?php
                        endif;
                        ?>
                            
                </tbody>
                </table>

                <?php
                $primera = ($paginaa - 5) > 1 ? $paginaa - 5 : 1;
                $ultima = ($paginaa + 5) < $numeropaginas ? $paginaa + 5 : $numeropaginas;

                if($numeropaginas>=1): ?>
                    <br>
                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination justify-content-center">
                            <li class="page-item active">
                                <div class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">
                                    Página <?php echo $paginaa; ?> de <?php echo $numeropaginas; ?>
                                </div>
                            </li>
                            <?php if($paginaa==1): ?>
                                <li class="disabled page-item" title="Prímera">
                                    <a class="page-link" href="#"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="disabled page-item" title="Anterior">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Prímera">
                                    <a class="page-link" href="?paginaa=1 &busqueda=<?php echo $busqueda;?>"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="page-item" title="Anterior">
                                    <a class="page-link" href="?paginaa=<?php echo $paginaa-1; ?>&busqueda=<?php echo $busqueda; ?>" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php endif;

                                for ($i = $primera; $i <= $ultima; $i++){
                                    if ($paginaa == $i)
                                        echo '<li class="active page-item">
                                            <a class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">'.$paginaa.'</a>
                                        </li>';
                                    else
                                        echo '<li class="page-item">
                                            <a class="page-link" href="?paginaa='.$i.'&busqueda='.$busqueda.'">'.$i.'</a>
                                            </li>';
                                }

                            if($paginaa==$numeropaginas):?>
                                <li class="disabled page-item" title="Siguiente">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="disabled page-item" title="Última">
                                    <a class="page-link" href="#"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Siguiente">
                                    <a class="page-link" href="?paginaa=<?php echo $paginaa+1;?>&busqueda=<?php echo $busqueda;?>" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="page-item" title="Última">
                                    <a class="page-link" href="?paginaa=<?php echo $numeropaginas;?>&busqueda=<?php echo $busqueda;?>"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                
            
    </div>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>