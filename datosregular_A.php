<?php 
    include "procesossesiones/seguridad.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ver Curso Regular</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
	<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<?php  
        IF($_SESSION['tipo'] == "COO" || $_SESSION['tipo'] == "DTA" || $_SESSION['tipo'] == "PLANEA" || $_SESSION['tipo'] == "PLANT"){
?>
<body>
	<?php 
		require_once "php/connect.php";
		require_once "procesosc/actualizarregular_A.php";
	 ?>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>