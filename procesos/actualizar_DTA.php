<?php 
	//Actualizar datos
	if (isset($_POST['id'])) {
			$id=$_POST['id'];
            $calidad=$_POST['calidad'];

			$consulta2=$pdo->prepare("UPDATE instructores SET calidad=:calidad WHERE id=:id");

            $consulta2->bindParam(':calidad',$calidad);
			$consulta2->bindParam(':id',$id);

			if($consulta2->execute()){
                echo '
                    <div class="container-fluid text-center alert alert-success" style="margin-bottom: 0px">
                        <h4>Datos Actualizados</h4>
                        <a class="btn btn-success" href="#inicio">Seguir editando</a>
                        <a class="btn btn-danger" href="instructores.php">Atras</a>
                    </div>
                ';
			}else{
                echo '
                    <a href="instructores.php" style="text-decoration: none">
                        <div class="container-fluid text-center alert alert-danger" style="margin-bottom: 0px">
                            <h4>Error: No se Actualizaron los datos</h4>
                        </div>
                    </a>
                ';
			}

	}

	//Recuperar datos
	if(isset($_GET['id'])){
		$id=$_GET['id'];
		$consulta=$pdo->prepare("SELECT I.*,M.nombre_mun FROM instructores AS I LEFT JOIN municipios AS M ON I.id_municipios = M.folio_mun  WHERE id=:id");
		$consulta->bindParam(":id",$id);
		$consulta->execute();
		if($consulta->rowCount()>=1){
			$fila=$consulta->fetch();

			echo '

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
        <style type="text/css">
        #formulario{
            box-shadow: 5px 5px 20px #9f9f9f;
            background-color: #e7e7e7;
        }
        a{
            color: #186d41 !important;
        }
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #9f282f !important;
        }
    </style>
</head>
<header>'; ?>
    <?php require_once "partes/nav_DTA.php"; ?>
<?php echo'
</header>
<body>
    <div class="page-header text-center">
        <h4><strong>ACTUALIZACIÓN - INSTRUCTORES</strong></h4>
    </div>

    <div class="container">
        <form action="" method="POST">

        <input type="hidden" name="id" value="'.$fila['id'].'">
       
        <div id="formulario" style="font-size:14px;">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11"><h6>Datos Personales</h6></div>
                <div class="row" id="titulo"><strong>Datos Generales</strong></div>
                <div class="row">
                    <label  class="col-sm-2">Expediente</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="expediente" min="0" value="'.$fila['Expediente'].'"  disabled>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">Nombre</label>
                    <div class="col-sm-10">
                        <input class="form-control col-sm-12 input-sm" type="text" name="nombre" required="" value="'.$fila['Nombre'].'"  disabled>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">CURP</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="curp" required="" value="'.$fila['Curp'].'"  disabled>
                    </div>

                    <label class="col-sm-2">Foto</label>
                    <div class="col-sm-4 text-center">
                        <img src="/SIICAT/img/inst/'.$fila['imagen'].'" width="200px"><br>
                        <p class="text-center" style="font-size: 10px;">Esta foto ya no puede ser cambiada.</p>
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Dirección</strong></div>
                <div class="row">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="calle" required="" value="'.$fila['Calle'].'" disabled>
                    </div>

                    <label class="col-sm-2">Localidad</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="localidad" required="" value="'.$fila['Localidad'].'" disabled>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">CP</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="cp" required="" min="0" value="'.$fila['CP'].'" disabled>
                    </div>

                    <label class="col-sm-2">Colonia Barrio</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="colonia" required="" value="'.$fila['Colonia_barrio'].'" disabled>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" name="id_mun" required="" disabled>
                            <option value="'.$fila['id_municipios'].'">'.$fila['nombre_mun'].'</option>
                        </select>

                    </div>

                    <label class="col-sm-2">Sección Elec</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="seccion" required="" min="0" value="'.$fila['Seccion_elec'].'" disabled>
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Informción adicional</strong></div>
                <div class="row">
                        <label class="col-sm-2">Estado Civil</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="civil" required="" value="'.$fila['Estado_civil'].'" disabled>
                        </div>
                        <label class="col-sm-2">F Nacimiento</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="nacimiento" required="" value="'.$fila['F_Nacimiento'].'" disabled>
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">No Teléfono</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="tel" required="" value="'.$fila['No_telefono'].'" disabled>
                        </div>

                        <label class="col-sm-2">Sexo</label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="sexo" required="" id="sel1" disabled>
                                <option value="'.$fila['Sexo'].'">'.$fila['Sexo'].'</option>
                            </select>
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">No Celular</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="texr" name="cel" required="" value="'.$fila['No_celular'].'" disabled>
                        </div>

                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="email" name="correo" required="" value="'.$fila['Email'].'" disabled>
                        </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11"><h6>Perfil Académico</h6></div>
                <div class="row" id="titulo"><strong>Formación</strong></div>
                <div class="row">
                        <label class="col-sm-2">Perfil</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="perfil" required="" value="'.$fila['Pefil'].'" disabled>
                        </div>

                        <label class="col-sm-2">Nivel Académico</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="nivel" required="" value="'.$fila['Nivel_academico'].'" disabled>
                        </div>
                </div>
            </div>

            
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Docencia</strong></div>
                <div class="row">
                        <label class="col-sm-2">Especialidad</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="especialidad" value="'.$fila['Especialidad'].'" disabled>
                        </div>

                        <label class="col-sm-2">Fecha Evaluación</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="evaluacion" value="'.$fila['Fecha_evaluacion'].'" disabled>
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">Fecha Alta</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="alta" value="'.$fila['F_Alta'].'" disabled>
                        </div>

                        <label class="col-sm-2">Puntaje</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="number" name="puntaje" min="0" value="'.$fila['Puntaje'].'" disabled>
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">Observaciones</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="observaciones" value="'.$fila['Observaciones'].'" disabled>
                        </div>

                        <label class="col-sm-2">Fecha Baja</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="baja" value="'.$fila['F_Baja'].'" disabled>
                        </div>
                </div>
                <div class="row text-left">
                        <label class="col-sm-2">Calidad</label>
                        <div class="col-sm-10">
                           <select class="form-control input-sm" type="text" name="calidad" id="sel1">
                                <option value="'.$fila['calidad'].'">'.$fila['calidad'].'</option>
                                <option>--- Seleccionar calidad ---</option>
                                <option value="BUENO">Bueno</option>
                                <option value="REGULAR">Regular</option>
                                <option value="MALO">Malo</option>
                            </select>
                        </div>
                </div>
            </div>

              <div class="container-fluid">
                <div class="row" id="titulo"><strong>Expediente Digital</strong></div>
                <div class="row text-left">
                         
                    <label class="col-sm-2">Acta de Nacimiento</label>
                    <div class="col-sm-4">
                         <a href="img/pdfinst/'.$fila['acta_doc'].'"target="_blank" ><i class="fa fa-eye"> </i> Acta de nacimiento</a><br>
                         
                    </div>

                    <label class="col-sm-2">Curriculum Vitae </label>
                    <div class="col-sm-4">
                        <a href="img/pdfinst/'.$fila['cv_doc'].'" target="_blank"><i class="fa fa-eye"> </i> Curriculum Vitae</a><br>

                    </div>
                </div>

                <div class="row text-left">
                         
                    <label class="col-sm-2">Curp</label>
                    <div class="col-sm-4">
                        <a href="img/pdfinst/'.$fila['curp_doc'].'" target="_blank"><i class="fa fa-eye"> </i> Curp</a><br>
                    </div>

                    <label class="col-sm-2">Experiencia Laboral </label>
                    <div class="col-sm-4">
                         <a href="img/pdfinst/'.$fila['expl_doc'].'" target="_blank"><i class="fa fa-eye"> </i> Experiencia Laboral</a><br>
                    </div>
            
                </div>

                   <div class="row text-left">
                         
                    <label class="col-sm-2">Domicilio</label>
                    <div class="col-sm-4">
                         <a href="img/pdfinst/'.$fila['dom_doc'].'" target="_blank"><i class="fa fa-eye"> </i> Domicilio</a><br>
                    </div>

                    <label class="col-sm-2">Experiencia Frente Grupo </label>
                    <div class="col-sm-4">
                         <a href="img/pdfinst/'.$fila['expfg_doc'].'" target="_blank"><i class="fa fa-eye"> </i> Experiencia Frente Grupo</a><br>
                    </div>
                </div>

                   <div class="row text-left"> 
                    <label class="col-sm-2">INE</label>
                    <div class="col-sm-4">
                         <a href="img/pdfinst/'.$fila['ine_doc'].'" target="_blank"><i class="fa fa-eye"> </i> INE</a><br><br>
                    </div>
                </div>
            </div>

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" value="Guardar">
            <a class="btn btn-danger" href="instructores.php" style="color: white !important;">Cancelar</a>
            </div> 
        </form>
    </div>
    <br>
</body>
</html>

			';

			}else{
			echo "Ocurrio un error";
		}
	}else{
		echo "Error no se pudo procesar la petición";
	}