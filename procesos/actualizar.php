<!DOCTYPE html>

<?php  
error_reporting( ~E_NOTICE );
 require_once "php/connect.php";
 
 if(isset($_GET['id']) && !empty($_GET['id']))
 {
  $id = $_GET['id'];
  $consulta = $pdo->prepare('SELECT I.*,M.nombre_mun FROM instructores AS I LEFT JOIN municipios AS M ON I.id_municipios = M.folio_mun  WHERE id=:id');
  $consulta->execute(array(':id'=>$id));
  $fila = $consulta->fetch(PDO::FETCH_ASSOC);
  extract($fila);
 }
 else
 {
  header("Location: instructores_vistaPLANE.php");
 }
 
 if(isset($_POST['btn_save_updates']))
 {

    $id=$_POST['id'];
    //$foto=$_POST['foto'];
    $curp=$_POST['curp'];
    $nombre=$_POST['nombre'];
    $calle=$_POST['calle'];
    $colonia=$_POST['colonia'];
    $localidad=$_POST['localidad'];
    $cp=$_POST['cp'];
    $seccion=$_POST['seccion'];
    $civil=$_POST['civil'];
    $sexo=$_POST['sexo'];
    $nacimiento=$_POST['nacimiento'];
    $alta=$_POST['alta'];
    $baja=$_POST['baja'];
    $perfil=$_POST['perfil'];
    $nivel=$_POST['nivel'];
    $especialidad=$_POST['especialidad'];
    $evaluacion=$_POST['evaluacion'];
    $puntaje=$_POST['puntaje'];
    $cel=$_POST['cel'];
    $tel=$_POST['tel'];
    $correo=$_POST['correo'];
    $observaciones=$_POST['observaciones'];
    $id_mun=$_POST['id_mun'];
   
  $imgFile = $_FILES['user_image']['name'];
  $tmp_dir = $_FILES['user_image']['tmp_name'];
  $imgSize = $_FILES['user_image']['size'];
     
  if($imgFile)
  {
   $upload_dir = 'img/inst/'; // upload directory 
   $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
   $userpic = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt;
   if(in_array($imgExt, $valid_extensions))
   {   
    if($imgSize < 5000000)
    {
     unlink($upload_dir.$fila['imagen']);
     move_uploaded_file($tmp_dir,$upload_dir.$userpic);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic = $fila['imagen']; // old image from database
  } 


   $imgFile2 = $_FILES['acta']['name'];
  $tmp_dir2 = $_FILES['acta']['tmp_name'];
  $imgSize2 = $_FILES['acta']['size'];
     
  if($imgFile2)
  {
   $upload_dir2 = 'img/pdfinst/'; // upload directory 
   $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions2 = array('pdf'); // valid extensions
   $userpic2 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt2;
   if(in_array($imgExt2, $valid_extensions2))
   {   
    if($imgSize2 < 5000000)
    {
     unlink($upload_dir2.$fila['imagen']);
     move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic2 = $fila['acta_doc']; // old image from database
  } 

   $imgFile3 = $_FILES['cv']['name'];
  $tmp_dir3 = $_FILES['cv']['tmp_name'];
  $imgSize3 = $_FILES['cv']['size'];
     
  if($imgFile3)
  {
   $upload_dir3 = 'img/pdfinst/'; // upload directory 
   $imgExt3 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions3 = array('pdf'); // valid extensions
   $userpic3 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt3;
   if(in_array($imgExt3, $valid_extensions3))
   {   
    if($imgSize3 < 5000000)
    {
     unlink($upload_dir3.$fila['acta_doc']);
     move_uploaded_file($tmp_dir3,$upload_dir3.$userpic3);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic3 = $fila['cv_doc']; // old image from database
  } 

  $imgFile4 = $_FILES['curp2']['name'];
  $tmp_dir4 = $_FILES['curp2']['tmp_name'];
  $imgSize4 = $_FILES['curp2']['size'];
     
  if($imgFile4)
  {
   $upload_dir4 = 'img/pdfinst/'; // upload directory 
   $imgExt4 = strtolower(pathinfo($imgFile4,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions4 = array('pdf'); // valid extensions
   $userpic4 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt4;
   if(in_array($imgExt4, $valid_extensions4))
   {   
    if($imgSize4 < 5000000)
    {
     unlink($upload_dir4.$fila['acta_doc']);
     move_uploaded_file($tmp_dir4,$upload_dir4.$userpic4);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic4 = $fila['curp_doc']; // old image from database
  } 


  $imgFile5 = $_FILES['expl']['name'];
  $tmp_dir5 = $_FILES['expl']['tmp_name'];
  $imgSize5 = $_FILES['expl']['size'];
     
  if($imgFile5)
  {
   $upload_dir5 = 'img/pdfinst/'; // upload directory 
   $imgExt5 = strtolower(pathinfo($imgFile5,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions5 = array('pdf'); // valid extensions
   $userpic5 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt5;
   if(in_array($imgExt5, $valid_extensions5))
   {   
    if($imgSize5 < 5000000)
    {
     unlink($upload_dir5.$fila['acta_doc']);
     move_uploaded_file($tmp_dir5,$upload_dir5.$userpic5);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic5 = $fila['expl_doc']; // old image from database
  } 

  $imgFile6 = $_FILES['domi']['name'];
  $tmp_dir6 = $_FILES['domi']['tmp_name'];
  $imgSize6 = $_FILES['domi']['size'];
     
  if($imgFile6)
  {
   $upload_dir6 = 'img/pdfinst/'; // upload directory 
   $imgExt6 = strtolower(pathinfo($imgFile6,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions6 = array('pdf'); // valid extensions
   $userpic6 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt6;
   if(in_array($imgExt6, $valid_extensions6))
   {   
    if($imgSize6 < 5000000)
    {
     unlink($upload_dir6.$fila['acta_doc']);
     move_uploaded_file($tmp_dir6,$upload_dir6.$userpic6);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic6 = $fila['dom_doc']; // old image from database
  } 

 $imgFile7 = $_FILES['expfg']['name'];
  $tmp_dir7 = $_FILES['expfg']['tmp_name'];
  $imgSize7 = $_FILES['expfg']['size'];
     
  if($imgFile7)
  {
   $upload_dir7 = 'img/pdfinst/'; // upload directory 
   $imgExt7 = strtolower(pathinfo($imgFile7,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions7 = array('jpeg', 'jpg', 'png', 'gif','pdf'); // valid extensions
   $userpic7 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt7;
   if(in_array($imgExt7, $valid_extensions7))
   {   
    if($imgSize7 < 5000000)
    {
     unlink($upload_dir7.$fila['acta_doc']);
     move_uploaded_file($tmp_dir7,$upload_dir7.$userpic7);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic7 = $fila['expfg_doc']; // old image from database
  } 

  $imgFile8 = $_FILES['ine']['name'];
  $tmp_dir8 = $_FILES['ine']['tmp_name'];
  $imgSize8 = $_FILES['ine']['size'];
     
  if($imgFile8)
  {
   $upload_dir8 = 'img/pdfinst/'; // upload directory 
   $imgExt8 = strtolower(pathinfo($imgFile7,PATHINFO_EXTENSION)); // get image extension
   $valid_extensions8 = array('pdf'); // valid extensions
   $userpic8 = $fila['Expediente']."_".rand(1000,1000000).".".$imgExt8;
   if(in_array($imgExt8, $valid_extensions8))
   {   
    if($imgSize8 < 5000000)
    {
     unlink($upload_dir8.$fila['acta_doc']);
     move_uploaded_file($tmp_dir8,$upload_dir8.$userpic8);
    }
    else
    {
     $errMSG = "El archivo debe pesar menos de 5MB.";
    }
   }
   else
   {
    $errMSG = "Inserte un archivo JPG, JPEG, PNG o GIF.";  
   } 
  }
  else
  {
   // if no image selected the old image remain as it is.
   $userpic8 = $fila['ine_doc']; // old image from database
  } 


  
  // if no error occured, continue ....
  if(!isset($errMSG))
  {
   $consulta2 = $pdo->prepare('UPDATE instructores SET imagen=:foto, Curp=:curp, Nombre=:nombre, Calle=:calle, Colonia_barrio=:colonia, Localidad=:localidad, CP=:cp, Seccion_elec=:seccion, Estado_civil=:civil, Sexo=:sexo, F_Nacimiento=:nacimiento, F_Alta=:alta, F_Baja=:baja, Pefil=:perfil, Nivel_academico=:nivel, Especialidad=:especialidad, Fecha_evaluacion=:evaluacion, Puntaje=:puntaje, No_celular=:cel, No_telefono=:tel, Email=:correo, Observaciones=:observaciones,acta_doc=:acta,cv_doc=:cv,curp_doc=:curp2,expl_doc=:expl,dom_doc=:domi,expfg_doc=:expfg,ine_doc=:ine, id_municipios=:id_mun WHERE id=:id');

    $consulta2->bindParam(':foto',$userpic);
    $consulta2->bindParam(':acta',$userpic2);
    $consulta2->bindParam(':cv',$userpic3);
    $consulta2->bindParam(':curp2',$userpic4);
    $consulta2->bindParam(':expl',$userpic5);
    $consulta2->bindParam(':domi',$userpic6);
    $consulta2->bindParam(':expfg',$userpic7);
    $consulta2->bindParam(':ine',$userpic8);
    $consulta2->bindParam(':curp',$curp);
    $consulta2->bindParam(':nombre',$nombre);
    $consulta2->bindParam(':calle',$calle);
    $consulta2->bindParam(':colonia',$colonia);
    $consulta2->bindParam(':localidad',$localidad);
    $consulta2->bindParam(':cp',$cp);
    $consulta2->bindParam(':seccion',$seccion);
    $consulta2->bindParam(':civil',$civil);
    $consulta2->bindParam(':sexo',$sexo);
    $consulta2->bindParam(':nacimiento',$nacimiento);
    $consulta2->bindParam(':alta',$alta);
    $consulta2->bindParam(':baja',$baja);
    $consulta2->bindParam(':perfil',$perfil);
    $consulta2->bindParam(':nivel',$nivel);
    $consulta2->bindParam(':especialidad',$especialidad);
    $consulta2->bindParam(':evaluacion',$evaluacion);
    $consulta2->bindParam(':puntaje',$puntaje);
    $consulta2->bindParam(':cel',$cel);
    $consulta2->bindParam(':tel',$tel);
    $consulta2->bindParam(':correo',$correo);
    $consulta2->bindParam(':observaciones',$observaciones);
    $consulta2->bindParam(':id_mun',$id_mun);
    $consulta2->bindParam(':id',$id);
    
    if($consulta2->execute()){
        header("refresh:0");
    }else{
        echo '
            <a href="instructores_vistaPLANE.php" style="text-decoration: none">
                <div class="container-fluid text-center alert alert-danger" style="margin-bottom: 0px">
                    <h4>Error: No se Actualizaron los datos</h4>
                </div>
            </a>
        ';
    }

  }    
 }
?>

<html lang="es">
<head>
    <meta charset="UTF-8">
        <style type="text/css">
        #formulario{
            box-shadow: 5px 5px 20px #9f9f9f;
            background-color: #e7e7e7;
        }
        a{
            color: #186d41 !important;
        }
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #9f282f !important;
        }
    </style>
</head>
<header>
    <?php require_once "partes/nav_PLANE.php"; ?>
</header>
<body>
    <div id="inicio" class="page-header text-center">
        <h4><strong>ACTUALIZAR - INSTRUCTORES</strong></h4>
    </div>

    <div class="container">
        <form method="post" enctype="multipart/form-data">

        <input type="hidden" name="id" value="<?php echo $fila['id'] ?>">
       
        <div id="formulario" style="font-size:14px;">
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11"><h6>Datos Personales</h6></div>
                <div class="row" id="titulo"><strong>Datos Generales</strong></div>
                <div class="row">
                    <label  class="col-sm-2">Expediente</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="expediente" min="0" value="<?php echo $fila['Expediente'] ?>" disabled>
                    </div>
                </div>


                <div class="row">
                    <label class="col-sm-2">Nombre</label>
                    <div class="col-sm-10">
                        <input class="form-control col-sm-12 input-sm" type="text" name="nombre" required="" value="<?php echo $fila['Nombre'] ?>">
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">CURP</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="curp" required="" value="<?php echo $fila['Curp'] ?>">
                    </div>

                    <label class="col-sm-2">Foto</label>
                    <div class="col-sm-4 text-center">
                        <img src="img/inst/<?php echo $fila['imagen'] ?>" width="200px"><br><br>
                        <input class="form-control input-sm" type="file" name="user_image" accept="image/*">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Dirección</strong></div>
                <div class="row">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="calle" required="" value="<?php echo $fila['Calle'] ?>">
                    </div>

                    <label class="col-sm-2">Localidad</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="localidad" required="" value="<?php echo $fila['Localidad'] ?>">
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">CP</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="cp" required="" min="0" value="<?php echo $fila['CP'] ?>">
                    </div>

                    <label class="col-sm-2">Colonia Barrio</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="text" name="colonia" required="" value="<?php echo $fila['Colonia_barrio'] ?>">
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">

                        <select class="form-control input-sm" name="id_mun" required="">
                            <option value="<?php echo $fila['id_municipios'] ?>"><?php echo $fila['nombre_mun'] ?></option>
                                <?php foreach ($rows as $row) {
                                    echo '<option value="'.$row['folio_mun'].'">'.$row['nombre_mun'].'</option>';
                                } ?>
                        </select>

                    </div>

                    <label class="col-sm-2">Sección Elec</label>
                    <div class="col-sm-4">
                        <input class="form-control col-sm-12 input-sm" type="number" name="seccion" required="" min="0" value="<?php echo $fila['Seccion_elec'] ?>">
                    </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Informción adicional</strong></div>
                <div class="row">
                        <label class="col-sm-2">Estado Civil</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="civil" required="" value="<?php echo $fila['Estado_civil'] ?>">
                        </div>
                        <label class="col-sm-2">F Nacimiento</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="nacimiento" required="" value="<?php echo $fila['F_Nacimiento'] ?>">
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">No Teléfono</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="tel" required="" value="<?php echo $fila['No_telefono'] ?>">
                        </div>

                        <label class="col-sm-2">Sexo</label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" type="text" name="sexo" required="" id="sel1">
                                <option value="<?php echo $fila['Sexo'] ?>"><?php echo $fila['Sexo'] ?></option>
                                <option>--- Seleccionar sexo ---</option>
                                <option value="FEMENINO">FEMENINO</option>
                                <option value="MASCULINO">MASCULINO</option>
                            </select>
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">No Celular</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="texr" name="cel" required="" value="<?php echo $fila['No_celular'] ?>">
                        </div>

                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="email" name="correo" required="" value="<?php echo $fila['Email'] ?>">
                        </div>
                </div>
            </div>

    
            <div class="container-fluid">
                <div class="row" id="titulo" style="background-color: #560f11"><h6>Perfil Académico</h6></div>
                <div class="row" id="titulo"><strong>Formación</strong></div>
                <div class="row">
                        <label class="col-sm-2">Perfil</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="perfil" required="" value="<?php echo $fila['Pefil'] ?>">
                        </div>

                        <label class="col-sm-2">Nivel Académico</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="text" name="nivel" required="" value="<?php echo $fila['Nivel_academico'] ?>">
                        </div>
                </div>
            </div>

            
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Docencia</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Especialidad</label>
                        <div class="col-sm-10">
                            <textarea class="form-control input-sm" type="text" name="especialidad" required="" rows="3"><?php echo $fila['Especialidad'] ?></textarea> 
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">Fecha Evaluación</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="evaluacion" value="<?php echo $fila['Fecha_evaluacion'] ?>">
                        </div>

                        <label class="col-sm-2">Puntaje</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="number" name="puntaje" min="0" value="<?php echo $fila['Puntaje'] ?>">
                        </div>
                </div>

                <div class="row">
                        <label class="col-sm-2">Fecha Alta</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="alta" value="<?php echo $fila['F_Alta'] ?>">
                        </div>

                        <label class="col-sm-2">Fecha Baja</label>
                        <div class="col-sm-4">
                            <input class="form-control col-sm-12 input-sm" type="date" name="baja" value="<?php echo $fila['F_Baja'] ?>">
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Observaciones</label>
                        <div class="col-sm-10">
                            <textarea class="form-control input-sm" type="text" name="observaciones" rows="3"><?php echo $fila['Observaciones'] ?></textarea>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Calidad</label>
                        <div class="col-sm-10">
                           <select class="form-control input-sm" type="text" name="calidad" id="sel1" disabled>
                                <option value="<?php echo $fila['calidad'] ?>"><?php echo $fila['calidad'] ?></option>
                            </select>
                        </div>
                </div>
            </div>

             <div class="container-fluid">
                <div class="row" id="titulo"><strong>Expediente Digital</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Acta de Nacimiento</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="acta" accept="application/pdf">
                        <a href="img/pdfinst/<?php echo $fila['acta_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Acta de nacimiento</a>
                    </div>

                    <label class="col-sm-2">Curriculum Vitae </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="cv" accept="application/pdf">
                        <a href="img/pdfinst/<?php echo $fila['cv_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Curriculum Vitae</a>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Curp</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="curp2" accept="application/pdf">
                        <a href="img/pdfinst/<?php echo $fila['curp_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Curp</a>
                    </div>

                    <label class="col-sm-2">Experiencia Laboral </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="expl" accept="" >
                        <a href="img/pdfinst/<?php echo $fila['expl_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Experiencia Laboral</a>
                    </div>
                </div>

                   <div class="row text-left">
                    <label class="col-sm-2">Domicilio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="domi" accept="application/pdf">
                        <a href="img/pdfinst/<?php echo $fila['dom_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Domicilio</a>
                    </div>

                    <label class="col-sm-2">Experiencia Frente Grupo </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="expfg" accept="" >
                        <a href="img/pdfinst/<?php echo $fila['expfg_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> Experiencia Frente Grupo</a>
                    </div>
                </div>

                   <div class="row text-left"> 
                    <label class="col-sm-2">INE</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="file" name="ine" accept="application/pdf">
                        <a href="img/pdfinst/<?php echo $fila['ine_doc'] ?>" target="_blank"><i class="fa fa-eye"> </i> INE</a>
                    </div>
                </div>

            </div>


        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" name="btn_save_updates" value="Guardar">
            <a class="btn btn-danger" href="instructores_vistaPLANE.php" style="color: white !important">Cancelar</a>
            </div> 
        </form>
    </div>
    <br>
</body>
</html>