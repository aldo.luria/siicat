<?php 
require_once "php/connect.php";

    if(isset($_POST['curso']) && isset($_POST['duracion']) && isset($_POST['calle']) && isset($_POST['colonia']) && isset($_POST['finicio']) && isset($_POST['ffin']) && isset($_POST['linicio']) && isset($_POST['lfin']) && isset($_POST['minicio']) && isset($_POST['mfin']) && isset($_POST['miinicio']) && isset($_POST['mifin']) && isset($_POST['jinicio']) && isset($_POST['jfin']) && isset($_POST['vinicio']) && isset($_POST['vfin']) && isset($_POST['sinicio']) && isset($_POST['sfin']) && isset($_POST['dinicio']) && isset($_POST['dfin']) && isset($_POST['status']) && isset($_POST['id_inst']) && isset($_POST['id_plan']) && isset($_POST['id_mun_c'])){
    }

	$curso=$_POST['curso'];
	$duracion=$_POST['duracion'];
	$calle=$_POST['calle'];                
	$colonia=$_POST['colonia']; 
	$finicio=$_POST['finicio'];
	$ffin=$_POST['ffin'];
	$linicio=$_POST['linicio'];
	$lfin=$_POST['lfin'];
	$minicio=$_POST['minicio'];
	$mfin=$_POST['mfin'];
	$miinicio=$_POST['miinicio'];
	$mifin=$_POST['mifin'];
	$jinicio=$_POST['jinicio'];
	$jfin=$_POST['jfin'];
	$vinicio=$_POST['vinicio'];
	$vfin=$_POST['vfin'];
	$sinicio=$_POST['sinicio'];
	$sfin=$_POST['sfin'];
	$dinicio=$_POST['dinicio'];
	$dfin=$_POST['dfin'];
	$status='Sugerido';
	$id_inst=$_POST['id_inst'];
	$id_plan=$_POST['id_plan'];
	$id_mun_c=$_POST['id_mun_c'];
	$observa=$_POST['observa'];

	$consulta=$pdo->prepare("INSERT INTO cursos2018_extension(curso,duracion,calle_c,colonia_c,fecha_inicio,fecha_fin,h_lunes_ini,h_lunes_fin,h_martes_ini,h_martes_fin,h_miercoles_ini,h_miercoles_fin,h_jueves_ini,h_jueves_fin,h_viernes_ini,h_viernes_fin,h_sabado_ini,h_sabado_fin,h_domingo_ini,h_domingo_fin,status,id_instructor,id_plantel,id_mun_c,observaciones_c) VALUES(:curso, :duracion, :calle, :colonia, :finicio, :ffin, :linicio, :lfin, :minicio, :mfin, :miinicio, :mifin, :jinicio, :jfin, :vinicio, :vfin, :sinicio, :sfin, :dinicio, :dfin, :status, :id_inst, :id_plan, :id_mun_c, :observa) ");

	$consulta->bindParam(':curso',$curso);
	$consulta->bindParam(':duracion',$duracion);
	$consulta->bindParam(':calle',$calle);
	$consulta->bindParam(':colonia',$colonia);
	$consulta->bindParam(':finicio',$finicio);
	$consulta->bindParam(':ffin',$ffin);
	$consulta->bindParam(':linicio',$linicio);
	$consulta->bindParam(':lfin',$lfin);
	$consulta->bindParam(':minicio',$minicio);
	$consulta->bindParam(':mfin',$mfin);
	$consulta->bindParam(':miinicio',$miinicio);
	$consulta->bindParam(':mifin',$mifin);
	$consulta->bindParam(':jinicio',$jinicio);
	$consulta->bindParam(':jfin',$jfin);
	$consulta->bindParam(':vinicio',$vinicio);
	$consulta->bindParam(':vfin',$vfin);
	$consulta->bindParam(':sinicio',$sinicio);
	$consulta->bindParam(':sfin',$sfin);
	$consulta->bindParam(':dinicio',$dinicio);
	$consulta->bindParam(':dfin',$dfin);
	$consulta->bindParam(':status',$status);
	$consulta->bindParam(':id_inst',$id_inst);
	$consulta->bindParam(':id_plan',$id_plan);
	$consulta->bindParam(':id_mun_c',$id_mun_c);
	$consulta->bindParam(':observa',$observa);

	if($consulta->execute()){
		header('Location: cursoextension.php');
	}else{
		echo "Error no se pudo almacenar la información";
	}


