<!DOCTYPE html>
<html>
<head>
</head>

<header>
	<nav id="inicio" class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="menu_COO.php"><img src="img/Tlaxcala-ICATLAX.png" width="200px" style="padding-left: 30px" title="Menú - Inicio"></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
			<a class="nav-link" href="menu_COO.php" style="color: #186d41 !important">Menú <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #186d41 !important">Aprobación de Cursos
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="cursoregular_vistaCOO.php">Curso Regular</a>
	          <a class="dropdown-item" href="cursoebc_vistaCOO.php">Curso EBC</a>
	          <a class="dropdown-item" href="cursoextension_vistaCOO.php">Curso de Extensión</a>
	          <a class="dropdown-item" href="cursocae_vistaCOO.php">Curso CAE</a>
	          <a class="dropdown-item" href="cursoroco_vistaCOO.php">Examen ROCO</a>
	        </div>
	      </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #186d41 !important">Cursos Aprobados
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="cursoregular_vistaCOO_A.php">Curso Regular</a>
	          <a class="dropdown-item" href="cursoebc_vistaCOO_A.php">Curso EBC</a>
	          <a class="dropdown-item" href="cursoextension_vistaCOO_A.php">Curso de Extensión</a>
	          <a class="dropdown-item" href="cursocae_vistaCOO_A.php">Curso CAE</a>
	          <a class="dropdown-item" href="cursoroco_vistaCOO_A.php">Examen ROCO</a>
	        </div>
	      </li>	      
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li style="padding-right: 20px; font-size: 17px;"><span class="fa fa-user-circle-o"></span>
	      	<?php
      			echo $_SESSION['nombre'];
	      	?> 
	  	  </li> 
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li style="padding-right: 30px; font-size: 15px;"><a style="color: #186d41 !important" href="logout.php?tk=<?php echo $_SESSION['token']; ?>"><span class="fa fa-sign-out" style="font-size: 17px"></span> Cerrar Sesión</a></li>
	    </ul>
	  </div>
	</nav>
<br>
</header>
</html>