<?php 
require_once "php/connect.php";

    if(isset($_POST['apellidop']) && isset($_POST['apellidom']) && isset($_POST['nombre']) && isset($_POST['foto']) && isset($_POST['curp']) && isset($_POST['domicilio']) && isset($_POST['localidad']) && isset($_POST['cp']) && isset($_POST['municipio']) && isset($_POST['estado']) && isset($_POST['civil']) && isset($_POST['discapacidad']) && isset($_POST['tel']) && isset($_POST['sexo']) && isset($_POST['edad']) && isset($_POST['correo']) && isset($_POST['imgcurp']) && isset($_POST['imgacta']) && isset($_POST['imgdomi']) && isset($_POST['imgestu'])){
    }

	$apellidop=$_POST['apellidop'];
	//$foto=$_POST['foto'];
	$apellidom=$_POST['apellidom'];
	$nombre=$_POST['nombre'];
	$curp=$_POST['curp'];
	$domicilio=$_POST['domicilio'];
	$localidad=$_POST['localidad'];
	$cp=$_POST['cp'];
	$municipio=$_POST['municipio'];
	$estado=$_POST['estado'];
	$civil=$_POST['civil'];
	$discapacidad=$_POST['discapacidad'];
	$tel=$_POST['tel'];
	$sexo=$_POST['sexo'];
	$edad=$_POST['edad'];
	$correo=$_POST['correo'];

// Recibo los datos de la foto
$nombre_img = $nombre."_".$_FILES['foto']['name'];
$tipo = $_FILES['foto']['type'];
$tamano = $_FILES['foto']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_img == !NULL) && ($_FILES['foto']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["foto"]["type"] == "image/gif")
   || ($_FILES["foto"]["type"] == "image/jpeg")
   || ($_FILES["foto"]["type"] == "image/jpg")
   || ($_FILES["foto"]["type"] == "image/png")
   || ($_FILES["foto"]["type"] == "image/pdf"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/alumno/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['foto']['tmp_name'],$directorio.$nombre_img);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_img == !NULL) echo "La foto es demasiado grande "; 
}

// Recibo los datos de la foto de la curp
$nombre_curp = $nombre."_".$_FILES['imgcurp']['name'];
$tipo = $_FILES['imgcurp']['type'];
$tamano = $_FILES['imgcurp']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_curp == !NULL) && ($_FILES['imgcurp']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["imgcurp"]["type"] == "image/gif")
   || ($_FILES["imgcurp"]["type"] == "image/jpeg")
   || ($_FILES["imgcurp"]["type"] == "image/jpg")
   || ($_FILES["imgcurp"]["type"] == "image/png")
   || ($_FILES["imgcurp"]["type"] == "image/pdf"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/alumno/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['imgcurp']['tmp_name'],$directorio.$nombre_curp);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_curp == !NULL) echo "La foto es demasiado grande "; 
}

// Recibo los datos de la foto del acta de nacimiento
$nombre_acta = $nombre."_".$_FILES['imgacta']['name'];
$tipo = $_FILES['imgacta']['type'];
$tamano = $_FILES['imgacta']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_acta == !NULL) && ($_FILES['imgacta']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["imgacta"]["type"] == "image/gif")
   || ($_FILES["imgacta"]["type"] == "image/jpeg")
   || ($_FILES["imgacta"]["type"] == "image/jpg")
   || ($_FILES["imgacta"]["type"] == "image/png")
   || ($_FILES["imgacta"]["type"] == "image/pdf"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/alumno/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['imgacta']['tmp_name'],$directorio.$nombre_acta);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_acta == !NULL) echo "La foto es demasiado grande "; 
}

// Recibo los datos de la foto del comprobante de domicilio
$nombre_domi = $nombre."_".$_FILES['imgdomi']['name'];
$tipo = $_FILES['imgdomi']['type'];
$tamano = $_FILES['imgdomi']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_domi == !NULL) && ($_FILES['imgdomi']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["imgdomi"]["type"] == "image/gif")
   || ($_FILES["imgdomi"]["type"] == "image/jpeg")
   || ($_FILES["imgdomi"]["type"] == "image/jpg")
   || ($_FILES["imgdomi"]["type"] == "image/png")
   || ($_FILES["imgdomi"]["type"] == "image/pdf"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/alumno/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['imgdomi']['tmp_name'],$directorio.$nombre_domi);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_domi == !NULL) echo "La foto es demasiado grande "; 
}

// Recibo los datos de la foto del certificado de estudios
$nombre_estu = $nombre."_".$_FILES['imgestu']['name'];
$tipo = $_FILES['imgestu']['type'];
$tamano = $_FILES['imgestu']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_estu == !NULL) && ($_FILES['imgestu']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["imgestu"]["type"] == "image/gif")
   || ($_FILES["imgestu"]["type"] == "image/jpeg")
   || ($_FILES["imgestu"]["type"] == "image/jpg")
   || ($_FILES["imgestu"]["type"] == "image/png")
   || ($_FILES["imgestu"]["type"] == "image/pdf"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/alumno/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['imgestu']['tmp_name'],$directorio.$nombre_estu);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_estu == !NULL) echo "La foto es demasiado grande "; 
}

	$consulta=$pdo->prepare("INSERT INTO alta_alumnos(apellido_p,apellido_m,nombre_a,sexo,curp,edad,num_tel,domicilio,localidad,CP,municipio,estado,estado_civil,discapacidad,imagen_actan,imagen_curp,imagen_compdom,imagen_compest,foto_alum,email) VALUES(:apellidop, :apellidom, :nombre, :sexo, :curp, :edad, :tel, :domicilio, :localidad, :cp, :municipio, :estado, :civil, :discapacidad, :imgacta, :imgcurp, :imgdomi, :imgestu, :foto, :correo) ");

	$consulta->bindParam(':apellidop',$apellidop);
	$consulta->bindParam(':apellidom',$apellidom);
	$consulta->bindParam(':nombre',$nombre);
	$consulta->bindParam(':sexo',$sexo);
	$consulta->bindParam(':curp',$curp);
	$consulta->bindParam(':edad',$edad);
	$consulta->bindParam(':tel',$tel);
	$consulta->bindParam(':domicilio',$domicilio);
	$consulta->bindParam(':localidad',$localidad);
	$consulta->bindParam(':cp',$cp);
	$consulta->bindParam(':municipio',$municipio);
	$consulta->bindParam(':estado',$estado);
	$consulta->bindParam(':civil',$civil);
	$consulta->bindParam(':discapacidad',$discapacidad);
	$consulta->bindParam(':imgacta',$nombre_acta);
	$consulta->bindParam(':imgcurp',$nombre_curp);
	$consulta->bindParam(':imgdomi',$nombre_domi);
	$consulta->bindParam(':imgestu',$nombre_estu);
	$consulta->bindParam(':foto',$nombre_img);
	$consulta->bindParam(':correo',$correo);

	if($consulta->execute()){
		header('Location: alta_alumnos.php');
	}else{
		echo "Error no se pudo almacenar la información";
	}


