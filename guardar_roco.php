<?php
 require_once "php/connect.php";
	
    if(isset($_POST['idcampo'])  && isset($_POST['idmodulo'])  && isset($_POST['idespecialidad']) && isset($_POST['fechainicio']) && isset($_POST['fechafin']) && isset($_POST['calle']) && isset($_POST['colonia']) && isset($_POST['lunesinicio']) && isset($_POST['lunesfin']) && isset($_POST['martesinicio']) && isset($_POST['martesfin']) && isset($_POST['miercolesinicio']) && isset($_POST['miercolesfin']) && isset($_POST['juevesinicio']) && isset($_POST['juevesfin']) && isset($_POST['viernesinicio']) && isset($_POST['viernesfin']) && isset($_POST['sabadoinicio']) && isset($_POST['sabadofin']) && isset($_POST['domingoinicio']) && isset($_POST['domingofin']) && isset($_POST['status']) && isset($_POST['id_inst']) && isset($_POST['id_plan']) && isset($_POST['id_mun_c'])){
	}
 
	$idcampo=$_POST['idcampo'];  
	$idmodulo=$_POST['idmodulo'];   
	$idespecialidad=$_POST['idespecialidad'];  
	$calle=$_POST['calle'];                
	$colonia=$_POST['colonia'];              
	$fechainicio=$_POST['fechainicio'];
	$fechafin=$_POST['fechafin'];
	$lunesinicio=$_POST['lunesinicio'];
	$lunesfin=$_POST['lunesfin'];
	$martesinicio=$_POST['martesinicio'];
	$martesfin=$_POST['martesfin'];
	$miercolesinicio=$_POST['miercolesinicio'];
	$miercolesfin=$_POST['miercolesfin'];
	$juevesinicio=$_POST['juevesinicio'];
	$juevesfin=$_POST['juevesfin'];
	$viernesinicio=$_POST['viernesinicio'];
	$viernesfin=$_POST['viernesfin'];
	$sabadoinicio=$_POST['sabadoinicio'];
	$sabadofin=$_POST['sabadofin'];
	$domingoinicio=$_POST['domingoinicio'];
	$domingofin=$_POST['domingofin'];
	$status='Sugerido';
	$id_inst=$_POST['id_inst'];
	$id_plan=$_POST['id_plan'];
	$id_mun_c=$_POST['id_mun_c'];	
	$observa=$_POST['observa'];
	

	$consulta=$pdo->prepare("INSERT INTO cursos2018_roco(id_campo_c,id_modulo_c,id_especialidad_c,calle_c,colonia_c,fecha_inicio,fecha_fin,h_lunes_ini,h_lunes_fin,h_martes_ini,h_martes_fin,h_miercoles_ini,h_miercoles_fin,h_jueves_ini,h_jueves_fin,h_viernes_ini,h_viernes_fin,h_sabado_ini,h_sabado_fin,h_domingo_ini,h_domingo_fin,status,id_instructor,id_plantel,id_mun_c,observaciones_c) VALUES(:idcampo, :idmodulo,:idespecialidad, :calle, :colonia, :fechainicio, :fechafin, :lunesinicio, :lunesfin, :martesinicio, :martesfin, :miercolesinicio, :miercolesfin, :juevesinicio, :juevesfin, :viernesinicio, :viernesfin, :sabadoinicio, :sabadofin, :domingoinicio, :domingofin, :status, :id_inst, :id_plan, :id_mun_c, :observa) ");

	$consulta->bindParam(':idcampo',$idcampo);
	$consulta->bindParam(':idmodulo',$idmodulo);
	$consulta->bindParam(':idespecialidad',$idespecialidad);
	$consulta->bindParam(':calle',$calle);
	$consulta->bindParam(':colonia',$colonia);
	$consulta->bindParam(':fechainicio',$fechainicio);
	$consulta->bindParam(':fechafin',$fechafin);
	$consulta->bindParam(':lunesinicio',$lunesinicio);
	$consulta->bindParam(':lunesfin',$lunesfin);
	$consulta->bindParam(':martesinicio',$martesinicio);
	$consulta->bindParam(':martesfin',$martesfin);
	$consulta->bindParam(':miercolesinicio',$miercolesinicio);
	$consulta->bindParam(':miercolesfin',$miercolesfin);
	$consulta->bindParam(':juevesinicio',$juevesinicio);
	$consulta->bindParam(':juevesfin',$juevesfin);
	$consulta->bindParam(':viernesinicio',$viernesinicio);
	$consulta->bindParam(':viernesfin',$viernesfin);
	$consulta->bindParam(':sabadoinicio',$sabadoinicio);
	$consulta->bindParam(':sabadofin',$sabadofin);
	$consulta->bindParam(':domingoinicio',$domingoinicio);
	$consulta->bindParam(':domingofin',$domingofin);
	$consulta->bindParam(':status',$status);
	$consulta->bindParam(':id_inst',$id_inst);
	$consulta->bindParam(':id_plan',$id_plan);
	$consulta->bindParam(':id_mun_c',$id_mun_c);
	$consulta->bindParam(':observa',$observa);

	if($consulta->execute()){
		header('Location: cursoroco.php');
	}else{
		echo "Error no se pudo almacenar la información";
	}