<?php 
	$username = $_POST['username'];
	$clave = md5($_POST['clave']);

	$consulta=$pdo->prepare("SELECT * FROM usuarios WHERE username=:username AND pass=:clave");
	$consulta->bindParam('username',$username);
	$consulta->bindParam('clave',$clave);
	$consulta->execute();
	if ($consulta->rowCount()>=1) {
		session_start();
		$fila=$consulta->fetch();
		$_SESSION['nombre']=$fila['Nombre_usr'];
		$_SESSION['username']=$fila['username'];
		$_SESSION['tipo']=$fila['tipo'];
		$_SESSION['token']=md5(uniqid(mt_rand(),true));
		if ($_SESSION['tipo'] == "COO") {
			header("Location: menu_COO.php");
		}elseif ($_SESSION['tipo'] == "DTA") {
			header("Location: menu_DTA.php");
		}elseif ($_SESSION['tipo'] == "PLANEA") {
			header("Location: menu_PLANE.php");
		}elseif ($_SESSION['tipo'] == "PLANT") {
			header("Location: menu_PLA.php");
		}else{
			echo '
				<div class="container-fluid text-center">
			 		<div class="row">
	            		<div class="col-md text-center alert alert-danger" style="margin-bottom: 0px">
	               			<h4>Error</h4>
	               			<h6>Tipo de cuenta no especificado.</h6>
	            		</div>
	            	</div>    
	            </div>  
			';
			session_destroy();
		}
	}else{
		echo '
			<div class="container-fluid text-center">
		 		<div class="row">
            		<div class="col-md text-center alert alert-danger" style="margin-bottom: 0px">
               			<h4>Error</h4>
               			<h6>Verifique sus datos.</h6>
            		</div>
            	</div>    
            </div>   
			';
	}