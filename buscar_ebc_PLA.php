<?php 
    include "procesossesiones/seguridad.php";
    include "php/connect.php";
    include "phpbus/paginadorbuscaebcapropla.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Curso EBC</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/popper.min.js"></script> 
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <style type="text/css">
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #7a1315 !important;
            color: white !important;
        }
        ul.pagination li{
            font-size: 15px;
        }
        table{
            box-shadow: 5px 5px 20px #9f9f9f;
        }
        ul.pagination li{
            box-shadow: 5px 5px 10px #9f9f9f;
        }
    </style>
</head>
<?php  
        IF($_SESSION['tipo'] == "PLANT"){
?>
<header>    
    <?php require_once "partes/nav_PLA.php"; ?>
</header>
<body>
	<?php
        $busqueda = $_REQUEST['busqueda'];
        if(empty($busqueda))
        {
            header("location: cursoebc_vistaPLA.php");
        }

        $busqueda = $_GET['busqueda'];

        $sql = $pdo->prepare("SELECT EBC.*,P.nombre,I.*,O.nombreModulo,E.especialidad,M.nombre_mun FROM cursos2018_ebc AS EBC LEFT JOIN plantel AS P ON EBC.id_plantel = P.id LEFT JOIN instructores AS I ON EBC.id_instructor = I.id LEFT JOIN oferta AS O ON EBC.id_modulo_c = O.id LEFT JOIN modulosespecialidades AS E ON O.idEspecialidad = E.id_especialidad LEFT JOIN municipios AS M ON EBC.id_mun_c = M.folio_mun WHERE EBC.status = 'Aprobado' AND P.nombre = '".$usuario."' AND (folio LIKE '%".$busqueda."%' OR I.Expediente LIKE '%".$busqueda."%' OR I.Nombre LIKE '%".$busqueda."%' OR P.nombre LIKE '%".$busqueda."%' OR E.especialidad LIKE '%".$busqueda."%' OR O.nombreModulo LIKE '%".$busqueda."%' OR M.nombre_mun LIKE '%".$busqueda."%' OR EBC.fecha_inicio LIKE '%".$busqueda."%' OR EBC.status LIKE '%".$busqueda."%') ORDER BY folio DESC LIMIT $inicio, $regpagina");
        $sql ->execute(array('busqueda' =>$busqueda));
        $resultado = $sql->fetchAll();
        
    ?>


	<div id="tabla">

			<div class="page-header">
                <h4><strong>CONSULTA - CURSOS2018_EBC</strong></h4>
            </div>

            <form action="buscar_ebc_PLA.php" method="GET" class="form_sear">
                        <div class="container-fluid">
                        <div class="row">
                            <input type="text" class="form-control col-md justify-content-start" name="busqueda" id="busqueda" placeholder="Búsqueda rápida" value="<?php echo $busqueda; ?>">
                            <button type="submit" class="btn btn-default  justify-content-start"><i class="fa fa-search"></i></button>
                            <div class="col-md"></div>
        	               <!--<a href="#ventana1" class="btn btn-success btn-lage  justify-content-end" data-toggle="modal">Nuevo <i class="fa fa-plus-circle"></i></a>-->
                        </div>
                        </div>
            </form>
            <br>

            <div class="text-right">

            	<div class="modal fade" id="ventana1">
                    <div class="modal-dialog modal-lg">   
                        <div class="modal-content">
                                <div class="modal-header">
                                    <button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <!-- contenido-->
                                <div class="modal-body">

                                    <?php require_once "formularios/registro_cebc.php"; ?>
                                        
                                </div>

                                <div class="modal-footer">
                                </div>
                        </div>
                    </div>
                </div>

            </div>

			<table class="table table-bordered table-striped table-hover table-condensed responsive">
				<thead>
                   <tr>     
                        <th class="text-center">Ver</th>
                        <th class="text-center">Folio</th>
                        <th class="text-center">Expediente</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Plantel</th>
                        <th class="text-center">Especialidad</th>
                        <th class="text-center">Módulo</th>
                        <th class="text-center">Municipio</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Hoja Asignacion</th>
                    </tr>


				</thead>
				<tbody>
                            <?php
                   if($totalregistros>=1):
                    foreach ($resultado as $row):
                        ?>    
                        
                   <tr>
                        <td class='text-center'><?php echo "<a href='datosebc_PLA.php?folio=".$row['folio']."'><i class='fa fa-eye' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                        <td class='text-center'><strong><?php echo $row['folio']  ?></strong></td>
                        <td class='text-center'><strong><?php echo $row['Expediente']  ?></strong></td>
                        <td><?php echo $row['Nombre'] ?></td>
                        <td><?php echo $row['nombre'] ?></td>
                        <td><?php echo $row['especialidad'] ?></td>
                        <td><?php echo $row['nombreModulo'] ?></td>
                        <td class='text-nowrap'><?php echo $row['nombre_mun'] ?></td>
                        <td class='text-center text-nowrap'><?php echo $row['fecha_inicio'] ?></td>
                        <td class="text-center"><?php echo $row['status'] ?></td>
                         <td class='text-center'><?php echo "<a href='reporte_ebc.php?folio=".$row['folio']."' target='_blank'><i class='fa fa-print' style='font-size:20px; color: #186d41;'></i></a>"?></td>
                    </tr>
                    
                    <?php    
                    endforeach; 
                        else:
                    ?>    

                        <tr>
                            <td colspan='11' class="text-center"><strong>No hay cursos</strong></td>
                        </tr>
                        
                        <?php
                        endif;
                        ?>
                            
                </tbody>
                </table>

                <?php
                $primera = ($paginaa - 5) > 1 ? $paginaa - 5 : 1;
                $ultima = ($paginaa + 5) < $numeropaginas ? $paginaa + 5 : $numeropaginas;

                if($numeropaginas>=1): ?>
                    <br>
                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination justify-content-center">
                            <li class="page-item active">
                                <div class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">
                                    Página <?php echo $paginaa; ?> de <?php echo $numeropaginas; ?>
                                </div>
                            </li>
                            <?php if($paginaa==1): ?>
                                <li class="disabled page-item" title="Prímera">
                                    <a class="page-link" href="#"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="disabled page-item" title="Anterior">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Prímera">
                                    <a class="page-link" href="?paginaa=1 &busqueda=<?php echo $busqueda;?>"><i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="page-item" title="Anterior">
                                    <a class="page-link" href="?paginaa=<?php echo $paginaa-1; ?>&busqueda=<?php echo $busqueda; ?>" aria-label="Previous">
                                        <span aria-hidden="true"><i class="fa fa-caret-left"></i></span>
                                    </a>
                                </li>
                            <?php endif;

                                for ($i = $primera; $i <= $ultima; $i++){
                                    if ($paginaa == $i)
                                        echo '<li class="active page-item">
                                            <a class="page-link" style="background-color:#ddd; color:#7a1315 !important; border: 1px solid #ddd;">'.$paginaa.'</a>
                                        </li>';
                                    else
                                        echo '<li class="page-item">
                                            <a class="page-link" href="?paginaa='.$i.'&busqueda='.$busqueda.'">'.$i.'</a>
                                            </li>';
                                }

                            if($paginaa==$numeropaginas):?>
                                <li class="disabled page-item" title="Siguiente">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="disabled page-item" title="Última">
                                    <a class="page-link" href="#"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php else: ?>
                                <li class="page-item" title="Siguiente">
                                    <a class="page-link" href="?paginaa=<?php echo $paginaa+1;?>&busqueda=<?php echo $busqueda;?>" aria-label="Next">
                                        <span aria-hidden="true"><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </li>
                                <li class="page-item" title="Última">
                                    <a class="page-link" href="?paginaa=<?php echo $numeropaginas;?>&busqueda=<?php echo $busqueda;?>"><i class="fa fa-step-forward"></i></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                
            
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html"; ?>
</footer>
</html>