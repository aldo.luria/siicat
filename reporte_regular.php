<?php
	include 'plantilla.php';
	require 'php/connect.php';

IF($_SESSION['tipo'] == "DTA" || $_SESSION['tipo'] == "COO" || $_SESSION['tipo'] == "PLANEA" || $_SESSION['tipo'] == "PLANT"){
	
	$consulta=$pdo->prepare("SELECT * FROM cursos2018_regular");
	$consulta->execute();
	
	$pdf = new PDF('P','mm','Letter');
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Arial','B',12);
	
	
	$pdf->SetFont('Arial','',12);
	
    //Recuperar datos
    if(isset($_GET['folio'])){
        $folio=$_GET['folio'];
        $consulta=$pdo->prepare("SELECT CR.*,P.nombre,I.*,O.nombreModulo,E.especialidad,M.nombre_mun FROM cursos2018_regular AS CR LEFT JOIN plantel AS P ON CR.id_plantel = P.id LEFT JOIN instructores AS I ON CR.id_instructor = I.id LEFT JOIN oferta AS O ON CR.id_modulo_c= O.id LEFT JOIN modulosespecialidades AS E ON CR.id_especialidad_c = E.id_especialidad LEFT JOIN municipios AS M ON CR.id_mun_c = M.folio_mun WHERE folio=:folio");
        $consulta->bindParam(":folio",$folio);
        $consulta->execute();
        if($consulta->rowCount()>=1){
            $fila=$consulta->fetch();

        
       
   $pdf->Cell(130,70, $fila['Expediente'],0,2,'C');
  $pdf->Cell(130,-50,utf8_decode($fila['Curp']),0,2,'C');
  $pdf->Cell(160,72,utf8_decode($fila['Nombre']),0,2,'C');
  $pdf->Cell(130,-54,utf8_decode($fila['nombre']),0,2,'C');
    $pdf->Cell(140,75,utf8_decode($fila['especialidad']),0,2,'C');
    $pdf->Cell(190,-51,utf8_decode($fila['nombreModulo']),0,2,'C');
  $pdf->Cell(130,75,utf8_decode($fila['nombre_mun']),0,2,'C');
  $pdf->Cell(135,-59,utf8_decode($fila['fecha_inicio']),0,2,'C');
  $pdf->Cell(250,59,utf8_decode($fila['fecha_fin']),0,2,'C');

  
    $pdf->Cell(83,-4,utf8_decode($fila['h_lunes_ini']),0,2,'C');
    $pdf->Cell(83,27,utf8_decode($fila['h_lunes_fin']),0,2,'C');
    $pdf->Cell(123,-50,utf8_decode($fila['h_martes_ini']),0,2,'C');
    $pdf->Cell(123,73,utf8_decode($fila['h_martes_fin']),0,2,'C');
    $pdf->Cell(165,-97,utf8_decode($fila['h_miercoles_ini']),0,2,'C');
    $pdf->Cell(165,121,utf8_decode($fila['h_miercoles_fin']),0,2,'C');
    $pdf->Cell(207,-144,utf8_decode($fila['h_jueves_ini']),0,2,'C');
    $pdf->Cell(207,166,utf8_decode($fila['h_jueves_fin']),0,2,'C');
    $pdf->Cell(250,-188,utf8_decode($fila['h_viernes_ini']),0,0,'C');
    $pdf->Cell(-250,-166,utf8_decode($fila['h_viernes_fin']),0,0,'C');
    $pdf->Cell(293,-188,utf8_decode($fila['h_sabado_ini']),0,0,'C');
    $pdf->Cell(-293,-166,utf8_decode($fila['h_sabado_fin']),0,0,'C');
    $pdf->Cell(337,-188,utf8_decode($fila['h_domingo_ini']),0,0,'C');
    $pdf->Cell(-337,-166,utf8_decode($fila['h_domingo_fin']),0,0,'C');
    $pdf->Cell(342,-414,utf8_decode($fila['folio']),0,0,'C');
       }
       
  }else{
    echo "<tr>
                 <td colspan='24'>No hay datos</td>
                  </tr>";  

  }
	
	$pdf->Output();

}else{
echo'
    <div class="container-fluid text-center">
        <div class="row">
            <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                <h4>Advertencia</h4>
                <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
            </div>
        </div>    
    </div> 
';
}
?>