<?php 
require_once "php/connect.php";

    if(isset($_POST['foto']) && isset($_POST['curp']) && isset($_POST['nombre']) && isset($_POST['calle']) && isset($_POST['localidad']) && isset($_POST['cp']) && isset($_POST['colonia']) && isset($_POST['nacimiento']) && isset($_POST['sexo']) && isset($_POST['cel']) && isset($_POST['perfil']) && isset($_POST['nivel']) && isset($_POST['especialidad']) && isset($_POST['id_mun'])){
    }

    foreach($pdo->query('SELECT MAX(Expediente) FROM instructores') as $row) {
    }
    $result = $row['MAX(Expediente)'] + 1;

	$expediente=$result;
	//$foto=$_POST['foto'];
	$curp=$_POST['curp'];
	$nombre=$_POST['nombre'];
	$calle=$_POST['calle'];
	$colonia=$_POST['colonia'];
	$localidad=$_POST['localidad'];
	$cp=$_POST['cp'];
	$seccion=$_POST['seccion'];
	$civil=$_POST['civil'];
	$sexo=$_POST['sexo'];
	$nacimiento=$_POST['nacimiento'];
	$alta=$_POST['alta'];
	$baja=$_POST['baja'];
	$perfil=$_POST['perfil'];
	$nivel=$_POST['nivel'];
	$especialidad=$_POST['especialidad'];
	$evaluacion=$_POST['evaluacion'];
	$puntaje=$_POST['puntaje'];
	$cel=$_POST['cel'];
	$tel=$_POST['tel'];
	$correo=$_POST['correo'];
	$observaciones=$_POST['observaciones'];
	$calidad="Sin calidad";
	$id_mun=$_POST['id_mun'];

// Recibo los datos de la foto
$nombre_img = $expediente."_".$_FILES['foto']['name'];
$tipo = $_FILES['foto']['type'];
$tamano = $_FILES['foto']['size'];
 
//Si existe foto y tiene un tamaño correcto
if (($nombre_img == !NULL) && ($_FILES['foto']['size'] <= 5000000)) 
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["foto"]["type"] == "image/gif")
   || ($_FILES["foto"]["type"] == "image/jpeg")
   || ($_FILES["foto"]["type"] == "image/jpg")
   || ($_FILES["foto"]["type"] == "image/png"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/SIICAT/img/inst/';
      // Muevo la foto desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['foto']['tmp_name'],$directorio.$nombre_img);
    } 
    else 
    {
       //si no cumple con el formato
       echo "No se puede subir una foto con ese formato ";
    }
} 
else 
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_img == !NULL) echo "La foto es demasiado grande "; 
}

	$consulta=$pdo->prepare("INSERT INTO instructores(Expediente,imagen,Curp,Nombre,Calle,Colonia_barrio,Localidad,CP,Seccion_elec,Estado_civil,Sexo,F_Nacimiento,F_Alta,F_Baja,Pefil,Nivel_academico,Especialidad,Fecha_evaluacion,Puntaje,No_celular,No_telefono,Email,Observaciones,calidad,id_municipios,condicion,acta_doc,curp_doc,dom_doc,ine_doc,expfg_doc,expl_doc,cv_doc,actualizacion_doc) VALUES(:expediente, :foto, :curp, :nombre, :calle, :colonia, :localidad, :cp, :seccion, :civil, :sexo, :nacimiento, :alta, :baja, :perfil, :nivel, :especialidad, :evaluacion, :puntaje, :cel, :tel, :correo, :observaciones, :calidad, :id_mun, '', '', '', '', '', '', '', '', '') ");

	$consulta->bindParam(':expediente',$expediente);
	$consulta->bindParam(':foto',$nombre_img);
	$consulta->bindParam(':curp',$curp);
	$consulta->bindParam(':nombre',$nombre);
	$consulta->bindParam(':calle',$calle);
	$consulta->bindParam(':colonia',$colonia);
	$consulta->bindParam(':localidad',$localidad);
	$consulta->bindParam(':cp',$cp);
	$consulta->bindParam(':seccion',$seccion);
	$consulta->bindParam(':civil',$civil);
	$consulta->bindParam(':sexo',$sexo);
	$consulta->bindParam(':nacimiento',$nacimiento);
	$consulta->bindParam(':alta',$alta);
	$consulta->bindParam(':baja',$baja);
	$consulta->bindParam(':perfil',$perfil);
	$consulta->bindParam(':nivel',$nivel);
	$consulta->bindParam(':especialidad',$especialidad);
	$consulta->bindParam(':evaluacion',$evaluacion);
	$consulta->bindParam(':puntaje',$puntaje);
	$consulta->bindParam(':cel',$cel);
	$consulta->bindParam(':tel',$tel);
	$consulta->bindParam(':correo',$correo);
	$consulta->bindParam(':observaciones',$observaciones);
	$consulta->bindParam(':calidad',$calidad);
	$consulta->bindParam(':id_mun',$id_mun);


	if($consulta->execute()){
		header('Location: instructores_vistaPLANE.php');
	}else{
		echo "Error no se pudo almacenar la información";
	}


