<?php 
    include "procesossesiones/seguridad.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Menú</title>
    <link rel="shortcut icon" href="img/icono.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style type="text/css">
        .col-md{
            background-color: #e7e7e7;
            box-shadow: 0px 0px 2px #9f9f9f;
            margin: 10px;
            padding: 10px;
            min-height: auto;
            height: 100%;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        a p{
           text-decoration:none;
        }
        .col-md:hover {
            box-shadow: 1px 1px 10px rgba(0,0,0,0.5);
            margin-top: -5px;
            margin-bottom: 25px;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        ul.acorh,
        ul.acorh * {
        margin: 0;
        padding: 0;
        border: 0;
        }
        ul.acorh {
        list-style: none;
        }
        ul.acorh li {
        list-style: none;
        }
        ul.acorh li a {
        display: block;
        padding: 5.99px 0px 5.99px 0px;
        text-decoration: none;
        border-bottom: 1px solid #ccc;
        }
        ul.acorh li ul {
        max-height: 0;
        list-style: none;
        overflow: hidden;
        transition: .3s all ease-in;
        }
        ul.acorh li li a {
        padding: 10px 0px 10px 0px;
        background: #999;
        color: #000;
        font-size: 16px;
        border: 0;
        border-bottom: 1px solid #ccc;
        box-sizing: border-box;
        }
        ul.acorc li li:last-child a {
        border-bottom: 0;
        }
        ul.acorh li:hover ul {
        max-height: 300px;
        transition: .3s all ease-in;
        }
        ul.acorh li a:hover {
        background: #186d41;
        color: #fff;
        }
    </style>
</head>
<?php  
        IF($_SESSION['tipo'] == "DTA"){
?>
<header>
    <?php require_once "partes/header.html"; ?>
</header>
<body>
<div class="container text-center">

        <div class="page-header">
            <h4><strong>MENÚ</strong></h4>
        </div>

        <div class="row justify-content-center">
            <div class="col-md">
                <a href="instructores.php">
                <p style="color: #7a1315">
                <i class="fa fa-graduation-cap" style="font-size:50px" title="INSTRUCTORES"></i>
                </p>
                </a>
                <p style="color: black">Calificar Instructores</p>
                <a href="instructores.php" class="btn btn-success btn-block btn-home-admin">INSTRUCTORES</a>
            </div>
        </div>

           
        <div class="row justify-content-center">
            <div class="col-md">
                <a href="#">
                <p style="color: #7a1315">
                <i class="fa fa-id-badge" style="font-size:50px" title="ASIGNACIÓN DE INSTRUCTORES"></i>
                </p>
                </a>
                <p style="color: black">Asignación de los instructores sugeridos</p>
                <ul class="acorh">
                    <li><a href="#" class="btn btn-success btn-block btn-home-admin">ASIGNACIÓN DE INSTRUCTORES</a>
                        <ul>
                            <li><a href="cursoregular_vistaDTA.php">Asignar a Curso Regular</a></li>
                            <li><a href="cursoebc_vistaDTA.php">Asignar a Curso EBC</a></li>
                            <li><a href="cursoextension_vistaDTA.php">Asignar a Curso de Extensión</a></li>
                            <li><a href="cursocae_vistaDTA.php">Asignar a Curso CAE</a></li>
                            <li><a href="cursoroco_vistaDTA.php">Asignar a Examen ROCO</a></li>
                        </ul> 
                    </li>
                </ul>
            </div>
        </div>

         <div class="row justify-content-center">
            <div class="col-md">
                <a href="#">
                <p style="color: #7a1315">
                <i class="fa fa-folder-open" style="font-size:50px" title="ASIGNACIÓN DE CURSOS"></i>
                </p>
                </a>
                <p style="color: black">Ver Cursos Aprobados</p>
                <ul class="acorh">
                    <li><a href="#" class="btn btn-success btn-block btn-home-admin">CURSOS</a>
                        <ul>
                        <li><a href="cursoregular_vistaCOO_A.php">Curso Regular</a></li>
                            <li><a href="cursoebc_vistaCOO_A.php">Curso EBC</a></li>
                            <li><a href="cursoextension_vistaCOO_A.php">Curso de Extensión</a></li>
                            <li><a href="cursocae_vistaCOO_A.php">Curso CAE</a></li>
                            <li><a href="cursoroco_vistaCOO_A.php">Examen ROCO</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
</div>

</body>
<?php
        }else{
        echo'
            <div class="container-fluid text-center">
                <div class="row">
                    <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                        <h4>Advertencia</h4>
                        <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                    </div>
                </div>    
            </div> 
        ';
    }
?>
<footer>
    <?php require_once "partes/footer.html" ?>
</footer>
</html>