<?php
	include 'plantilla_extension.php';
	require 'php/connect.php';
	
IF($_SESSION['tipo'] == "DTA" || $_SESSION['tipo'] == "COO" || $_SESSION['tipo'] == "PLANEA" || $_SESSION['tipo'] == "PLANT"){


	 $consulta=$pdo->prepare("SELECT * FROM cursos2018_extension");
     $consulta->execute();
	
	$pdf = new PDF('P','mm','Letter');
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Arial','B',12);
	
	
	$pdf->SetFont('Arial','',12);
	
	//Recuperar datos
    if(isset($_GET['folio'])){
        $folio=$_GET['folio'];
        $consulta=$pdo->prepare("SELECT CE.*,P.nombre,I.*,M.nombre_mun FROM cursos2018_extension AS CE LEFT JOIN plantel AS P ON CE.id_plantel = P.id LEFT JOIN instructores AS I ON CE.id_instructor = I.id LEFT JOIN municipios AS M ON CE.id_mun_c = M.folio_mun WHERE folio=:folio");
        $consulta->bindParam(":folio",$folio);
        $consulta->execute();
        if($consulta->rowCount()>=1){
            $fila=$consulta->fetch();

        
       
    
  $pdf->Cell(130,79, $fila['Expediente'],0,2,'C');
     $pdf->Cell(160,-54,utf8_decode($fila['Nombre']),0,2,'C');
    $pdf->Cell(130,73,utf8_decode($fila['Curp']),0,2,'C');
   $pdf->Cell(130,-51,utf8_decode($fila['nombre']),0,2,'C');
  $pdf->Cell(130,74,utf8_decode($fila['curso']),0,2,'C');
  $pdf->Cell(130,-46,utf8_decode($fila['nombre_mun']),0,2,'C');
 $pdf->Cell(135,66,utf8_decode($fila['fecha_inicio']),0,2,'C');
  $pdf->Cell(255,-66,utf8_decode($fila['fecha_fin']),0,2,'C');


   $pdf->Cell(83,120,utf8_decode($fila['h_lunes_ini']),0,2,'C');
    $pdf->Cell(83,-94,utf8_decode($fila['h_lunes_fin']),0,2,'C');
    $pdf->Cell(123,67,utf8_decode($fila['h_martes_ini']),0,2,'C');
    $pdf->Cell(123,-41,utf8_decode($fila['h_martes_fin']),0,2,'C');
    $pdf->Cell(165,16,utf8_decode($fila['h_miercoles_ini']),0,2,'C');
    $pdf->Cell(165,8,utf8_decode($fila['h_miercoles_fin']),0,2,'C');
    $pdf->Cell(208,-32,utf8_decode($fila['h_jueves_ini']),0,2,'C');
    $pdf->Cell(208,56,utf8_decode($fila['h_jueves_fin']),0,2,'C');
    $pdf->Cell(250,-80,utf8_decode($fila['h_viernes_ini']),0,2,'C');
    $pdf->Cell(250,104,utf8_decode($fila['h_viernes_fin']),0,2,'C');
    $pdf->Cell(295,-128,utf8_decode($fila['h_sabado_ini']),0,2,'C');
    $pdf->Cell(295,152,utf8_decode($fila['h_sabado_fin']),0,2,'C');
    $pdf->Cell(337,-175,utf8_decode($fila['h_domingo_ini']),0,0,'C');
    $pdf->Cell(-337,-152,utf8_decode($fila['h_domingo_fin']),0,0,'C');  
     $pdf->Cell(343,-402,utf8_decode($fila['folio']),0,0,'C');
       }

  }else{
    echo "<tr>
                 <td colspan='24'>No hay datos</td>
                  </tr>";  

  }
	
	$pdf->Output();

    }else{
    echo'
        <div class="container-fluid text-center">
            <div class="row">
                <div class="col-12 text-center alert alert-danger" style="margin-bottom: 0px">
                    <h4>Advertencia</h4>
                    <h6>Usted no tiene permitido el acceso a esta parte del sitio.</h6>
                </div>
            </div>    
        </div> 
    ';
}
?>