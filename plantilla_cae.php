<?php
	include "procesossesiones/seguridad.php";
	require 'fpdf/fpdf.php';
	
	class PDF extends FPDF
	{
		function Header()
		{
			$this->Image('img/formatos/formato_cae.jpg', 0, 0, 216 );
			$this->SetFont('Arial','B',15);
			$this->Cell(30);
			//$this->Cell(120,10, 'Asignacion de Instructores',0,0,'C');
			$this->Ln(20);
		}
			
		function Footer()
		{
			$this->SetY(-10);
			$this->SetFont('Arial','I', 8);
			$this->Cell(0,10, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C' );
		}		
	}
?>