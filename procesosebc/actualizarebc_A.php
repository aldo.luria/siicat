<?php 
    //Actualizar datos
    if (isset($_POST['folio'])) {
    $folio=$_POST['folio'];
    $observa=$_POST['observa'];

    $consulta2=$pdo->prepare("UPDATE cursos2018_ebc SET observaciones_c=:observa WHERE folio=:folio");

    $consulta2->bindParam(':observa',$observa); 
    $consulta2->bindParam(':folio',$folio);

            if($consulta2->execute()){
                echo '
                    <div class="container-fluid text-center alert alert-success" style="margin-bottom: 0px">
                        <h4>Datos Actualizados</h4>
                        <a class="btn btn-success" href="#inicio">Seguir editando</a>
                        <a class="btn btn-danger" href="cursoebc_vistaCOO_A.php">Atras</a>
                    </div>      
                ';
            }else{
                echo '
                    <a href="cursoebc_vistaCOO_A.php" style="text-decoration: none">
                        <div class="container-fluid text-center alert alert-danger" style="margin-bottom: 0px">
                            <h4>Error: No se Actualizaron los datos</h4>
                        </div>
                    </a>
                ';
            }

    }
 
    //Recuperar datos
    if(isset($_GET['folio'])){
        $folio=$_GET['folio'];
        $consulta=$pdo->prepare("SELECT EBC.*,P.nombre,I.Nombre,I.Expediente,O.*,E.*,C.*,M.* FROM cursos2018_ebc AS EBC LEFT JOIN plantel AS P ON EBC.id_plantel = P.id LEFT JOIN instructores AS I ON EBC.id_instructor = I.id LEFT JOIN oferta AS O ON EBC.id_modulo_c = O.id LEFT JOIN modulosespecialidades AS E ON O.idEspecialidad = E.id_especialidad LEFT JOIN modulosformacion AS C ON E.id_modulo = C.idmodulo LEFT JOIN municipios AS M ON EBC.id_mun_c = M.folio_mun WHERE EBC.folio=:folio");
        $consulta->bindParam(":folio",$folio);
        $consulta->execute();
        if($consulta->rowCount()>=1){
            $fila=$consulta->fetch();

            echo '

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
        <style type="text/css">
        #formulario{
            box-shadow: 5px 5px 20px #9f9f9f;
            background-color: #e7e7e7;
        }
        a{
            color: #186d41;
        }
        li a{
            color: #186d41 !important;
        }
        .btn-success{
            background-color: #186d41 !important;
        }
        .btn-danger{
            background-color: #9f282f !important;
        }
    </style>
    <script type="text/javascript" language="javascript" src="js/ajax.js"></script>
</head>
<header>'; ?>
    <?php
    if ($_SESSION['tipo'] == "COO") {
        require_once "partes/nav_COO.php"; 
    }elseif ($_SESSION['tipo'] == "DTA") {
        require_once "partes/nav_DTA.php"; 
    }elseif ($_SESSION['tipo'] == "PLANEA") {
        require_once "partes/nav_PLANE.php"; 
    }elseif ($_SESSION['tipo'] == "PLANT") {
        require_once "partes/nav_PLA.php"; 
    }
    ?><?php echo'
</header>
<body>
    <div class="page-header text-center">
        <h4><strong>VER REGISTRO - CURSO2018_EBC</strong></h4>
    </div>               

    <div class="container">
        <form action="" method="POST" name="formebc">

        <input type="hidden" name="folio" value="'.$fila['folio'].'">
       
        <div id="formulario">
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos del Instructor</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Instructor</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" name="id_inst" required="" disabled>
                                <option value="'.$fila['id_instructor'].'">'.$fila['Nombre'].'</option>
                                <option>--- Seleccionar Instructor ---</option>';?>
                            <?php 
                                 foreach ($rows as $row) {
                                    echo '<option value="'.$row['id'].'">'.$row['Nombre'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div>
                </div>
            </div>


            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Datos de Curso</strong></div>
                <div class="row text-left">
                    <label class="col-sm-2">Plantel</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="id_plan" required="" disabled>
                            <option value="'.$fila['id_plantel'].'">'.$fila['nombre'].'</option>';?>
                            <?php 
                                foreach ($rows2 as $row2) {
                                    echo '<option value="'.$row2['id'].'">'.$row2['nombre'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div>

                    <label class="col-sm-2">Municipio</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm" name="id_mun_c" required="" disabled>
                            <option value="'.$fila['id_mun_c'].'">'.$fila['nombre_mun'].'</option>';?>
                            <?php 
                                foreach ($rows3 as $row3) {
                                    echo '<option value="'.$row3['folio_mun'].'">'.$row3['nombre_mun'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div> 
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Calle</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="calle" value="'.$fila['calle_c'].'" required="" disabled>
                    </div>

                    <label class="col-sm-2">Colonia</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" name="colonia" value="'.$fila['colonia_c'].'" required="" disabled>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Campo</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" type="number" name="idcampo" onchange="from(document.formebc.idcampo.value,';?><?php echo"'cargaresp',";?> <?php echo"'cargar_datos_cmbo.php'";?> <?php echo ');" required="" disabled>
                            <option value="'.$fila['id_campo_c'].'">'.$fila['modulo'].'</option>'; ?>
                            <?php 
                                $sql = "SELECT * FROM modulosformacion ORDER BY idmodulo ASC";
                                $result = $pdo->query($sql);
                                $rows = $result->fetchAll();
                                foreach ($rows as $row) {
                                    echo '<option value="'.$row['idmodulo'].'">'.$row['modulo'].'</option>';
                                }
                            ?>
                        <?php echo '
                        </select>
                    </div>
                </div>

            <div id="cargaresp">

                <div class="row text-left">
                    <label class="col-sm-2">Especialidad</label>
                    <div class="col-sm-10">
                        <select class="form-control input-sm" type="number" name="idespecialidad" required="" disabled>
                            <option value="'.$fila['id_especialidad_c'].'">'.$fila['especialidad'].'</option>
                        </select>
                    </div>
                </div>  

                <div class="row text-left">
                    <label class="col-sm-2">Módulo</label>
                    <div class="col-sm-10" id="cargarmod">
                        <select class="form-control input-sm" type="number" name="idmodulo" required="" disabled>
                            <option value="'.$fila['id_modulo_c'].'">'.$fila['nombreModulo'].'</option>
                        </select>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Cve Especialidad</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" value="'.$fila['cveEspecialidad'].'" disabled>
                    </div>

                    <label class="col-sm-2">Cve Módulo</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="text" value="'.$fila['cveModulo'].'" disabled>
                    </div>
                </div>

                <div class="row text-left">
                    <label class="col-sm-2">Duración</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="number" value="'.$fila['duracion'].'" disabled>
                    </div>
                </div>

            </div>
                                 
                <div class="row text-left">
                    <label class="col-sm-2">Fecha Inicio</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="fechainicio" value="'.$fila['fecha_inicio'].'" required="" disabled>
                    </div>

                    <label class="col-sm-2">Fecha Fin</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" type="date" name="fechafin" value="'.$fila['fecha_fin'].'" required="" disabled>
                    </div>
                </div>
            </div>

                            
            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Horarios</strong></div>
                <div class="row text-left">
                        <label class="col-sm-2">Lunes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="lunesinicio" value="'.$fila['h_lunes_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Lunes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="lunesfin" value="'.$fila['h_lunes_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Martes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="martesinicio" value="'.$fila['h_martes_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Martes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="martesfin" value="'.$fila['h_martes_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Miércoles Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="miercolesinicio" value="'.$fila['h_miercoles_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Miércoles Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="miercolesfin" value="'.$fila['h_miercoles_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Jueves Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="juevesinicio" value="'.$fila['h_jueves_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Jueves Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="juevesfin" value="'.$fila['h_jueves_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Viernes Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="viernesinicio" value="'.$fila['h_viernes_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Viernes Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="viernesfin" value="'.$fila['h_viernes_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Sábado Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sabadoinicio" value="'.$fila['h_sabado_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Sábado Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="sabadofin" value="'.$fila['h_sabado_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Domingo Inicio</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="domingoinicio" value="'.$fila['h_domingo_ini'].'" disabled>
                        </div>
                        <label class="col-sm-2">Domingo Fin</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="time" name="domingofin" value="'.$fila['h_domingo_fin'].'" disabled>
                        </div>
                </div>

                <div class="row text-left">
                        <label class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" type="text" name="status" value="'.$fila['status'].'" disabled>
                        </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row" id="titulo"><strong>Observaciones</strong></div>
                <div class="row text-left">
                        <div class="col-sm-12">
                            <textarea class="form-control input-sm" name="observa" style="height:150px;" >'.$fila['observaciones_c'].'</textarea>
                        </div>
                </div>
            </div>

        </div>                             
            <div class="text-center"><br>
            <input class="btn btn-success" type="submit" value="Guardar">
            <a class="btn btn-danger" href="cursoebc_vistaCOO_A.php">Regresar</a>
            </div>
        </form>
    </div>    
    <br>
</body>
</html>';
            }else{
            echo "Ocurrio un error";
        }
    }else{
        echo "Error no se pudo procesar la petición";
    }