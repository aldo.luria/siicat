$(document).ready(function(){
	$('a[data-confirm3]').click(function(ev){
		var href = $(this).attr('href');
		if(!$('#confirm-delete3').length){
			$('body').append('<div class="modal fade" id="confirm-delete3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-success text-white"><strong>ACTUALIZAR REGISTRO</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body text-center" style="font-size:15px;">¿Instructor <strong> competente</strong> ?<hr><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button> <a class="btn btn-success text-white" id="dataComfirmOK3">Aceptar</a></div></div></div></div>');
		}
		$('#dataComfirmOK3').attr('href', href);
        $('#confirm-delete3').modal({show: true});
		return false;
		
	});
});