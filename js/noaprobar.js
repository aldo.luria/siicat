$(document).ready(function(){
	$('a[data-confirm5]').click(function(ev){
		var href = $(this).attr('href');
		if(!$('#confirm-delete5').length){
			$('body').append('<div class="modal fade" id="confirm-delete5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-danger text-white"><strong>RECHAZAR REGISTRO</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body text-center" style="font-size:15px;">¿Desea <strong>rechzar</strong> curso?<hr><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button> <a class="btn btn-success text-white" id="dataComfirmOK5">Aceptar</a></div></div></div></div>');
		}
		$('#dataComfirmOK5').attr('href', href);
        $('#confirm-delete5').modal({show: true});
		return false;
		
	});
});