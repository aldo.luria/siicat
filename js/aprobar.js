$(document).ready(function(){
	$('a[data-confirm4]').click(function(ev){
		var href = $(this).attr('href');
		if(!$('#confirm-delete4').length){
			$('body').append('<div class="modal fade" id="confirm-delete4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-success text-white"><strong>APROBAR REGISTRO</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body text-center" style="font-size:15px;">¿Desea <strong>aprobar</strong> curso?<hr><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button> <a class="btn btn-success text-white" id="dataComfirmOK4">Aceptar</a></div></div></div></div>');
		}
		$('#dataComfirmOK4').attr('href', href);
        $('#confirm-delete4').modal({show: true});
		return false;
		
	});
});