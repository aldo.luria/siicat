-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2018 a las 07:42:02
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siicat`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alta_alumnos`
--

CREATE TABLE `alta_alumnos` (
  `id_alumno` int(100) NOT NULL,
  `apellido_p` varchar(45) DEFAULT NULL,
  `apellido_m` varchar(45) DEFAULT NULL,
  `nombre_a` varchar(80) DEFAULT NULL,
  `sexo` varchar(10) DEFAULT NULL,
  `curp` varchar(20) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `num_tel` varchar(30) DEFAULT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `CP` int(11) DEFAULT NULL,
  `municipio` int(11) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `estado_civil` varchar(25) DEFAULT NULL,
  `discapacidad` varchar(50) DEFAULT NULL,
  `imagen_actan` varchar(300) DEFAULT NULL,
  `imagen_curp` varchar(300) DEFAULT NULL,
  `imagen_compdom` varchar(300) DEFAULT NULL,
  `imagen_compest` varchar(300) DEFAULT NULL,
  `foto_alum` varchar(300) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alta_alumnos`
--

INSERT INTO `alta_alumnos` (`id_alumno`, `apellido_p`, `apellido_m`, `nombre_a`, `sexo`, `curp`, `edad`, `num_tel`, `domicilio`, `localidad`, `CP`, `municipio`, `estado`, `estado_civil`, `discapacidad`, `imagen_actan`, `imagen_curp`, `imagen_compdom`, `imagen_compest`, `foto_alum`, `email`) VALUES
(2, 'López', 'González', 'Brayan', 'H', 'LOGB980923HTLPNR10', 19, '2471213492', 'Calle Francisco Villa #31', 'San Francisco Yancuitlalpan', 90505, 13, 'Tlaxcala', 'Soltero', 'Ninguna', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Lopez', 'Gonzalez', 'Brayan', 'MASCULINO', 'LOGB980923HTLPNR10', 19, '2471213492', 'Calle Francisco Villa #31', 'San Francisco Yancuitlalpan', 90505, 13, 'Tlaxcala', 'soltero', 'ninguna', 'Brayan_wallhaven-640380.jpg', 'Brayan_image-7.jpg', 'Brayan_wallhaven-641319.jpg', 'Brayan_wallhaven-640818.jpg', 'Brayan_image.jpg', 'brayan.gintama@gmail.com'),
(4, 'Lopez', 'Gonzalez', 'Brayan', 'MASCULINO', 'LOGB980923HTLPNR10', 20, '2471213492', 'Calle Francisco Villa #31', 'San Francisco Yancuitlalpan', 90505, 13, 'Tlaxcala', 'soltero', 'ninguna', 'Brayan_HERETIC_DESUUUUU.jpg', 'Brayan_33197445_1835989810036617_1754109328526147584_n.jpg', 'Brayan_icatlax.jpg', 'Brayan_image.jpg', 'Brayan_34072619_2110185852553129_2826480757311012864_n.png', 'brayan.gintama@gmail.com'),
(6, 'Luría', 'García', 'Aldo Giovanni', 'MASCULINO', 'LUGA981029HTLRRL09', 19, '2461345205', 'Calle Tulipanes 20B', 'Geovillas', 90110, 33, 'Tlaxcala', 'CASADO', 'Ninguna', 'Aldo Giovanni_Captura de pantalla (3).png', 'Aldo Giovanni_Captura de pantalla (2).png', 'Aldo Giovanni_Captura de pantalla (4).png', 'Aldo Giovanni_Captura de pantalla (5).png', 'Aldo Giovanni_Captura de pantalla (1).png', 'aldo.luria@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_curso2018cae`
--

CREATE TABLE `alumno_curso2018cae` (
  `id_ac` int(11) NOT NULL,
  `id_alumno_c` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_curso2018cae`
--

INSERT INTO `alumno_curso2018cae` (`id_ac`, `id_alumno_c`, `id_curso`) VALUES
(1, 6, 1),
(2, 6, 2),
(3, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_curso2018ebc`
--

CREATE TABLE `alumno_curso2018ebc` (
  `id_ac` int(11) NOT NULL,
  `id_alumno_c` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_curso2018ebc`
--

INSERT INTO `alumno_curso2018ebc` (`id_ac`, `id_alumno_c`, `id_curso`) VALUES
(1, 6, 1),
(2, 6, 3),
(3, 4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_curso2018ext`
--

CREATE TABLE `alumno_curso2018ext` (
  `id_ac` int(11) NOT NULL,
  `id_alumno_c` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_curso2018ext`
--

INSERT INTO `alumno_curso2018ext` (`id_ac`, `id_alumno_c`, `id_curso`) VALUES
(1, 6, 1),
(2, 6, 2),
(3, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_curso2018reg`
--

CREATE TABLE `alumno_curso2018reg` (
  `id_ac` int(11) NOT NULL,
  `id_alumno_c` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_curso2018reg`
--

INSERT INTO `alumno_curso2018reg` (`id_ac`, `id_alumno_c`, `id_curso`) VALUES
(1, 6, 7),
(2, 6, 5),
(3, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_curso2018roco`
--

CREATE TABLE `alumno_curso2018roco` (
  `id_ac` int(11) NOT NULL,
  `id_alumno_c` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_curso2018roco`
--

INSERT INTO `alumno_curso2018roco` (`id_ac`, `id_alumno_c`, `id_curso`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_curso`
--

CREATE TABLE `asignacion_curso` (
  `Folio` int(11) NOT NULL,
  `Expediente` int(11) DEFAULT NULL,
  `Id_plantel` int(11) DEFAULT NULL,
  `Id_especialidad` int(11) NOT NULL,
  `Id_curso` int(11) DEFAULT NULL,
  `Modalidad` varchar(40) DEFAULT NULL,
  `Lugar` varchar(80) DEFAULT NULL,
  `Fecha_inicio` date DEFAULT NULL,
  `Fecha_fin` date DEFAULT NULL,
  `Hora_inicio` time DEFAULT NULL,
  `Hora_fin` time DEFAULT NULL,
  `Total_hrs` int(11) DEFAULT NULL,
  `Dias` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `id_especialidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `nombre`, `id_especialidad`) VALUES
(1, 'Mercadotecnia  en la  micro y Pequeña Empresa', 1),
(2, 'Administración en la micro y pequeña Empresa', 1),
(3, 'Servicio y comunicación con el cliente', 1),
(4, 'Servicios de atención telefónica y tele mercadeo', 1),
(5, 'Manejo de herramientas para auditoria', 1),
(6, 'Trafico de marcación y tramitación anual', 1),
(7, 'Asesoría en comercialización de Bienes y Inmuebles', 1),
(8, 'Servicio de comensales', 3),
(9, 'Preparación de bebidas', 3),
(10, 'Control de costos de alimentación y Bebidas', 3),
(11, 'Preparación de alimentos', 3),
(12, 'Atención del cliente del vino', 3),
(13, 'Resguardo del vino', 3),
(14, 'Pastelería y dulces Finos', 3),
(15, 'Manejo Higiénico de Alimentos y Bebidas', 3),
(16, 'tejido a Mano', 4),
(17, 'Bordado en Tela', 4),
(18, 'Chaquira', 4),
(19, 'Macramé', 4),
(20, 'Cestería', 5),
(21, 'Administración Artesanal', 5),
(22, 'Oleo aplicado', 5),
(23, 'Restauración Artística', 5),
(24, 'Talla de madera', 5),
(25, 'Pirograbado', 5),
(26, 'Resinas Y Moldes', 5),
(27, 'Repujado en Metal', 7),
(28, 'Vidrio Artístico', 7),
(29, 'Esmalte a Fuego', 7),
(30, 'Herrería Artística', 7),
(31, 'Artesanías con pastas, pinturas y acabados', 6),
(32, 'Cuidado de Niños Lactantes en centros de atención Infantil', 8),
(33, 'Cuidado de Niños Maternales en Centros de Atención Infantil', 8),
(34, 'Cuidado de Niños Preescolares en Centro de Atención Infantil', 8),
(35, 'Taquigrafía', 9),
(36, 'Servicios Secretariales', 9),
(37, 'Mecanografía asistido por computadora', 9),
(38, 'Alta Costura', 22),
(39, 'Confección de Prendas para Dama y niña', 22),
(40, 'Confección de prendas para Caballero y Niño', 22),
(41, 'Patronaje y Graduación', 22),
(42, 'Contabilidad General con paquete Contable', 2),
(43, 'Contabilidad de Costos Asistida por Computadora', 2),
(44, 'Determinación de Obligaciones Fiscales', 2),
(45, 'Repintado de los componentes de la Carrocería del Vehículo Automotriz', 19),
(46, 'Dibujo de Modas', 20),
(47, 'Diseño', 20),
(48, 'Modelado', 20),
(49, 'Patronaje', 20),
(50, 'Graduación de Patrones', 20),
(51, 'Costura, confecciones y Bordado', 20),
(52, 'Formado de Piezas por el método de Tornado', 18),
(53, 'Pintura en cerámica Bajo Esmalte', 18),
(54, 'Pintura en Cerámica Tipo Talavera', 18),
(55, 'Producción de piezas de cerámica por el proceso de vaciado', 18),
(56, 'Preparación de pastas Cerámicas para procesos de Formado', 18),
(57, 'Carpintería de Puertas, Ventanas y Armarios', 17),
(58, 'Preparación de Materia Prima en la Elaboración de Muebles de Madera', 17),
(59, 'Pintado de piezas, Componentes y Muebles de Madera', 17),
(60, 'Elaboración de Piezas Torneadas', 17),
(61, 'Terminado de Muebles de Madera', 17),
(62, 'Restauración de Muebles', 17),
(63, 'Rauteado de Piezas para Muebles de Madera', 17),
(64, 'Instalación de sistema Eléctrico Residencial', 25),
(65, 'Instalación del Sistema Eléctrico Industrial', 25),
(66, 'Mantenimiento a Motores Eléctricos', 25),
(67, 'Mantenimiento de Aparatos Domésticos', 25),
(68, 'Construcción de redes de Telecomunicaciones', 25),
(69, 'Instalación de Sistemas Electrónicos de Seguridad', 25),
(70, 'Instalación y Reparación de Sistemas de Comunicación', 25),
(71, 'Reparación al sistema de Control de Emisión de Gases Contaminantes', 26),
(72, 'Afinación de motores a Gasolina con Sistema de Inyección de Combustible', 26),
(73, 'Sistema  Eléctrico', 26),
(74, 'Autotrónica', 26),
(75, 'Reparación del Sistema de Frenos', 26),
(76, 'Cuidados Faciales y Corporales', 16),
(77, 'Cuidado de Manos y Pies', 16),
(78, 'Maquillaje del Rostro', 16),
(79, 'Corte y Peinado del Cabello', 16),
(80, 'Color y transformación en el Cabello', 16),
(81, 'Diseño de Flores de Ocasión', 23),
(82, 'ventas de Servicios de Viajes', 10),
(83, 'Reservación de Servicios Hoteleros', 12),
(84, 'Recepción del Huésped', 12),
(85, 'Registro y Control de la Cuenta del Huéspedes', 12),
(86, 'Conserjería y de Comunicación Telefónica de Hotelería', 12),
(87, 'Servicio de Atención al Huésped', 12),
(88, 'Servicio de Habitaciones', 12),
(89, 'Supervisión de Habitaciones y Áreas Comunes', 12),
(90, 'Coordinación de los servicios de Hospedaje', 12),
(91, 'Organización de Eventos de Negocios, Sociales y Culturales', 12),
(92, 'Operación de Base de Datos', 14),
(93, 'Windows e Internet', 14),
(94, 'Elaboración de Textos', 14),
(95, 'Elaboración de Presentaciones Electrónicas', 14),
(96, 'Elaboración de Hojas de Calculo', 14),
(97, 'Ingles Comunicativo Básico inicial', 13),
(98, 'Ingles Comunicativo Básico superior', 13),
(99, 'Ingles Comunicativo Pre intermedio', 13),
(100, 'Ingles Comunicativo Intermedio', 13),
(101, 'instalación y Reparación del Sistema Hidrosanitario Residencial', 27),
(102, 'Instalación del Sistema de Gas', 27),
(103, 'Mantenimiento de Monitores', 15),
(104, 'Mantenimiento de Impresoras', 15),
(105, 'Mantenimiento a PC y Portátiles', 15),
(106, 'Mantenimiento de Redes de Área Local LAN', 15),
(107, 'Mantenimiento de Maquinas de Costura Recta', 29),
(108, 'Mantenimiento de Maquinas de Costura zigzag', 29),
(109, 'Mantenimiento de Maquinas de Costura Especiales', 29),
(110, 'Mantenimiento Mecánico Industrial', 28),
(111, 'Mantenimiento a Sistemas de Transmisión de Potencia Mecánica', 28),
(112, 'Mantenimiento de Bombas', 28),
(113, 'Mantenimiento de Reductores y Moto reductores de Velocidad', 28),
(114, 'Mantenimiento a Compresores de Aire comprimido de Uso Comercial e Industrial', 28),
(115, 'Reparación de Motores a Gasolina', 30),
(116, 'reparación del Sistema de Suspensión con Alineación y Balanceo', 30),
(117, 'Servicio y Reparación al Sistema  de Dirección', 30),
(118, 'Reparación del Sistema de Transmisión manual', 30),
(119, 'Reparación del Sistema transmisión Automática', 30),
(120, 'Reparación del Sistema de Embrague', 30),
(121, 'Reparación del Sistema de Frenos Básicos', 30),
(122, 'Reparación del Sistema de Inyección de los vehículos de Rango Medio y Servicio Pesado', 31),
(123, 'Reparación del Motor a Diésel de los vehículos de Rango Medio Y servicio Pesado', 31),
(124, 'Sistemas de Control Industrial de Motores Eléctricos', 24),
(125, 'Mantenimiento de Sistemas Neumáticos', 24),
(126, 'Dispositivos de Estado Solido', 24),
(127, 'Mantenimiento de Sistemas Electro neumáticos', 24),
(128, 'Electrónica Industrial', 24),
(129, 'Mantenimiento de Sistemas Hidráulicos', 24),
(130, 'Mantenimiento de Sistemas Electrohidráulicos', 24),
(131, 'Preparación y Conservación de Alimentos de Origen Animal', 11),
(132, 'Elaboración de mezclas para Conserva Alimenticias de Origen Vegetal', 11),
(133, 'Técnicas Especializadas de Carnicería', 11),
(134, 'Confección de Saco Sastre', 21),
(135, 'Confección de Chaleco y Falda Sastre', 21),
(136, 'Confección de Pantalón Sastre', 21),
(137, 'Soldadura Oxigas del Acero en Posiciones', 32),
(138, 'soldadura en Procesos Especiales TIG y MIG.', 32),
(139, 'Soldadura de Ventanería', 32),
(140, 'Aplicación de Soldadura por Arco Metálico Protegida con Gas', 32),
(141, 'Paileria Industrial', 32),
(142, 'Aplicación de Soldadura por Arco con Electrodo Metálico Revestido', 32),
(143, 'Soldadura por Arco con Tungsteno y Gas', 32),
(144, 'Curso de Prueba', 33),
(145, 'OtraPrueba', 33),
(146, 'Holgazaneria', 33),
(147, '', 33),
(148, '', 33),
(149, 'GLOBOFLEXIA', 33),
(150, 'PASTELERÍA', 33),
(151, 'CUIDADO DE MANOS Y DECORACIÓN DE UÑAS', 33),
(152, 'COCINA NAVIDEÑA', 33),
(153, 'ADORNOS NAVIDEÑOS', 33),
(154, 'N/A', 8),
(155, 'REPOSTERÍA FINA', 33),
(156, 'MANUALIDADES DE NAVIDAD', 33),
(157, 'FIGURAS DE GLOBO, LISTÓN, TELA Y PAPEL', 33),
(158, 'ARREGLOS NAVIDEÑOS', 33),
(159, 'VELAS AROMÁTICAS', 33),
(160, 'DECORACIÓN CON GLOBOS ', 33),
(161, 'CORTE, PEINADO Y MAQUILLAJE', 33),
(162, 'ARTESANÍAS CON FIBRAS TEXTILES', 33),
(163, 'BORDADO EN HILVÁN', 33),
(164, 'CORTE Y PEINADO', 33),
(165, 'DISEÑOS EN BISUTERÍA', 33),
(166, 'PREPARACIÓN DE ALIMENTOS', 33),
(167, 'DECORACIÓN  DE PASTELES.', 33),
(168, 'BORDADO DE LISTÓN', 33),
(169, 'REPOSTERÍA FINA Y GELATINA', 33),
(170, 'CORTES Y PEINADOS MODERNOS EN EL CABELLO', 33),
(171, 'CORTE DE CABELLO Y MAQUILLAJE', 33),
(172, 'DECORACION DE UÑAS Y MAQUILLAJE', 33),
(173, 'CONFECCIÓN DE PRENDAS DE INVIERNO', 33),
(174, 'INTRODUCCIÓN A LA LENGUA NAHUATL', 33),
(175, 'GELATINA ARTÍSTICA', 33),
(176, ' CORTE DE CABELLO, PERMACOLOGÍA Y COLORIMETRÍA', 33),
(177, ' CORTE DE CABELLO, PERMACOLOGÍA Y COLORIMETRÍA', 33),
(178, ' MANTENIMIENTO DE COMPUTADORAS PREVENTIVO Y CORRECTIVO', 33),
(179, 'INGLÉS BÁSICO INICIAL', 33),
(180, 'N/A', 1),
(181, 'N/A', 2),
(182, 'N/A', 3),
(183, 'N/A', 4),
(184, 'N/A', 5),
(185, 'N/A', 6),
(186, 'N/A', 7),
(187, 'N/A', 9),
(188, 'N/A', 10),
(189, 'N/A', 11),
(190, 'N/A', 12),
(191, 'N/A', 13),
(192, 'N/A', 14),
(193, 'N/A', 15),
(194, 'N/A', 16),
(195, 'N/A', 17),
(196, 'N/A', 18),
(197, 'N/A', 19),
(198, 'N/A', 20),
(199, 'N/A', 21),
(200, 'N/A', 22),
(201, 'N/A', 23),
(202, 'N/A', 24),
(203, 'N/A', 25),
(204, 'N/A', 26),
(205, 'N/A', 27),
(206, 'N/A', 28),
(207, 'N/A', 29),
(208, 'N/A', 30),
(209, 'N/A', 31),
(210, 'N/A', 32),
(211, 'ELABORACIÓN DE DULCES TÍPICOS REGIONALES', 33),
(212, 'RELACIONES HUMANAS Y ATENCIÓN AL PÚBLICO', 33),
(213, 'TRABAJOS CON PAPEL Y GLOBOS', 33),
(214, 'BORDADO EN BLUSAS CON CHAQUIRA', 33),
(215, 'CORTE DE CABELLO Y DECORACIÓN DE UÑAS', 33),
(216, 'CORTES. TINTES Y PEINADOS', 33),
(217, 'CONSERVA DE FRUTAS Y HORTALIZAS', 33),
(218, 'CALLE FERROCARRIL S/N PRESIDENCIA DE COMUNIDAD CHALMA SANTA ANA CHIAUTEMPAN', 33),
(219, 'PEINADO, MAQUILLAJE Y DECORACIÓN DE UÑAS', 33),
(220, 'ENSEÑANZA DE LA LENGUA NÁHUATL', 33),
(221, 'MANEJO DE RESIDUOS PELIGROSOS', 33),
(222, 'ORNAMENTO FLORAL', 33),
(223, 'CORTE DE CABELLO Y MAQUILLAJE', 33),
(224, 'ELABORACIÓN DE PIÑATAS ARTÍSTICAS', 33),
(225, 'APLICACIÓN DE UÑAS ACRÍLICAS', 33),
(226, 'SOPORTE Y MANTENIMIENTO DE EQUIPO DE COMPUTO', 33),
(227, 'TENDENCIAS EN CORTE Y PEINADO', 33),
(228, 'DESAROLLO HUMANO', 33),
(229, 'RELACIONES HUMANAS Y ATENCIÓN AL PUBLICO', 33),
(230, 'REPOSTERÍA FINA ', 33),
(231, 'DULCES ARTESANALES', 33),
(232, 'COCINA NAVIDEÑA Y PANADERÍA', 33),
(233, 'CORTE DE CABELLO, PERMACOLOGÍA Y COLORIMETRÍA', 33),
(234, 'RELACIONES HUMANAS Y ATENCIÓN AL PUBLICO', 33),
(235, 'LENGUAJE EN SEÑAS', 33),
(236, 'CONFECCIÓN DE ABRIGOS ', 33),
(237, 'DISEÑO EN BISUTERÍA ', 33),
(238, 'CORTES, TINTES Y PEINADOS ', 33),
(239, 'INGLÉS BÁSICO INICIAL', 33),
(240, 'PROGRAMACIÓN NEUROLINGÜISTICA APLICADA', 33),
(241, 'ELABORACIÓN DE PIÑATAS ', 33),
(242, 'GELATINA ARTÍSTICA Y REPOSTERÍA FINA', 33),
(243, 'CONFECCIÓN DE ABRIGOS Y CHAMARRAS ', 33),
(244, 'INGLÉS BÁSICO INICIAL', 33),
(245, 'ACTIVIDADES LUDICAS DE ENSEÑANZA APRENDIZAJE Y GESTION ESCOLAR', 33),
(246, 'WORD Y EXCEL', 33),
(247, 'MAQUILLAJE PROFESIONAL', 33),
(248, 'CARPINTERÍA BÁSICA ', 33),
(249, 'ARREGLO FLORALES', 33),
(250, 'REPOSTERÍA ', 33),
(251, 'MANUALIDADES NAVIDEÑAS', 33),
(252, 'EMPAQUE Y EMBALAJE DE ACUERDO A TU PRODUCTO', 33),
(253, 'WORD, EXCEL E INTERNET AVANZADO', 33),
(254, 'SOPORTE Y MANTENIMIENTO DE EQUIPO DE COMPUTO', 33),
(255, 'BORDADO EN FANTASÍA', 33),
(256, 'PROCESOS DE PRODUCCIÓN EN ALIMENTOS ', 33),
(257, 'PREPARACIÓN DE MERMELADAS Y CONSERVAS', 33),
(258, 'CORTES Y PEINADOS DEL CABELLO', 33),
(259, 'Formación de instructores en desarrollo humano, habilidades blandas y habilidades generenciales', 33),
(260, 'FIGURAS CON FIELTRO Y ELABORACIÓN DE PIÑATAS', 33),
(261, 'BORDADO, DESHILADO Y BORDADO EN TELA', 33),
(262, ' DISEÑOS EN MADERA', 33),
(263, 'COLOR,CORTE Y PEINADO.', 33),
(264, 'FIGURAS DE LISTON,TELA Y PAPEL', 33),
(265, 'ADMINISTRACIÓN DE MICRO Y PEQUEÑOS NEGOCIOS ', 33),
(266, 'Alineación del Estándar EC0217', 33),
(267, 'CONFECCIÓN DE PRENDAS PARA CABALLERO AVANZADO', 33),
(268, 'JOYERÍA FINA EN CHAPA DE ORO Y CRISTAL AVANZADO', 33),
(269, 'JOYERÍA FINA EN CHAPA DE ORO Y CRISTAL AVANZADO', 33),
(270, 'PINTURA EN CERAMICA II', 33),
(271, 'TALLADO Y MOLDEADO EN MADERA AVANZADO', 33),
(272, 'OPERACIÓN Y SISTEMAS DE APARATOS ELECTRODOMÉSTICOS II', 33),
(273, 'MAQUILLAJE ARTÍSTICO Y PEINADO', 33),
(274, 'WORD,EXCEL AVANZADO E INTERNET', 33),
(275, 'MANEJO DE HERRAMIENTAS', 33),
(276, 'MANEJO SANITARIO ', 33),
(277, 'TRAZO Y DISEÑO DE PRENDAS DE VESTIR', 33),
(278, 'CORTE, CONFECCIÓN Y DISEÑO DE PRENDAS DE VESTIR', 33),
(279, 'FIGURAS CON FIELTRO Y ELABORACIÓN DE PIÑATAS', 33),
(280, 'MAQUILLAJE PROFESIONAL Y FANTASÍA', 33),
(281, 'NECESIDADES EDUCATIVAS ESPECIALES', 33),
(282, 'ARTESANÍAS CON HOJAS DE MAÍZ', 33),
(283, 'DISEÑO DE PATRONES', 33),
(284, 'TRAZO Y DISEÑO DE PRENDAS DE VESTIR', 33),
(285, 'INGLÉS BÁSICO', 33),
(286, 'Formación de instructores en desarrollo humano, habilidades blandas y habilidades gerenciales', 33),
(287, 'HERRAMIENTAS PEDAGOGICAS DE INGLES AVANZADO', 33),
(288, 'ADMINISTRACIÓN DE COSTOS', 33),
(289, 'ADMINISTRACIÓN, CONTABILIDAD BÁSICA COMO SACAR COSTOS DE PRODUCCIÓN', 33),
(290, 'ELABORACIÓN DEL ESTRACTO DEL PROPOLEO, ELABORACIÓN DEL PROPOLEO CON MIEL', 33),
(291, 'HERRAMIENTAS Y EQUIPO DE TRABAJO ', 33),
(292, 'MANEJO Y USO DE HERAMIENTAS ', 33),
(293, 'INGLÉS ', 33),
(294, 'COCTELERÍA', 33),
(295, 'BORDADO DE FANTASÍA ', 33),
(296, 'COLORIMETRIA Y APLICACIÓN DE UÑAS ACRILICAS', 33),
(297, 'CORTE Y APLICACIÓN DE UÑAS ACRÍLICAS', 33),
(298, 'DISEÑOS TEXTILES DE NAVIDAD', 33),
(299, 'PANADERIA Y REPOSTERIA', 33),
(300, 'MAQUILLAJE Y PINTA CARITAS', 33),
(301, 'MAQUILLAJE Y PEINADO', 33),
(302, 'TEJIDO A MANO II', 33),
(303, 'REDACCIÓN Y ELABORACIÓN DE TEXTOS', 33),
(304, 'DECORACIONES NAVIDEÑAS', 33),
(305, 'INGLES COMUNICATIVO', 33),
(306, 'CORTES MODERNOS DE CABELLO', 33),
(307, ' Preparación y Conservación de Alimentos Derivados de la Carne de Cerdo', 33),
(308, ' Preparación y Conservación de Alimentos Derivados de la Carne de Cerdo', 33),
(309, 'Administración I', 33),
(310, 'BISUTERIA', 33),
(311, 'ARTESANÍAS CON HOJA DE MAÍZ', 33),
(312, 'PATERNIDAD AFECTIVA ', 33),
(313, 'PATERNIDAD AFECTIVA ', 33),
(314, 'OFIMÁTICA', 33),
(315, 'PLANEACIÓN DIDÁCTICA Y SU APLICACIÓN', 33),
(316, 'BORDADO CON LISTÓN', 33),
(317, 'INSTALACIÓN DE CALENTADOR SOLAR', 33),
(318, 'WORD, EXCEL AVANZADO E INTERNET', 33),
(319, 'FIGURAS DE ALTA DECORACIÓN', 33),
(320, 'MANEJO HIGIÉNICO DE ALIMENTOS', 33),
(321, 'REPÓSTERIA Y PANADERÍA', 33),
(322, 'DECORACIÓN DE UÑAS', 33),
(323, 'APLICACIÓN DE UÑAS', 33),
(324, 'CORTE Y PEINADO DEL CABELLO', 33),
(325, 'CORTE Y PEINADO DEL CABELLO', 33),
(326, 'HERRAMIENTAS Y EQUIPO DE TRABAJO', 33),
(327, 'SERVICIO Y COMUNICACIÓN CON EL CLIENTE', 33),
(328, 'DIBUJO Y RETOQUE FOTOGRÁFICO POR COMPUTADORA ', 33),
(329, 'MANEJO SANITARIO DE GRANJAS POR ETAPA PRODUCTIVA', 33),
(330, 'CONTROLADORES LÓGICOS PROGRAMABLES', 33),
(331, 'ELECTRICIDAD BÁSICA ', 33),
(332, 'ARREGLOS FLORALES', 33),
(333, 'PROCESOS DE GESTIÓN ADMINISTRATIVA', 33),
(334, 'REPOSTERÍA NAVIDEÑA', 33),
(335, 'TENDENCIAS EN MAQUILLAJE', 33),
(336, 'COMUNICACIÓN RELACIONES HUMANAS Y ATENCIÓN AL PUBLICO', 33),
(337, 'CORTE DE CABELLO', 33),
(338, 'PANADERÍA Y REPOSTERÍA', 33),
(339, 'APLICACIÓN DE UÑAS', 33),
(340, 'CORTE DEL CABELLO', 33),
(341, 'ADMINISTRACIÓN Y CONTABILIDAD BÁSICA', 33),
(342, 'BORDADO DE FANTASÍA', 33),
(343, 'Comunicación Asertiva y Formación de Instructores', 33),
(344, 'Sensibilización del Agente Educativo', 33),
(345, 'DISEÑO Y CONFECCIÓN DE ROPA INDUSTRIAL', 33),
(346, 'ALFARERIA', 33),
(347, 'DULCES ARTESANALES CON FIGURAS DE BOMBÓN Y CHOCOLATE', 33),
(348, 'REPOSTERÍA FINA', 33),
(349, 'OPERACIÓN DE MONTACARGAS', 33),
(350, 'PREPARACIÓN DE ALIMENTOS Y BEBIDAS', 33),
(351, 'COLOCACIÓN DE UÑAS ESCULTURALES AVANZADO', 33),
(352, 'DISEÑOS EN MADERA II', 33),
(353, 'ARTESANÍAS CON MALLA', 33),
(354, 'DECORACIÓN DE GLOBO, BOMBON Y CHOCOLATE', 33),
(355, 'FLORISTERIA', 33),
(356, 'CONOCIMIENTOS BÁSICOS DE CARPINTERÍA	', 33),
(357, 'APLICACIÓN Y DECORACIÓN DE UÑAS	', 33),
(358, 'PASTELES FINOS Y DECORACIÓN ', 33),
(359, 'INGLÉS AVANZADO', 33),
(360, 'TENDENCIAS EN CORTE Y PEINADO DEL CABELLO', 33),
(361, 'ELABORACIÓN DE BEBIDAS Y COCTELERÍA ', 33),
(362, 'OPERACIÓN DE TABLA DE DATOS EN EXCEL', 33),
(363, 'BORDADO, DESHILADO Y BISUTERÍA ', 33),
(364, 'INGLÉS BÁSICO AVANZADO', 33),
(365, 'FIGURAS DE GLOBO, LISTÓN, TELA Y PAPEL 2 ', 33),
(366, 'PANADERIA Y PASTELERIA', 33),
(367, 'COCINA MEXICANA', 33),
(368, 'PREPARACIÓN DE PASTELES', 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos2018_cae`
--

CREATE TABLE `cursos2018_cae` (
  `folio` int(11) NOT NULL,
  `curso` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `calle_c` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia_c` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `total_horas` int(11) DEFAULT NULL,
  `h_lunes_ini` time DEFAULT NULL,
  `h_lunes_fin` time DEFAULT NULL,
  `h_martes_ini` time DEFAULT NULL,
  `h_martes_fin` time DEFAULT NULL,
  `h_miercoles_ini` time DEFAULT NULL,
  `h_miercoles_fin` time DEFAULT NULL,
  `h_jueves_ini` time DEFAULT NULL,
  `h_jueves_fin` time DEFAULT NULL,
  `h_viernes_ini` time DEFAULT NULL,
  `h_viernes_fin` time DEFAULT NULL,
  `h_sabado_ini` time DEFAULT NULL,
  `h_sabado_fin` time DEFAULT NULL,
  `h_domingo_ini` time DEFAULT NULL,
  `h_domingo_fin` time DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_instructor` int(11) NOT NULL,
  `id_plantel` int(11) NOT NULL,
  `id_mun_c` int(11) NOT NULL,
  `observaciones_c` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos2018_cae`
--

INSERT INTO `cursos2018_cae` (`folio`, `curso`, `duracion`, `calle_c`, `colonia_c`, `fecha_inicio`, `fecha_fin`, `total_horas`, `h_lunes_ini`, `h_lunes_fin`, `h_martes_ini`, `h_martes_fin`, `h_miercoles_ini`, `h_miercoles_fin`, `h_jueves_ini`, `h_jueves_fin`, `h_viernes_ini`, `h_viernes_fin`, `h_sabado_ini`, `h_sabado_fin`, `h_domingo_ini`, `h_domingo_fin`, `status`, `id_instructor`, `id_plantel`, `id_mun_c`, `observaciones_c`) VALUES
(1, 'Curso CAE', 250, 'Presidencia Municipal editada', 'Colonia nueva editada', '2018-02-04', '2018-02-15', 20, '10:00:00', '12:00:00', '10:00:00', '12:00:00', '00:00:00', '00:00:00', '13:00:00', '15:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '17:00:00', '20:00:00', 'Asignado', 383, 2, 22, ''),
(2, 'Programación', 13, 'asd', 'asd123', '2018-07-30', '2018-07-03', NULL, '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 107, 2, 4, ''),
(3, 'Programación xd', 15, 'del brayan', 'del brayan xd', '2018-08-07', '2018-08-08', NULL, '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1787, 2, 33, '(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.) Editado gg otra vez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos2018_ebc`
--

CREATE TABLE `cursos2018_ebc` (
  `folio` int(11) NOT NULL,
  `id_campo_c` int(11) DEFAULT NULL,
  `id_especialidad_c` int(11) DEFAULT NULL,
  `id_modulo_c` int(11) DEFAULT NULL,
  `calle_c` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia_c` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `total_horas` int(11) DEFAULT NULL,
  `h_lunes_ini` time DEFAULT NULL,
  `h_lunes_fin` time DEFAULT NULL,
  `h_martes_ini` time DEFAULT NULL,
  `h_martes_fin` time DEFAULT NULL,
  `h_miercoles_ini` time DEFAULT NULL,
  `h_miercoles_fin` time DEFAULT NULL,
  `h_jueves_ini` time DEFAULT NULL,
  `h_jueves_fin` time DEFAULT NULL,
  `h_viernes_ini` time DEFAULT NULL,
  `h_viernes_fin` time DEFAULT NULL,
  `h_sabado_ini` time DEFAULT NULL,
  `h_sabado_fin` time DEFAULT NULL,
  `h_domingo_ini` time DEFAULT NULL,
  `h_domingo_fin` time DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_instructor` int(11) NOT NULL,
  `id_plantel` int(11) NOT NULL,
  `id_mun_c` int(11) NOT NULL,
  `observaciones_c` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos2018_ebc`
--

INSERT INTO `cursos2018_ebc` (`folio`, `id_campo_c`, `id_especialidad_c`, `id_modulo_c`, `calle_c`, `colonia_c`, `fecha_inicio`, `fecha_fin`, `total_horas`, `h_lunes_ini`, `h_lunes_fin`, `h_martes_ini`, `h_martes_fin`, `h_miercoles_ini`, `h_miercoles_fin`, `h_jueves_ini`, `h_jueves_fin`, `h_viernes_ini`, `h_viernes_fin`, `h_sabado_ini`, `h_sabado_fin`, `h_domingo_ini`, `h_domingo_fin`, `status`, `id_instructor`, `id_plantel`, `id_mun_c`, `observaciones_c`) VALUES
(1, 1, 1, 2, 'Presidencia de Comunidad', '', '2018-02-02', '2018-02-27', 0, '10:00:00', '15:00:00', '00:00:00', '00:00:00', '15:00:00', '20:00:00', '00:00:00', '00:00:00', '17:00:00', '19:00:00', '00:00:00', '00:00:00', '17:00:00', '19:00:00', 'Asignado', 1812, 3, 61, ''),
(3, 7, 7, 31, 'Papalotla', '', '2018-06-29', '2018-06-29', NULL, '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 107, 4, 61, ''),
(4, 5, 3, 12, 'del brayan editado', 'del brayan otra vez editado', '2018-07-30', '2018-07-30', NULL, '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1714, 2, 4, ''),
(5, 6, 4, 16, 'del brayan', 'del brayan', '2018-08-07', '2018-08-07', NULL, '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1821, 2, 45, '(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.) Otra vez ninguna 2\r\n\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos2018_extension`
--

CREATE TABLE `cursos2018_extension` (
  `folio` int(11) NOT NULL,
  `curso` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `calle_c` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia_c` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `h_lunes_ini` time DEFAULT NULL,
  `h_lunes_fin` time DEFAULT NULL,
  `h_martes_ini` time DEFAULT NULL,
  `h_martes_fin` time DEFAULT NULL,
  `h_miercoles_ini` time DEFAULT NULL,
  `h_miercoles_fin` time DEFAULT NULL,
  `h_jueves_ini` time DEFAULT NULL,
  `h_jueves_fin` time DEFAULT NULL,
  `h_viernes_ini` time DEFAULT NULL,
  `h_viernes_fin` time DEFAULT NULL,
  `h_sabado_ini` time DEFAULT NULL,
  `h_sabado_fin` time DEFAULT NULL,
  `h_domingo_ini` time DEFAULT NULL,
  `h_domingo_fin` time DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_instructor` int(11) NOT NULL,
  `id_plantel` int(11) NOT NULL,
  `id_mun_c` int(11) NOT NULL,
  `observaciones_c` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos2018_extension`
--

INSERT INTO `cursos2018_extension` (`folio`, `curso`, `duracion`, `calle_c`, `colonia_c`, `fecha_inicio`, `fecha_fin`, `h_lunes_ini`, `h_lunes_fin`, `h_martes_ini`, `h_martes_fin`, `h_miercoles_ini`, `h_miercoles_fin`, `h_jueves_ini`, `h_jueves_fin`, `h_viernes_ini`, `h_viernes_fin`, `h_sabado_ini`, `h_sabado_fin`, `h_domingo_ini`, `h_domingo_fin`, `status`, `id_instructor`, `id_plantel`, `id_mun_c`, `observaciones_c`) VALUES
(1, 'Curso de Extensión', 120, 'Presidencia de Comunidad editada', 'Colonia nueva', '2018-02-06', '2018-02-27', '10:00:00', '13:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '14:00:00', '17:00:00', '00:00:00', '00:00:00', '10:00:00', '12:00:00', '09:00:00', '12:00:00', 'Sugerido', 134, 10, 22, ''),
(2, 'Programación editada', 63, 'del brayan editada', 'drl brayan otra vez editada', '2018-07-30', '2018-07-30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1700, 2, 22, ''),
(3, 'Programación', 10, 'del brayan', 'del brayan ora vez', '2018-08-07', '2018-08-07', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1298, 2, 3, '(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.) prueba gg 2\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos2018_regular`
--

CREATE TABLE `cursos2018_regular` (
  `folio` int(11) NOT NULL,
  `id_campo_c` int(11) DEFAULT NULL,
  `id_especialidad_c` int(11) DEFAULT NULL,
  `id_modulo_c` int(11) DEFAULT NULL,
  `calle_c` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia_c` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `h_lunes_ini` time DEFAULT NULL,
  `h_lunes_fin` time DEFAULT NULL,
  `h_martes_ini` time DEFAULT NULL,
  `h_martes_fin` time DEFAULT NULL,
  `h_miercoles_ini` time DEFAULT NULL,
  `h_miercoles_fin` time DEFAULT NULL,
  `h_jueves_ini` time DEFAULT NULL,
  `h_jueves_fin` time DEFAULT NULL,
  `h_viernes_ini` time DEFAULT NULL,
  `h_viernes_fin` time DEFAULT NULL,
  `h_sabado_ini` time DEFAULT NULL,
  `h_sabado_fin` time DEFAULT NULL,
  `h_domingo_ini` time DEFAULT NULL,
  `h_domingo_fin` time DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_instructor` int(11) NOT NULL,
  `id_plantel` int(11) NOT NULL,
  `id_mun_c` int(11) NOT NULL,
  `observaciones_c` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos2018_regular`
--

INSERT INTO `cursos2018_regular` (`folio`, `id_campo_c`, `id_especialidad_c`, `id_modulo_c`, `calle_c`, `colonia_c`, `fecha_inicio`, `fecha_fin`, `h_lunes_ini`, `h_lunes_fin`, `h_martes_ini`, `h_martes_fin`, `h_miercoles_ini`, `h_miercoles_fin`, `h_jueves_ini`, `h_jueves_fin`, `h_viernes_ini`, `h_viernes_fin`, `h_sabado_ini`, `h_sabado_fin`, `h_domingo_ini`, `h_domingo_fin`, `status`, `id_instructor`, `id_plantel`, `id_mun_c`, `observaciones_c`) VALUES
(1, 26, 51, 207, 'Centro de la Comunidad', '', '2018-02-10', '2018-02-24', '09:00:00', '12:00:00', '00:12:00', '00:12:00', '09:00:00', '12:00:00', '00:12:00', '00:12:00', '00:12:00', '00:12:00', '00:12:00', '00:12:00', '15:00:00', '20:00:00', 'Aprobado', 195, 2, 61, ''),
(3, 4, 2, 6, 'Papalotla', '', '2018-06-28', '2018-06-29', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Sugerido', 1812, 4, 61, ''),
(5, 13, 21, 87, 'tlaxcalaxxd', '', '2018-06-29', '2018-06-30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '04:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Sugerido', 1812, 5, 61, ''),
(6, 13, 19, 78, 'San PAblo', '', '2018-07-12', '2018-07-12', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1807, 5, 61, ''),
(7, 1, 1, 2, 'del brayan editada', 'del brayan otra vez editada', '2018-07-30', '2018-07-30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Sugerido', 1072, 2, 4, ''),
(8, 6, 4, 18, 'CHIAU', 'TEMPMAN', '2018-08-08', '2018-08-09', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Asignado', 1072, 2, 10, 'nuevo '),
(9, 5, 3, 13, 'CHIAU', 'TEMPMAN', '2018-08-08', '2018-08-09', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1072, 2, 10, '(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.) NO TENGO XD\r\nAhora si tengo gg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos2018_roco`
--

CREATE TABLE `cursos2018_roco` (
  `folio` int(11) NOT NULL,
  `id_campo_c` int(11) DEFAULT NULL,
  `id_especialidad_c` int(11) DEFAULT NULL,
  `id_modulo_c` int(11) DEFAULT NULL,
  `calle_c` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia_c` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `h_lunes_ini` time DEFAULT NULL,
  `h_lunes_fin` time DEFAULT NULL,
  `h_martes_ini` time DEFAULT NULL,
  `h_martes_fin` time DEFAULT NULL,
  `h_miercoles_ini` time DEFAULT NULL,
  `h_miercoles_fin` time DEFAULT NULL,
  `h_jueves_ini` time DEFAULT NULL,
  `h_jueves_fin` time DEFAULT NULL,
  `h_viernes_ini` time DEFAULT NULL,
  `h_viernes_fin` time DEFAULT NULL,
  `h_sabado_ini` time DEFAULT NULL,
  `h_sabado_fin` time DEFAULT NULL,
  `h_domingo_ini` time DEFAULT NULL,
  `h_domingo_fin` time DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_instructor` int(11) NOT NULL,
  `id_plantel` int(11) NOT NULL,
  `id_mun_c` int(11) NOT NULL,
  `observaciones_c` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos2018_roco`
--

INSERT INTO `cursos2018_roco` (`folio`, `id_campo_c`, `id_especialidad_c`, `id_modulo_c`, `calle_c`, `colonia_c`, `fecha_inicio`, `fecha_fin`, `h_lunes_ini`, `h_lunes_fin`, `h_martes_ini`, `h_martes_fin`, `h_miercoles_ini`, `h_miercoles_fin`, `h_jueves_ini`, `h_jueves_fin`, `h_viernes_ini`, `h_viernes_fin`, `h_sabado_ini`, `h_sabado_fin`, `h_domingo_ini`, `h_domingo_fin`, `status`, `id_instructor`, `id_plantel`, `id_mun_c`, `observaciones_c`) VALUES
(1, 14, 22, 92, 'Kings Landing', '', '2018-02-01', '2018-02-28', '08:00:00', '10:00:00', '00:00:00', '00:00:00', '09:00:00', '11:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '10:00:00', '20:00:00', 'Asignado', 383, 6, 61, ''),
(2, 14, 22, 92, 'Papalotlaxd', '', '2018-06-29', '2018-06-30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Asignado', 262, 1, 61, ''),
(3, 14, 22, 92, 'tlaxcalaxxd', '', '2018-06-29', '2018-06-29', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Asignado', 1411, 5, 61, ''),
(4, 4, 2, 8, 'del brayan editado', 'del brayan otra vez editado', '2018-07-30', '2018-07-30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 738, 2, 4, ''),
(5, 5, 3, 14, 'asd', 'asdd', '2018-08-09', '2018-08-22', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 'Aprobado', 1463, 2, 45, '(Escriba aquí las observaciones que tenga sobre el curso y elimine este texto.) editado 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distritos`
--

CREATE TABLE `distritos` (
  `id` int(11) NOT NULL,
  `id_municipio` int(11) DEFAULT NULL,
  `distrito` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `distritos`
--

INSERT INTO `distritos` (`id`, `id_municipio`, `distrito`) VALUES
(1, 1, 8),
(2, 2, 8),
(3, 3, 2),
(4, 4, 11),
(5, 5, 4),
(6, 5, 5),
(7, 6, 1),
(8, 7, 11),
(9, 8, 11),
(10, 9, 3),
(11, 10, 8),
(12, 10, 9),
(13, 11, 5),
(14, 12, 5),
(15, 13, 10),
(16, 13, 11),
(17, 14, 5),
(18, 15, 6),
(19, 16, 10),
(20, 17, 12),
(21, 18, 8),
(22, 19, 14),
(23, 20, 1),
(24, 21, 1),
(25, 22, 12),
(26, 23, 14),
(27, 24, 6),
(28, 25, 15),
(29, 26, 8),
(30, 27, 15),
(31, 28, 12),
(32, 29, 13),
(33, 30, 3),
(34, 31, 2),
(35, 32, 14),
(36, 33, 7),
(37, 34, 2),
(38, 35, 3),
(39, 36, 6),
(40, 37, 10),
(41, 38, 3),
(42, 39, 3),
(43, 40, 5),
(44, 41, 12),
(45, 42, 13),
(46, 43, 5),
(47, 44, 13),
(48, 45, 4),
(49, 46, 3),
(50, 47, 3),
(51, 48, 12),
(52, 49, 14),
(53, 50, 12),
(54, 51, 14),
(55, 52, 3),
(56, 53, 14),
(57, 54, 13),
(58, 55, 5),
(59, 56, 14),
(60, 57, 14),
(61, 58, 12),
(62, 58, 13),
(63, 59, 12),
(64, 60, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidades`
--

CREATE TABLE `especialidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `especialidades`
--

INSERT INTO `especialidades` (`id`, `nombre`) VALUES
(1, 'Administración'),
(2, 'Contabilidad'),
(3, 'Alimentos y bebidas'),
(4, 'Artesanías con Fibras Textiles'),
(5, 'Artesanías de alta precisión'),
(6, 'Artesanías con pastas, pinturas y acabados'),
(7, 'Artesanías Metálicas'),
(8, 'Asistencia Educativa'),
(9, 'Asistencia Ejecutiva'),
(10, 'Gestión Y venta de Servicios Turísticos'),
(11, 'Producción Industrial de Alimentos'),
(12, 'Hotelería'),
(13, 'Inglés'),
(14, 'Informática'),
(15, 'Mantenimiento de Equipos y Sistemas Computacionales'),
(16, 'Estilismo y Bienestar personal'),
(17, 'Diseño y Fabricación de Muebles de Madera'),
(18, 'Diseño y Elaboración de cerámica'),
(19, 'Diseño e imagen de la carrocería'),
(20, 'Diseño de Modas'),
(21, 'Sastrería'),
(22, 'Confección Industrial de Ropa'),
(23, 'Floristería'),
(24, 'Mecatrónica'),
(25, 'Electricidad'),
(26, 'Electrónica Automotriz'),
(27, 'Instalaciones Hidráulicas y de Gas'),
(28, 'Mantenimiento Industrial'),
(29, 'Mantenimiento de Maquinas de Costura'),
(30, 'Mecánica Automotriz'),
(31, 'Mecánica Diésel'),
(32, 'Soldadura y Palería'),
(33, 'N/A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructores`
--

CREATE TABLE `instructores` (
  `id` int(11) NOT NULL,
  `Expediente` int(11) DEFAULT NULL,
  `imagen` varchar(250) NOT NULL,
  `Curp` varchar(20) DEFAULT NULL,
  `Nombre` varchar(80) DEFAULT NULL,
  `Calle` varchar(80) DEFAULT NULL,
  `Colonia_barrio` varchar(100) DEFAULT NULL,
  `Localidad` varchar(100) DEFAULT NULL,
  `CP` int(11) DEFAULT NULL,
  `Seccion_elec` int(11) DEFAULT NULL,
  `Estado_civil` varchar(25) DEFAULT NULL,
  `Sexo` varchar(10) DEFAULT NULL,
  `F_Nacimiento` varchar(20) DEFAULT NULL,
  `F_Alta` varchar(20) DEFAULT NULL,
  `F_Baja` varchar(20) DEFAULT NULL,
  `Pefil` varchar(100) DEFAULT NULL,
  `Nivel_academico` varchar(100) DEFAULT NULL,
  `Especialidad` text,
  `Fecha_evaluacion` varchar(20) DEFAULT NULL,
  `Puntaje` float DEFAULT NULL,
  `No_celular` varchar(30) DEFAULT NULL,
  `No_telefono` varchar(30) DEFAULT NULL,
  `Email` varchar(80) DEFAULT NULL,
  `Observaciones` text,
  `calidad` varchar(200) NOT NULL,
  `id_municipios` int(11) NOT NULL,
  `condicion` varchar(255) NOT NULL,
  `acta_doc` varchar(255) NOT NULL,
  `curp_doc` varchar(255) NOT NULL,
  `dom_doc` varchar(255) NOT NULL,
  `ine_doc` varchar(255) NOT NULL,
  `expfg_doc` varchar(255) NOT NULL,
  `expl_doc` varchar(255) NOT NULL,
  `cv_doc` varchar(255) NOT NULL,
  `actualizacion_doc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instructores`
--

INSERT INTO `instructores` (`id`, `Expediente`, `imagen`, `Curp`, `Nombre`, `Calle`, `Colonia_barrio`, `Localidad`, `CP`, `Seccion_elec`, `Estado_civil`, `Sexo`, `F_Nacimiento`, `F_Alta`, `F_Baja`, `Pefil`, `Nivel_academico`, `Especialidad`, `Fecha_evaluacion`, `Puntaje`, `No_celular`, `No_telefono`, `Email`, `Observaciones`, `calidad`, `id_municipios`, `condicion`, `acta_doc`, `curp_doc`, `dom_doc`, `ine_doc`, `expfg_doc`, `expl_doc`, `cv_doc`, `actualizacion_doc`) VALUES
(17, 911, '', 'HEHS541126HTLRRL03', 'JOSÉ SILVESTRE  HERNÁNDEZ HERNÁNDEZ', 'C. FRCO. I MADERO NO. 74', 'SECC. SEGUNDA', 'SECC. SEGUNDA', 90620, 0, 'CASADO (A)', 'H', '11/26/1954', '04/25/2016', '', 'T', 'TECNICO', 'MEC', '05/20/2011', 88, '', '    46 1 02 53', '', 'sin observaciones', '0', 1, '', '', '', '', '', '', '', '', '0'),
(26, 858, '', 'MAGD741224HTLLLL09', 'DELFINO  MALDONADO GALICIA', 'CUAUHTEMMOC 84', 'SECCI', 'SEGUNDA SECC.', 90670, 0, 'CASADO (A)', 'H', '12/24/1974', '01/19/2016', '', 'T', 'DIPLOMADO', 'ADMINISTRAC', '05/19/2011', 84, '241 130 05 10', '246 45 95749', '', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(56, 943, '', 'PECL860728MTLRNZ06', 'LIZBETH PÉREZ CANO', 'C. IGNACIO CARRANZA NO. 170', 'null', 'CENTRO', 90700, 0, 'SOLTERO (A', 'M', '07/28/1986', '', '', 'LICENCIADO EN GASTRONOM', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '05/26/2011', 84.5, '248 102 7819', '248-48-7-00-89', 'liza_2807@hotmail.com', 'sin observaciones', '0', 19, '', '', '', '', '', '', '', '', '0'),
(71, 955, '', 'MUMR600904MTLXDS05', 'ROSA MUÑOZ MEDEL', 'AV. PUEBLA NORTE NO. 38', 'SECCI', 'SECC. SEGUNDA', 90740, 0, 'CASADO (A)', 'M', '05/26/1960', '', '', 'T', 'SECUNDARIA', 'ARTESAN', '05/26/2011', 80, '2461439932', '', '', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(76, 961, '', 'HEHG720105MTLRRL07', 'GLORIA HERNÁNDEZ HERNÁNDEZ', 'CALLE JOSEFA ORT', 'null', 'SAN SIM', 90447, 0, 'CASADO (A)', 'M', '01/05/1972', '01/28/2013', '', 'T', '', 'ESTILISMO Y BIENESTAR PERSONAL', '06/08/2011', 88, '241 121 92 42', ' 01 241 41 5 47 52', 'MARILYNGMM@HOTMAIL.COM', 'sin observaciones', '0', 40, '', '', '', '', '', '', '', '', '0'),
(86, 968, '', 'CECE760603MDFRBR00', 'ERIKA VIOLETA CERVANTES CABRERA', 'CALLE ZARAGOZA NO. 2', 'null', 'GUADALUPE VICTORIA', 90710, 0, 'CASADO (A)', 'M', '06/03/1976', '06/20/2013', '', 'T', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '06/08/2011', 91, '246-494-22-14', '01-246-41-6-2018', 'ERIKA_VIOLETA_INSTRUCTORA@HOTMAIL.C', 'sin observaciones', '0', 23, '', '', '', '', '', '', '', '', '0'),
(104, 72, '', 'PEQC620828MDFRZC05', 'CECILIA CAROLINA PÉREZ QUEZADA', 'PLAZA DEL CENTRO EDIF. B-1 DEPTO.3 ', 'PETROQUIMICA', 'SAN PABLO APETATITLAN', 90600, 8, 'SOLTERO (A', 'F', '08/28/1962', '02/14/2013', '', 'INSTRUCTORA DE INGLES', 'TECNICO', 'INGLES/COMPUTACIÓN', '06/13/2011', 89, '246 127 3723', '46 4 70 17', 'polito_6862@hotmail.com', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(107, 985, '', 'SACR851111MTLSRC10', '\r\nMA. DEL ROCÍO SASTRÉ CERERO', 'CALLE HIDALGO NO. 59', 'null', 'GUADALUPE IXCOTLA', 90810, 0, 'null', 'M', '11/11/1985', '10/24/2013', '', 'LIC. LENGUAS MODERNAS APLICADAS', 'LICENCIATURA', 'INGL', '06/13/2011', 88.5, '246 141 5376', '', 'chio11_1@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(111, 989, '', 'VAMR820607HTLZLB06', 'ROBERTO VÁZQUEZ MELÉNDEZ', 'C. 10 DE MAYO NO. 6', 'BARR. XAXALA', 'BARR. XAXALA', 90800, 0, 'SOLTERO (A', 'H', '06/07/1982', '07/25/2011', '', 'T', 'BACHILLERATO', 'MEC', '06/13/2011', 85.5, '246-1136894', '  101 8106', 'robertovazquezmelendez@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(121, 318, '', 'MEFV720520MPLDNR02', 'VERÓNICA MEDINA FENTÁNEZ', 'AV. INDUSTRIAS ORIENTE  NO. 30', '', 'PANZACOLA', 90796, 7, 'CASADA ', 'F', '20/05/1972', '04/11/2016', '', 'BELLEZA', 'SUB-PROFECIONAL ', 'ESTILISMO Y BIENESTAR PERSONAL', '06/13/2011', 83, '0442221569595', '012222632054', 'vero_yahin@yahoo.com.mx', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(134, 1008, '', 'TIMC841227MTLZNR03', 'MARIA DEL CARMEN TIZAPAN MENDOZA', 'PRIVADA DE ALLENDE PONIENTE NO. 80-7', NULL, 'BARRIO XIMENTLA', 90800, 0, 'SOLTERO (A', 'F', '12/27/1984', '06/15/2016', '', 'LICENCIADO EN LENGUAS MODERNAS APLICADAS; T', 'LICENCIATURA', 'INGLES', '06/16/2011', 93, '246 494 7171', '246 464 0190', 'car27_1@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(140, 1013, '', 'NAGG641213MTLVRD00', 'MA. GUADALUPE NAVA GARCÍA', 'JOSEFA ORT', 'null', 'CENTRO', 90300, 0, 'SOLTERO (A', 'M', '12/13/1964', '03/13/2013', '', 'NORMAL DE PRE-ESCOLAR', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '06/16/2011', 88, '', '01 241 123 0856', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(141, 1014, '', 'TOMA850502MTLRDN00', 'MARIA ANGÉLICA TORRES MADIN', 'AV. FERROCARRIL NO. 8', 'CENTRO', 'COL. CENTRO', 90200, 0, 'SOLTERO (A', 'M', '05/02/1985', '06/09/2014', '', 'LICENCIADO EN LENGUAS MODERNAS APLICADAS', 'LICENCIATURA', 'INGL', '06/16/2011', 87.5, '045 749 105 0445', '918 1965', 'angitm@hotmail.com', 'sin observaciones', '0', 6, '', '', '', '', '', '', '', '', '0'),
(152, 1025, '', 'LOPA650604MMCPRN07', 'M. DE LOS ÁNGELES LÓPEZ PÉREZ', 'C. ESPERANZA NORTE NO. 43', 'SECC. 2DA', 'SECCI', 90740, 0, 'SOLTERO (A', 'M', '06/04/1965', '04/26/2013', '', 'T', 'BACHILLERATO', 'ALIMENTOS Y BEBIDAS', '06/16/2011', 82, '2461236492', '49 7 59 61', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(157, 217, '', 'RORT710307MTLDTM03', 'TOMASA RODRÍGUEZ RETAMA', 'C. JU', 'null', 'TECOMALUCAN', 90250, 0, 'CASADO (A)', 'M', '03/07/1970', '08/19/2013', '', 'T', 'SECUNDARIA', 'ESTILISMO Y BIENESTAR PERSONAL', '06/16/2011', 81, '241 124 22 64', '01 241 49 6 31 73', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(166, 1035, '', 'PELP741011MTLRNL00', 'PILAR PÉREZ LUNA', 'C. HIDALGO NO. 23', 'null', 'SAN JUAN HUACTZINCO', 90190, 0, 'CASADO (A)', 'M', '10/11/1974', '05/17/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '06/22/2011', 89, '246 49 7 55 74', '', 'phili_74@hotmail.com', 'sin observaciones', '0', 53, '', '', '', '', '', '', '', '', '0'),
(175, 1044, '', 'ZELX671219MTLMPC06', 'XOCHITL ZEMPOALTECA LÓPEZ', 'CALLE TEPETICPAC NO. 19 INT. 2', 'null', 'COL. SAN ISIDRO LA LOMA', 90060, 0, 'CASADO (A)', 'M', '12/19/1967', '', '', 'T', 'BACHILLERATO', 'ARTESAN', '06/22/2011', 80, '246 110 4837', '', 'ZEMPLO@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(178, 785, '', 'MOME801107HPLRRR09', 'ERNESTO MORALES MARTÍNEZ', 'ALVARO OBREGON No 5', '', ' SAN BUENAVENTURA', 90790, 6, 'SOLTERO (A', 'M', '07/11/1980', '01/29/2013', '', '', 'TECNICO', 'ARTESANIAS CON MACRAME ', '06/23/2011', 89, '', '2222811996', '', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(185, 1052, '', 'GUOI690513MTLTRM02', 'IMELDA GUTIÉRREZ ORTIZ', 'AVENIDA TRINIDAD N', '', 'L', 90620, 0, 'CASADO (A)', 'M', '05/13/1969', '09/03/2013', '', 'TÉCNICO EN ARTESANÍAS FAMILIARES  ', 'TECNICO', 'ARTESANIAS CON FIBRAS TEXTILES', '09/09/2015', 85, '246-161-80-60', '49 7 14 97 ', 'imeldatauro@hotmail.com', 'sin observaciones', '0', 1, '', '', '', '', '', '', '', '', '0'),
(187, 1054, '', 'RECA850824MPLYSN01', 'ANGÉLICA REYES CASTILLO', 'CERRADA ZAHUAPAN S/N', 'null', ' COL. ITURBIDE', 90250, 0, 'SOLTERO (A', 'M', '08/24/1985', '', '', 'LIC. EN INFORM', 'LICENCIATURA', 'INFORM', '06/23/2011', 80.5, '241 414 4798', '241 496 1031', 'agryes@hotmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(188, 1055, '', 'COPE680328MTLRRS09', 'MARÍA ESPERANZA CORTE PÉREZ', 'CALLE ADOLFO L', NULL, 'SECC. 4A', 90740, 0, 'CASADO (A)', 'F', '03/28/1968', '09/05/2016', '', 'SECUNDARIA', 'SECUNDARIA', 'ALIMENTOS Y BEBIDAS', '06/23/2011', 80, '246 106 0710', '49 7 40 22', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(193, 1059, '', 'ROMG780219MPLDRD04', 'GUADALUPE RODRÍGUEZ MARTÍNEZ', 'ANDADOR CHINITA EDIF. 20-3', 'null', ' UNIDAD HAB. SANTA CRUZ', 90550, 0, 'CASADO (A)', 'M', '02/19/1978', '09/24/2013', '', 'LICENCIATURA PSICOLOG', 'LICENCIATURA', 'ADMINISTRAC', '06/30/2011', 86, '2461627084', '15 22 086', 'ludis_romart@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(195, 200, '', 'MOFL690616MTLRGT07', 'LETICIA MORENO FIGUEROA', 'AV. DE LA CONCEPCION No 10', '', ' SANTA JUSTINA ECATEPEC', 90123, 7, 'SOLTERO (A ', 'F', '16/06/1969', '05/03/2013', '', 'ADMNISTRACION DE EMPRESAS', 'BACHILLERATO TECNICO', 'SERVICIOS DE BELLEZA ', '06/30/2011', 85, '2461399245', '012484815830', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(201, 1064, '', 'MIGR780930MTLRRB04', 'MARÍA RUBIELA GISSELA MIRANDA GEORGE', 'C. CUAHUTEMOC NO. 11', 'null', 'SAN ESTEBAN TIZATL', 90100, 0, 'CASADO (A)', 'M', '09/30/1978', '08/05/2016', '', 'ING. INDUSTRAIL', 'LICENCIATURA', 'HOTELER', '06/30/2011', 80.5, '246 109 6934', '', 'm2rg2_lea@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(207, 1071, '', 'COPO560526MTLRTL06', 'MARÍA OLIVIA CORTÉZ PATIÑO', 'PRIVADA PERFECTO G', 'null', 'Barrio de Tlapacoya', 90800, 0, 'null', 'M', '05/26/1956', '01/29/2013', '', 'T', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '07/07/2011', 84.5, ' 246 124 6400', '46 4 24 57', 'olicortes@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(211, 44, '', 'LOSV740307MTLPNR09', 'VERÓNICA LÓPEZ SÁNCHEZ', 'CALLE NEGRETE OTE NO. 104', 'CENTRO', 'HUAMANTLA', 90500, 10, 'CASADO (A)', 'M', '07/03/1974', '', '', 'CONFECCION', 'BACHILLERATO', 'CONFECCION INDUSTRIAL DE ROPA', '07/07/2011', 81, '', '01 247 47 2 82 22', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(215, 577, '', 'HERS751008HTLRMR02', 'SERGIO HERNÁNDEZ RAMÍREZ', 'SEPTIMA AVENIDA NO. 505', 'EL CARME ', 'APIZACO ', 90338, 4, 'CASADO (A)', 'M', '08/10/1975', '10/08/2013', '', 'COMPUTACIÓN ADMINISTRATIVA Y CONTABLE', 'TECNICO', 'OPERACIÓN DE MICRO COMPUTADORAS', '06/16/2011', 88, '0442411206285', '2411130988', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(219, 808, '', 'FOFH771013MOCLRL09', 'HILDA PATRICIA FLORES FIERRO', 'HERMENEGILDO GALEANA NO. 9-A', 'null', 'ZARAGOZA', 90160, 0, 'CASADO (A)', 'M', '10/13/1977', '08/26/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '02/09/2017', 85.12, '2461843662', '2383935862', 'patotierro1727@hotmail.com', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(226, 886, '', 'COBA770111MDFRZL00', 'ALEJANDRA CORDERO BAEZ', '21 DE MARZO 308', 'null', 'COL. F', 90300, 0, 'CASADO (A)', 'M', '01/11/1977', '09/23/2013', '', 'LICENCIATURA EN INTERVENCI', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '12/09/2010', 81, '', '2414178432', 'alecorderob@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(230, 815, '', 'COSS721105MTLNLG09', 'SIGELINDA CONDE SALAUZ', 'AV. HIDALGO NO. 6', 'SANTA MARIA ', 'IXTULCO', 90105, 7, '', 'F', '05/11/1972', '01/28/2013', '', 'LICENCIATURA EN TELESECUNDARIA', 'LICENCIATURA', 'REPOSTERIA', '08/01/2016', 89, '0442464930984', '2464647688', 'linda3_3adnil@yahoo.com.mx', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(231, 22, '', 'GAXE700829MTLLCL03', 'ELVA GALICIA XOCHITEMOL', 'CALLE PRINCIPAL 134', '', 'SAN MIGUEL XALTIPAN', 90670, 7, 'CASADO (A)', 'M', '29/08/1970', '12/03/2015', '', 'GERENCIA Y SUPERVICION EN LA INDUSTRIA DEL VESTIDO ', 'TECNICO', 'SASTRERIA', '10/17/2011', 93, '2461284864', '012464158031', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(233, 747, '', 'CAAN661004MTLRBL00', 'NELSI MARÍA CARRO ABDAL', 'AV;JUAREZ  No 81', 'QUINTO BARRIO ', 'PANOTLA', 90140, 6, 'SOLTERO (A', 'F', '04/10/1966', '06/04/2013', '', 'LICENCIATURA EN DOCENCIA TECNOLOGICA', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '07/29/2016', 85, '246 171 6030', '246 4625341  ', '', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(234, 209, '', 'ROHT741015MTLBRR09', 'TERESITA ROBLES HERNÁNDEZ', 'C. HIDALGO NO. 9', '', 'SAN IDELFONSO HUEYOTLIPAN', 90240, 5, 'SOLTERO (A', 'F', '15/10/1974', '08/27/2013', '', 'CULTURA DE BELLEZA', 'TECNICO PROFECIONAL ', 'SERVICIOS DE BELLEZA ', '07/13/2009', 83, '241 112 8386', '241-41-5-00-36', '', 'sin observaciones', '0', 14, '', '', '', '', '', '', '', '', '0'),
(238, 590, '', 'EIPC610925MTLSRL03', 'MA. CLEÓFAS ESPINOZA PÉREZ', 'PRIV. VIOLETA NO. 23', '', 'LA TRINIDA TENEXYECAC', 90121, 6, 'CASADO (A)', 'M', '22/09/1961', '06/19/2014', '', 'CORTE Y CONFECCION ', 'TECNICO', 'CONFECCION INDUSTRIAL DE ROPA', '07/19/2011', 85, '2461046591', '4672902', '', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(240, 1080, '', 'VACJ681022HPLZNM07', 'JAIME VALENTIN VÁZQUEZ CANO', 'C. 21 DE MARZO NO. 8-A', 'null', 'COL. EMILIANO ZAPATA', 90140, 0, 'SOLTERO (A', 'H', '10/22/1968', '08/28/2013', '', 'T', 'PASANTE', 'INFORM', '07/14/2011', 84, '2461229023', '46 2 68 13', 'jassdas@hotmail.com', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(242, 1082, '', 'PATA790404HTLRLL08', 'ALBERTO PAREDES TLACHI', 'C. DOLORES NO. 22', 'null', 'BARRIO DE TEXCACOAC', 90800, 0, 'SOLTERO (A', 'H', '04/04/1979', '02/05/2016', '', 'PASANTE LIC. EN INFORM', 'PASANTE', 'INFORM', '07/14/2011', 83, '246 129 0889', '', 'TLACHIS_98@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(250, 1090, '', 'EASJ910930MTLSNN03', 'JANETH ESCAMILLA SÁNCHEZ', 'MANUEL MOTA NO. 4', 'COLONIA SABINAL', 'TLAXCALA ', 90250, 2, 'CASADO (A)', 'M', '30/09/1991', '03/08/2013', '', 'ESTILISMO', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '07/14/2011', 80, '2411177648', '', 'mjos91@hotmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(253, 498, '', 'BAIE670424MHGXSL09', 'MARÍA ELENA BAÑOS ISLAS', 'VICENTRE GUERRERO No 5', '', 'SAN JUAN TOTOLAC', 90605, 8, 'SEPARADA', 'M', '04/24/1967', '09/20/2016', '', 'EN TELESECUNDARIA', 'LICENCIATURA', 'CONFECCION INDUSTRIAL DE ROPA ', '06/14/2011', 93, '2461943695', '2461162322', 'MARYBI_05@HOTMAIL.COM', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(258, 1097, '', 'OERO590416HVZRDC00', 'OCIEL ORTEGA RODRÍGUEZ', 'MARIANO MATAMOROS 1521', 'null', 'SAN MART', 90339, 0, 'CASADO (A)', 'H', '04/16/1959', '09/22/2011', '', 'INGENIERO MEC', 'LICENCIATURA', 'ADMINISTRAC', '06/14/2011', 89, '01 241 113 0359', '2411149581', 'oortega16@gmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(262, 802, '', 'CODC740428MTLRZT09', '\r\nCATALINA CORDERO DÍAZ', 'COMETZI N. 11', '', 'SAN JORGE TEZOQUIPAN', 90147, 6, 'CASADO (A)', 'M', '28/04/1974', '06/20/2014', '', 'AUXILIAR EN  ENFERMERIA', '', 'SERVICIOS DE BELLEZA ', '07/27/2016', 89, '2461119852', '', 'linacod_74yo@hotmail.com', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(267, 1104, '', 'HUAC771116MTLRGL02', 'CLARA HUERTA AGUILAR', 'CALLE 3 SUR NO. 51', 'null', 'BARR. SAN ANTONIO', 90550, 0, 'CASADO (A)', 'M', '11/16/1977', '03/03/2016', '', 'T', 'BACHILLERATO', 'CONFECCI', '07/14/2011', 84, '247 108 0271', '', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(275, 1112, '', 'XEDE781117HPLLVL04', 'ELPIDIO XELO DÁVILA', 'CALLE 33 NO. 204', 'null', 'COL. XICOHTENCATL', 90070, 0, 'CASADO (A)', 'H', '11/17/1978', '10/30/2012', '', 'T', 'TECNICO', 'MEC', '07/14/2011', 81, '246 103 8464', '246 46 6 79 02', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(276, 1113, '', 'EANE670214MHGSVS06', 'MARÍA ESPERANZA ESCALONA NAVA', 'PRIVADA RETORNO NACIONAL N', 'null', 'BARRIO MIRAFLORES OCOTL', 90100, 0, 'SOLTERO (A', 'M', '02/14/1967', '07/04/2014', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '07/14/2011', 81, '246-480 79 61', '46 2 0073', 'esperanza85escalona@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(279, 1116, '', 'LETA500814MTLNLR13', 'MA. AURORA E. LEÓN TLATELPA ', 'C. 5 DE MAYO 603-A', NULL, 'COL. CENTRO', 90300, 0, 'CASADO (A)', 'M', '08/14/1950', '02/24/2016', '', 'T', 'SECUNDARIA', 'ALIMENTOS Y BEBIDAS', '07/14/2011', 80, '044 241  40 7 68 77', '01 241 41 7 2837', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(282, 343, '', 'CUAF760204MTLHRL06', 'FLORENCIA CUAHUTLE ARCE', ' 15 DE FEBRERO No. 9 SECCION 6', 'SANTA MARIA TLACATECPAC', 'CONTLA', 90670, 11, '', 'F', '04/02/1976', '05/21/2013', '', 'CONFECCION  INDUSTRIAL DE ROPA', 'TECNICO', 'CONFECCION INDUSTRIAL DE ROPA ', '07/22/2011', 85, '', '0442461474000', '', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(284, 190, '', 'GOCJ590109MTLNRL25', 'MARA JULIA GONZÁLEZ CARBAJAL', 'PROLONGACION  16 SEPTIEMBRE No 265', '', 'SANTA URSULA ZIMATEPEC ', 90450, 5, 'DIVORCIADA', 'F', '09/01/1959', '05/10/2016', '', '', 'SECUNDARIA', 'ESTILISMO Y BIENESTAR PERSONAL', '02/22/2010', 87, '2411114722', '012414171096', '', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(285, 295, '', 'VASR610417HTLSND08', 'RODOLFO VÁZQUEZ SÁNCHEZ', 'CALLE AMADO NERVO NO. 10', 'null', 'LA TRINIDAD TENEXYECAC', 90121, 0, 'CASADO (A)', 'H', '04/17/1961', '10/15/2013', '', 'T', 'TECNICO', 'ELECTRICIDAD', '08/04/2011', 91, '246-101-58-01', '46-7-26-50', '', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(286, 186, '', 'SAFG640409MDFNRR09', 'GEORGINA SÁNCHEZ FERNÁNDEZ', 'CALLE BUGAMBILIAS No. 144', 'EL CARMEN', 'TZOMPANTEPEC', 90490, 3, 'CASADO (A)', 'F', '09/04/1964', '04/02/2013', '', 'PROFESIONAL EN ESTILISMO', 'TECNICO', 'SERVICIOS DE BELLEZA', '09/28/2011', 87, '0452414191586', '012414152648', '', 'sin observaciones', '0', 38, '', '', '', '', '', '', '', '', '0'),
(292, 198, '', 'SAMP700313MTLNRT09', 'PATRICIA SÁNCHEZ MORALES', 'MORELOS NO. 8', '', 'SANTA CRUZ TECHACHALCO', 90145, 7, '', 'F', '13/03/1970', '04/28/2017', '', '', 'TECNICO', 'COFECCION INDUSTRIAL ', '09/19/2011', 86, '', '4673274', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(296, 486, '', 'LODJ620619MTLPZL08', 'JULIANA LÓPEZ DÍAZ', 'CALLE 20 DE AGOSTO NO. 607', '', 'APIZACO ', 90407, 4, 'SOLTERA', 'F', '19/06/1962', '02/09/2016', '', 'CULTURA DE BELLEZA ', 'TECNICO ', 'ESTILISMO Y BIENESTAR PERSONAL', '09/01/2011', 87, '2411386877', '012414100615', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(300, 171, '', 'CEJJ560213MTLRRL03', 'MARÍA JULIA DE LOS ÁNGELES CERÓN JUÁREZ', 'AV. REVOLUCION 7', 'PABLO CONCEPCIÓN HIDALGO', 'ALTZAYANCA ', 90550, 11, 'SOLTERO (A', 'F', '02/13/1956', '01/14/2013', '', '', 'SECUNDARIA', 'CONFECCIÓN INDUSTRIAL DE ROPA/REPOSTERÍA', '09/01/2011', 90, '241 108 8801', '', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(302, 401, '', 'FOPI600922MTLLRR13', 'MARÍA IRAÍS ALICIA FLORES PARRAGUIRRE', 'FRANCISCO I. MADERO No; 1', 'GUADALUPE ', 'CUAPIAXTLA', 90560, 0, 'CASADO (A)', 'F', '22/09/1960', '07/12/2016', '', '', 'BACHILLERATO', 'REPÓSTERIA Y PANADERIA', '09/01/2011', 94, '2411087093', '012464629651', 'daferisa1992@hotmail.com', 'sin observaciones', '0', 8, '', '', '', '', '', '', '', '', '0'),
(304, 33, '', 'MOCC540918MTLNMT09', 'MARIA CATALINA  MONTES CAMPECHE', ' UNO #19', 'PUEBLO DE LA CRUZ', 'TLAXCALA', 90535, 10, 'CASADO (A)', 'M', '18/09/1954', '', '', 'CORTE Y CONFECCION Y TALLA Y SOBRE MEDIDA ', 'SECUNDARIA ', 'CONFECCION INDUSTRIAL DE ROPA', '10/19/2011', 82, '', '01 2474723669', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(305, 62, '', 'BASJ680413MTLRZL05', 'JULIA LORETO BRAVO SAUZA', 'C. LAGOS NO. 46', 'null', 'FRACC. NUEVOS HORIZONTES', 90500, 0, 'DIVORCIADA', 'M', '04/13/1968', '12/03/2015', '', 'T', 'SECUNDARIA', 'ALIMENTOS Y BEBIDAS', '09/01/2011', 83, '247 103 43 36', '01 247 47 2 16 57', 'july_enigmatica@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(315, 841, '', 'VADM780627MTLSZR00', 'MMARISOL VÁZQUEZ DÍAZ', 'AV. BENITO JU', 'null', 'LA TRINIDAD TENEXYECAC', 90121, 0, 'SOLTERO (A', 'M', '06/27/1978', '', '', 'LIC. EN TELESECUNDARIA / T', 'LICENCIATURA', 'ESTILISMO Y BIENESTAR PERSONAL', '07/27/2016', 91.16, '2461434541', '12464673162', 'marisolvasquez7777@gmail.com', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(316, 253, '', 'JIRC731023MTLMYR02', 'MARIA DEL CARMEN JIMENEZ  REYES', ' EZEQUIEL M. GRACIA #50', 'EL MIRADOR', ' OCOTLAN', 90100, 7, 'CASADO (A)', 'M', '23/10/1973', '03/22/2017', '', 'TECNICO EN ESTILISMO Y BIENESTAR PERSONAL', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '02/16/2017', 85, '', '2461910180', 'mileydi_estrella@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(344, 1142, '', 'AAMA770521MTLTXN08', 'ANGÉLICA ATA MUÑOZ', 'C. FRANCISCO I MADERO NO. 23', 'null', 'SAN FRANCISCO TEPEYANCO', 90180, 0, 'SOLTERO (A', 'M', '05/21/1977', '01/13/2014', '', 'ING. COMPUTACI', 'LICENCIATURA', 'INFORM', '06/28/2011', 92, '246 129 9309', '', 'atha_78@yahoo.com.mx', 'sin observaciones', '0', 29, '', '', '', '', '', '', '', '', '0'),
(349, 1147, '', 'JURB670820HDFRDR02', 'BERNARDO JUÁREZ RODRÍGUEZ', '', 'null', 'LA TRINIDAD TENEXYECAC', 90121, 0, 'CASADO (A)', 'H', '08/20/1967', '08/08/2011', '', 'T', 'BACHILLERATO', 'ARTESAN', '07/29/2011', 86, '246 143 89 53', '246 467 3105', 'bermatesan@hotmail.com', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(361, 1158, '', 'GAGR701119MPLRRN09', 'REINA GARCÍA GARCÍA', 'CALLE TEZIUTLAN NO. 45', 'null', 'STA. CRUZ AQUIAHUAC', 90733, 0, 'CASADO (A)', 'M', '11/19/1970', '10/07/2013', '', 'T', 'TECNICO', 'ARTESAN', '07/28/2011', 80, '246 118 64 24', '41-6-13-18', '', 'sin observaciones', '0', 32, '', '', '', '', '', '', '', '', '0'),
(365, 924, '', 'GOVM850621HTLMZS00', 'MISAEL ADMIN GÓMEZ VÁZQUEZ', 'CALLE HIDALGO NO. 15', 'null', 'SAN PEDRO MU', 90830, 0, 'CASADO (A)', 'H', '06/21/1985', '09/06/2013', '', 'LICENCIATURA EN CONTADUR', 'LICENCIATURA', 'INFORM', '05/12/2011', 81, '2461428095', '', 'misa_g_v@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(383, 1177, '', 'VEAR740819MTLLGC00', '\r\nMARÍA DEL ROCÍO VELÁZQUEZ AGUILAR', 'C. LIM', 'null', 'FRACC. LOMA LINDA', 90100, 0, 'CASADO (A)', 'M', '08/19/1974', '', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/10/2011', 80, ' 246 124 3647', '1440056', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(386, 224, '', 'CARL530305MTLRJL03', 'LILIA CARBAJAL ROJAS', ' 8-A NO. 4509', 'XICOHTENCATL', 'LA LOMA ', 90070, 7, 'CASADO (A)', 'F', '05/03/1953', '06/17/2014', '', 'LIC. EN LINGUISTICA APLICADA', 'LICENCIATURA', 'SECRETARIADO ASISTIDO POR COMPUTADORA E INGLES', '08/11/2011', 92, '2461252017', '4622577', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(389, 499, '', 'PEOA680826HTLRLL05', 'ALEJANDRO PÉREZ OLVERA', 'CALLE SIM', 'null', 'SAN SIME', 90241, 0, 'UNI', 'H', '08/26/1968', '02/08/2013', '', 'T', 'LICENCIATURA', 'INGL', '08/11/2011', 91, '045 241 108 8765', '01 241 49 6 2724', 'rosi.castillo@hotmail.com', 'sin observaciones', '0', 14, '', '', '', '', '', '', '', '', '0'),
(398, 78, '', 'PAHL760810MTLDRR08', 'LORENA PADILLA HERNÁNDEZ', '3RA PRIVADA DE JUAREZ S/N', 'SAN ESTEBAN', 'TIZATLAN', 90100, 0, 'SOLTERO (A', 'F', '10708/1976', '', '', 'DIPLOMADO DE INGLES', 'MAESTRIA', 'INGLES', '08/11/2011', 87, '2461599182', '4662910', 'lorraine7677@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(412, 1200, '', 'TUHN781112HTLXRP01', 'NAPOLEÓN TUXPAN HERRERA', 'AV. FRANCISCO VILLA SUR 10', 'null', 'SAN LORENZO AXOCOMANITLA', 90750, 0, 'UNI', 'H', '11/12/1978', '', '', 'INGENIERO EN COMPUTACI', 'LICENCIATURA', 'INFORM', '08/11/2011', 82, '044 246 122 5134', '01 246 49 7 36 89', 'ntuxpan3@hotmail.com', 'sin observaciones', '0', 54, '', '', '', '', '', '', '', '', '0'),
(419, 1207, '', 'HUGC790305MTLRNL01', 'CELSA HUERTA GONZÁLEZ', 'LOTE 8 NO. 22', 'null', 'FRACC. XALCUENTLA SANTA ', 90450, 0, 'CASADO (A)', 'M', '03/05/1979', '', '', 'T', '', 'ESTILISMO Y BIENESTAR PERSONAL', '08/19/2011', 85, ' 241 106 00 97', '241 41 8 39 34', 'aslec_79@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(431, 792, '', 'PEFC791231MTLDRT07', 'CATALINA PEDRAZA FERNÁNDEZ', 'JAZMINES NO. 8', 'FRACIONAMIENTO LOS SAUCES 1', 'SANTA URSULA ZIMATEPEC YAUHQUEMECAN', 90450, 5, 'CASADO (A)', 'F', '31/12/1979', '04/04/2013', '', 'DISEÑO DE PATRONES INDUSTRIALES Y ALTA COSTURA', 'TECNICO', 'CONFECCION INDUSTRIAL DE ROPA ', '08/18/2011', 83, '241212261', '4175819', 'katy.2261@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(437, 1218, '', 'CECJ740119MDFRRD09', 'JUDITH CERVANTES CORONA', 'CALLE 21 MARZO NO. 11', 'null', '1RA SECCI', 90492, 0, 'CASADO (A)', 'M', '01/19/1974', '05/07/2013', '', 'PROFESOR DE  EDUCACI', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '08/18/2011', 80, '044 241 117 39 49', '', '', 'sin observaciones', '0', 52, '', '', '', '', '', '', '', '', '0'),
(440, 1164, '', 'LEAE800208MTLZLV06', 'EVA MARÍA LEZAMA ALCANTAR', 'C. BENITO JU', 'null', 'BARRIO EL ALTO', 90640, 0, 'SOLTERO (A', 'M', '02/08/1980', '03/25/2013', '', 'LICENCIADO EN INFORM', 'LICENCIATURA', 'INFORM', '08/11/2011', 89.5, '246-119-85-44', '246 46 1 22 42', 'evamaryleza@yahoo.com.mx', 'sin observaciones', '0', 26, '', '', '', '', '', '', '', '', '0'),
(459, 20, '', 'EIEC561021MTLLLL05', 'CELIA ELIOSA ELIOSA', 'PRIVADA CAPULIN #5', 'TEPETLAPA', 'LOMA BONITA ', 90800, 7, 'CASADO (A)', 'F', '21/10/1956', '', '', 'CONFECCION INDUSTRIAL DE ROPA ', 'TECNICO', 'CONFECCION INDUSTRIAL DE ROPA ', '08/29/2011', 83, '2461587547', '4666159', 'celiaeliosa@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(461, 722, '', 'RERB840919MTLYST08', 'BEATRIZ REYES RIOS', 'AND NARDO No  9-B', '', 'EL SABINAL', 90102, 7, 'CASADO (A)', 'M', '19/09/1984', '01/15/2013', '', 'ESTLISTA PROFECIONAL ', 'TECNICO', 'SERVICIOS DE BELLEZA', '08/29/2011', 81, '2461271649', '2461271649', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(479, 1253, '', 'SATL670810MTLLRR07', 'LORENZA SALAZAR TORRES', 'AND. LOS LAURELES NO. 50 A', 'null', 'FRACC. LA VIRGEN', 90140, 0, 'CASADO (A)', 'M', '10/08/1967', '09/20/2011', '', 'T', 'SECUNDARIA', 'ESTILISMO Y BIENESTAR PERSONAL', '08/31/2011', 85, '246 129 1233', '', 'munek-pek@hotmail.com', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(481, 188, '', 'VACI600617MTLZNS08', 'ISAURA VÁZQUEZ CONTRERAS', 'JUAREZ N', 'null', 'SANTA MARIA ATLIHUETZIA', 9459, 0, 'DIVORCIADA', 'M', '06/17/1960', '01/28/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/31/2011', 84.5, '', '241 133 8363', 'chayis1900@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(484, 1257, '', 'BAGM550215MMNZRR00', 'MARTHA BAÉZ GARIBAY', 'ALFARERO SUR NO. 1', 'null', 'COL. LOMA BONITA', 90090, 0, 'CASADO (A)', 'M', '02/15/1955', '07/29/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/31/2011', 82, '246 119 91 01', '', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(490, 1263, '', 'SAPJ440423HTLLRR05', 'JORGE CRUZ SALAZAR PÉREZ', '5 DE MAYO NO. 79', 'null', 'IXTULCO', 90105, 0, 'SOLTERO (A', 'H', '04/23/1944', '01/24/2013', '', 'T', 'TECNICO', 'INGLES', '08/31/2011', 80, '246 105 66 68', '', 'salazarjef_433@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(504, 1273, '', 'SARA750110MTLNDD08', 'ADRIANA SÁNCHEZ RODRÍGUEZ', 'CARRETERA TEPEHITEC NO. 22', 'null', 'COL. EJIDAL', 90115, 0, 'CASADO (A)', 'M', '01/10/1975', '02/23/2013', '', 'T', 'TECNICO', 'ARTESAN', '09/08/2011', 85, '246 135 2928', '46 6 85 86', 'adrianasanrod@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(505, 1274, '', 'BACM650308MPLRRR06', 'MARTHA PATRICIA BARCENA CARRASCO', 'PRIVADA HAYUNTAMIENTO 1310 LINDA VISTA', 'null', 'STA ANITA HUILOAC', 90300, 0, 'CASADO (A)', 'M', '03/08/1965', '02/13/2014', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '09/08/2011', 85, '222 316 5688', '', 'yarfani.3@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(515, 828, '', 'FEVE830914MTLRRL07', 'ELIZABETH FERNÁNDEZ VARGAS', 'AQUILES SERD', 'null', 'COL. EL CARMEN', 90338, 0, 'CASADO (A)', 'H', '09/14/1983', '05/06/2016', '', 'LIC. PSICOLOG', 'LICENCIATURA', 'INFORM', '06/02/2010', 92, '241 112 5408', '', 'ELY_FDEZ14@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(517, 794, '', 'IAMM710321MTLBNR08', 'MARINA AIDE IBARRA MENESES', 'JACARANDAS NO. 38', 'null', 'FRACC. REAL DE BOSQUE', 90356, 0, 'CASADO (A)', 'M', '03/21/1971', '06/20/2016', '', 'T', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '08/03/2016', 89.32, '7491071577', '2414197437', 'marina_aide_ibarra_meneses@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(537, 1299, '', 'AOTF800120MTLNZB03', 'FABIOLA ANGOA TZONI', '5 DE MAYO #22', 'null', 'BARRIO SAN JUAN 2DO.', 90580, 0, 'CASADO (A)', 'M', '01/20/1980', '10/15/2013', '', 'LIC. INFORM', 'LICENCIATURA', 'INFORM', '09/20/2011', 80.05, '247-100 76 61', '247-47-2-93-62', 'angoa4@hotmail.com', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(543, 1306, '', 'SAHA730410MTLLRL03', 'MARIA ALEJANDRA SALDAÑA HERNÁNDEZ', 'C NEGRETE PTE 605', 'null', 'BARR SAN  ANTONIO', 90505, 0, 'CASADO (A)', 'M', '04/10/1973', '08/18/2015', '', 'PASANTE EN LICENCIATURA DE LINGU', 'CARRERA TRUNCA', 'INGLES', '09/29/2011', 91, '247 105 62 48', '247 109 09 89', 'ALESSANDRASH@HOTMAIL.COM', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(544, 1307, '', 'PAHS610421MTLLRL03', 'SILVIA PALESTINA HUERTA', 'CALLE EMILIANO ZAPATA S/N', '', 'COL. CENTRO', 90540, 0, 'CASADO (A)', 'M', '04/21/1961', '02/13/2013', '', 'ARTESANÍAS', 'PRIMARIA', 'ARTESANÍAS CON FIBRAS TEXTILES; INDUSTRIALIZACIÓN DE HOJA DE MAÍZ', '09/29/2011', 89, '241 122 66 05', '241 415 61 12', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(548, 1310, '', 'OELE620427MTLRMD00', 'EDITH MONSERRAT ORTEGA LIMA', 'AV. REVOLUCI', NULL, 'COL. REFORMA', 90800, 0, 'SEPARADA', 'M', '04/27/1962', '01/18/2013', '', 'TÉCNICO', 'TECNICO', 'ARTESANÍAS CON FIBRAS TEXTILES', '08/31/2011', 86, '044-246-110-58-20', '46-4-38-43', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(574, 1328, '', 'LURL680811MTLNZS09', 'MA. LUISA LUNA  RAZO', 'CALLE JAZMIN #39', 'null', 'SANTA URSULA ZIMATEPEC', 90450, 0, 'CASADO (A)', 'M', '08/11/1968', '08/20/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/14/2011', 82, '241-103-90-26', '241-41-7-88-96', '', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(575, 1329, '', 'FODT661227MTLLCR17', 'MA. TERESA FLORES DÉCTOR', 'ZARAGOZA OTE. #101-B', 'null', 'COL. CENTRO', 90500, 0, 'CASADO (A)', 'M', '12/27/1966', '03/23/2016', '', 'CULTORA DE BELLEZA', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/13/2011', 81.5, '045-247-47-101-18-48', '01247-47-2-08-13', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(578, 694, '', 'FOHR830228MTLLRS02', 'ROSA EDITH FLORES HERNÁNDEZ', 'AV. 5 DE MAYO #43', 'SAN JOSE XICOHTENCATL', 'HUAMANTLA', 90500, 10, 'SOLTERO (A', 'F', '28/02/1983', '', '', ' EN LINGUISTICA APLICADA', 'LICENCIATURA', 'INGLES', '10/21/2011', 98, '', '2471042900', 'yerfecysfriends@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(591, 8, '', 'BOML630511MTLNNS07', 'LUISA BONILLA MENDOZA', 'PROLONGACIÓN FCO. I. MADERO #2025', '', 'SAN ANDRES AHUASHUATEPEC', 90491, 0, 'CASADO (A)', 'F', '05/11/1963', '02/14/2013', '', 'EMFERMERA AUXILIAR', 'TECNICO', 'FLORISTERIA', '10/24/2011', 85, '241-126-12-25', '01241-41-7-55-91', 'maryluisa_20@hotmail.com', 'CONSTANCIAS EN ARTESANÍAS FAMILIARES\r\nPROGRAMADOR ANALISTA\r\nINGENIERO QUIMICO\r\nTEJIDO A MANO   \r\nMARQUETERÍA \r\nMIGAJÓN\r\nJUGUETERÍA \r\nMACRAMÉ\r\nCHAQUIRA\r\nPINTURA EN CERÁMICA \r\nPINTURA TEXTIL\r\nTEJIDO MECÁNICO FAMILIAR \r\nBORDADO EN TELA\r\nPOLIESTER \r\nTRABAJOS EN PAPEL Y CARTÓN ', '0', 38, '', '', '', '', '', '', '', '', '0'),
(596, 1345, '', 'HEBR830603HTLRZG02', 'RIGOBERTO HERNÁNDEZ BÁEZ', 'CALLE B #7', 'null', 'COL. BEATRIZ PAREDES; SAN BERNAB', 90710, 0, 'SOLTERO (A', 'H', '06/03/1983', '', '', 'LIC. EN INFORM', 'LICENCIATURA', 'INFORM', '10/21/2011', 83.5, '2461838663', '246-46-7-81-16', 'rieba_83@hotmail.com', 'sin observaciones', '0', 23, '', '', '', '', '', '', '', '', '0'),
(600, 1347, '', 'TILC740901MTLZZR00', 'CAROLINA TIZAPANTZI LEZAMA', 'C. ALCANFORE 3', 'null', 'PANOTLA', 90140, 0, 'CASADO (A)', 'M', '09/01/1974', '07/30/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/21/2011', 83.5, '246-121-25-14', '46-4-43-21', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(621, 1394, '', 'COHJ640606MPLRRS03', 'MARÍA DE JESUS H.  CORTÉS HERNÁNDEZ', 'C. SATURNO 4 FRACC SAN JOSE', '', 'ACTIPAC', 90490, 0, 'CASADA', 'M', '06/06/1964', '02/08/2013', '', 'T', 'SECUNDARIA', 'ARTESAN', '02/02/2012', 82, '241 118 62 64', '', 'CHUYCORTEZHDZ@HOTMAIL.COM', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(622, 1405, '', 'OOEG691117MTLRSR02', 'GEORGINA ORDOÑEZ ESCAMILLA', 'VERA Y ZURIA #10', NULL, 'SANTA CRUZ', 90240, 0, 'CASADA', 'M', '11/17/1969', '', '', 'TÉCNICO', 'TECNICO', 'ARTESANÍAS CON FIBRAS TEXTILES  ', '02/02/2012', 87, '246-106-87-07', '', 'M.V.Z.JESUSJUAREZ@HOTMAIL.COM', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(640, 1402, '', 'ZAEE781218MMNMQL01', 'ELVIA ZAMORA EQUIHUA', 'CALLE OTONGATEPETL #3 INTERIOR B', 'null', 'BELEN ATZITZIMITITLAN', 90605, 0, 'CASADA', 'M', '12/18/1978', '02/08/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '02/02/2012', 88, '246-105-33-55', '', 'ZAMORAELVIA@HOTMAIL.COM', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(648, 1395, '', 'HEMJ830417HTLRRN08', 'JUAN HERNÁNDEZ MARTÍNEZ', 'CALLE 20 DE NOVIEMBRE #126-A', 'null', 'BARRIO DE SANTA CRUZ', 90500, 0, 'CASADO', 'H', '04/17/1983', '', '', 'LICENCIADO EN CIENCIAS DE LA EDUCACI', 'LICENCIATURA', 'INFORM', '02/02/2012', 93.5, '247-101-11-79', '247-47-2-34-98', 'JUAN_HMT@HOTMAIL.COM', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(651, 1400, '', 'CAIJ660522HTLRSV02', 'JAVIER CARRASCO ISLAS', 'AVENIDA 16 DE SEPTIEMBRE #505-3', 'null', 'APIZACO', 90300, 0, 'SOLTERO', 'H', '05/22/1966', '04/03/2013', '', 'LICENCIADO EN ADMINISTRACI', '', 'ADMINISTRAC', '02/02/2012', 89.5, '2411187173', '241-41-7-06-92', 'INNOVACIONMATAMOROS@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(653, 1403, '', 'MARG870420MTLNYB09', 'GABRIELA MANOATL REYES', ' CALLE CARVAJAL #1', 'null', 'APETATITLAN', 90600, 0, 'SOLTERA', 'M', '04/20/1987', '01/19/2016', '', 'LICENCIADO EN NEGOCIOS INTERNACIONALES; MAESTRIA EN ADMINISTRACI', 'LICENCIATURA', 'CONTABILIDAD', '02/02/2012', 88, '246-122-40-00', '', 'gaby.1288@hotmail.com', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(654, 1430, '', 'RADE810918HTLMMD03', 'EDUARDO RAMÍREZ DOMÍNGUEZ', 'CALLE CUAUHT', 'null', 'TECOPILCO', 90445, 0, 'SOLTERO', 'H', '09/18/1981', '06/09/2016', '', 'PASANTE INGENIERO EN ELECTR', 'PASANTE', 'INFORM', '02/15/2012', 93, '241-108-21-11', '49-6-23-69', 'EDUARDO_RAMIREZ_DOMINGUEZ@HOTMAIL.C', 'sin observaciones', '0', 55, '', '', '', '', '', '', '', '', '0'),
(657, 708, '', 'CAZR861024HTLNVF00', 'RAFAEL CANDIA ZAVALA', ' VERACRUZ #12', 'CENTRO', 'SANCTORUM L.CARDENAS ', 90230, 1, '', 'M', '24/10/1986', '09/19/2013', '', 'LENGUAS MODERNAS ', 'LICENCIATURA', 'INGLES ', '02/02/2012', 91, '7481057905', '017489181874', 'rafaybrit@hotmail.com', 'sin observaciones', '0', 20, '', '', '', '', '', '', '', '', '0'),
(687, 1442, '', 'CASE760417MTLMNL00', 'ELIZABETH CAMARÓN SÁNCHEZ', '2DA. CERRADA DE MANANTIALES PONIENTE #3', 'null', 'SECCI', 90740, 0, 'CASADA', 'M', '04/17/1976', '', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '02/23/2012', 80, '246 104 1122', '', '', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(688, 1456, '', 'VAVF600101MTLZZR03', 'FERNANDA VÁZQUEZ VÁZQUEZ', 'PRIVADA ANTONIO CARVAJAL #2', 'null', 'COLONIA ANIMAS', 90110, 0, 'SEPARADA', 'M', '01/01/1960', '', '', 'T', 'TECNICO', 'CONFECCI', '02/23/2012', 80, '246-121-02-05', '246-46-6-30-43', 'FERNANDAVAZQUEZ01@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(691, 1458, '', 'DIMM670118MTLZXR08', 'MARGARITA DÍAZ MUÑOZ', 'CALLE BENITO JU', 'null', 'SANTA CRUZ TECHACHALCO', 90145, 0, 'CASADA', 'M', '01/18/1967', '05/09/2017', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/25/2017', 90.5, '246 126 6911', '46 7 32 24', '', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(694, 1461, '', 'FUVN730607MTLNZR05', 'NARDA TERESITA FUENTES VÁZQUEZ', 'AVENIDA HIDALGO PONIENTE #214', 'null', 'COLONIA CENTRO', 90500, 0, 'CASADA', 'M', '06/07/1973', '02/22/2017', '', 'LICENCIADO EN TRABAJO SOCIAL', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '03/01/2012', 86, '247 109 1477', '', 'NTFV_73@HOTMAIL.COM', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(697, 1466, '', 'PEJA870427HTLRRD07', 'ADRIAN PÉREZ JUÁREZ', 'PRIVADA HIDALGO #7 AVENIDA TLAXCALA', 'null', 'SAN SIME', 90241, 0, 'SOLTERO', 'H', '04/27/1987', '08/09/2012', '', 'T', 'TECNICO', 'INGL', '02/29/2012', 83, '241 414 6347', '241 496 22 48', 'theking12384@hotmail.com', 'sin observaciones', '0', 14, '', '', '', '', '', '', '', '', '0'),
(701, 1469, '', 'GUAR761001HDFRGM01', 'ROMEL GUERRA AGUILAR', 'PRIVADA VIOLETA No.24', NULL, 'LA TRINIDAD TENEXYECAC', 90121, 6, 'UNION LIBRE', 'H', '01/10/1976', '', '', 'FILOSOFIA ', 'LIECENCIATURA ', 'MANTENIMIENTO Y REPARACION DE MAQUINAS DE COSTURA', '03/01/2012', 81, '2461288478', '4668305', '', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(708, 1476, '', 'MOLM860912MTLRRR08', 'MARISA MORALES LARA', 'AVENIDA REVOLUCI', 'null', 'CONCEPCI', 90550, 0, 'CASADA', 'M', '09/12/1986', '03/08/2016', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '03/13/2012', 80.5, '276 111 8875', '2761096924', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(715, 1483, '', 'PEHK901104MTLRRR09', 'KARINA PÉREZ HERNÁNDEZ', 'CALLE PALO HUERFANO 3', 'null', 'COLONIA CENTRO', 90620, 0, 'CASADA', 'M', '11/04/1990', '02/25/2016', '', 'T', 'TECNICO', 'ARTESAN', '03/22/2012', 84, '246 150 4462', '', 'kari110411@yahoo.com.mx', 'sin observaciones', '0', 1, '', '', '', '', '', '', '', '', '0'),
(720, 1489, '', 'COXF670604MTNLR08', 'FRANCISCA EMMA CONDE XELHUANTZI', 'PRIVADA HEROE DE NACOZARI No 44 SECC. 1ra', 'Primera seccion ', 'San Bernardino Contla', 90670, 8, 'CASADA', 'M', '04/06/1967', '09/09/2013', '', 'Licenciado en la enseñanza de lenguas extranjeras ', 'Carrera trunca', 'Alimentos y bebidas ', '03/22/2012', 80, '2461301381', '24610097', 'paloma8090@hotmail.com', 'Reposteria; panaderia; decoracion de pasteles; gelatinas y flanes', '0', 18, '', '', '', '', '', '', '', '', '0'),
(731, 1501, '', 'XOLA720624MTLCPR01', 'ARGELIA XOCHIPA LÓPEZ', 'CALLE ', 'null', 'LOS REYES QUIAHUIXTL', 90160, 0, 'CASADA', 'M', '06/24/1972', '05/31/2016', '', 'T', 'BACHILLERATO', 'ARTESAN', '03/29/2012', 87, '246-148-47-55', '46-8-61-24', '', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(738, 1508, '', 'RAAA750304MPLMPD07', 'ADRIANA RAMIRO APARICIO', '1RO. DE MAYO S/N', 'null', 'SAN MATIAS TEPETOMATITLAN', 90605, 0, 'CASADO', 'M', '03/04/1975', '10/18/2012', '', 'BACHILLERATO', 'BACHILLERATO', 'ARTESAN', '04/02/2012', 82, '2464595718', '246-41-5-02-03', 'CUETZARA2006@HOTMAIL.COM', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(742, 1513, '', 'VAVP690318HTLZZS03', 'PASTOR VÁZQUEZ VÁZQUEZ', 'AVENIDA GUADALUPE VICTORIA #31', 'null', 'SANTIAGO MICHAC', 90721, 0, 'CASADO', 'H', '03/18/1969', '', '', 'T', 'TECNICO', 'INGL', '03/29/2012', 80, '246-46-603-91', '246-46-6-80-02', '', 'sin observaciones', '0', 23, '', '', '', '', '', '', '', '', '0'),
(743, 1511, '', 'SARA920526HTLNYB09', 'JOSÉ ABRAHAM SÁNCHEZ REYES', 'CALLE 5 DE MAYO #12', 'null', 'APETATITLAN', 90600, 0, 'SOLTERO', 'H', '05/26/1992', '08/23/2013', '', 'T', 'TECNICO', 'ELECTRICIDAD', '03/29/2012', 80.5, '246 157 3942', '246-10-2-20-54', 'CECYTE-08@HOTMAIL.COM', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(749, 1485, '', 'SEGL620923HTLRTN02', 'LINO ADÁN SERRANO GUTIÉRREZ', 'CALLE GIRASOL #9 U. HAB. CUATRO SE', 'null', 'ATLAHAPA', 90110, 0, 'CASADO', 'H', '09/23/1962', '06/24/2013', '', 'T', 'TECNICO', 'INGL', '03/22/2012', 82, '246-104-81-16', '', 'LINOTEACHER@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(777, 1546, '', 'HEOY840208MDFRRN05', 'YANET HERNÁNDEZ OROZCO', 'FRANCISCO GONZ', 'null', 'COLONIA CENTRO', 90120, 0, 'CASADA', 'M', '02/08/1984', '01/28/2013', '', 'T', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '05/17/2012', 89, '248-113-87-71', '', 'LUIS-ANGEL1224@HOTMAIL.COM', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(779, 1550, '', 'RAGR810118HTLMRB05', 'RUBEN RAMOS GRACIA', 'CALLE DALIA #22', 'null', 'COLONIA INDUSTRIAL BUENOS AIRES', 90800, 0, 'UNION LIBR', 'H', '01/18/1981', '11/14/2016', '', 'LICENCIADO EN ARTES PLAST', 'LICENCIATURA', 'ARTESAN', '05/17/2012', 87, '2464698488', '01-246-46-4-03-60', 'RUBENYSS@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(781, 1544, '', 'LEZH880813HPLNVM09', 'HUEMAN ITZCOATL LEÓN ZAVALETA', 'ANDADOR AZUCENA #6-B', 'null', 'COLONIA EL SABINAL', 90102, 0, 'SOLTERO', 'H', '08/13/1988', '02/20/2013', '', 'LICENCIADO EN GASTRONOM', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '05/09/2012', 81, '246-149-80-71', '246-46-2-34-35', 'HUEMAN_75@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(785, 1548, '', 'MOLP751208MVZGNR02', 'PERLA YADIRA MOGOLLÓN LANDA', 'NIÑOS HEROES No. 10', '', 'SAN MATIAS TEPETOMATITLAN', 90606, 0, 'CASADA', 'M', '12/08/1975', '09/18/2013', '', 'LICENCIADO EN TELESECUNDARIA', 'LICENCIATURA', 'ARTESAN', '05/17/2012', 83, '246-135-20-41', '', 'PMOGOLLON2@HOTMAIL.COM', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(791, 1560, '', 'JISP640317HTLMNT02', 'PATRICIO JIMÉNEZ SÁNCHEZ', 'CALLE MAXIMIANO XILOTL #130 INT. A', 'null', 'SECCI', 90780, 0, 'CASADO', 'H', '03/17/1964', '02/13/2013', '', 'T', 'SECUNDARIA', 'ARTESAN', '05/17/2012', 81.76, '246-105-77-82', '12222631848', '', 'sin observaciones', '0', 42, '', '', '', '', '', '', '', '', '0'),
(796, 1565, '', 'VAHJ840828HTLZRS02', 'JESUS MIGUEL VÁZQUEZ HERNÁNDEZ', '2DA. PRIVADA DELFINO MONTIEL #5', 'null', 'COLONIA REFORMA', 90800, 0, 'SOLTERO', 'H', '08/28/1984', '08/09/2013', '', 'T', 'BACHILLERATO', 'ALIMENTOS Y BEBIDAS', '05/24/2012', 87.5, '2461106343', '1441261', 'j_mi84@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(803, 1568, '', 'TOON610708MOCLRL01', 'NOELY TOLEDO ORDAZ', 'C. SAN ANTONIO 178', 'null', 'FRACCIONAMIENTO SAN CARLOS', 90508, 0, 'CASDA', 'M', '07/08/1961', '02/28/2013', '', 'T', 'BACHILLERATO', 'ALIMENTOS Y BEBIDAS', '05/24/2012', 81, '247-101-52-34', '01-247-47-2-22-70', 'NELY_TOLEDO@HOTMAIL.COM', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(809, 1577, '', 'LOSK861219MTLPLR08', 'KARINA LÓPEZ SILVA', 'CALLE SAN GABRIEL #16 INT. 1', 'null', 'CENTRO', 90000, 0, 'SOLTERA', 'M', '12/19/1986', '08/17/2012', '', 'T', 'LICENCIATURA', 'ESTILISMO Y BIENESTAR PERSONAL', '05/30/2012', 82, '246-119-84-61', '', 'KYTHELOSI_19@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(812, 1578, '', 'SAHA860111HTLNRL04', 'ALEJANDRO SÁNCHEZ HERNÁNDEZ', 'CALLE SONORA 7-A', 'null', 'SAN JOS', 90500, 0, 'CASADO', 'H', '01/11/1986', '03/14/2013', '', 'T', 'TECNICO', 'INFORM', '05/30/2012', 80, '2471094780', '01-247-47-2-81-94', 'TUNNEZ_INTERNET@HOTMAIL.COM', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(855, 1369, '', 'BAVE770318HTLRLD01', 'EDUARDO BARCEINAS VALENCIA', 'CALLE SIN NOMBRE CUARTA SECCION 28', 'null', 'SANTA MARIA ATLIHUETZIA', 90459, 0, 'CASADO', 'H', '03/18/1977', '09/17/2013', '', 'PLOMERO', 'PRIMARIA', 'INSTALACIONES HIDR', '/  /', 0, '241 407 8898', '4611857', 'EDUARDOBARCEINAS@YAHOO.COM', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(861, 1607, '', 'QULJ910702MTLNPN02', 'MARÍA JENNY   QUINTOS LÓPEZ', 'ADOLFO RUIZ CORTINEZ #4', 'null', 'NUEVA AMPLIACION', 90441, 0, 'CASADA', 'M', '07/02/1991', '02/03/2016', '', 'T', 'TECNICO', 'ARTESAN', '06/28/2012', 87, '241-117-49-79', '', 'estrella_jenny_91@hotmail.com', 'sin observaciones', '0', 11, '', '', '', '', '', '', '', '', '0'),
(866, 1389, '', 'JUMA740412HTLRDN04', 'ANGEL JUAREZ MEDELLIN', 'CALLE FCO. I MADERO S/N', 'null', 'COL EL CARMEN', 90290, 0, 'CASADO', 'H', '04/12/1974', '09/12/2013', '', 'PLOMERO', 'PRIMARIA', 'INSTALACIONES HIDR', '/  /', 0, '2411085345', '', '', 'sin observaciones', '0', 12, '', '', '', '', '', '', '', '', '0'),
(867, 1387, '', 'CEBS601028HTLRCM19', 'SIMON AGAPITO CERVANTES BECERRA', 'CALLE  AGUSTIN MELGAR 6', 'null', 'BENITO JUAREZ', 90232, 0, 'CASADO', 'H', '10/28/1960', '03/17/2016', '', 'PLOMERO', 'SECUNDARIA', 'INSTALACIONES HIDR', '/  /', 0, '7481054766', '7487662227', '', 'sin observaciones', '0', 45, '', '', '', '', '', '', '', '', '0'),
(877, 94, '', 'VABE740415MTLZZL00', 'ELVIA VAZQUEZ BAEZ', 'FRANCISCO VILLA No 4', '', 'SAN LUCAS TECOPILCO', 90441, 5, 'CASADA', 'F', '25/04/1974', '08/07/2013', '', 'AGRONOMO AGROINDUSTRIAL', 'INGENIERIA', 'INDUSTRIALIZACIÓN DE ALIMENTOS', '06/28/2012', 83, '2414074830', '012414962258', 'elvis270401@yahoo.com.mx', 'sin observaciones', '0', 55, '', '', '', '', '', '', '', '', '0'),
(879, 1609, '', 'SOSS580202HPLRNL01', 'SALVADOR SORIANO SANCHEZ', 'CALLE ESCULTORES 16', 'null', 'LOMA BONITA', 90090, 0, 'CASADO', 'H', '02/02/1958', '01/27/2014', '', '', 'LICENCIATURA', 'INSTALACIONES HIDR', '06/28/2012', 86, '2461088531', '2461209995', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(880, 1610, '', 'AUGC820110MTLHLL04', 'CELIA AHUACTZIN GALINDO', 'C EMILIO SANCHEZ PIEDRAS 73', 'null', 'COLONIA CHALMA', 90800, 0, 'SOLTERA', 'M', '01/10/1982', '06/27/2013', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '06/28/2012', 83, '2464804855', '4642209', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(919, 1659, '', 'RAID860319HDFMSV00', 'JOSE DAVID RAMY ISLAS', '5 DE MAYO No.95 SECCION 5 SEPTIMA ', NULL, '', 90670, 8, 'CASADO', 'H', '03/19/1986', '11/12/2013', '', 'INSTRUCTOR ', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '08/09/2012', 85, '2461158120', '', 'casanova_440@msn.com', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(930, 213, '', 'MAGL640509MDFNRR03', 'LAURA DEL SOCORRO MANJARREZ GIRON', 'ARBOLEDAS N', 'null', 'IVET', 90355, 0, 'SEPARADA', 'M', '05/09/1964', '04/25/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '07/26/2012', 92, '241 108 1414', '', 'girona64@outlook.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(932, 1643, '', 'ROJM601126MTLJMR07', 'MAURICIA ROJAS JIMENEZ', 'AV MEXICO TLAXCALA 20', '', 'SANTA JUSTINA ECATEPEC', 90123, 0, 'M. SOLTERA', 'F', '11/26/1960', '02/25/2013', '', 'GASTRONOMÍA', 'DIPLOMADO', 'ALIMENTOS Y BEBIDAS', '07/26/2012', 90, '2461064994', '', 'MAURICIAMCULTURAL@HOTMAIL.COM', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(934, 1645, '', 'PEPI700201HTLRRG07', 'JOSE IGNACIO PEREZ PEREZ', 'C PRIMERO DE MAYO 45', 'null', 'BARRIO DE TEXCACOAC', 90800, 0, 'DIVORCIADO', 'H', '02/01/1970', '12/17/2012', '', '', 'TECNICO', 'INGL', '07/26/2012', 89, '2461189423', '', 'PATUKA_6@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(945, 1681, '', 'NAPG661218MTLVDR04', 'MA. GRACIELA NAVA PADILLA', 'CALLE ARTESANIAS 8', 'null', 'SAN ESTEBAN TIZATLAN', 0, 0, 'CASADA', 'M', '12/18/1966', '02/13/2013', '', '', 'TECNICO', 'FLORISTERIA', '08/28/2012', 82, '2461394461', '1175029', 'GERAR27@YAHOO.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(947, 1668, '', 'LUMA660216MDFCNN07', 'MARIA ANTONIETA LUCIANO MENESES', 'PRIV. FIDENCO DE LA GANDARA  2307', 'null', 'COLONIA FATIMA', 90357, 0, 'CASADA', 'M', '02/16/1966', '01/23/2014', '', '', 'LICENCIATURA', 'INFORM', '08/23/2012', 92, '241 100 87 89', '4180763', 'LUCIANO_8@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(960, 1682, '', 'COMS881127MTLYRN02', 'SANDRA COYOMATZI MARTINEZ', 'CALLE CUAUHTEMOC 197B', 'null', 'BARR SANTA CRUZ MATLACAHUACAN', 90840, 0, 'CASADA', 'M', '11/27/1988', '04/16/2013', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/28/2012', 92, '2461263548', '2461297525', 'SANDRIURIX.MARTZ@HOTMAIL.COM', 'sin observaciones', '0', 50, '', '', '', '', '', '', '', '', '0'),
(964, 1686, '', 'PIGE850703MTLDNG02', 'MARIA EUGENIA PIEDRAS GONZALEZ', 'CALLE JAZMINES 12', 'null', 'COL. LOMA FLORIDA', 90300, 0, 'CASADA', 'M', '07/03/1965', '01/15/2016', '', '', 'LICENCIATURA', 'INGL', '09/05/2012', 87, '241 4113628', '12414178760', 'PETITE_AMIE@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(971, 1697, '', 'TOAG561120MTLRRD01', 'GUADALUPE TORRES ARELLANO', 'CALLE 6 PONIENTE 16', 'null', 'BARRIO SAN JUAN PRIMERO', 90580, 0, 'CASADA', 'M', '11/20/2012', '02/28/2013', '', '', 'TECNICO', 'ARTESAN', '09/09/2012', 80, '2471082939', '', '', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(974, 1693, '', 'FAHG690618MTLRRB02', 'GABRIELA  ELVIRA FRAGOSO HERNANDEZ', 'AVENIDA JUAREZ 85', 'null', 'COLONIA CENTRO', 90000, 0, 'DIVORCIADA', 'M', '06/18/1969', '01/24/2013', '', '', 'TECNICO', 'ADMINISTRAC', '09/19/2012', 84, '2461005568', '4664766', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0');
INSERT INTO `instructores` (`id`, `Expediente`, `imagen`, `Curp`, `Nombre`, `Calle`, `Colonia_barrio`, `Localidad`, `CP`, `Seccion_elec`, `Estado_civil`, `Sexo`, `F_Nacimiento`, `F_Alta`, `F_Baja`, `Pefil`, `Nivel_academico`, `Especialidad`, `Fecha_evaluacion`, `Puntaje`, `No_celular`, `No_telefono`, `Email`, `Observaciones`, `calidad`, `id_municipios`, `condicion`, `acta_doc`, `curp_doc`, `dom_doc`, `ine_doc`, `expfg_doc`, `expl_doc`, `cv_doc`, `actualizacion_doc`) VALUES
(984, 1704, '', 'GACJ670208HTLRYN05', 'JOSE JUAN OMAR GRACIA COYOTZI', 'AV. 5 DE MAYO 69-1', 'null', 'SANTA MARIA IXTULCO', 90105, 0, 'CASADO', 'H', '02/08/1967', '08/16/2016', '', '', 'LICENCIATURA', 'ADMINISTRACI', '09/27/2012', 86, '2464948889', '', 'josejuanomar@yahoo.com.mx', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(990, 1707, '', 'TOPE790804HTLRRD07', 'EDUARDO TORRES PEREZ', 'CALLE HIDALGO 246 AQUIAHUAC', 'null', 'SAN FRANCISCO TETLANOHCAN', 90840, 0, 'CASADO', 'H', '08/04/1979', '06/24/2013', '', '', 'TECNICO', 'DISE', '09/27/2012', 84, '', '4617733', '', 'sin observaciones', '0', 50, '', '', '', '', '', '', '', '', '0'),
(993, 1710, '', 'JIGA890720MPLMRN05', 'ANDREA JIMENEZ GARCIA', 'CALLE DOMINGO ARENAS  5', 'null', 'SAN DIEGO XOCOYUCAN', 90122, 0, 'SOLTERA', 'M', '07/20/1989', '08/15/2016', '', '', 'LICENCIATURA', 'ARTESAN', '09/27/2012', 83, '2481594378', '4815367', 'ANDY.2089@HOTMAIL.COM', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(996, 1713, '', 'GAMG670509MTLLRR06', 'GREGORIA GALICIA MARTINEZ', 'CALLE VICENTE GUERRERO 1', 'null', 'BARRIO TEPETLAPA', 90140, 0, 'CASADA', 'M', '05/09/1967', '06/17/2013', '', '', 'TECNICO', 'ARTESAN', '09/27/2012', 82, '2461414880', '1442084', '', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(997, 1714, '', 'FOHK741127MDFLRR09', 'KARLA FLORES HUERTA', '1RA. CERRADA LA SOLEDAD 1 LOTE 2 INT. 1', 'null', 'SANTA URSULA', 90450, 0, 'U. LIBRE', 'M', '11/27/1974', '06/24/2013', '', '', 'TECNICO', 'INGL', '09/27/2012', 80, '2411000168', '241 41 2 11 93', 'KARLA2@HOTMAIL.COM', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(998, 1718, '', 'SAGM871021HTLNZR03', 'MARCO ANTONIO SANCHEZ GUZMAN', 'PRIVADA VISTA HERMOSA  1', 'null', 'SAN JUAN HUACTZINCO', 90195, 0, 'SOLTERO', 'H', '10/21/1987', '05/16/2013', '', '', 'TECNICO', 'MEC', '10/11/2012', 90.5, '2461256941', '12494975954', 'DEXMAX_DEX@HOTMAIL.COM', 'sin observaciones', '0', 53, '', '', '', '', '', '', '', '', '0'),
(999, 1719, '', 'NOCE840601HTLHLM03', 'EMANUEL NOHPAL CALVA', 'CALLE HIDALGO 8', 'null', 'SANTA ANA PORTALES', 90730, 0, 'U. LIBRE', 'H', '06/01/1984', '04/19/2013', '', '', 'LICENCIATURA', 'INFORM', '10/11/2012', 89.5, '246 457 45 68', '246 114 46 51', 'AMAN.VEL@HOTMAIL.COM', 'sin observaciones', '0', 32, '', '', '', '', '', '', '', '', '0'),
(1010, 1728, '', 'ROGM760413HPLSNN08', 'MANUEL DE LA ROSA GONZALEZ', 'PRIVADA TLAXCALA 7', 'null', 'SAN BUENA VENTURA', 90796, 0, 'U. LIBRE', 'H', '04/13/1976', '01/29/2013', '', '', 'LICENCIATURA', 'INFORM', '10/18/2012', 88.5, '22 22 70 88 70', '01222 168 41 87', 'MNAXA@HOTMAIL.COM', 'sin observaciones', '0', 41, '', '', '', '', '', '', '', '', '0'),
(1017, 1733, '', 'ROOE711027MTLDRL03', 'ELBA RODRIGUEZ OROSTICO', 'AV. CUAUHTEMOC S/N', 'null', 'BENITO JUAREZ', 90235, 0, 'CASADA', 'M', '10/27/1971', '02/28/2013', '', '', 'SECUNDARIA', 'ARTESAN', '10/25/2012', 86.6, '7751244474', '17487662361', 'EIROOD27@HOTMAIL.COM', 'sin observaciones', '0', 45, '', '', '', '', '', '', '', '', '0'),
(1020, 1737, '', 'PACA820327MTLLNL04', 'ALEJANDRA PAUL CONDE', 'CALLE 16 DE SEPTIEMBRE 62', NULL, 'IXTLAHUCA', 90670, 0, 'SOLTERA', 'M', '03/27/1982', '10/01/2013', '', '', 'TECNICO', 'ARTESANÍAS CON FIBRAS TEXTILES', '11/06/2012', 95, '2464155225', '12464155225', 'ALESANDRAPAU@HOTMAIL.COM  ', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1026, 1742, '', 'MECH821226MHGJRL02', 'MARIA HILDA MEJIA CORONADO', '2 NORTE 4A', 'null', 'COLONIA EL MIRADOR', 90200, 0, 'SOLTERA', 'M', '12/26/1982', '04/11/2013', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '11/14/2012', 84, '7491018275', '', 'SETALIK_2612@HOTMAIL.COM', 'sin observaciones', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1037, 1747, '', 'CECF760118MTLRRL03', 'FLORIDA CERON CERVANTEZ', '2DA. PRIVADA DE AV. REVOLUCION 7', 'null', 'CONCEPCION HIDALGO', 90550, 0, 'CASADA', 'M', '11/18/1960', '03/08/2016', '', '', 'TECNICO', 'ARTESAN', '11/22/2012', 80.25, '2761106121', '2471104342', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1051, 1764, '', 'TEVG741003HTLMSR09', 'GERARDO TEMOLTZI VASQUEZ', 'ITURBIDE 10', NULL, 'TIZATLAN', 90100, 0, 'SOLTERO', 'M', '10/03/1974', '03/05/2013', '', '', 'SECUNDARIA', 'DISE', '02/28/2013', 83, '2461728792', '', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1053, 1773, '', 'POMS830823MTLLXR07', 'SARA POLVO MUÑOZ', 'AVENIDA MALINTZI PONIENTE 222', 'null', 'MU', 90830, 0, 'SOLTERA', 'M', '08/23/1983', '08/24/2013', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '02/28/2013', 82.4, '2461642879', '4649734', 'BELLOTA_194@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1054, 1771, '', 'VEMN910226HTLLNS08', 'NESTOR ALEJANDRO VELAZQUEZ MONTIEL', 'AVENIDA SAN ANTONIO ESQUINA CON CRUCERO No.101', '', 'TLAXCALA', 90660, 3, 'SOLTERO', 'H', '26/02/1991', '03/12/2013', '', 'CHEF', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '02/28/2013', 80, '2411260263', '414152337', 'alex_skate10@hotmail.com', 'sin observaciones', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1058, 1768, '', 'VACG570210HTLZRL02', 'GUILLERMO VAZQUEZ CARRASCO', 'JUAREZ NORTE 510', 'null', 'HUAMANTLA', 90500, 0, 'SEPARADO', 'H', '02/10/1957', '08/19/2013', '', '', 'BACHILLERATO', 'INSTALACIONES HIDR', '02/28/2013', 83, '', '12474714907', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1059, 1775, '', 'COLB630709MTLNML17', 'BLANCA CONTRERAS LIMA', 'PRIVADA VICENTE GUERRERO 1', 'null', 'SANTA ELENA', 90620, 0, 'SOLTERA', 'M', '07/09/1963', '05/23/2013', '', '', 'TECNICO', 'ARTESAN', '02/28/2013', 90, '2461404409', '2464612141', '', 'sin observaciones', '0', 1, '', '', '', '', '', '', '', '', '0'),
(1067, 1782, '', 'AOCC650209MTLNJR04', 'MARIA CIRILA ANGOA CAJERO', '11 ORIENTE #1', 'null', 'BARRIO SAN GABRIEL 2DO.', 90580, 0, 'CASADA', 'M', '02/09/1965', '04/15/2013', '', '', 'TECNICO', 'ARTESAN', '03/14/2013', 86.64, '4721081867', '2471086759', '', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1072, 413, '', 'SATC670527MTLNNR02', '\r\nCAROLINA SANCHEZ TANECO', 'AVENIDA HIDALGO #11', 'CENTRO', 'XALOZTOC', 90460, 3, 'CASADA', 'F', '27/05/1967', '01/26/2016', '', '', 'SECUNDARIA', 'ARTESANIAS FAMILIARES', '03/14/2013', 92, '2411020340', '2414130758', '', 'sin observaciones', '0', 39, '', '', '', '', '', '', '', '', '0'),
(1073, 1779, '', 'FOMI710603MDFLRS08', 'ISAAC FLORES MARTINEZ', 'AVENIDA 20 DE NOVIEMBRE #72', 'null', '2DA. SECCION', 90780, 0, 'CASADO', 'H', '06/03/1971', '05/10/2013', '', '', 'SECUNDARIA', 'DISE', '03/14/2013', 91, '2228665420', '12222811071', 'isaac030671@hotmail.com', 'sin observaciones', '0', 42, '', '', '', '', '', '', '', '', '0'),
(1092, 1802, '', 'PERB571204MTLRJR08', 'MARIA BARBARA PEREZ ROJANO', 'PRIVADA DE LA VIRGEN #6', 'null', 'COL. CENTRO', 90800, 0, 'CASADA', 'M', '12/04/1957', '03/01/2017', '', '', 'PASANTE', 'AGRONOMIA', '03/14/2013', 80, '2461337851', '4640961', 'BARBARACIELO@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1094, 1812, '', 'GALH471013HTLRMG07', 'HUGO GRACIA LIMA', 'PRIV. MORELOS PONIENTE #25-2', 'null', 'CHIAUTEMPAN', 90800, 0, 'CASADO', 'H', '10/13/1947', '09/14/2016', '', '', 'PASANTE', 'ARTESAN', '03/20/2013', 80, '2464698793', '4640207', 'AQUA_HUGRALIM@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1095, 1811, '', 'SESS700220MTLRRV01', 'SEVERINA SERRANO SUAREZ', 'VENUSTIANO CARRANZA # 7', 'null', 'TETLATLAHUCA', 90730, 0, 'CASADA', 'M', '02/20/1970', '01/05/2016', '', '', 'DIPLOMADO', 'ESTILISMO Y BIENESTAR PERSONAL', '03/20/2013', 81, '2461872423', '', '', 'sin observaciones', '0', 32, '', '', '', '', '', '', '', '', '0'),
(1096, 1810, '', 'AUGA940828HTLGNG07', 'AGUSTIN AGUILAR GONZALEZ', 'CALLE POTRERO # 6', 'null', 'SANTA CRUZ AQUIAHUAC', 90733, 0, 'SOLTERO', 'H', '08/28/1994', '01/15/2016', '', '', 'TECNICO', 'CONFECCI', '03/20/2013', 81, '5548866572', '2461298496', 'GUZ_CRAZY@HOTMAIL.COM', 'sin observaciones', '0', 32, '', '', '', '', '', '', '', '', '0'),
(1105, 1824, '', 'MUCY791031MTLXCN05', 'YENNY MUÑOZ COCOLETZI', '20 DE  MAYO 11', 'null', 'SECCION 1RA.', 90670, 0, 'CASADA', 'M', '10/31/1979', '11/04/2013', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/04/2013', 84.4, '246 221 23 53', '12461440344', 'VENMU_39@HOTMAIL.COM', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1108, 1831, '', 'ZAZE890426MTLMCR05', 'ERIKA ZAMORA ZECUA', 'DIEGO MARTIN 110', 'null', 'SAN PEDRO TLALCUAPAN', 90845, 0, 'SOLTERA', 'M', '04/26/1989', '08/28/2013', '', '', 'LICENCIATURA', 'INFORM', '04/04/2013', 81.66, '2464948626', '', 'ERI_TAU@HOTMAIL.COM', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1111, 1815, '', 'PASJ890217HTLRNS08', 'JOSE DE JESUS PAREDES SANCHEZ', 'ANGEL VILLA VERDE 1005', 'null', 'COVADONGA', 90407, 0, 'SOLTERO', 'H', '02/17/1989', '07/02/2013', '', '', 'PASANTE', 'ALIMENTOS Y BEBIDAS', '04/04/2013', 92.66, '241 122 61 65', '', 'PP_RE.BEL@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1113, 1819, '', 'JUXN880801MTLRCH08', 'NOHEMI JUAREZ XOCHITIOTZI', 'MARIANO MATAMOROS 11', 'null', 'SECCION SEXTA', 90670, 0, 'SOLTERA', 'M', '08/01/0988', '12/03/2015', '', '', 'LICENCIATURA', 'INGL', '04/04/2013', 83.83, '2461553174', '4610145', 'NAHOMI618@HOTMAIL.COM', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1114, 1821, '', 'HUOR890828HPLRXC02', 'RICARDO HUERTA DE LA O', 'ARETILLOS 1904', 'LOMA FLORIDA 2', 'TLAXCALA ', 90356, 4, 'U. LIBRE', 'H', '28/08/1989', '08/19/2013', '', 'GASTRONOMIA; CHEF ', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '04/04/2013', 86, '2414076491', '', 'chef-kikin@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1128, 1838, '', 'DISR740124MTLZNS08', 'ROSARIO DIAZ SANTOS', 'CALLE BENITO JUAREZ # 21', 'null', 'BARRIO XOLALPA', 90790, 0, 'CASADA', 'M', '01/24/1974', '07/22/2016', '', '', 'LICENCIATURA', 'PRODUCCCI', '04/18/2013', 85, '2225244501', '', 'chayito240174@outlook.com', 'sin observaciones', '0', 41, '', '', '', '', '', '', '', '', '0'),
(1129, 1839, '', 'JUHP860116MTLRRT00', 'PATRICIA JUAREZ HERNANDEZ', 'JOSEFA ORTIZ DE DOMINGUEZ 7', 'null', 'PLAN DE AYALA', 91430, 0, 'CASADA', 'M', '01/16/1986', '05/20/2013', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/18/2013', 86.96, '2414198333', '2411083748', 'CHIKIQUIKI@HOTMAIL.COM', 'sin observaciones', '0', 31, '', '', '', '', '', '', '', '', '0'),
(1132, 218, '', 'CEGV660515MTLRRC08', 'VICTORIA CERVANTES GARCIA', 'DEL TRABAJO SIN NUMERO', 'XICOHTENCATL', 'SAN JOSE ', 90500, 10, 'CASADA', 'F', '15/05/1966', '05/13/2013', '', 'CULTOR DE BELLEZA', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/18/2013', 86, '', '2471102320', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1134, 1841, '', 'SEJR830619MHGRRT05', 'RUTH SERRANO JUAREZ', 'AVENIDA 1RO DE MAYO 51', 'null', 'BARRIO DEL ALTO', 90640, 0, 'SOLTERA', 'M', '06/19/1983', '09/09/2013', '', '', 'LICENCIATURA', 'INGLES', '04/17/2013', 85.96, '2461033198', '4610368', 'SIRUTH1@HOTMAIL.COM', 'sin observaciones', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1146, 1852, '', 'CAVV751124MDFLRR02', 'VIRGINIA PAOLA CALDERON VARGAS', 'HUEXOTITLA  No 30 ', ' ESQUINA REVOLUCIÓN ', 'TOTOLAC', 90160, 3, 'CASADA', 'M', '24/11/1975', '02/13/2014', '', 'ESTILISTA', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '05/09/2013', 87, '2461349190', '2461247334', 'pao.calderon@live.com.mx', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1150, 1856, '', 'VACV820521HTLZML07', 'VALENTIN VAZQUEZ CUAMATZI', 'FRANCISCO I MADERO 5', 'null', 'OCOTLAN TEPATLAXCO', 90692, 0, 'U. LIBRE', 'H', '05/21/1982', '09/04/2013', '', '', 'LICENCIATURA', 'INFORM', '05/09/2013', 85, '2461104079', '4155006', 'ING.VALENTIN.VAZQUEZ@HOTMAIL.COM', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1152, 1858, '', 'LUSC731021MTLNGL09', 'CELIA LUNA SEGUNDO', 'MARIANO MATAMOROS 39', 'null', 'COLONIA CENTRO', 90122, 0, 'CASADA', 'M', '10/21/1973', '05/20/2014', '', '', 'SECUNDARIA', 'ARTESAN', '05/23/2013', 91.68, '2461274438', '', '', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1153, 1859, '', 'COBG780512MTLNRB06', 'GABRIELA M. CONTRERAS BARRERA', 'AQUILES SERDAN 1214', NULL, 'JESUS Y SAN JUAN', 90350, 0, 'M. SOLTERA', 'M', '05/12/1978', '02/13/2014', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '05/23/2013', 88, '241 116 43 29', '', 'GABICHAMOXA@HOTMAIL.COM', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1158, 1865, '', 'PELK881228MTLRMN07', 'KENYA LEE PEREZ LEMUS', 'CALZADA FERNANDO BRETON 34', 'null', 'ACXOTLA DEL RIO', 90160, 0, 'CASADA', 'M', '12/28/1988', '09/04/2013', '', '', 'LICENCIATURA', 'INGLES', '05/28/2013', 85.68, '2461587694', '4621342', 'PEREZLK2888@GMAIL.COM', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1163, 1866, '', 'SACN730315MTLNRN08', 'NANCY SANCHEZ CERVANTES', 'AVENIDA IGNACIO ZARAGOZA 126', 'null', 'COLONIA IDECO', 90300, 0, 'M. SOLTERA', 'M', '03/15/1973', '06/13/2013', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '06/06/2013', 89, '241 119 76 52', '12414177768', 'NANCYSANCHEZCERVANTES@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1168, 1871, '', 'OIAV770429MDFRRR07', 'VERONICA ORTIZ DE ZARATE ARMENTA', 'PRIV EMILIANO ZAPATA 4', 'null', 'ATEMPAN', 90010, 0, 'SOLTERA', 'M', '04/29/1977', '', '', '', 'BACHILLERATO', 'ARTESAN', '08/08/2013', 86, '246 757 78 47', '46 8 57 13', 'VEKARTE@YAHOO.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1169, 357, '', 'JURJ520519HVZRMR08', 'JORGE LUIS JUAREZ ROMERO', 'VICTORIA SUR 205-6', NULL, 'CENTRO', 90500, 0, 'CASADO', 'M', '05/19/1992', '01/15/2016', '', '', 'TECNICO', 'DISE', '08/16/2013', 87, '2471092245', '01247 1024103', 'TALLERDELMAESTRO@HOT.MAIL.COM', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1176, 1878, '', 'MECA890507MTLLRN09', 'ANABEL MELENDEZ CERVANTES', 'JOSEFA CASTELAR  8', 'null', 'TLAXCALA', 90000, 0, 'SOLTERA', 'M', '05/07/1989', '10/02/2013', '', '', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '09/09/2013', 85.1, '2464630027', '', 'ANEUROSYM310@HOTMAIL.COM', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1180, 607, '', 'AAPR890527HTLLRG00', 'ROGELIO ALVAREZ PORTILLO', 'AV. CRUZ COLORADA 50', 'null', 'ZACATELCO', 90740, 0, 'SOLTERO', 'H', '05/27/1989', '02/04/2014', '', '', 'PASANTE', 'ALIMENTOS Y BEBIDAS', '10/21/2013', 85.32, '246 116 94 28', '', 'ROLIZ2789@HOTMAIL.COM', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1187, 1888, '', 'FOCJ881229HTLLRV02', 'JAVIER FLORES CARRASCO', 'PLAZA DEL TULIPAN EDIFICIO F 12 DEPARTAMENTO 2', 'UNIDAD HABITACIONAL INFONAVIT ', 'NANACAMILPA', 90280, 1, 'SOLTERO', 'H', '12/29/1988', '01/23/2014', '', 'PROFECIONAL ', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '01/02/2014', 88, '17487660509', '7481060555', 'carrasco13@hotmail.com', 'sin observaciones', '0', 21, '', '', '', '', '', '', '', '', '0'),
(1189, 677, '', 'DAPE721211MDFVLM06', 'EMMA DAVILA PALACIOS', 'AV. JUAREZ NO. 114 CENTRO', 'null', 'APIZACO', 90300, 0, 'SOLTERA', 'M', '12/11/1972', '01/23/2014', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '01/02/2014', 86.8, '', '241 414 81 29', 'DAVILA_DPALACIOS@HOTMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1192, 1892, '', 'HEAJ621231HTLRRN05', 'JOSE JUAN FRANCISCO HERNANDEZ ARROYO', 'ARTICULO 123 NO. 134', 'null', 'SANTA ROSA', 90340, 0, 'CASADO', 'H', '12/31/1962', '01/23/2014', '', '', 'SECUNDARIA', 'MEC', '01/02/2014', 85.72, '2411087452', '12414176771', 'JOSEJUAN.HERNANDEZARROYO@GMAIL.COM', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1209, 1909, '', 'SORM950801MTLLMR08', 'MARCIA JAQUELINE SOLIS RAMIREZ', 'CALLE 2 SUR S/N', 'null', 'EL ROSARIO', 90256, 0, 'null', 'M', '08/01/1995', '05/07/2014', '', '', 'DIPLOMADO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/22/2014', 88.33, '241 123 5958', '241 412 5090', 'GOML36@LIVE.COM', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1210, 1910, '', 'ROIC420701HDFDBR06', 'CARLOS RODRIGUEZ IBARRA', 'CALLE 27 # 810', 'null', 'LA LOMA XICOHTENCATL', 90070, 0, 'null', 'H', '07/01/1942', '04/08/2016', '', '', '', 'ALIMENTOS Y BEBIDAS', '05/12/2014', 86.5, '', '246 144 5001', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1216, 1931, '', 'CALP790326MTLRZL07', 'PALOMA GPE. CARMONA LOZANO', 'CALLE ACTIPAC N', '', 'TETLA  DE LA SOLIDARIDAD', 90430, 0, '', 'M', '03/26/1979', '10/07/2016', '', '', 'PASANTE', 'ADMINISTRAC', '05/23/2014', 93, '241 124 0036', '241 496 8006', 'paloma_gcl@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1228, 1916, '', 'SASG760123MTLLLL09', 'GLORIA SALVADOR SOLIS', '9 NORTE N', '', 'IXTENCO', 90580, 0, 'CASADO', 'F', '23/01/1976', '04/20/2016', '', 'TÉCNICO', 'BACHILLERATO', 'ARTESANÍAS CON FIBRAS TEXTILES ', '06/27/2014', 92, '247 471 0740', '', '', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1232, 1943, '', 'SAMJ740112HTLNNN04', 'JUAN CARLOS SANCHEZ MUNIVE', 'CALLE MANUEL SALDA', 'null', 'BARRIO SAN SEBASTIAN', 90800, 0, 'null', 'H', '01/12/1974', '11/07/2016', '', '', 'PASANTE', 'MEC', '05/27/2014', 88.2, '246 102 0932', '246 461 8020', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1235, 1945, '', 'MUFU891013HTLXLR04', 'JOSE URIEL MUÑOZ FLORES', 'CALLE CUAUHTEMOC S/N', 'null', 'GUADALUPE IXCOTLA', 90810, 0, 'null', 'H', '10/13/1989', '03/22/2016', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '05/22/2014', 87.8, '2461550165', '', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1241, 1951, '', 'LOJJ851220MTLPMN09', 'JANETH LOPEZ JIMENEZ', 'ANDADOR NEVADO DE TOLUCA  N', 'null', 'UNIDAD HAB. SAN RAFAEL ATLIXTAC', 90350, 0, 'null', 'M', '12/20/1985', '01/14/2016', '', '', 'LICENCIATURA', 'SASTRER', '05/23/2014', 86.48, '241 108 8837', '241 418 0965', 'dm_janeth@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1252, 1962, '', 'AOZM830507MMCRPR00', 'MIRNA DALIA ARGOTE ZEPEDA', 'CERR. SILVESTRE REVUELTAS EDIF. 119 DPTO. N', 'null', 'UNIDAD HAB. TLAPANCALCO', 90118, 0, 'null', 'M', '05/07/1983', '01/26/2016', '', '', 'LICENCIATURA', 'ADMINISTRACI', '05/22/2014', 85.32, '246 125 4050', '', 'margotezepeda@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1264, 1974, '', 'HELF760701HTLRNR05', 'FRANCISCO JAVIER HERNANDEZ LEON', 'CALLE 12 DE OCTUBRE N', 'null', 'BARRIO SAN LUCAS', 90505, 0, 'null', 'H', '07/01/1976', '09/09/2016', '', '', 'PASANTE', 'ALIMENTOS Y BEBIDAS', '05/22/2014', 85, '247 104 2484', '247 472 4878', 'frjahele@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1275, 1927, '', 'CECL671105MTLRRC06', 'LUCY LAURA CERÓN CERVANTES', 'AV. PUENTECILLOS S/N', 'null', 'CONCEPCION HIDALGO', 90550, 0, 'null', 'M', '11/05/1967', '08/24/2016', '', '', 'BACHILLERATO', 'ARTESAN', '09/01/2014', 85, '276 107 5388', '', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1277, 323, '', 'MAVT710606MTLRLR01', 'MARÍA TRINIDAD MARTÍNEZ VALADEZ', 'CALLEJON FERNANDEZ LEAL 10', '', 'EL CARMEN TEQUEXQUITLA', 90570, 11, 'UNION LIBRE ', 'F', '06/06/1971', '04/07/2016', '', 'CULTURA DE BELLEZA', 'TECNICO', 'SERVICIOS DE BELLEZA ', '09/03/2014', 87, '2761109989', '2764775247', 'valadezmt@hotmail.com', 'sin observaciones', '0', 7, '', '', '', '', '', '', '', '', '0'),
(1279, 1978, '', 'SAGC900812MTLNNN02', 'MARIA CONCEPCION SANCHEZ GONZALEZ', 'ALVARO OBREGON # 12', 'null', 'TLAXCO', 90250, 0, 'null', 'M', '08/12/1990', '', '', '', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '10/08/2014', 85.5, '241 101 5032', '', 'stars_120890@hotmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1291, 1990, '', 'CAMI831128MTLBDV01', 'IVONNE MONSERRAT CABALLERO MEDEL', 'ANDADOR HERREROS #8', 'null', 'UNIDAD STA CRUZ', 90800, 0, 'null', 'M', '11/28/1983', '01/12/2016', '', '', 'BACHILLERATO', 'ESTILISMO Y BIENESTAR PERSONAL', '02/24/2015', 86.16, '2461563960', '5464646596', 'ivonn.montserrat92@gmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1293, 1992, '', 'ROLS800804MPLLNN04', 'SANDRA ROLDÁN LUNA', 'PRIV. REVOLUCI', 'null', 'XITOTOTLA', 90740, 0, 'null', 'M', '08/04/1980', '03/23/2016', '', '', 'SECUNDARIA', 'ARTESAN', '02/24/2015', 85.28, '2461069847', '2464972325', '', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1295, 1994, '', 'SAFC680320HTLNLT06', 'CUTBERTO SÁNCHEZ FLORES', 'CALLE TLACUALOYAN NORTE', 'null', 'SAN LORENZO TLACUALOYAN', 90450, 0, 'null', 'H', '03/20/1968', '03/17/2016', '', '', 'TECNICO', 'INSTALACIONES HIDR', '02/24/2015', 85, '2411122072', '', '', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1298, 1997, '', 'GORA890510HTLNDB03', 'ABELARDO GONZÁLEZ RODRÍGUEZ', 'CALLE FRANCISCO VILLA', 'null', 'JOSE MARIA MORELOS BUENAVISTA', 90250, 0, 'null', 'H', '05/10/1989', '03/28/2017', '', '', 'LICENCIATURA', 'MEC', '03/18/2015', 87, '2411226392', '2414122253', 'barracudo_0791@hotmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1304, 2003, '', 'GOCA890629MTLNRN01', 'ANA JAQUELINE GONZALEZ CERVANTES', 'PRIV. 5 DE MAYO # 290', 'null', 'SAN JOSE TETEL', 90454, 0, 'null', 'M', '06/29/1989', '04/07/2016', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '03/26/2015', 89.52, '2414100821', '', 'jakygonzalez19@gmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1308, 2007, '', 'LISM621221MPLMLR07', 'MARTHA PATRICIA LIMA SALDAÑA', 'UNIDAD HABITACIONAL PANZACOLA EDIF 39 DEPTO. C', 'null', 'PANZACOLA', 90800, 0, 'null', 'M', '12/21/1962', '12/03/2015', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '03/26/2015', 87, '2461922386', '', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1317, 2016, '', 'OIHG800319HDFRRS00', 'JOSE GUSTAVO ORTÍZ HERNÁNDEZ', 'AV. DEL BOSQUE SUR # 10', 'null', 'FRACC. DEL BOSQUE; OCOTLAN', 90000, 0, 'null', 'H', '01/01/1000', '12/04/2015', '', '', 'SECUNDARIA', 'INGL', '04/16/2015', 85, '045 222 50 61 024', '2464626791', 'sinder452003@yahooo.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1322, 2019, '', 'VEHA950302HTLLRL00', 'ALAN ROBERTO VELÁZQUEZ HERNÁNDEZ', 'PRIV. LIEBRES # 6', 'null', 'COL. LOMA PARAISO', 90355, 0, 'null', 'H', '03/02/1995', '12/03/2015', '', '', 'BACHILLERATO', 'INGL', '04/16/2015', 88.48, '2411243369', '2414179206', 'fantasmadeesparta72@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1323, 2020, '', 'LOLI920316HTLPPV00', 'IVAN ULISES LÓPEZ LÓPEZ', 'CALLE EMILIANO ZAPATA # 20', 'GUADALUPE CUAUHTEMOC', 'TLAXCALA ', 90421, 5, 'CASADO', 'H', '016/03/1992', '12/04/2015', '', 'LENGUAS MODERNAS APLICADAS ', 'LICENCIATURA', 'INGLES', '04/16/2015', 87, '2411145428', '', 'b13-king@hotmail.com', 'sin observaciones', '0', 11, '', '', '', '', '', '', '', '', '0'),
(1324, 2021, '', 'PECE740701MTLRSS01', 'ESTHER PÉREZ CISNEROS', 'CALLE BARRANCA  SECA # 4', 'null', 'SECCION TERCERA EXQUITLA', 90740, 0, 'null', 'M', '07/01/1974', '08/23/2016', '', '', 'LICENCIATURA', 'DERECHO', '04/16/2015', 87, '2461287838', '4971750', 'licesther@hotmail.com', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1336, 2033, '', 'RERF641120MTLYDL04', 'MA. FELIX REYES RODR', 'CALLE TIERRA Y LIBERTAD # 67', 'null', 'RANCHER', 90232, 0, 'null', 'M', '11/20/1972', '', '', '', 'SECUNDARIA', 'ARTESAN', '04/16/2015', 85, '7481067122', '', '', 'sin observaciones', '0', 47, '', '', '', '', '', '', '', '', '0'),
(1337, 2034, '', 'CECN720605MTLRRN04', 'NOEMI CERÓN CERVANTES', 'AV. PUENTECILLOS S/N', 'null', 'CONCEPCI', 90500, 0, 'null', 'M', '06/05/1972', '02/16/2016', '', '', 'TECNICO', 'ARTESAN', '04/16/2015', 85, '2761126471', '', 'nccpr.popam@gmail.com', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1338, 2035, '', 'HEGR810222MTLRLS02', 'ROSA ISELA HERNÁNDEZ GALICIA', 'CALLE ALLENDE # 4', 'null', 'COL. CENTRO', 90290, 0, 'null', 'M', '02/22/1981', '12/03/2015', '', '', 'BACHILLERATO', 'INGL', '04/16/2015', 85, '2411206668', '2414153572', 'redrosehg@hotmail.com', 'sin observaciones', '0', 12, '', '', '', '', '', '', '', '', '0'),
(1341, 2038, '', 'LAMN701120MDFZRR01', 'MARÍA NORA LÁZARO MARTÍNEZ', 'CALLE LAS GARZAS # 1-A', 'null', 'SAN DIEGO XOCOYUCAN', 90122, 0, 'null', 'M', '11/20/1970', '03/03/2016', '', '', 'SECUNDARIA', 'ARTESAN', '04/16/2015', 85, '2481411173', '', 'myloveangelessixx@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1343, 2048, '', 'BARN900803HTLTMZ00', 'NAZARIO BAUTISTA RAMOS', 'CALLE LERDO # 25', 'null', 'BARRIO DE GUADALUPE', 90570, 0, 'null', 'H', '08/03/1990', '03/30/2016', '', '', 'LICENCIATURA', 'INFORM', '05/04/2015', 85.32, '2471213947', '', 'naz_thebest@hotmail.com', 'sin observaciones', '0', 7, '', '', '', '', '', '', '', '', '0'),
(1344, 2057, '', 'GOSC791105MTLNNL08', 'MARIA CLAUDIA GONZALEZ SANCHEZ', 'ANDADOR NO. 3-3 U. HABITACIONAL CTM', 'null', 'APIZACO', 90010, 0, 'null', 'M', '11/05/1979', '01/18/2016', '', '', 'LICENCIATURA', 'HOTELER', '05/04/2015', 88, '12414147878', '', 'angelstandbyme_5@hotmail.om', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1345, 2051, '', 'PAPG821216MTLLRR07', 'GRISEL PALOMINO PARRAGUIRRE', 'AV. ZARAGOZA NO. 53 SAN', 'null', 'SAN JOS', 90500, 0, 'null', 'M', '12/16/1982', '04/07/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '04/30/2015', 87.04, '', '12474728010', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1346, 2040, '', 'POPA860301MTLRRL02', 'ALMA CELIA PORTILLO PEREZ', 'AV DEMOCRACIA  # 90', 'null', 'SECCION SEGUNDA', 90780, 0, 'null', 'M', '03/01/1986', '', '', '', 'LICENCIATURA', 'ESTILISMO Y BIENESTAR PERSONAL', '05/04/2015', 93.68, '', '2222791734', 'celi_sol03@hotmail.com', 'sin observaciones', '0', 42, '', '', '', '', '', '', '', '', '0'),
(1347, 2041, '', 'PAHO660101HTLDRD05', 'ODILÓN PADILLA HERNÁNDEZ', 'CALLE EMILIANO ZAPATA # 7', 'null', 'SAN SEBASTIAN; TIZATLAN', 90100, 0, 'null', 'H', '01/01/1996', '03/17/2016', '', '', 'LICENCIATURA', 'INSTALACIONES HIDR', '05/04/2015', 92.84, '2464764362', '2464661610', 'hernan_es@msn.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1352, 2046, '', 'SAFA840101MTLNLR09', 'ARACELI SAN JUAN FLORES', 'PRIV. XICOHTENCATL # 4', 'null', 'TIZATLAN', 90100, 0, 'null', 'M', '01/01/1984', '10/17/2016', '', '', 'LICENCIATURA', 'PRODUCCCI', '05/04/2015', 89.48, '2461431701', '2461175478', 'ara2219@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1357, 2053, '', 'BALM630504MTLZNN17', 'MA. MONICA BAEZ LEÓN', 'CALLE GUERRERO SUR # 208', 'null', 'COL. CENTRO', 90500, 0, 'null', 'M', '05/04/1963', '', '', '', 'BACHILLERATO', 'ALIMENTOS Y BEBIDAS', '05/05/2015', 86.84, '2471068891', '2471063063', '', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1359, 2055, '', 'CEHL930827MTLRRS02', 'LUISA CERCADO HERRERA', 'CALLE PUENTECILLOS # 2', 'null', 'COL. CONCEPCI', 90550, 0, 'null', 'M', '08/27/1993', '04/07/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '05/05/2015', 85.12, '', '2471015198', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1361, 2058, '', 'LIJW840902HPLMML01', 'WALIA DAVID LIMA JIMENEZ', 'AV. RIO GRIJALVA # 103-A', 'FRACCIONAMIENTO VISTA HERMOSA', 'OCOTLAN', 90010, 7, '', 'H', '02/09/1684', '12/03/2015', '', 'LINGUISTICA APLICADA', 'LICENCIATURA', 'INGLES', '05/05/2015', 85, '2461099960', '2464647636', 'jljmjjb2003@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1376, 2072, '', 'BASD730511MTLRZL03', 'DULCE MARÍA BRAVO SAUZA', 'ALHEL', '', 'CUATRO SE', 90110, 0, '', 'F', '05/11/1973', '02/25/2016', '', 'LICENCIATURA EN TRABAJO SOCIAL; SOCIOLOGÍA; PSICOLOGÍA', 'LICENCIATURA', 'ADMINISTRACIÓN; ASISTENCIA EDUCATIVA', '06/25/2015', 86, '2464808067', '2411318073', 'gaviotabravo@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1383, 2077, '', 'QUCL820925MTLXST00', 'LETICIA QUIXTIANO CISNEROS', 'CALLE BARRANCA SECA # 4', 'null', 'SECC 3RA COL. EXQUITLA', 90740, 0, 'null', 'M', '09/25/1982', '03/31/2016', '', '', 'LICENCIATURA', 'INGL', '07/02/2015', 88.64, '246 757 95 26', '49 7 17 50', 'quixtiano_09@yahoo.com', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1387, 2081, '', 'CAHM800401MTLDRR04', 'MARGARITA CADENA HERNÁNDEZ', 'DE LA LUNA # 2', 'null', 'REAL DEL SOL; ACUITLAPILCO', 90110, 0, 'null', 'M', '04/01/1980', '', '', '', 'LICENCIATURA', 'INGL', '07/02/2016', 86.96, '2461453173', '', 'najlamargarita0116@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1389, 2083, '', 'PONJ880619MTLLVN07', 'JANETH POLVO NAVA', 'FRANCISCO I. MADERO # 16', '', 'STA CRUZ TETELA', 90830, 0, '', 'M', '06/19/1988', '01/26/2017', '', 'LICENCIATURA EN INFORMÁTICA', 'LICENCIATURA', 'INFORMÁTICA; ADMINISTRACIÓN; SERVICIO Y ATENCIÓN AL CLINETE', '07/02/2015', 86, '2461382294', '', 'vianney.123@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1391, 2085, '', 'MURM740210HTLXMG09', 'MIGUEL MUÑOZ RAMÍREZ', 'ZARAGOZA # 6', NULL, 'SECC 4TA', 90870, 0, NULL, 'M', '02/10/1974', '08/15/2016', '', '', 'LICENCIATURA', 'AGRONOMIA', '07/02/2015', 85, '2461495336', '2222793420', '', 'sin observaciones', '0', 17, '', '', '', '', '', '', '', '', '0'),
(1394, 2075, '', 'PERF831020MTLRDB01', 'FABIOLA ALEJANDRA PEREZ RODRIGUEZ', 'PLAZA DEL POCITO EDIF. 16 DEPTO 7 U. HAB. SANTA CRUZ', 'null', 'CHIAUTEMPAN', 90800, 0, 'null', 'M', '10/20/1983', '03/03/2016', '', '', 'LICENCIATURA', 'FLORISTERIA', '07/21/2015', 85.96, '246 494 7519', '246 468 7136', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1395, 1258, '', 'MUHR760102MTLXRS09', 'ROSALBA MUÑOZ HERNÁNDEZ', 'PRIVADA MATAMOROS PONIENTE No 318 Int. 11', NULL, 'BARRIO LA PRECIOSA', 90500, 10, 'Casada', 'M', '02/01/1976', '02/24/2016', '', 'Cultora de Belleza', 'BACHILLERATO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/31/2011', 82, '2471069019', '', 'ross_jjred@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1396, 2087, '', 'HEVI871012MTLTZV09', 'IVONNE ODETT HERNANDEZ VAZQUEZ', 'AÑO DE JUAREZ NO. 231   ', 'null', 'BARRIO DE SAN JOSE', 90505, 0, 'null', 'M', '10/12/1987', '04/01/2016', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '07/23/2015', 91, '442474758620', '12474721619', 'odefliv@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1397, 2088, '', 'EISJ731207HJCSRR07', 'JORGE LUIS ESPINOZA SERRANO', 'BOULEVAR BEATRIZ PAREDES NO. 4', 'null', 'TLAXCALA', 90000, 0, 'null', 'H', '07/12/1973', '04/13/2016', '', '', 'TECNICO', 'INGLES', '07/02/2015', 88, '2411083318', '2464601300', 'jorge.cadillac10@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1403, 2094, '', 'FOSE831230MTLLND08', 'EDITH FLORES SANCHEZ', 'null', 'null', 'SAN DIONISIO YAHUQUEMECAN', 90450, 0, 'null', 'M', '12/30/1983', '02/29/2016', '', '', 'LICENCIATURA', 'PRODUCCCI', '08/07/2915', 88.26, '', '452411537485', 'flores_se@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1411, 2102, '', 'FOMY850227MTLLRR03', '\r\nYORCELY FLORES MARTINEZ', 'FERNANDO MONTES DE OCA #8', 'null', 'LA LOMA XICOHTENCATL', 90070, 0, 'null', 'M', '02/27/1985', '09/14/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/14/2015', 86.08, '2461959264', '2461319108', 'chik.ymm@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1413, 2104, '', 'HEHR900306MTLRRS00', 'ROSA HERNANDEZ HERNANDEZ', 'AV. PEDREGAL #25-A 2DA SECCION', 'null', 'null', 90850, 0, 'null', 'M', '03/06/1990', '01/12/2016', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '03/06/2015', 85.28, '2461543441', '12464619494', 'vtahendez@outlook.com', 'sin observaciones', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1414, 2105, '', 'LILC920101MTLMZY07', 'CYNTIA NAYELI LIMA LOAIZA', 'PRIV PINOS #8 INT 6 ATEMPAN', 'null', 'null', 90100, 0, 'null', 'M', '01/01/1992', '06/21/2016', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '08/14/2015', 85.16, '2461976968', '12464628132', 'black_rose_0125@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1415, 2106, '', 'ROFY860611MTLDNZ04', 'YAZMIN RODRIGUEZ DE LA FUENTE', 'CALLE PINO SUAREZ #1', 'null', 'SECCION SEGUNDA', 90670, 0, 'null', 'M', '06/11/1986', '02/05/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/13/2015', 85.16, '2461295270', '(01246) 46-1-12-11', 'yaz20_132@hotmail.com', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1417, 2108, '', 'TERJ910513HTLXDR00', 'JORGE LUIS TEXIS RODRIGUEZ', 'AV. REFORMA #54 1A SECCION', 'null', 'null', 90660, 0, 'null', 'H', '05/13/1991', '01/19/2017', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '08/14/2015', 85, '442461343022', '', 'koit_true@hotmail.com', 'sin observaciones', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1422, 2113, '', 'RONY770110MTLBTY14', 'YEYMI ROBLES NIETO', 'PRIV. JUAN PABLO SEGUNDO #18 COL. LA AURORA', 'null', 'null', 90812, 0, 'null', 'M', '01/10/1977', '09/22/2016', '', '', 'LICENCIATURA', 'INFORM', '08/08/2015', 90.32, '2461343749', '2464975017', 'lalunarobles2@htomail.com', 'sin observaciones', '0', 29, '', '', '', '', '', '', '', '', '0'),
(1428, 2119, '', 'MEMI790423HTLNRS02', 'ISRAEL MENESES MERCHANT', 'AV. 1', 'null', 'null', 90000, 0, 'null', 'H', '04/23/1979', '01/12/2016', '', '', 'LICENCIATURA', 'INGLES', '09/03/2015', 85.28, '442464762613', '2464661469', 'elmerchant@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1432, 2123, '', 'BAVJ720811HDFDLR04', 'JORGE LUIS BADILLO VALVERDE', 'CALLE QUETZALCOATL #82', 'SAN DIONISIO', 'TLAXCALA', 0, 5, 'CASADO ', 'H', '28/06/1975', '03/17/2016', '', '', 'SECUNDARIA', 'TALLA DE MADERA;MASCARAS DE CARNAVAL; ELABORACIÓN DE TRAJES DE CARNAVAL Y PLOME', '09/03/2015', 0, '2411032299', '012414154819', '', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1434, 2125, '', 'AAMF670823HTLLNL01', 'FELIPE JORGE ALVAREZ MENA', 'EMETERIO ARENAS #14', 'null', '2DA SECCION', 90740, 0, 'null', 'H', '08/23/1967', '05/19/2016', '', '', 'LICENCIATURA', 'PRODUCCCI', '09/24/2015', 92, '2461028009', '2464973116', 'felipejorge2003@yahoo.com.mx', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1441, 2131, '', 'HESM871113MTLRMR05', 'MARYCRUZ HERNANDEZ SAMPEDRO', 'CALLE GUADALUPE VICTORIA  #13', 'null', 'null', 90710, 0, 'null', 'M', '11/13/1987', '', '', '', 'LICENCIATURA', 'ARTESAN', '10/13/2015', 85.32, '22-25-40-92-22', '41-6-09-20', '', 'sin observaciones', '0', 23, '', '', '', '', '', '', '', '', '0'),
(1442, 2132, '', 'FAHE841122MPLJRS05', 'ESPERANZA FAJARDO HERRERA', 'CALLE FRANCISCO SARABIA #114', 'null', 'null', 92500, 0, 'null', 'M', '11/22/1984', '09/19/2016', '', '', 'LICENCIATURA', 'PRODUCCI', '10/13/2015', 89.48, '241-153-6413', '01-241-49-616-43', 'elijosejulio@hotmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1444, 2134, '', 'GUFB570520MTLVLR04', 'BERNARDINA GUEVARA FLORES', 'CALLE REVOLUCION MEXICANA #28', 'null', 'null', 90250, 0, 'null', 'M', '05/20/1957', '04/05/2016', '', '', 'SECUNDARIA', 'ARTESAN', '10/13/2015', 86.16, '241-110-1517', '', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1445, 2135, '', 'CUCB800617MTLMNT05', 'BEATRIZ CUAMATZI CONDE', 'CALLE 20 DE MAYO #78', 'null', 'VILLA CHICA', 90670, 0, 'null', 'M', '06/17/1980', '05/30/2016', '', '', 'LICENCIATURA', 'ARTESAN', '10/13/2015', 85.32, '2461044829', '45-8-29-01', '', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1446, 2136, '', 'PEGS891226MTLRTN05', 'SANDRA PEREZ GUTIERREZ', '1RA SECCION 36.', 'null', 'SAN BARTOLOME MATLALOHCAN', 90430, 0, 'null', 'M', '12/26/1989', '09/06/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/13/2015', 85.84, '241-103-9877', '', '', 'sin observaciones', '0', 31, '', '', '', '', '', '', '', '', '0'),
(1447, 2137, '', 'CAMO660921HTLRNS06', 'OSCAR CARRASCO MENDOZA', 'AV. PINOS #3103', 'null', 'LOMA FLORIDA SA SECCION', 90356, 0, 'null', 'H', '09/21/1966', '03/23/2016', '', '', 'LICENCIATURA', 'INFORM', '10/13/2015', 85.32, '241-419-1868', '41-7-74-84', 'bola_gc@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1448, 2138, '', 'GAOL740521MTLLRT04', 'LETICIA GALAN OROZCO', 'CALLE PABLO SIDAR #67', 'null', 'BARRIO DE JESUS', 90970, 0, 'null', 'M', '05/21/1974', '01/26/2016', '', '', 'BACHILLERATO', 'ARTESAN', '/  /', 85, '222-820-4021', '01-222-282-3071', 'lety7421@hotmail.com', 'sin observaciones', '0', 25, '', '', '', '', '', '', '', '', '0'),
(1450, 2140, '', 'SADL891108HTLNVS09', 'LUIS FERNANDO SANTOS DÁVILA', 'BLVD. EMILIO SANCHEZ PIEDRAS NO. 1411', 'null', 'COL. FERROCARRILERA', 90342, 0, 'null', 'H', '11/08/1989', '02/09/2016', '', '', 'LICENCIATURA', 'HOTELER', '10/21/2015', 91.68, '2411194748', '12414174539', 'alucord_238@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1454, 2143, '', 'FOBF480205HTLLLL03', 'FELIPE FLORES BALDERAS', 'INDEPENDENCIA 41', NULL, 'CHALMA', 90800, 0, 'CASADO', 'M', '02/05/1948', '02/02/2016', '', 'CARPINTERIA', 'PRIMARIA', 'DISEÑO Y FABRICACIÓN DE MUEBLES DE MADERA', '12/14/2015', 88, '246 171 9015', '246 464 0637', '', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1457, 2146, '', 'MOCB850205MTLNVR09', 'BERENICE DULCE MARIA MONTALVO COVARRUBIAS', 'MATAMOROS PTE # 212', 'null', 'HUAMANTLA', 90500, 0, 'null', 'M', '02/05/1985', '03/16/2016', '', '', 'LICENCIATURA', 'INGL', '12/14/2015', 85.26, '247 108 6866', '247 472 2159', 'bere.covarrubias@gmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1461, 2148, '', 'SAFL721027HTLNLZ13', 'LÁZARO IV SANCHEZ FLORES', 'BARBERAN Y COLLAR 25-4 SA', 'null', 'MARTIN DE PORRE', 90337, 0, 'null', 'H', '10/27/1972', '02/03/2016', '', '', 'LICENCIATURA', 'INFORM', '01/21/2016', 92.68, '2411167152', '2414180948', 'livsanchez@msn.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1462, 2149, '', 'POSF600314HPLNBR08', 'FRANCISCO JOSE LEONARDO PONCE SIBAJA', 'JOSEFA CASTELAN NO. 36', 'null', 'CENTRO', 90000, 0, 'null', 'H', '03/14/1960', '', '', '', 'DIPLOMADO', 'INGL', '01/21/2016', 89.32, '2461502653', '', 'francponc@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1463, 2150, '', 'QUZA940522HTLCRG08', 'AGUSTIN QUECHOL ZARATE', 'PROGRESO NO.23', 'null', 'TEXCACOAC', 90806, 0, 'null', 'H', '05/22/1994', '02/03/2016', '', '', 'LICENCIATURA', 'INGL', '01/21/2016', 89, '2461751445', '2464644672', 'xxagustinxx@gmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1464, 2151, '', 'HEVA930112HPLRZL09', 'ALEJANDRO HERNANDEZ VAZQUEZ', 'SANTA ELENA NO.25', 'null', 'SANTIAGO MICHAC', 90721, 0, 'null', 'H', '01/12/1993', '01/28/2016', '', '', 'LICENCIATURA', 'INGL', '01/21/2016', 88.64, '2481374772', '2461544017', 'kmus41@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1469, 2156, '', 'DIMB740709MTLZXL04', 'BLANCA OFELIA DIAZ MUÑOZ', 'EXQUITLA NO.37', 'null', 'ZACATELCO', 90740, 0, 'null', 'M', '07/09/1974', '02/09/2016', '', '', 'TECNICO', 'INGL', '01/18/2016', 85.69, '2461881209', '2227080045', 'atlantaga2000@msn.com', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1470, 2157, '', 'ZEMA720102HTLCXR07', 'ARTURO ZECUA MUÑOZ', 'AV. MALINTZI 206', 'null', 'SAN PEDRO MU', 0, 0, 'null', 'H', '01/02/1972', '01/28/2016', '', '', 'TECNICO', 'MANTENIMIENTO DE M', '01/22/2016', 85.32, '6642328784', '2464167056', 'arturozecuam@gmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1471, 2158, '', 'MOCP650721HTLNRR09', 'PRAXEDIS MONTIEL CORTE', '2DA PRIV JUAN CUAMATZI 8 4TA SECC', 'null', 'XICOHTZINGO', 90780, 0, 'null', 'H', '01/01/1000', '01/17/2017', '', '', 'LICENCIATURA', 'INGL', '02/05/2016', 85, '2225602232', '', '', 'sin observaciones', '0', 42, '', '', '', '', '', '', '', '', '0'),
(1473, 2160, '', 'NEFJ880127HTLTLS00', 'JOSUE FABIAN NETZAHUALCOYOTZI FLORES', '5 DE MAYO # 69', 'null', 'CONTLA', 90670, 0, 'null', 'H', '01/27/1988', '02/19/2016', '', '', 'PASANTE', 'INGL', '02/16/2016', 85.12, '246 106 5216', '2461040558', 'josuefabianf88@outlook.com', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1474, 2161, '', 'LOJG900828MTLPMB04', 'GABRIELA LOPEZ JIMENEZ', 'ANDADOR NEVADO DE TOLUCA # 15; FRACC. SAN RAFAEL ATLIXTAC', 'null', 'APIZACO', 90350, 0, 'null', 'M', '08/28/1990', '08/05/2016', '', '', 'LICENCIATURA', 'ADMINISTRAC', '02/15/2016', 85.12, '241 132 8922', '241 418 0965', 'lh_gabriela@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1477, 2164, '', 'OOAA890528MTLRRN06', 'ANA RUBI ORDO ARJONA', '12 DE MAYO #4; SAN JOS', 'null', 'YAUHQUEMECAN', 90459, 0, 'null', 'M', '05/28/1989', '05/16/2016', '', '', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '02/19/2016', 89, '2461594840', '', 'arescodornis23@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1478, 2165, '', 'MOCW900218MTLRNL05', 'WALLY DALILA MORALES CANO', 'AV. IGNACIO CARRANZA #17; COLONIA CENTRO', 'null', 'TEPETITLA', 90700, 0, 'null', 'M', '02/18/1990', '05/13/2016', '', '', 'LICENCIATURA', 'CONFECCI', '02/18/1990', 87.8, '2481630365', '', 'wallydalmc@hotmail.com', 'sin observaciones', '0', 19, '', '', '', '', '', '', '', '', '0'),
(1479, 2166, '', 'HEGJ610411HTLRNN02', 'JOSÉ JUAN FELIPE RÚBEN HERNÁNDEZ GONZÁLEZ', 'CALLE PINTO #36; LOS POTRILLOS', 'null', 'OCOTLAN', 90100, 0, 'null', 'H', '04/11/1961', '08/05/2016', '', '', 'LICENCIATURA', 'HOTELER', '02/16/2016', 86, '2461028315', '', 'feliperubenhernandez@yahoo.com.mx', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1480, 2167, '', 'UEGV630207HVZRTC08', 'VÍCTOR MANUEL URCELAY GUTIÉRREZ', 'PRIV. XICOHTENCALT #109-A', 'FATIMA', 'APIZACO', 90357, 10, 'CASADO', 'H', '21/07/1970', '04/11/2016', '', 'COCINERO', '', 'ALIMENTOS Y BEBIDAS', '02/16/2016', 85, '2411232888', '2414172647', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1481, 2168, '', 'ROFJ860506HTLMLS04', 'JOSUÉ ROMERO FLORES', 'AV. PROGRESO #15; SN. COSME', 'null', 'ATLANMAXAC', 90180, 0, 'null', 'H', '05/06/1986', '08/16/2016', '', '', 'LICENCIATURA', 'INFORM', '02/18/2016', 85.28, '2461635484', '4692633', 'rofjosue@gmail.com', 'sin observaciones', '0', 29, '', '', '', '', '', '', '', '', '0'),
(1484, 2171, '', 'ROMJ900816HTLCJS01', 'JOSUÉ ROCHA MEJÍA', 'AV. JU', 'null', 'NANACAMILPA', 90280, 0, 'null', 'H', '08/16/1990', '03/23/2016', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '02/17/2016', 86, '7481008674', '7487660471', 'josue_darvin@hotmail.com', 'sin observaciones', '0', 21, '', '', '', '', '', '', '', '', '0'),
(1485, 2172, '', 'AUYK940616MMCGNR07', 'KARLA AGUILAR YONCA', 'JUAN PONCE DE LE', 'null', 'IXTENCO', 90580, 0, 'null', 'M', '06/16/1994', '03/16/2016', '', '', 'TECNICO', 'INGL', '03/11/2016', 85.8, '2471210206', '', 'k_aguilar92@hotmail.com', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1486, 2173, '', 'GAXN731109MTLLCR03', 'NORA GALICIA XOCHITEMOL', 'REFORMA # 114', 'SAN MIGUEL', 'XALTIPAN', 90680, 8, NULL, 'M', '9/011/1973', '02/13/2017', '', 'TECNICO PEDAGOGICO', 'LICENCIATURA', 'COMPUTACION; ASISTENCIA EDUCATIVA', '03/10/2016', 92, '246 469 1040', '', 'norans109@hotmail.com', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1487, 2174, '', 'ZEVH821026HTLMZM01', 'HOMERO ZEMPOALTECA VAZQUEZ', 'NICOLAS BRAVO # 22', NULL, 'ZARAGOZA', 90160, 0, NULL, 'M', '10/26/1982', '03/17/2017', '', '', 'LICENCIATURA', 'INFORMÁTICA', '03/11/2016', 91, '246 104 9597', '', 'homber33157@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1491, 2178, '', 'MUHM661007HTLXRR00', 'MARCOS MUÑOZ HERNANDEZ', 'MOCTEZUMA # 22', 'null', 'CONTLA', 90670, 0, 'null', 'H', '10/07/2061', '07/19/2016', '', '', 'TECNICO', 'ELECTRICIDAD', '03/10/2016', 85, '246 181 0605', '', 'markos10prodigy.net.mx', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1493, 2180, '', 'GARG810615MTLRYR02', 'GRACIELA GRANDE REYES', 'CALMECATL 131;', NULL, 'SAN MIGUEL TLAMAHUCO', 90160, 0, NULL, 'M', '06/15/1981', '08/23/2016', '11/10/2017', '', 'SECUNDARIA', 'ARTESAN', '04/01/2016', 8, '', '2461337625', 'asaldanat@gmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1494, 2181, '', 'AIHC690513MPLLRR04', 'MARIA DEL CARMEN ALVIREZ HERNANDEZ', 'EMILIANO ZAPATA #7', 'null', 'ACXOTLA DEL RIO', 90160, 0, 'null', 'M', '05/13/1969', '', '', '', 'TECNICO', 'ARTESAN', '04/01/2016', 89, '4191003386', '2461280813', '', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1496, 2183, '', 'MOSN920725MTLLNL04', 'NELY MOLINA SANTIAGO', 'CALLE SOR JUANA INES DE LA CRUZ #47', 'null', 'TEOTLALPAN', 90168, 0, 'null', 'M', '07/25/1992', '', '', '', 'LICENCIATURA', 'INFORM', '03/31/2016', 85.28, '', '2464933182', 'nely.molinasgo@gmail.com', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1501, 2188, '', 'AIRA871118HTLTDL01', 'ALFONSO ATRIANO RODRIGUEZ', 'JOSEFA ORTIZ DE DOMINGUEZ #40', 'null', 'TETLANOHCAN', 90840, 0, 'null', 'H', '11/18/1987', '02/28/2017', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '05/31/2016', 0, '2461551414', '2464166589', '', 'sin observaciones', '0', 50, '', '', '', '', '', '', '', '', '0'),
(1502, 2189, '', 'MUPN901006MHGXND02', 'NADIR MUÑOZ PINEDA', 'AZUCENA S/N', 'null', 'LA UNION EJIDAL', 90264, 0, 'null', 'M', '10/06/1990', '06/16/2016', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '06/07/2016', 85.5, '2411236285', '2411316986', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1503, 2190, '', 'VACR890427HTLSRS00', 'ROSALIO VASQUEZ CARMONA', 'JOSE ARAMBURU No.109', '', 'TLAXCALA ', 90450, 13, NULL, 'H', '27/04/1989', '', '', 'ARTISTA EN GLOBOS ', 'BACHILLERATO', 'DECORACION CON GLOBOS ', '06/10/2016', 85, '2411178000', '', 'rosalio_1989@outlook.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1506, 2193, '', 'FOPB860917MTLLRR09', 'BRISA FLORES PAREDES', ' PRIV. JALAPA #6', 'null', 'MIRAFLORES', 90100, 0, 'null', 'M', '09/17/1986', '', '', '', 'LICENCIATURA', 'INFORM', '06/10/2016', 85, '', '2464592107', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1507, 2194, '', 'PUGR790226HTLLRF04', 'RAFAEL PLUMA GEORGE', 'INDEPENDENCIA #28', 'null', 'TECPA MORALES', 90830, 0, 'null', 'H', '02/26/1979', '08/15/2016', '', '', 'SECUNDARIA', 'DISE', '06/10/2016', 85, '', '2228468485', '', 'sin observaciones', '0', 8, '', '', '', '', '', '', '', '', '0'),
(1508, 2195, '', 'GOTF691013HMNNRL04', 'FILIBERTO GONZALEZ TORRES', 'DIAMANTE NO.88', 'null', 'LA JOYA', 90114, 0, 'null', 'H', '10/13/1969', '09/14/2016', '', '', 'LICENCIATURA', 'DISE', '06/30/2016', 90.32, '241336146', '2464622825', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1509, 2196, '', 'QUMJ860330HTLRNH02', 'JHONATAN QUIROZ MINOR', 'MIGUEL HIDALGO NO. 7', 'SAN MIGUEL XOCHITECA ', 'NATIVITAS', 91710, 14, '', 'H', '30/03/1986', '07/20/2016', '', 'MECANICO ESPECIALISTA EN MOTORES DE COMBUSTION ', 'LICENCIATURA', 'MECANICO AUTOMOTRIS ', '07/06/2016', 86, '5559611787', '2464161895', 'Jhonquirozminor96@outllook.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1510, 2197, '', 'AUMH831229HTLGLB01', 'HEBER AGUILAR MELENDEZ', '16 DE SEPTIEMBRE NO. 13', 'null', 'BARRIO TECPA PLUMA', 0, 0, 'null', 'H', '12/29/1983', '08/19/2016', '', '', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '06/29/2016', 85.8, '2461095636', '2464619148', '', 'sin observaciones', '0', 48, '', '', '', '', '', '', '', '', '0'),
(1511, 2198, '', 'ROCA950917HPLSRG07', 'AGUSTIN ROSAS DE LA CRUZ', 'PEDREGAL  S/N', 'null', 'SAN LUCAS TLACOCHCALCO', 90640, 0, 'null', 'H', '09/17/1995', '10/24/2016', '', '', 'LICENCIATURA', 'MEC', '07/06/2016', 85.96, '2461963310', '', '', 'sin observaciones', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1514, 2201, '', 'TOSL830525HPLRNS02', 'LUIS FERNANDO TORRENTERA SANCHEZ', 'TOPOGRAFOS NO.30', 'null', 'COL. LOMA BONITA', 90090, 0, 'null', 'H', '05/25/1983', '08/02/2016', '', '', 'LICENCIATURA', 'INGL', '07/15/2016', 91.16, '4620348', '2461117037', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1515, 2202, '', 'MEOM700217HTLJRR00', 'JOSÉ MARCIAL MEJÍA ORDUÑA', 'REFORMA NTE. 413 B', 'null', 'COL. CENTRO', 90500, 0, 'null', 'H', '02/17/1970', '08/09/2016', '', '', 'LICENCIATURA', 'INFORM', '07/25/2016', 91.16, '', '247-472-3450', 'jmmejia70@hotmail.com', 'sin observaciones', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1521, 2207, '', 'PEPA740129MTLRRN03', 'ANGELICA PEREZ PEREZ', 'MORELOS NO.4', 'null', 'CENTRO', 90240, 0, 'null', 'M', '01/29/1974', '08/29/2016', '', '', '', 'CONFECCI', '/  /', 0, '2414146891', '2414150390', '', 'sin observaciones', '0', 14, '', '', '', '', '', '', '', '', '0');
INSERT INTO `instructores` (`id`, `Expediente`, `imagen`, `Curp`, `Nombre`, `Calle`, `Colonia_barrio`, `Localidad`, `CP`, `Seccion_elec`, `Estado_civil`, `Sexo`, `F_Nacimiento`, `F_Alta`, `F_Baja`, `Pefil`, `Nivel_academico`, `Especialidad`, `Fecha_evaluacion`, `Puntaje`, `No_celular`, `No_telefono`, `Email`, `Observaciones`, `calidad`, `id_municipios`, `condicion`, `acta_doc`, `curp_doc`, `dom_doc`, `ine_doc`, `expfg_doc`, `expl_doc`, `cv_doc`, `actualizacion_doc`) VALUES
(1524, 2210, '', 'CONA880730MTLRVR04', 'ARACELI KARINA CORONA NAVA', 'REFORMA NO.33', '', 'SAN MIGUEL XALTIPAN', 90670, 0, '', 'F', '07/30/1988', '08/29/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/25/2016', 85, '2461955001', '', '', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1528, 2214, '', 'MOTJ900816HTLLLN06', 'JEAN ELI MOLINA TLAPALE', 'BOULEVAR REVOLUCION No.12 BIS ', NULL, 'ATEMPAN', 90000, 7, 'SOLTERO ', 'H', '16/008/1990', '', '', 'GASTRONOMIA ', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '08/10/2016', 87, '', '2461094766', 'pachizmolina88@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1529, 2215, '', 'GAZV710714MTLRMN03', 'VENTURA A. GARCÍA ZAMORA', 'MARCIAL AGUILA NO.139', 'FRACCIONAMIENTO LA NORIA ', 'NORIA 1', 90360, 4, 'SOLTERA', 'M', '14/07/1971', '09/05/2016', '', 'ESTILISMO', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/25/2016', 86, '2411330215', '2411213412', '', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1530, 2216, '', 'AEZC720908HPLRPR09', 'CRESCENCIO ARCE ZEPEDA', 'CALLE MALINTZI # 35', 'null', 'SAN ISIDRO BUENSUCESO', 0, 0, 'null', 'H', '09/08/1972', '09/27/2016', '', '', 'PASANTE', 'DISE', '08/16/2016', 86.32, '2223256439', '1222910451', 'arcezepedac@gmail.com', 'sin observaciones', '0', 25, '', '', '', '', '', '', '', '', '0'),
(1531, 2217, '', 'MOBM900729MTLRRT04', 'BEATRIZ MORA MORALES', 'PRIV. 5 DE MAYO S/N', 'null', 'BARRIO DE ZAPOTLA', 90110, 0, 'null', 'M', '07/29/1990', '09/14/2016', '', '', 'LICENCIATURA', 'CONFECCI', '08/25/2016', 86, '', '2411457850', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1533, 2219, '', 'AEOG880522MTLNRR00', 'GRACIELA ÁNGELES ORTEGA', '5 ORIENTE NO.23', 'null', 'IXTENCO', 90580, 0, 'null', 'M', '05/22/1998', '09/01/2016', '', '', 'LICENCIATURA', 'INFORM', '08/25/2016', 85.6, '2471066206', '2474729054', 'gracy15@live.com.mx', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1535, 2221, '', 'GALC771219HTLLZH00', 'CUAUHT GALINDO LEZAMA', 'DOMINGO ARENAS NO.91', 'null', 'SAN JORGE TEZOQUIPAN', 90147, 0, 'null', 'H', '12/19/1977', '08/30/2016', '', '', 'MAESTRIA', 'ADMINISTRAC', '08/08/2016', 90.32, '', '2461682470', 'galindo.lezama@gmail.com', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1537, 222, '', 'XIAA540626HTLCHN01', 'JOSÉ ANSELMO G. XICOHTENCATL AHUATZI', 'HERREROS OTE. #17', '', 'LOMA BONITA', 90090, 0, '', 'M', '06/26/1954', '02/09/2017', '', '', 'LICENCIATURA', 'INGL', '09/14/2016', 97, '', '2464805193', 'mtroguillermo@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1543, 2229, '', 'RASF791224MMCMRL05', 'MARÍA FLORIBEL RAMÍREZ SERRANO', '16 DE SEPTIEMBRE #12', 'null', 'TEPETITLA', 90700, 0, 'null', 'M', '12/24/1979', '09/22/2016', '', '', 'BACHILLERATO', 'ARTESAN', '09/20/2016', 85, '', '2481565923', 'flor_16rs@outlook.es', 'sin observaciones', '0', 19, '', '', '', '', '', '', '', '', '0'),
(1545, 2231, '', 'CARM910204HTLSMR00', 'MARCO ANTONIO CASTRO RAMIREZ', 'MATAMOROS NO. 67', 'null', 'DOMINGO ARENAS', 90280, 0, 'null', 'H', '02/04/1991', '01/11/2017', '', '', 'LICENCIATURA', 'INFORM', '09/21/2016', 85.8, '', '748766452', 'marco.castro22@outlook.com', 'sin observaciones', '0', 21, '', '', '', '', '', '', '', '', '0'),
(1546, 2232, '', 'SAHF870829HTLNRR06', 'FRANCISCO JAVIER SANTILLÁN HERNÁNDEZ', 'LARDIZABAL NO. 210', 'null', 'COL. CENTRO', 90300, 0, 'null', 'H', '08/29/1987', '10/19/2016', '', '', 'LICENCIATURA', 'CONTABILIDAD', '09/21/2016', 85.8, '2411564454', '2414186184', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1547, 2233, '', 'PAFE900830HTLRRR00', 'ERICK ISRAEL PAREDES FARRERA', 'CALLE FRANCISCO SARABIA # 1501', 'null', 'COL. SAN MARTIN DE PORRES', 90337, 0, 'null', 'H', '08/30/1990', '10/03/2016', '', '', 'LICENCIATURA', 'INFORM', '09/27/2016', 92, '12414186055', '2411347792', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1548, 2234, '', 'GASG921216MTLRNB01', 'GABRIELA GARCÍA SÁNCHEZ', 'ALDAMA # 12', 'null', 'EMILIANO ZAPATA', 90150, 0, 'null', 'M', '12/16/2016', '10/06/2016', '', '', 'TECNICO', 'ALIMENTOS Y BEBIDAS', '09/08/2016', 85, '', '2461954989', '', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1549, 2235, '', 'BIRL721007MTLRDL07', 'LILIANA BRINDIS RODRIGUEZ', 'CALLE ZARAGOZA #6 INT. 1', 'null', 'COLONIA CENTRO', 90200, 0, 'null', 'M', '10/07/2016', '10/13/2016', '', '', 'TECNICO', 'INGL', '10/04/2016', 85, '', '7499180906', '', 'sin observaciones', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1550, 2236, '', 'BEZP790426MTLRMT05', 'PETRA BERISTAIN ZEMPOALTECA', 'AV. INDEPENDECIA #68', 'null', 'COLONIA SAN FRANCISCO TEMETZONTLA', 90140, 0, 'null', 'M', '04/26/2016', '02/08/2017', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/06/2016', 85, '2461202249', '2461064049', 'petraberistainz@outlook.es', 'sin observaciones', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1552, 854, '', 'MEFD690917HTLLLG09', 'DAGOBERTO MELENDEZ FLORES', 'PROLONGACI', 'null', 'EL ALTO', 90800, 0, 'null', 'H', '09/17/1969', '11/17/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '10/14/2016', 88.6, '2461260805', '2461143149', 'gabynails2011@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1553, 2238, '', 'HESY820517MTLRNZ17', 'YAZMIN HERNÁNDEZ SÁNCHEZ', 'NETZAHUALCOYOTL 50 LAHERRADURA', 'null', 'CALPULALPAN', 90204, 0, 'null', 'M', '05/17/1982', '11/11/2016', '', '', 'SECUNDARIA', 'ARTESAN', '10/12/2016', 87.64, '5531065016', '7491059518', 'yazminhernandez241@gmail.com', 'sin observaciones', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1555, 2240, '', 'XOGD801223MTLCRL02', 'MARIA DELFINA XOCHITIOTZI GUARNEROS', 'FRANCISCO I. MADERO 7', NULL, 'SECCION SEXTA', 90670, 0, NULL, 'M', '12/23/1980', '11/08/2016', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '11/03/2016', 85, '2461795209', '', '', 'sin observaciones', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1556, 2241, '', 'MOFG740710HTLRGN07', 'GONZALO MORALES FIGUEROA', 'XICOHTENCATL NO. 6', 'null', 'CENTRO XALTOCAN', 90440, 0, 'null', 'H', '07/10/1974', '11/07/2016', '', '', 'LICENCIATURA', 'DISE', '10/28/2016', 85, '2411353683', '2414154075', '', 'sin observaciones', '0', 40, '', '', '', '', '', '', '', '', '0'),
(1560, 2245, '', 'LOHA901004HTLPRL04', 'ALEJANDRO LOPEZ HERNANDEZ', 'PRIV. FRANCISCO HERNANDEZ R NO.18', 'BARRIO CHICO ', 'TLAXCALA ', 90225, 2, '', 'H', '04/010/1990', '01/24/2017', '', 'PROFECIONAL ', 'LICENCIATURA', 'LENGUAS EXTRANGERAS ', '01/11/2017', 92, '', '2461336757', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1563, 2248, '', 'FOBB890105MTLLZR04', 'BERENICE FLORES BAEZ', 'INSURGENTES NO. 6 B ', 'SANTA MARIA', 'ACUITLAPILCO', 90110, 7, 'UNION LIBRE', 'M', '05/09/1989', '04/18/2017', '', 'COCINERO', 'PROFECIONAL', 'GASTRONOMIA', '11/28/2016', 85, '2461566859', '4680524', 'chefsita_89@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1564, 2249, '', 'LEPR670322MTLNRS06', 'ROSAURA LEON PEREZ', 'CARRET. TLAXCALA PUEBLA 60', '', 'SANTA MARIA ACUITLAPILCO', 90110, 0, '', 'M', '03/23/1967', '02/08/2017', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '01/30/2017', 87, '2464680890', '', 'rous.lp@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1566, 2251, '', 'EORD960612MMCSML04', 'DULCE SARAI ESCOBEDO ROMERO', 'CERRADA GLADIOLA EDIFICIO 111; DEPARTAMENTO 1; JARDINEZ DE APIZACO', 'null', 'SAN BENITO XALTOCAN', 90453, 0, 'null', 'M', '06/12/1996', '02/24/2017', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '01/25/2017', 85.8, '241 100 4649', '241 410 1576', 'nenito_1960@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1568, 2253, '', 'PACI680824MDFZNS02', 'MARIA ISABEL DE LA PAZ CONTRERAS', 'CALLE PROGRESO # 12 A U. HAB. VISOC UAT', 'null', 'TLAXCALA', 90110, 0, 'null', 'M', '08/24/1968', '05/02/2017', '', '', '', 'ASISTENCIA EDUCATIVA', '02/09/2017', 85.28, '2461769651', '2464685103', 'isa_2408@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1569, 2254, '', 'MAGA870721MTLTRN06', 'ANGELINA MATAMOROS GARCIA', 'C. SIN NOMBRE S/N; COLONIA LOMA FLORIDA', 'null', '1RA. SECCI', 90550, 0, 'null', 'H', '07/21/1987', '02/27/2017', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '02/10/2017', 86.12, '', '2761049491', 'angelina_cescet@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1572, 2256, '', 'DILS731206MTLZNC04', 'SOCORRO DIAZ LUNA', 'CRISTOBALITO 5TA. SECCION NO. 69', ' SAN CRISTOBALITO', 'SANTA MARIA ATLIHUETZIA', 90459, 5, NULL, 'M', '06/12/1973', '04/12/2017', '', 'TECNICO EN INGLES', 'LICENCIATURA ', 'INGLES', '02/14/2017', 90, '2464945045', '2464612727', 'cooo_dic06@hotmail.com', 'sin observaciones', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1573, 2257, '', 'GOLC890807MTLMPR01', 'CRISTINA GOMEZ LOPEZ', 'NOCHE BUENA S/N', 'null', 'COL. LOMA FLORIDA 1RA. SECCI', 90356, 0, 'null', 'M', '08/07/1989', '', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '02/07/2017', 86.96, '2411524711', '2414173159', 'stark_@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1577, 2261, '', 'PEBO860429HUZRLS01', 'OSCAR PEREZ BLANCO', 'AV.LIBERTAD #5; COLONIA CENTRO', '', 'SAN JERONIMO ZACUALPAN', 90738, 0, '', 'M', '04/29/1986', '04/11/2017', '', '', 'LICENCIATURA', 'INGL', '02/21/2017', 89, '', '2461236285', 'pb_oscar@hotmail.com', 'sin observaciones', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1578, 2262, '', 'LORS860916HPLPMN00', 'SANDRO IVAN LOPEZ RAMIREZ', 'BENITO JUAREZ #20 A; ', 'TLACOTEPEC DE JOSE MANZO', 'SAN SALVADOR EL VERDE', 74130, 0, NULL, 'H', '19/09/1986', '03/27/2017', '', '', 'TECNICO', 'ADMINISTRACION', '02/08/2017', 88, '', '2221594082', 'samyaenero01@hotmail.com', 'sin observaciones', '0', 61, '', '', '', '', '', '', '', '', '0'),
(1580, 2264, '', 'LIIG700227HTLMPB06', 'GABRIEL LIMA IPATZI', 'AMADOR CARRASCO #22', 'null', 'BARRIO TEXCACOAC', 90800, 0, 'null', 'H', '02/27/1970', '', '', '', 'LICENCIATURA', 'SOLDADURA Y PAILER', '02/09/2017', 86.96, '2461478380', '2461030135', 'paztzili57@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1587, 2271, '', 'ROAE891106HTLBGD05', 'EDGAR ROBLES AGUILA', 'BLVD. LA PRESA #2', 'null', 'COLONIA CENTRO', 90195, 0, 'null', 'H', '11/06/1989', '04/10/2017', '', '', 'LICENCIATURA', 'INFORM', '02/21/2017', 85.28, '2464971747', '2461470797', 'marprecor_tlax@live.com', 'sin observaciones', '0', 53, '', '', '', '', '', '', '', '', '0'),
(1592, 2276, '', 'LOPF750730MDFPLL02', 'FLOR DE JANETTE LOPEZ PALMA', 'C. FRESNOS NO. 56; UNIDAD HABITACIONAL 4', 'null', '4 SE', 90110, 0, 'null', 'M', '07/30/1969', '04/21/2017', '', '', 'LICENCIATURA', 'ADMINISTRAC', '03/06/2017', 86.12, '', '2464636998', 'psic_fdjaneth@hotmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1593, 2277, '', 'CECH690703MTLRRN01', 'HINORITA CERÓN CERVANTES', 'AV. PUENTECILLOS NO. 7', 'null', 'CONCEPCI', 90550, 0, 'null', 'M', '07/03/1969', '03/22/2017', '', '', 'BACHILLERATO', 'ARTESAN', '03/02/2017', 85.7, '', '2471040820', '', 'sin observaciones', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1596, 2280, '', 'HEGH620903HTLRNC09', 'JOSÉ HÉCTOR HERNÁNDEZ GONZÁLEZ', 'C. FRESNOS # 311; FRACCIONAMIENTO APIZAQUITO', 'null', 'APIZACO', 90401, 0, 'null', 'H', '09/03/1962', '', '', '', 'TECNICO', 'MANTENIMIENTO AUTOMOTRIZ', '03/21/2017', 85.16, '2411443005', '', 'mecind_hector@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1602, 2286, '', 'HEDM740708MTLRZR09', 'MARGARITA ISABEL HERNÁNDEZ DÍAZ', 'C. 23 NO. 424', '', 'LOMA XICOHTENCATL', 90070, 0, '', 'F', '07/08/1974', '', '', '', 'TECNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '03/28/2017', 86, '2464942954', '', ' bielabsc@yahoo.com.mx', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1607, 2291, '', 'MEHV720917MPLSRR03', 'VERÓNICA MESTIZA HERNÁNDEZ', 'PRIV. ALONSO DE AVILA NO.316', 'null', 'FRACC. PASO DE CORTES', 90356, 0, 'null', 'M', '09/17/1972', '', '', '', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '03/28/2017', 85.28, '', '2411724957', 'vemehe76@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1614, 2298, '', 'MEOR910106MJCNRY09', 'REYNA ANGELES MÉNDEZ ORTEGA', 'AV. DEL TRABAJO NO.16', 'null', 'BELEN ATZITZIMITITLAN', 90605, 0, 'null', 'M', '01/06/1991', '04/11/2017', '', '', 'PASANTE', 'ASISTENCIA EDUCATIVA', '03/28/2017', 85.12, '2462190952', '2464150161', 'a_mendez-91@hotmail.com', 'sin observaciones', '0', 2, '', '', '', '', '', '', '', '', '0'),
(1615, 2299, '', 'MEBS780805HTLNLR05', 'SERGIO MENDIETA BELLO', 'CALLE CUAUHTEMOC NUM. 8', 'null', 'TLAXCO', 90250, 0, 'null', 'H', '01/01/1000', '05/02/2017', '', '', 'TECNICO', 'INSTALACIONES HIDR', '/  /', 0, '2411610089', '2411257798', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1616, 2300, '', 'HEHL660901HTLRRN06', 'LEONARDO HERNANDEZ HERNANDEZ', 'CALLE JOS', 'null', 'SECCION SEXTA', 90850, 0, 'null', 'H', '01/01/1001', '05/02/2017', '', '', 'TECNICO', 'INSTALACIONES HIDR', '/  /', 0, '', '2461271457', '', 'sin observaciones', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1618, 2302, '', 'HEER550403HTLRSM02', 'JOSÉ RAMÓN SERAFÍN HERNÁNDEZ ESCALONA', '7 ORIENTE NUM. 12', 'null', 'IXTENCO', 90580, 0, 'null', 'M', '01/01/1003', '', '', '', '', 'INSTALACIONES HIDR', '/  /', 0, '', '2227217737', '', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1619, 2303, '', 'HEPA920918HTLRTB02', 'ABISAI HERNÁNDEZ PATLANI', '7 ORIENTE NUM. 12', 'null', 'IXTENCO', 90580, 0, 'null', 'M', '01/01/1004', '05/05/2017', '', '', 'TECNICO', 'INSTALACIONES HIDR', '/  /', 0, '', '2229804629', '', 'sin observaciones', '0', 16, '', '', '', '', '', '', '', '', '0'),
(1620, 2304, '', 'PEFA930706MTLRRN05', 'ANA KAREN PÉREZ FERNÁNDEZ', 'C. TLAXCO NUM. 127', 'null', 'LA NORIA 1', 90360, 0, 'null', 'M', '07/06/1993', '', '', '', 'LICENCIATURA', 'ADMINISTRAC', '/  /', 91.16, '2411246030', '2414183399', '', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1621, 2305, '', 'IAML910503MTLSNN00', 'LINA MARYCRUZ ISLA MONTES', 'EMILIO SANCHEZ PIEDRAS # 3; COL. CENTRO', 'null', 'CHIAUTEMPAN', 90800, 0, 'null', 'M', '05/03/1991', '', '', '', 'LICENCIATURA', 'INGL', '04/25/2017', 91.16, '246 198 7444', '246 464 0168', 'marylyn35@hotmail.com', 'sin observaciones', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1622, 2306, '', 'CELG921211MTLRCD06', 'GUADALUPE CRESCENCIO LUCERO', 'RETORNO NACIONAL PTE. # 15', 'null', 'OCOTLAN', 90100, 0, 'null', 'M', '12/11/1992', '', '', '', 'LICENCIATURA', 'PRODUCCCI', '04/25/2017', 88.48, '246 117 5458', '', 'lupita_creslu@gmail.com', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1623, 2307, '', 'HERL811008MTLRVN05', 'LEONILA HERNANDEZ RIVERA', 'REVOLUCION MEXICANA # 27; COL. CENTRO', 'null', 'TLAXCO', 90250, 0, 'null', 'M', '10/08/1981', '', '', '', 'PASANTE', 'ESTILISMO Y BIENESTAR PERSONAL', '04/25/2017', 87.8, '241 414 5614', '', '', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1624, 2308, '', 'CULJ920605MTLCMQ02', 'JAQUEBETH CUECUECHA LIMA', 'CALLE 10 S/N; COL. LA LOMA XICOHTENCATL', 'null', 'TLAXCALA', 90000, 0, 'null', 'M', '06/05/1992', '', '', '', 'LICENCIATURA', 'PRODUCCCI', '04/25/2017', 87.8, '246 109', '', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1625, 2309, '', 'CIAN890609MTLSJN05', 'NANCY CISNEROS AJUECH', 'MOCTEZUMA # 6 SECCION TERCERA', 'null', 'ZACATELCO', 90740, 0, 'null', 'M', '06/09/1989', '', '', '', 'LICENCIATURA', 'HOTELER', '04/25/2017', 87.22, '246 156 2580', '249 497 0795', 'nan_cisneros@hotmail.com', 'sin observaciones', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1626, 2310, '', 'HEIV931024MTLRSN08', 'VIANEY HERNANDEZ ISLAS', 'INDEPENDENCIA NACIONAL # 15; COL. CENTRO', 'null', 'TLAXCO', 90250, 0, 'null', 'M', '10/24/1993', '', '', '', 'LICENCIATURA', 'DISE', '04/25/2017', 85.58, '222 847 2425', '249 496 0280', 'vane4336@gmail.com', 'sin observaciones', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1627, 2311, '', 'CUML881018MTLPNC18', 'LUCERO CUAPIO MINOR', 'TEPECTIPAC # 4; COL. SAN JUAN', NULL, 'SAN JUAN TOTOLAC', 90160, 0, NULL, 'M', '10/18/1988', '10/05/2017', '', 'LICENCIATURA EN NUTRICIÓN', 'LICENCIATURA', 'PRODUCCCIÓN INDUSTRIAL DE ALIMENTOS', '04/25/2017', 85, '241 118 7648', '', 'lucumi_18@hotmail.com', 'sin observaciones', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1628, 2312, '', 'CATA730415MDFBBL09', 'ALICIA CABALLERO TOBON', 'CALLE DEL SOL # 35; FRACC. REAL DEL SOL', 'null', 'TLAXCALA', 90110, 0, 'null', 'M', '04/15/1973', '', '', '', 'TECNICO', 'ASISTENCIA EDUCATIVA', '04/25/2017', 85.12, '246 116 9337', '', '', 'sin observaciones', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1630, 2313, '', 'SONA980424MTLSRL03', 'ALINE VICTORIA SOSA NARVA', 'AV. VICENTE GUERRERO NO. 18', 'null', 'SANTA INES TECUEXCOMAC', 90126, 0, 'null', 'M', '04/24/1998', '', '', '', 'LICENCIATURA', 'INGL', '05/10/2017', 87.96, '2461118058', '12464672474', 'aline.sosanarvaez@hotmail.com', 'sin observaciones', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1631, 2314, '', 'HEPA950219HTLRRL09', 'ALVARO HERRERA PEREZ', 'JESUS CARRANZA #1023;', NULL, 'JESUS Y SAN JUAN', 90358, 0, 'SOLTERO', 'H', '02/19/1995', '', '', 'INGENIERO EN TECNOLOGÍAS DE LA INFORMACIÓN', 'LICENCIATURA', 'INFORMATICA', '05/11/2017', 86, '2411307507', '12414172386', 'alvaroherreraap95@gmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1632, 2315, '', 'PORG740830MTLTMD02', 'GUADALUPE SONIA POTRERO ROMERO', 'VENUSTIANO CARRANZA #40', 'null', 'BARRIO DE JESUS', 90970, 0, 'null', 'M', '08/30/1974', '', '', 'INGENIERO AGRONOMO', 'LICENCIATURA', 'PRODUCCCI', '05/10/2017', 86.28, '2221334269', '12222717156', 'potrero_sonia@hotmail.com', 'sin observaciones', '0', 25, '', '', '', '', '', '', '', '', '0'),
(1633, 2316, '', 'SAOF900821HTLNLR05', 'FRANCISCO JAVIER SANTILLAN OLMOS', 'ANDADOR CORDOBA #303', 'null', 'HOGARES FERROCARRILEROS', 90329, 0, 'null', 'H', '08/21/1990', '', '', 'PASANTE LICENCIATURA EN ADMINISTRACIÓN', 'LICENCIATURA', 'ADMINISTRAC', '05/11/2017', 85.12, '2461010305', '12414170201', 'javier_olmos_s@hotmail.com', 'sin observaciones', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1634, 2351, '', 'NECA740908HTLTTD04', 'ADRÍAN CUATECONTZI NETZAHUAL', 'CALLE 9 No. 208', 'LA LOMA XICOTENCATL', 'TLAXCALA', 90000, 7, 'CASADO', 'M', '08/09/1974', '31/08/2017', '', 'LICENCIADO EN LINGUISTICA APLICADA', 'LICENCIATURA', 'INGLÉS', '03/08/2017', 95360, '2461542982', '2464646931', 'adriannetza@hotmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1635, 2352, '', 'CUFE870416HTLTLD01', 'EDUARDO FLORES CUATECONTZI', 'AV. HIDALGO No. 167', 'SECCION PRIMERA', 'CONTLA', 90670, 8, 'SOLTERO', 'M', '16/04/1987', '31/08/2017', '', 'INGENIERO INDUSTRIAL', 'LICENCIATURA', 'INFORMÁTICA', '11/07/2017', 95040, '2461642715', '2464646931', 'ing.eduarducf@gmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1636, 2353, '', 'SAPC930428MTLNRR08', 'CAROLINA SÁNCHEZ PÉREZ', '30 DE JULIO No. 24', 'LOMAS DE SANTA ANITA', 'APIZACO', 90300, 4, 'SOLTERA', 'F', '28/04/1993', '31/08/2017', '', 'LICENCIADA EN GASTRONOMIA', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '03/08/2017', 91400, '2411151510', '2414179575', 'sanpercarito@gmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1637, 2354, '', 'MAHE660809MTLCRL04', 'MARÍA ELENA DIMPNA MACIAS HERNÁNDEZ', 'AV. MORELOS No. 31', 'CENTRO', 'CALPULALPAN', 90200, 1, 'CASADA', 'F', '09/08/1966', '31/08/2017', '', 'LICENCIADA EN DERECHO', 'LICENCIATURA', 'HABILIDADES BLANDAS; ADMINISTRACIÓN', '26/06/2017', 90700, '7491014171', '', 'tqwdim@hotmail.com', 'IMPARTE CURSOS DE DERECHO; MOTIVACIÓN; SUPERACIÓN Y LIDERAZGO', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1638, 2355, '', 'TOCP580905MDFRRT06', 'PATRICIA TORRES CARREÑO', 'IMPRESORES PONIENTE No.23', 'LOMA BONITA', 'TLAXCALA', 90090, 7, 'SOLTERA', 'F', '05/09/1958', '31/08/2017', '', 'LICENCIADA EN EDUCACIÓN PREESCOLAR', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '01/08/2017', 90220, '', '5569682069', 'ptxilanka@gmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1639, 2357, '', 'FOSG630718MTLLRD01', 'MARÍA GPE. ELENA FLORES SUÁREZ', 'JUSTO SIERRA No. 22', '', 'SAN GABRIEL CUAHUTLA', 90117, 7, 'SOLTERA', 'F', '18/07/1963', '31/08/2017', '', 'LIC. EN TRABAJO SOCIAL; MAESTRÍA EN EDUCACIÓN SUPERIOR', 'MAESTRÍA', 'ASISTENCIA EDUCATIVA', '03/08/2017', 90220, '2461878093', '', 'cibergrafia@hotmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1640, 2358, '', 'MOPA930424MTLGRN05', 'ANDREA MAYAN MOGOLLAN PÉREZ', 'HERMANEGILDO GALEANA No. 5', '', 'SANTO TORIBIO XICOTZINGO', 0, 13, 'SOLTERA', 'F', '24/04/1993', '31/08/2017', '', 'PASANTE LICENCIATURA EN GASTRONOMIA', 'PASANTE LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '03/08/2017', 89320, '2227171057', '2222791378', 'play-girl03@hotmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 42, '', '', '', '', '', '', '', '', '0'),
(1641, 2333, '', 'VALZ841201MTLLMD07', 'ZAID YADIRA VALENCIA LIMÓN', 'PRIVADA TULIPÁN; FRACCIONAMIENTO LAS FLORES', 'MIRAFLORES', 'TLAXCALA', 90114, 7, 'CASADA', 'F', '01/12/1984', '31/08/2017', '', 'LICENCIADA EN MERCADOTECNIA', 'LICENCIATURA', 'ADMINISTRACIÓN', '16/05/2017', 87680, '2464591111', '2461175072', 'zyvl15@gmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1642, 2360, '', 'VERD980720MTLLMND2', 'DIANA LAURA VÉLEZ RAMÍREZ', '20 DE NOVIEMBRE S/N', '', 'SANTIAGO MICHAC', 90721, 0, 'SOLTERA', 'F', '20/05/1998', '31/08/2017', '', 'TÉCNICO EN INGLÉS', 'CARRETA TÉCNICA', 'INGLÉS', '03/08/2017', 87320, '2464162999', '2481429860', 'dianalauvrc98@gmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 23, '', '', '', '', '', '', '', '', '0'),
(1643, 2350, '', 'LEFC950312MTLNRL00', 'CELINA LEÓN FERNÁNDEZ', 'CALLE 2 S/N', '', 'EL ROSARIO', 90256, 0, 'SOLTERA', 'F', '12/03/1995', '31/08/2017', '', 'TSU EN DISEÑO Y MODA INDUSTRIAL', 'TECNICO SUPERIOR UNIVERSITARIO', 'CONFECCIÓN INDUSTRIAL DE ROPA', '24/07/2017', 86640, '', '2414116901', 'celina_disenomoda@outlook.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1644, 2356, '', 'ROMV970113MTLMLR04', 'VERÓNICA ITZEL ROMERO MELÉNDEZ', 'GUADALUPE VICTORIA No. 1', '', 'SANTIAGO MICHAC', 90721, 14, 'SOLTERA', 'F', '13/01/1997', '31/08/2017', '', 'TÉCNICO EN INGLÉS', 'CARRERA TÉCNICA', 'INGLÉS', '03/08/2017', 85280, '2481131355', '', 'imagine_lennon97@outlook.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 23, '', '', '', '', '', '', '', '', '0'),
(1645, 2361, '', 'CUSA860506HTLTLL05', 'ALBERT CUATIANQUIZ SILVA', '16 DE SEPTIEMBRE No. 68', 'DOLORES AQUIAHUAC', 'SAN FRANCISCO TETLANOHCAN', 90840, 12, 'CASADO', 'M', '06/05/1986', '31/08/2017', '', 'PASANTE LICENCIATURA EN ENSEÑANZA DEL IDIOMA INGLÉS', 'PASATE LICENCIATURA', 'INGLÉS', '14/07/2017', 85120, '', '2464602416', 'cuatianquizsilva@hotmail.com', 'EXPERIENCIA FRENTE A GRUPOS', '0', 32, '', '', '', '', '', '', '', '', '0'),
(1646, 2331, '', 'BAZJ670311HTLXMS07', 'JESUS BAÑUELOS ZEMPOALTECA', 'HERRADURA NO.13  ', 'FRACC. SANTA ELENA', 'PANOTLA                                                         ', 90140, 0, 'CASADO', 'H', '11/03/1967', '31/08/2017', '', 'LICENCIADO EN ECONOMIA', 'LICENCIATURA', 'MERCADOTECNIA PARA MICROEMPRESAS', '20/06/2017', 86.54, '2461379907', '', 'jesusbanuelos@gmail.com   ', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1647, 2328, '', 'HENG890226MTLRVB00', 'GABRIELA HERNÁNDEZ NAVA', '20 DE NOVIEMBRE NO.7   ', '', 'SAN MIGUEL CONTLA', 90640, 0, 'SOLTERO', 'F', '26/02/1989', '31/08/2017', '', 'LICENCIADO EN GASTRONOMIA', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '20/06/2017', 87.64, '2461329628    ', '', 'gp_flak@hotmail.com   ', '', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1648, 2326, '', 'IICM530223MVZXBR06', 'MARTHA ELBA IÑIGUEZ COBOS', 'JORNALEROS NO.27    ', 'LOMA BONITA       ', '       ', 90090, 0, 'CASADO', 'F', '23/02/1953', '31/08/2017', '', 'LICENCIADO EN INGLÉS', 'LICENCIATURA', 'INGLÉS', '10/05/2017', 88.02, '', '2461423366          ', '', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1649, 2324, '', 'GARA890711HTLRDD06', 'EDELBERTO GARCIA RODRIGUEZ', 'DEL RIO NO.14     ', '', 'TECOLOTLA       ', 90608, 0, 'SOLTERO', 'H', '11/07/1989', '31/08/2017', '', 'LICENCIADO EN CONTABILIDAD', 'LICENCIATURA', 'CONTABILIDAD', '20/06/2017', 95.36, '2211515504     ', '2464644182', 'agal_gr11@hotmail.com ', '', '0', 2, '', '', '', '', '', '', '', '', '0'),
(1650, 2323, '', 'POTD930422HTLPLN00', 'DANIEL POPOCATL TELLEZ', 'CARRETERA VIA CORTA PUEBLA SANTA ANA; KM 29', 'EL ALTO', 'CHIAUTEMPAN', 90800, 0, 'SOLTERO', 'H', '22/04/1993', '31/08/2017', '', 'TÉCNICO EN ALIMENTOS', 'CARRERA TÉCNICA', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS', '28/03/2017', 85.12, '2461726084 ', '', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1651, 2322, '', 'AOTV781108MVZNRC09', 'VICTORINA DOLORES  ANTONIO TIRZO', 'RETORNO FRAY FRANCISCO DE LOS ANGELES; EDIF.  211 DEPTO. 3 ', 'UNIDAD HABIT. 4TO. SEÑORIO', 'TLAXCALA', 90100, 0, 'CASADA', 'F', '08/11/1978', '31/08/2017', '', 'PASANTE DE LINGUISTICA APLICADA', 'PASANTE', 'INGLÉS', '30/05/2017', 85.16, '2461295497 ', '012464661168        ', 'lolita_vic78@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1652, 2321, '', 'MEGE831108MTLNRD14', 'EDITH MENDIETA GARCIA', 'PRIVADA SAN ANTONIO MZ 1 LT #59 ', 'FRACCIONAMIENTO SAN ANTONIO    ', 'SAN ANTONIO CUAXOMULCO  ', 90660, 0, 'CASADO', 'F', '08/11/1983', '31/08/2017', '', 'LICENCIADA EN NUTRICIÓN', 'LICENCIATURA', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS', '30/05/2017', 85, '2461340629', '', 'mendieta_garci@hotmail.com', '', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1653, 2320, '', 'RAHF760128HTLMRL03', 'FELIX RAMÍREZ HERNÁNDEZ', '3RA. PRIVADA DE ROBERTO COVARRUBIAS # 22 ', 'CENTRO', 'HUAMANTLA', 90500, 0, 'CASADO', 'H', '28/01/1976', '31/08/2017', '', 'INGENIERO CIVIL', 'LICENCIATURA', 'INSTALACIONES HIDRÁULICAS Y DE GAS', '30/05/2017', 85.96, '2471160557          ', '012474721942', 'felichex752009@hotmail.com ', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1654, 2319, '', 'PAHJ670328HASDRM02', 'JAIME PADILLA HERNÁNDEZ', 'TERCERA DE JUAREZ #6', '', 'SAN ESTEBAN TIZATLAN', 90100, 0, 'CASADO', 'M', '28/03/1967', '31/08/2017', '21/11/2017', 'INGENIERO AGRONOMO', 'LICENCIATURA', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS', '30/05/2017', 89, '2464701622 ', '2464667910          ', 'j.padilla.h.28.67@gmail.com', 'REPORTE POR PARTE DE LA PRESIDENTE DE COMUNIDAD DE GUADALUPE VITORIA POR NO CUMPLIR CON ESPECTATIVAS; Y PRESENTARSE EN ESTADO INCOVENIENTE', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1655, 2317, '', 'JULA890202MTLRNL09', 'ALMA OLIMPIA JUÁREZ LUNA', 'JOSE MARIA MORELOS # 9 ', ' 6A SECCION   ', 'TEOLOCHOLCO ', 90850, 0, 'SOLTERA', 'F', '02/02/1989', '31/08/2017', '', 'LICENCIADO EN EDUCACIÓN SECUNDARIA', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '30/05/2017', 91, '2461424443', '', 'olimpia_0289@outlook.es   ', '', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1656, 2011, '', 'PEMT620409MTLRRM07', 'TOMASA  PEREZ MARQUEZ', 'ROMERO No.10', 'zavaleta', 'SAN DAMIAN TEXOLOC', 90731, 0, 'CASADA', 'M', '09/04/1962', '02/10/2016', '', 'CULTURA DE BELLEZA', 'MAESTRIA', 'ESTILISMO Y BIENESTAR PERSONAL', '', 85, '2461068696', '012464615047', '', '', '0', 49, '', '', '', '', '', '', '', '', '0'),
(1658, 36, '', 'LOFE540718MHGPRS06', 'MARIA ESTHER LOPEZ FRAGOSO', 'ERNESTO VIVEROS ', 'DEL CAN No 76', 'ALMOLOYA', 0, 0, 'CASADA', 'F', '18/07/1954', '', '', 'DISEÑO DE MODAS ', 'TÉCNICO', 'CONFECCIÓN DE MODAS ', '', 93, '7751315310', '017489122937', '', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1659, 30, '', 'CUPL690608MTLCRR09', 'LAURA CUECUECHA PEREZ', 'TLAHUICOLE No5', 'CENTRO', 'SANTA ANA CHIAUTEMPAN ', 90800, 7, 'DIVOCIADA ', 'F', '08/06/1969', '', '', 'INDUSTRIAL Y ALTA COSTURA', 'CERTIFICADO ', 'CONFECCION INDUSTRIAL DE ROPA ', '', 0, '2461206490', '2464644087', 'lcuecuechap@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1661, 63, '', 'IAVR710221HTLSZB08', 'JOSE RUBEN  ISLA VAZQUEZ', 'MOCTEZUMA No 21', 'REFORMA', 'CHIAHUTEMPAN ', 90800, 10, 'SOLTERO', 'M', '21/02/1971', '', '', 'CONTADURIA PUBLICA', 'LICENCIATURA', 'CONTABILIDAD', '', 0, '', '012464648662', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1663, 223, '', 'CACE730801MTLNNS02', 'ESPERANZA  CANO CANO', 'NIÑOS HEROES No 2', 'CENTRO ', 'TEPETITLA DE LARDIZABAL ', 0, 5, 'SOLTERA ', 'M', '01/08/1972', '', '', 'CONTADOR PRIVADO ', 'TECNICO ', 'SECRETARIADO ASISTIDO POR COMPUTADORA ', '', 0, '', '2484870515', '', '', '0', 19, '', '', '', '', '', '', '', '', '0'),
(1664, 1474, '', 'LECG601014MTLNLL14', 'MA. GUILLERMINA  LEÓN CALDERÓN', 'CALLE PINOS', 'SANTA ROSA', 'APIZACO', 90340, 0, 'DIVORCIADO', 'F', '14/10/1960', '25/08/2017', '', 'TÉCNICO', 'TÉCNICO EN MECANOGRAFÍA  ', 'ALIMENTOS Y BEBIDAS', '16/08/2017', 90, '241-122-55-54 ', '', '', 'GELATINA ARTISTÍCA; PANADERÍA; REPOSTERÍA Y PASTELERÍA\r\nELABORACIÓN DE PASTELES Y PRODUCTOS DE REPOSTERÍA \r\nPASTELERÍA Y DULCES FINOS \r\nPREPARACIÓN DE ALIMENTOS', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1665, 5, '', 'ZADB480104MTLMSR00', 'BERTHA ZAMORA DÍAS', 'C. BENITO JUÁREZ S/N', '', 'COL. CUAUHTEMOC ', 90500, 0, 'CASADO', 'F', '04/01/1948', '10/08/2017', '', 'TÉCNICO EN ARTESANÍAS FAMILIARES', 'TÉCNICO', 'ARTESANÍAS CON FIBRAS TEXTÍLES', '06/08/2011', 86, ' 247 108 6032', '', '', 'REPUJADO EN METAL\r\nTEJIDO A MANO\r\nBISUTERÍA\r\nBISUTERÍA CON SEMILLAS', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1666, 2362, '', 'GODE820112MTLNRL00', 'ELENA ALDONSA   GONZÁLEZ DURÁN', 'PRIV. MOLINATLA NO. 1', '', 'TIZATLÁN   ', 90100, 0, 'CASADA ', 'F', '12/01/1982', '05/09/2017', '', 'CONTADORA PUBLICA', 'LICENCIATURA', 'CONTABILIDAD', '17/08/2017', 9368, '2461358465  ', '2464157249', 'elenagodu@gmail.com ', 'PASTELERÍA Y DULCES FINOS\r\nCONTABILIDAD DE COSTOS ASISTIDA POR COMPUTADORA\r\nCONTABILIDAD GENERAL CON PAQUETE CONTABLE', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1667, 2363, '', 'MOPG860207MTLRLD03', 'MARÍA GUADALUPE MORENO PILOTZI', 'CALLE UNIÓN NO.74', '', 'TEPETLAXCO ', 90821, 0, 'CASADA ', 'F', '07/02/1986', '11/09/2017', '', 'INGENIERIA', 'INGENIERO AGRO INDUSTRIAL  ', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS', '17/08/2017', 9032, '2821001213', '', 'ing.luís@gmail.com', 'PASTELERÍA Y DULCES FINOS', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1668, 848, '', 'GAET51118MTLRSM04', 'TOMASA TERESA  GARCIA  ESTRADA', '4 PONIENTE No 8 ', '', 'SAN PABLO ZITLALTEPEC', 90590, 10, '', 'F', '18/11/1955', '', '', '', 'SECUNDARIA ', 'ARTESANIAS FAMILIARES ', '', 0, '', '012234780104', '', '', '0', 37, '', '', '', '', '', '', '', '', '0'),
(1669, 846, '', 'ROWG800221HTLDZR01', 'GERARDO  RODRIGUEZ  VAZQUEZ', '16 SEPTIEMBRE No 17', '', 'XALOSTOC', 90250, 2, '', 'M', '21/0271980', '', '', 'ELECTROMECÁNICA ', 'INGENIERIA ', 'OPERACION DE MICRO COMPUTADORAS ', '', 0, '2419193404', '0456671605849', 'gerard_rv@hotmail.com', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1670, 2349, '', 'VIFG701101MDFLLD03', 'MARÍA GUADALUPE VILLANUEVA FLORES', 'MORELOS #80 ', '', 'SECCIÓN SEGUNDA   ', 90850, 0, 'VIUDA', 'F', '01/11/1970', '21/07/2017', '', 'TÉCNICO', 'TÉCNICO', 'ARTESANÍAS CON FIBRAS TEXTILES', '14/07/2017', 85, '246-171-91-53', '246-46-1-80-64  ', 'maryguvi-maya@hotmail.com', '', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1671, 847, '', 'HEGL890222MTLRRS05', 'LESLI HERNANDEZ  GRIJALVA', 'PRIV. LLANO VIEJO MZA2 11 ', 'SAN MIGUEL CONTLA', 'SANTA CRUZ TLAXCALA', 0, 8, '', 'F', '22/02/1989', '', '', 'INFORMATICA', 'TECNICO', 'OPERACIÓN DE MICRO COMPUTADORAS', '', 0, '', '', '', '', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1672, 849, '', 'MOHM810224MPLRYR06', 'MATHA  MORALEZ  HOYOS', 'PRIV.FELIPE ANGELES  S/N', '2  BARRIO', 'PANOTLA', 90140, 10, 'CASADA', 'F', '24/02/1981', '', '', 'INFORMATICA ADMINISTRATIVA', 'TECNICO', 'OPERTACION DE MICRO COMPUTADORAS', '', 0, '2461048739', '4664627', 'mar19_81@live.com', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1673, 850, '', 'MOCM820923MTLNHY03', 'MAYDELLIN MODRAGO  CHAMORRO', 'AV.JUAREZ No  401', 'CENTRO', 'APIZACO', 0, 6, 'SOLTERA', 'F', '23/08/1982', '', '', 'MEDICINA', 'FACULTAD ', 'PRIMEROS AUXILIOS', '', 0, '2222809122', '2411131974', 'maydellin@live.com.mx', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1674, 851, '', 'CAGB830328MTLBRT08', 'BEATRIZ  CABRERA  GARCIA', 'UNIDAD SANTA CRUZ EDF.9 DEPARTAMENTO 3', '', 'SANTA ANA ', 0, 4, 'SOLTERA', 'M', '28/03/1983', '', '', 'NUTRICION ', 'LICENCIATURA ', 'PREPARACION DE ALIMENTOS ', '', 0, '0452461110763', '012464645990', 'polibet@hotmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1675, 2364, '', 'MODR860207HTLRLC06', 'RICARDO  MORENO PILOTZI', 'CALLE UNIÓN NO. 74', '', 'TEPETLAXCO ', 90821, 0, 'SOLTERO', 'M', '07/02/1986', '11/09/2017', '', 'INGENIERO MECATRONICO', 'INGENIERIA', 'INGENIERIA MECANICA', '17/08/2017', 8980, '2464943893', '', '', 'ING. MECATRONICA', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1676, 2365, '', 'TANG661206HTLLVS00', 'JOSÉ GUSTAVO TLAPAPAL NAVA', 'AV. FERROCARRIL MEXICANO NO.3', '', 'IXTULCO', 90105, 0, 'CASADO', 'M', '06/12/1966', '07/10/2017', '', 'CONTABILIDAD', 'LICENCIATURA', 'CONTABILIDAD', '17/08/2017', 86, '2461375203', '2464621863', 'gustavo_6612@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1677, 2366, '', 'MAPJ740110HDFNRN09', 'JUAN MANOATL PÉREZ', 'NICOLAS BRAVO No. 14-C', 'BARRIO DEL ALTO', 'SANTA CRUZ', 90640, 0, 'CASADO', 'M', '01/10/1974', '07/10/2017', '', 'APLICACION DE HOJA DE ORO; SOLDADURA; ELECTRICIDAD; ', 'TECNICO', 'ARTESANÍAS DE ALTA PRECISIÓN ', '16/08/2017', 87, '2461814579', '', '', '', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1678, 2367, '', 'HENC830915HUZRTR03', 'CARLOS ANTONIO HERNÁNDEZ NIETO', 'PROL. MOISES COCA No. 25', '', 'MIRAFLORES', 90110, 0, 'SOLTERO', 'M', '15/09/1983', '07/10/2017', '', 'ARQUITECTURA', 'LICENCIATURA', 'ARTESANÍAS EN MADERA', '17/08/2017', 86, '2461488632', '', 'fuenteatonio83@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1679, 2368, '', 'AOHA800127HYNLLL03', 'ALEJANDRO RAFAEL ALCOCER HOIL', 'AV. PUEBLA No. 56', 'CRUZ DORADA', 'SEGUNDA SECCIÓN', 90740, 0, 'CASADO', 'M', '27/01/1980', '07/10/2017', '', 'TÉCNICO EN INGLÉS', 'TÉCNICO EN INGLÉS', 'INGLÉS', '17/08/2017', 86, '9981485026', '2464974201', 'alexcancun14@yahoo.com', '', '0', 44, '', '', '', '', '', '', '', '', '0'),
(1680, 2283, '', 'PEHR940311MTLRRT08', 'RUTH PÉREZ HERNÁNDEZ', 'C. VICENTE GUERRERO No.5', '', 'AMAXAC DE GUERRERO', 90620, 0, 'SOLTERA', 'F', '11/03/1994', '05/04/1994', '', 'RESPOSTERÍA', 'TECNICO', 'ALIMENTOS Y BEVIDAS', '', 86, '2461976499', '2464611110', 'rut.xis@hotmail.com', '', '0', 1, '', '', '', '', '', '', '', '', '0'),
(1681, 2382, '', 'ROHC950506HTLMRR01', 'CARLOS ROMERO HERNÁNDEZ', 'NARCISO MENDOZA SUR No. 119', 'CENTRO', 'HUAMANTLA', 90500, 0, 'SOLTERO', 'M', '06/05/1995', '07/10/2017', '', 'PASANTE EN ADMINISTRACIÓN', 'INGENIERIA MECATRONICA', 'INGLÉS', '14/09/2017', 86, '2471033311', '2474723131', 'carlos19romero95h@gmail.com', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1682, 2369, '', 'SUSH801030MDFRNY04', 'MARÍA HAYDE SUÁREZ SANDOVAL', '3 DE MAYO No. 11', '', 'SANTA ANITA HUILOAC', 90407, 0, 'SOLTERA', 'F', '30/10/1980', '07/10/2017', '', 'EDUCACIÓN ESPECIAL', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '14/09/2017', 94, '2461164431', '2414178173', 'haydesusa33@gmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1683, 2370, '', 'RANS860325MTLMLY04', 'SUYAPA RAMÍREZ NOLLA', 'AV. CHAMIZAL No 2', 'SEGUNDA SECCIÓN', 'CUAXOMULCO', 90660, 0, 'CASADA', 'F', '25/03/1986', '07/10/2017', '', 'INGENIERIA', 'MAESTRÍA EN CIENCIA DE ALIMENTOS', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS; ALIMENTOS Y BEBIDAS', '14/09/2017', 91, '2411168636', '2414152099', 'suyapa_2503@hotmail.com', '', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1684, 2371, '', 'HEAT591006MTLRRR05', 'MARÍA TERESA ORALIA HERNÁNDEZ ARAGÓN', 'ALDAMA PONIENTE No. 206-A', 'CENTRO', 'HUAMANTLA', 90500, 0, 'CASADA', 'F', '06/10/1959', '07/10/2017', '', ' LICENCIATURA EN EDUCACIÓN', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '17/09/2017', 92, '2471189187', '', 'loralia_hdez@hotmail.com', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1685, 2372, '', 'AAPR691121MTLPLC17', 'M DEL RECUERDO B. APARICIO  POLANCO', '8 DE MAYO No. 31', 'AMAROTA', '', 90100, 0, 'CASADA', 'F', '21/11/1969', '07/10/2017', '', 'PSICOLOGÍA', 'LICENCIATURA EN PSICOLOGÍA', 'ADMINISTRACIÓN', '14/09/2017', 92, '2464701864', '', 'barbara.2010@live.com.mx', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1686, 1521, '', 'CUBB850430MTLHTT08', 'BEATRIZ CUAHUTLE BAUTISTA', 'CALLE BENITO JUÁREZ #9', '', 'CONTLA DE JUAN CUAMATZI', 90670, 0, 'CASADA', 'F', '30/04/1985', '01/0!2009', '', 'ADMINISTRACIÓN', 'LICENCIATURA', 'HOTELERÍA; INGLÉS; NAHUATL; ', '', 88, '246-142-73-14', '24646-4-56-61', 'KOSAMAOTLZI@HOTMAIL.COM', '', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1687, 2373, '', 'FEJA740403MDFRMN03', 'ANA MARÍA FERNÁNDEZ JIMÉNEZ', 'PEDRO BENITEZ GARCÍA EDIF. 5 DEPTO 2', 'INFONAVIT', 'CALPULALPAN', 90800, 0, 'SOLTERA', 'F', '03/04/1974', '07/10(2017', '', 'LICENCIATURA EN CONTABILIDAD', 'LICENCIATURA EN CONTADURÍA PÚBLICA', 'CONTABILIDAD E INFORMÁTICA', '14/09/2017', 90, '7491060386', '7499181038', 'dulce-ana2001@hotmail.com', '', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1688, 2260, '', 'FOFF860420HTLLLB02', 'FABIÁN FLORES FLORES', 'PROL. ANTONIO DÍAZ VARELA No.120', 'INDUSTRIAL', 'CHIAUTEMPAN', 90800, 0, 'CASADO', 'M', '20/04/1986', '07/10/2017', '', 'LICENCIATURA EN EDUCACIÓN', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '14}709/2017', 90, '2461152169', '2464645328', 'flores.fabian@hotmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1689, 2375, '', 'AAFO800521HTLLRS03', 'OSWALDO IVÁN ÁLVAREZ  FARFÁN', 'NOGAL S/N', '', 'UNIÓN EJIDAL', 90264, 0, 'CASADO', 'M', '21/05/1980', '07/10/2017', '', 'LICENCIATURA EN CONTADURÍA PÚBLICA', 'PASANTE DE MAESTRÍA EN ADMINISTRACIÓN', 'CONTABILIDAD', '14/09/2017', 89, '7489150731', '7751050361', 'oswaldo.ivan@hotmail.com', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1690, 2376, '', 'AIZV640323HTLVRC02', 'VICTOR  ÁVILA ZARATE', 'SEGUNDA PRIVADA CAMPECHE No 22', 'EL ALTO', 'CHIAUTEMPAN', 90807, 0, 'CASADO', 'M', '23/03/1964', '07/10/2017', '', 'TÉCNICO', 'TÉCNICO PROFESIONAL EN MÚSICA', 'TEJÍDO', '14/09/2017', 89, '2461040951', '2464644852', 'labrasaurio@hotmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1691, 2377, '', 'CODG780312MTLNLD08', 'MA. GUADALUPE CONTRERAS DELGADILLO', 'MOCTEZUMA No. 72', 'VALLE DE LAS FLORES', 'NANACAMILPA', 90210, 0, 'CONCUBINATO', 'M', '12/03/1978', '07/10/2017', '', 'PSICOLOGÍA', 'PASANTE DE LICENCIATURA DE PSICOLOGÍA', 'ADMINISTRACIÓN', '14/09/2017', 89, '5562529115', '', 'gpedelco12@gmail.com', '', '0', 21, '', '', '', '', '', '', '', '', '0'),
(1692, 2378, '', 'GAML680104MDFLYT08', 'LETICIA GALINDO MAYÉN', 'RUÍZ CORTIÍNEZ No. 3', '', 'ATEMPAN', 90100, 0, 'VIUDA', 'F', '05/01/1968', '07/10/2017', '', 'LICENCIATURA HISPANOAMERICANA', 'LICENCIATURA', 'ADMINISTRACIÓN', '14/09/2017', 89, '2461092268', '2464627229', 'legama_68@outlook.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1693, 2379, '', 'AALA641011HTLYNL05', 'ALFREDO AYALA LUNA', 'ALAMEDA S/N', 'SAN FRANCISCO YANCUITLALPAN', 'HUAMANTLA', 90500, 0, 'SOLTERO', 'M', '11/10/1964', '07/10/2017', '', 'LICENCIATURA EN EDUCACION ESPECIAL', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '14/09/2017', 88, '2471106633', '2474724035', 'alfredo_obdc@hotmail.com', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1694, 2380, '', 'FORO890106HTLLDM03', 'OMAR FLORES RODRÍGUEZ', 'AV. HIDALGO No. 16', 'DOLORES', 'ACUIAHUAC', 90845, 0, 'UNIÓN LIBRE', 'M', '10/01/1989', '07/10/2017', '', 'LICENCIATURA', 'LICENCIATURA EN INFORMÁTICA', 'INFORMÁTICA', '14/09/2017', 86, '2461604887', '2464166532', 'omarfe_06@hotmail.com', '', '0', 50, '', '', '', '', '', '', '', '', '0'),
(1695, 2381, '', 'OECT830221HTLRRM08', 'TOMÁS ORTEGA CORONA', 'BENITO JUÁREZ No 35', '', 'TLACOCALPAN', 90620, 0, 'UNIÓN LIBRE', 'M', '21/02/1983', '07/10/2017', '', 'LICENCIATURA EN CONTADURÍA', 'LICENCIATURA', 'CONTABILIDAD; ADMINISTRACIÓN', '14/09/2017', 86, '2467575293', '', 'ortegatom@hotmail.com', '', '0', 1, '', '', '', '', '', '', '', '', '0'),
(1696, 2383, '', 'MOSR960413HTLNNB08', 'JOSÉ ROBERTO MONTIEL SÁNCHEZ', 'AV. REPUBLICA DE HAITÍ S/N', '', 'ACTIPAN', 90430, 0, 'SOLTERO', 'M', '13/04/1996', '07/10/2017', '', 'TÉCNICO EN INGLÉS Y ALEMÁN', 'TÉCNICO', 'INGLÉS Y ALEMÁN', '14/09/2017', 85, '2411010837', '2411612127', 'roberto.montiel.96@hotmail.com', '', '0', 31, '', '', '', '', '', '', '', '', '0'),
(1697, 2384, '', 'ZAGL810810MTLCRR00', 'LORENA ZACAHUA GARCÍA', 'PRIV. SAN ANTONIO No. 43', 'TEPECHAPA', 'CUAXOMULCO', 90660, 0, 'CASADA', 'F', '10/08/1981', '07/10/2017', '', 'TECNICO EN CONFECCIÓN', 'LICENCIATURA ', 'CONFECCIÓN INDUSTRIAL DE ROPA; ASISTENCIA EDUCATIVA', '14/09/2017', 85, '2411335203', '2464613536', 'estanciapollitos_50@hotmail.com', '', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1698, 2385, '', 'HEGG720708MPLRRD02', 'GUADALUPE HERNÁNDEZ  GARCÍA', 'PRIV. FLAMINGOS No. 5', 'MIRAFLORES', 'TLAXCALA', 90134, 0, 'SOLTERA', 'F', '08/07/1972', '07/10/2017', '', 'TECNICO EN CONFECCIÓN', 'TECNICO EN CORTE Y CONFECCIÓN', 'CONFECCIÓN INDUSTRIAL DE ROPA; BISUTERÍA', '14/09/2017', 85, '2461644576', '', 'guadalupehg1a@outlook.es', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1699, 2386, '', 'CUHB791007MTLRRR02', 'BERENICE CRUZ HERNÁNDEZ', 'ALCONFORES S/N', 'NUEVA FLORIDA', 'APIZACO', 90355, 0, 'SOLTERA', 'F', '07/10/1979', '07/10/2017', '', 'ESTILISTA PROFESIONAL', 'TÉCNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '14/09/2017', 85, '', '2414110217', 'cruzb1619@gmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1700, 2387, '', 'MODA820817MTLRZD07', 'ADRIANA  MORALES  DÍAZ', 'JACARANDAS No. 535', 'PASO DE CORTÉS', 'APIZACO', 90356, 0, 'CASADA', 'F', '17/08/1982', '07/10/2017', '', 'ARTESANÍAS Y MANUALIDADES', 'TÉCNICO EN CONSTRUCCIÓN', 'APLICACIÓN DE LAMINADO DE ORO', '14/09/2017', 85, '2411125681', '2414170324', 'bombonadri1708@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1701, 2388, '', 'CABF780221HTLRRL03', 'FELIPE CARMONA BRIONES', 'FRANCISCO MONTEJO No 116; LT7; M23', '', 'APIZACO', 90356, 0, 'CASADO', 'M', '21/02/1978', '07/10/2017', '', 'MANTENIMIENTO MECÁNICO', 'TECNICO EN CONBUSTIÓN INTERNA', 'MANTENIMIENTO INDUSTRIAL', '14/09/2017', 86, '2411227641', '', 'carmonafelipe231@gmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1702, 2389, '', 'PEPE940507HTLRRD09', 'EDWIN PÉREZ PÉREZ', 'PRIV. DE LOS PFRSNOS No. 2', '', 'SAN JERONIMO', 90738, 0, 'SOLTERO', 'M', '07/05/1994', '07/10/2017', '', 'INGENIERO EN TICs', 'INGENIERÍA', 'INFORMÁTICA', '14/09/2017', 85, '2461567256', '', 'edwin_perez207@hotmail.com', '', '0', 51, '', '', '', '', '', '', '', '', '0'),
(1703, 2390, '', 'BAFD891004MTLDNL08', 'DELIA BERENICE BADILLO FUENTES', 'EMILIANO ZAPATA S/N', '', 'SAN JOSÉ CUAMATZINGO', 90421, 0, 'SOLTERA', 'F', '09/10/1989', '07/10/2017', '', 'ESTILÍSTA PROFESIONAL', 'TECNICO', 'ESTILÍSMO Y BIENESTAR PERSONAL', '14/09/2017', 85, '2411088891', '2414123328', 'ailed0427@hotmail.com', '', '0', 11, '', '', '', '', '', '', '', '', '0'),
(1704, 2391, '', 'MAPH640212HTLNRG00', 'HUGO  MANOATL PÉREZ', 'AV. DURANGO S/N', 'EL ALTO', 'CHIAUTEMPAN', 90800, 0, 'CASADO', 'M', '12/02/1964', '07/10/2017', '', 'ARTESANO', 'TECNICO APLICACIÓN DE LAMINADO DE HOJA DE ORO', 'APLICACIÓN DE LAMINADO DE HOJA DE ORO', '14/09/2017', 85, '2461608481', '', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1705, 2392, '', 'LOCL711211MTLZLR06', 'MARÍA DE LOURDES LOZANO CALVA', 'PORFIRIO DÍAZ No. 11', '', 'TEZOQUIPAN', 90147, 0, 'CASADA', 'F', '11/12/1971', '07/10/2017', '', 'AGRONOMÍA; TECNICO EN REFORZAMIENTO DE CONOCIMIENTO PARA ASISTENCIA EDUCATIVA', 'INGENIERIA AGRONOMA', 'ASISTENCIA EDUCATIVA; INGENIERIA AGRONOMA Y AGROINDUSTRIAL', '29/09/2017', 92, '2461164431', '', 'lululozano@yahoo.com', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1706, 2393, '', 'XIPR850318MTLCRO17', 'RAQUEL XICOHTENCATL PÉREZ', 'SANTA URSULA ZIMATEPEC No. 13', '', 'YAUHQUEMECAN', 90660, 0, 'UNIÓN LIBRE', 'F', '18/03/1985', '07/10/2017', '', 'LICENCIATURA EN GASTRONOMÍA', 'PASANTE EN LICENCIATURA EN GASTRONOMÍA', 'ALIMENTOS Y BEBIDAS', '29/09/2017', 91, '2411351394', '', 'rxraquelx3@gmail.com', '', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1707, 2394, '', 'RILC800919MTLVPN03', 'CONCEPCIÓN RIVERA LÓPEZ', 'AQUILES SERDÁN No. 1106', 'SAN MIGUEL', '', 90300, 0, 'SOLTERA', 'F', '19/09/1980', '07/10/2017', '', 'LICENCIATURA EN EDUCACIÓN ESPECIAL', 'LICENCIATURA EN EDUCACIÓN ESPECIAL', 'ASISTENCIA EDUCATIVA', '29/09/2017', 87, '2411025427', '', 'riveralopez.c@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1708, 2395, '', 'BOMI930119HTLLIG07', 'IGNACIO BOLAÑOS MEJÍA', '16 DE SEPTIEMBRE No. 25', '', 'BENITO XALTOCAN', 90453, 0, 'SOLTERO', 'M', '19/01/1993', '07/10/2017', '', 'LICENCIADO EN LENGUAS MODERNAS', 'LICENCIADO EN LENGUAS MODERNAS', 'INGLÉS', '29/09/2017', 87, '2414146657', '', '', '', '0', 43, '', '', '', '', '', '', '', '', '0'),
(1709, 2396, '', 'AUAP911205HTLGGS02', 'PASCUAL ÁGUILA ÁGUILA', 'PORVENIR No.1', 'SECCIÓN PRIMERA', 'TEOLOCHOLCO', 90850, 0, 'SOLTERO', 'M', '05/12/1991', '07/10/2017', '', 'CIENCIAS DE LA EDUCACIÓN', 'PASANTE DE LICENCIATURA', 'ASISTENCIA EDUCATIVA', '29/09/2017', 86, '2461070659', '2464617739', 'pacoaa12@gmail.com', '', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1710, 2397, '', 'MUFM830902MTLXLR09', 'MIREYA AIDE MUÑOZ FLORES', 'ANTIGUO CAMINO REAL No. 23', '', 'SANTA MARIA IXTULCO', 90105, 0, 'SOLTERA', 'F', '02/09/1983', '07/10/2017', '', 'LICENCIATURA EN IDIOMAS EUROPEOS', 'LICENCIATURA', 'INGLÉS; IDIOMAS; Y BISUTERÍA', '29/09/2017', 85, '2461794711', '2464663370', 'italianlove@live.com.mx', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1711, 2398, '', 'LOMG720528HTLZRR06', 'GERMÁN  LOZADA MARQUEZ', 'DR. MIGUEL HERNÁNDEZ No. 42', 'CENTRO', 'CHIAUTEMPAN', 90800, 0, 'CASADO', 'M', '28/05/1972', '07/10/2017', '', 'ADMINISTRACIÓN', 'LICENCIATURA', 'ADMINISTRACIÓN; TÉCNICO EN INGLES NIVEL C', '29/09/2017', 85, '2461684447', '', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1712, 2399, '', 'OIRJ950119HTLRDN05', 'JUAN CARLOS ORTÍZ RODRÍGUEZ', 'EMILIANO ZAPATA No. 7', '', 'TECOMALUACAN', 90270, 0, 'SOLTERO', 'M', '19/01/1995', '07/10/2017', '', 'INFORMÁTICA', 'TECNICO', 'INFORMÁTICA', '29/09/2017', 85, '2411593008', '', 'jcarlosortiz@gmail.com', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1714, 2346, '', 'RORC680523HTLDMM02', 'CAMILO RODRÍGUEZ RAMÍREZ', 'INDEPENDENCIA No. 13', 'ASILAC', 'SANTA CRUZ', 90640, 0, 'CASADO', 'M', '23/05/1964', '', '', 'INGENIERIA MECATRONICA', 'INGENIERIA', 'MECATRONICA; SEGURIDAD INDUSTRIAL; CORTE Y SOLDADURA; TRABJO EN ALTURAS; ESPACIOS CONFINADOS;', '', 87, '2461560165', '2464613617', 'camilorodriguez2373@gmail.com', '', '0', 26, '', '', '', '', '', '', '', '', '0'),
(1715, 2341, '', 'SOSD750409HTLSLM09', 'DEMETRIO SOSA  SALINAS', 'INDEPENDENCIA #99 ', '', 'BUENAVISTA', 90250, 0, 'CASADO', 'M', '09/04/1975', '19/07/2017', '', 'LICENCIADO EN INFORMÁTICA ', 'LICENCIATURA', 'INFORMÁTICA', '11/07/2017', 86, '241-145-97-35', '', 'salinitas01@hotmail.com', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1716, 1986, '', 'OIGA570121HTLRRL00', 'ALFREDO ORTIZ GARCÍA', 'AV. INSTITUTO TECNOLOGICO DE APIZACO N° 3021  ', '', 'SAN ANDRES AHUASHUATEPEC ', 90201, 0, 'CASADO', 'M', '21/01/1957', '19/02/2015', '', 'LICENCIADO EN ADMINISTRACIÓN DE EMPRESAS', 'LICENCIATURA', 'ADMINISTRACIÓN; COMUNICACION EFECTIVA Y DESARROLLO HUMANO                   ', '16/02/2015', 86, '241 221 9038', '        ', '', '', '0', 38, '', '', '', '', '', '', '', '', '0'),
(1717, 2184, '', 'SOHE840816MTLSRS09', 'ESTHER  SOSA HERNÁNDEZ', 'MARIANO MATAMOROS #1306 ', 'SAN MARTIN DE PORRES', 'APIZACO', 90300, 0, 'UNIÓN LIBRE', 'F', '16/08/1984', '06/06/2016', '', 'LICENCIADO EN CIENCIAS DE LA FAMILIA', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA; LENGUAJE A SEÑAS MEXICANAS', '31/03/2016', 85, '2411063659', '2414173804', 'sosa_691984@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1718, 2400, '', 'SAJY7791013MTLNRZ08', 'YAZMÍN SÁNCHEZ JUÁREZ', 'EDIF. 8 DEPTP. 8; PETROQUÍMICA', '', 'APETATITLAN', 90600, 0, 'UNIÓN LIBRE', 'F', '13/10/1979', '17/10/2017', '', 'LICENCIATURA EN CONTADURÍA PÚBLICA', 'LICENCIATURA', 'CONTABILIDAD; ENSEÑANZA EFECTIVA DE MATEMÁTICAS', '06/10/2017', 90, '2461163534', '2464647747', 'yazmin_san1310@hotmail.com', '', '0', 2, '', '', '', '', '', '', '', '', '0'),
(1719, 2401, '', 'MUMA921003MPLXNN09', 'ANAHÍ FRANCISCA MUÑOZ MÉNDEZ', 'AV. NARCISO MENDOZA No.6', '', 'SAN MATEO', 90700, 0, 'SOLTERA', 'F', '03/10/1992', '17/10/2017', '', 'INGENIERÍA DE PROCESOS ALIMENTARIOS', 'INGENIERÍA', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS', '08/10/2017', 87, '2481106162', '2484870352', 'any_mendez1992@hotmail.com', '', '0', 19, '', '', '', '', '', '', '', '', '0');
INSERT INTO `instructores` (`id`, `Expediente`, `imagen`, `Curp`, `Nombre`, `Calle`, `Colonia_barrio`, `Localidad`, `CP`, `Seccion_elec`, `Estado_civil`, `Sexo`, `F_Nacimiento`, `F_Alta`, `F_Baja`, `Pefil`, `Nivel_academico`, `Especialidad`, `Fecha_evaluacion`, `Puntaje`, `No_celular`, `No_telefono`, `Email`, `Observaciones`, `calidad`, `id_municipios`, `condicion`, `acta_doc`, `curp_doc`, `dom_doc`, `ine_doc`, `expfg_doc`, `expl_doc`, `cv_doc`, `actualizacion_doc`) VALUES
(1720, 2402, '', 'DIQD870413MHGZNN07', 'DONAJÍ DÍAZ QUINTERO', 'HORTENCIA S/N', '', 'LA UNIÓN EJIDAL', 90264, 0, 'UNIÓN LIBRE', 'F', '13/04/1987', '17/10/2017', '', 'INGENIERÍA EN INFORMÁTICA', 'INGENIERÍA', 'INFORMÁTICA', '06/10/2017', 86, '7751010040', '2414965102', 'donaji_ji@hotmail.com', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1721, 2403, '', 'ROCJ720409HTLSTC02', 'JACINTO ROSARIO CUATECÓN', 'CONSTITUCIÓN No.814', 'CERRITO DE GUADALUPE', 'APIZACO', 90323, 0, 'SOLTERO', 'M', '09/04/1972', '17/10/2017', '', 'LICENCIATURA EN DERECHO', 'LICENCIATURA', 'DERECHO', '06/10/2017', 85, '2411145232', '2414183800', 'jacinto19:72@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1722, 215, '', 'AUVA530421MTLGRN06', 'ANCELMA ANGELA  AGUILAR VERA', 'ADOLFO LÓPEZ MATEOS NO. 25   ', '', 'FRANCISCO VILLA ', 90231, 0, 'CASADO', 'F', '21/04/1953', '01/01/2009', '', 'TÈCNICO CULTURA DE BELLEZA', 'TÉCNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '08/09/2011', 87, '749 105 3793', '748 766 2071', '', '', '0', 47, '', '', '', '', '', '', '', '', '0'),
(1723, 2252, '', 'PEVI901219MTLRZM03', 'IMELDA PÉREZ VÁZQUEZ', '16 DE SEPTIEMBRE 28', '', 'SAN MIGUEL CONTLA', 90640, 0, 'CASADA', 'F', '19/12/1980', '08/02/2017', '', 'TÉCNICO', 'TÉCNICO', 'ARTESANÍAS CON FIBRAS TEXTILES', '27/01/2017', 0, '246 109 9746', '', '', '', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1724, 2267, '', 'FELJ730814HTLRMS03', 'JESUS DAVID FERNÁNDEZ  LIMA', '', '', '', 90800, 0, '', 'M', '14/08/1973', '07/03/2017', '', 'TÉCNICO', 'TÉCNICO', 'DISEÑO Y FABRICACIÓN DE MUEBLES DE MADERA', '10/02/2017', 85, '2464946982', '2461157041', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1725, 2237, '', 'TOBY780819MTLCRS02', 'YESENIA TOACHE BRINDIS', 'ALLENDE #51 ', 'COL. CHAPULTEPEC', 'NANACAMILPA', 90280, 0, 'CASADO', 'F', '19/08/1980', '', '13/10/2016', 'ESTILISMO Y BIENESTAR PERSONAL ', 'TÉCNICO', 'ESTILÍSMO Y BIENESTAR PERSONAL', '11/10/2016', 86, '7481068355', '', 'yesenia_toa@hotmail.com  ', '', '0', 21, '', '', '', '', '', '', '', '', '0'),
(1726, 962, '', 'TOSB860920MTLRCR03', 'BERENICE TORRES  SAUCEDO', 'CALLE PROGRESO NO. 51 ', 'BARR. POTRERO', '', 90790, 0, 'SOLTERO', 'F', '20/09/1986', '01/01/2009', '', 'LICENCIADO EN GASTRONOMÍA ', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '08/06/2011', 87, '222 347 5522', '222 263 2600', 'bere20_nice@hotmail.com', '', '0', 41, '', '', '', '', '', '', '', '', '0'),
(1727, 897, '', 'CAMF640526HTLNNL00', 'FELIPE  CANDIA MENDOZA', 'BARRIO DE JESÚS XOLALPAN', '', 'TETLANOHCAN', 90840, 0, 'CASADO', 'M', '26/05/1964', '', '', 'INGENIERO MECATRÓNICO; TÉCNICO EN INFORMÁTICA', 'INGENIERIO', 'INFORMÁTICA', '14/07/2017', 90, '246-105-07-55', '', 'actuador2@hotmail.com  ', '', '0', 50, '', '', '', '', '', '', '', '', '0'),
(1728, 1770, '', 'BASR820403HTSNNC06', 'RICARDO   BANDA SANTIAGO', 'SAN JUAN 10', 'COLONIA NAZARET', '', 90208, 0, 'SOLTERO', 'M', '03/04/1982', '', '', 'LICENCIADO EN DERECHO   ', 'LICENCIATURA', 'INFORMÁTICA', '28/02/2013', 80, '7491032035 ', '', '007@HOTMAIL.COM', '', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1729, 2222, '', 'JIDD940530MHGMZN02', 'DIANA GABRIELA JIMÉNEZ DÍAZ', 'ADOLFO LOPEZ MATEOS 36  ', '', 'SOLTEPEC', 90265, 0, 'SOLTERA', 'F', '30/05/1994', '02/09/2016', '', 'LICENCIADO EN GASTRONOMÍA', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '01/09/2016', 90, '241 410 2119', '', 'dianis_123@outlook.com ', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1730, 2404, '', 'TIMD821226MPLNRY05', 'DEEYCI TINO MARTÍNEZ', 'REFORMA S/N', '', 'XALTELULCO', 90660, 0, 'CASADA', 'F', '26/12/1982', '25/10/2017', '', 'LICENCIATURA EN ADMINISTRACIÓN DE EMPRESAS', 'LICENCIATURA', 'ADMINISTRACIÓN; ATENCIÓN AL CLIENTE; VENTAS; MERCADEO Y PROMOTORÍA', '16/10/2017', 94, '2331271291', '', 'xanachuchut@hotmail.com', '', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1731, 2405, '', 'QULA770904HDFNRR09', 'ARTURO QUAN KIU LARIOS', 'PRIV. DE LOS OCOTES No. 10', '', 'OCOTLAN', 90100, 0, 'SOLTERO', 'M', '04/09/1977', '24/10/2017', '', 'MAESTRÍA EN ADMINISTACIÓN; GESTIÓN DE INSTITUCIONES EDUCATIVAS', 'MAESTRÍA', 'ADMINISTRACIÓN; LIDERÁZGO; COMUNICACIÓN EFECTIVA; VALORES; ATENCIÓN A CLIENTES', '16/10/2017', 91, '2223084033', '', 'quankiu@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1732, 2406, '', 'OOJR830201HTLRMF04', 'RAFAEL OROPEZA JIMÉNEZ', 'ITURBIDE No. 30; ', 'SAN ONOFRE', 'CHIAUTEMPAN', 90800, 0, 'SOLTERO', 'M', '01/02/1983', '24/10/2017', '', 'MAESTRÍA EN CRIMINOLOGÍA; PRIMER RESPONDIENTE', 'MAESTRÍA', 'DERECHO', '16/10/2017', 91, '2461112149', '', 'rafaoropeza.tlax@gmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1733, 2407, '', 'PEVN87870825MTLRZN06', 'NANCY PÉREZ VÁZQUEZ', 'AV. LIBERTAD No.1509', 'JESÚS Y SAN JUAN', '', 90358, 0, 'SOLTERA', 'F', '25/08/1987', '24/10/2017', '', 'LICENCIADA EN CIENCIAS POLÍTICAS Y ADMÓN. PÚBLICA; CULTURA INSTITUCIONAL CON PERSPECTIVA DE GENERO', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA; DERECHO', '16/10/2017', 90, '2411012991', '', 'nice_825@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1734, 2408, '', 'XERK940816HTLLMV01', 'KEVIN JR XELHUANTZI ROMANO', '20 DE MAYO No. 19', '', 'XOPANTLA', 90670, 0, 'SOLTERO', 'M', '16/08/1994', '24/10/2017', '', 'INGENIERIA MECATRONICA', 'INGENIERIA', 'MECATRÓNICA; ROBÓTICA; INFORMÁTICA; ELECTRICIDAD; DISEÑO ASISTIDO POR COMPUTADORA', '16/10/2017', 90, '2461475507', '2464643556', 'kevin_xelhuantzi@innovathinkms.com', '', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1735, 2409, '', 'TORJ811230HTLRTV07', 'JAVIER TORÍZ GUTIÉRREZ', 'ANDADOR ESPERANZA No. 116', 'HOGARES FERROCARRILEROS', '', 90329, 0, 'CASADO', 'M', '30/12/1981', '24/10/2017', '', 'TÉCNICO EN INFORMÁTICA ADMINISTRATIVA; ', 'TÉCNICO', 'ADMINISTRACIÓN; PYMES; ASISTENCIA EDUCATIVA; INFORMÁTICA', '16/10/2017', 89, '2411113947', '2414172275', 'xavitoriztoro@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1736, 2335, '', 'MEPV850101MTLNRC08', 'VICTORIA MENDIETA PÉREZ', 'XICOHTÉNCATL #51 ', 'BARRIO DE JESÚS', 'SAN PABLO DEL MONTO', 90970, 0, 'CASADA', 'F', '01/01/1985', '19/07/2017', '', 'TÉCNICO', 'TÉCNICO', 'ARTESANÍAS METÁLICAS', '06/07/2017', 88, '246-184-69-34', '', 'vicki_mendieta@hotmail.com', '', '0', 25, '', '', '', '', '', '', '', '', '0'),
(1737, 1441, '', 'LOOM680504MTLPRN05', 'MA. MONICA A. LÓPEZ ORTEGA', 'CERRADA MONTE ALTO #102 DEPTO 7 ', 'FRACCIONAMIENTO LA ESTACIÓN', '', 90500, 0, 'SOLTERA', 'F', '04/05/1968', '', '', 'TÉCNICO EN ARTESANÍAS FAMILIARES  ', 'TÉCNICO', 'ARTESANÍAS CON FIBRAS TEXTILES  ', '16/02/2012', 80, '247-471-35-90       ', '247-47-2-44-01   ', '', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1738, 2056, '', 'GOLH870228HTLMPL00', 'HILARIO GÓMEZ LÓPEZ', 'CALLE NOCHE BUENA S/N', 'LOMA FLORIDA 1RA SECCIÓN', '', 90356, 0, 'CASADO', 'M', '', '19/05/2015', '', 'TÉCNICO MECÁNICO AUTOMOTRÍZ', 'TÉCNICO', 'TÉCNICO MECÁNICO ELÉCTRICO ', '05/05/2015', 85, '2414076389', '', 'concor_87@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1739, 2410, '', 'BEGF680515HTLLRC00', 'FACUNDO BELLO GARDUÑO', '4to. ANDADOR DE LA SIERRA No. 13-A', 'LA LOMA DE GUADALUPE', '', 90115, 0, 'CASADO', 'M', '15/05/1968', '30/10/2017', '', 'INGENIERIA EN COMPUTACIÓN', 'INGENIERÍA', 'SISTEMAS COMPUTACIONALES; INFORMATICA; MANTENIMIENTO PREVENTIVO Y CORRECTIVO DE SISTEMAS', '16/10/2017', 88, '2461953360', '2464624557', 'fbgarduno@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1740, 2411, '', 'VERP781108MPLZML06', 'PAOLA VEZ RAMOS', 'BLVD. REVOLUCIÓN No. 37-F', 'ATEMPAN', 'ATEMPAN', 90010, 0, 'CASADA', 'F', '08/11/1978', '30/10/2017', '', 'LICENCIATURA EN PSICOLOGÍA', 'LICENCIATURA', 'PSICOLOGÍA; HABILIDADES BLANDAS; ASISTENCIA EDUCATIVA', '16/10/2017', 88, '2461270314', '', 'pao.vez78@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1741, 2412, '', 'MEMA890726MTLSNS02', 'ANA KAREN MENDOZA MOSQUEDA', 'NICOLAS BRAVO No. 4', '', 'TOTOLAC', 90160, 0, 'SOLTERA', 'F', '26/07/1989', '30/10/2017', '', 'LICENCIATURA EN EDUCACIÓN PREESCOLAR', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA; ESTRATEGIAS DE INTERVENCIÓN PARA LA DISCAPACIDAD MULTIPLE', '16/10/2017', 87, '2461356274', '', 'yoana_26_6@hotmail.com', '', '0', 36, '', '', '', '', '', '', '', '', '0'),
(1742, 2413, '', 'NAMG960918MTLVXS04', 'GUSTAVO NAVA MUÑOZ', 'LA LAGUNA No. 53', '', 'SAN SEBASTIAN ATLAHPA', 90111, 0, 'SOLTERO', 'M', '18/09/1996', '30/10/2017', '', 'INGENIERIA MECATRONICA', 'PASANTE', 'INGLES; MECATRÓNICA; ELECTRICIDAD; MANTENIMIENTO INDUSTRIAL', '16/10/2017', 87, '2461378469', '2464681112', 'gus180996@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1743, 2414, '', 'TEGL920901HTLMRS05', 'LUIS ALBERTO TEHOZOL GUERRERO', 'ADOLFO LÓPEZ MATEOS No. 310', 'LA CAÑADA', '', 90360, 0, 'SOLTERO', 'M', '01/09/1992', '30/10/2017', '', 'LICENCIATURA EN CIENCIAS DE LA FAMILIA', 'LICENCIATURA', 'PSICOLOGÍA; ORIENTACIÓN FAMILIAR; NO VIOLENCIA', '16/10/2017', 86, '2414077474', '', 'beto_b.honda@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1744, 2415, '', 'MEVY820615MPLRLZ04', 'YAZMÍN MERINO VILLEGAS', 'PROL. PRIVADA MORELOS No. 6', '', 'IXTULCO', 90105, 0, 'SOLTERA', 'F', '15/06/1982', '30/10/2017', '', 'TÉCNICO EN ESTILÍSMO Y BIENESTAR PERSONAL', 'TÉCNICO', 'ESTILISMO Y BIENESTAR PERSONAL', '16/10/2017', 86, '2461118536', '2464640806', 'yamerino1506@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1745, 2416, '', 'RICJ670101HTLVRS07', 'JOSÉ DE JESÚS RIVERA CARMONA', 'POPOCATEPETL No. 302-2', 'FOVISSSTE LOMA VERDE', '', 90355, 0, 'CASADO', 'M', '01/01/1967', '30/10/2017', '', 'MAESTRÍA EN ADMINISTRACIÓN', 'MAESTRÍA', 'SEGURIDAD; MONTACARGA; MANEJO DE RESIDUOS PELIGROSOS; ADMINISTRACIÓN', '16/10/2017', 85, '8113934588', '', 'jriveracar@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1746, 2417, '', 'FOCA670918HTLLSL01', 'ALBERTO  FLORES  CASTAÑEDA', 'ING. ALEJANDRO GUILLOT No. 501', 'FATIMA', '', 90357, 0, 'CASADO', 'M', '18/09/19867', '30/10/2017', '', 'INGENIERÍA MECÁNICA', 'LICENCIATURA', 'SEGURIDAD INDUSTRIAL; SISTEMA DE CALIDAD; ADMINISTRACIÓN DE PROYECTOS', '16/10/2017', 85, '2411787232', '', 'alberto.florescastaneda@yahoo.com.mx', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1747, 2418, '', 'RODE940602MTLDRD01', 'EDÉN DE JESÚS RODRÍGUEZ  ORTÍZ', 'ALCANFORES 3ra. SECCIÓN No. 5', '', 'SANTA ANITA', 90407, 0, 'SOLTERA', 'F', '02/06/1994', '30/10/2017', '', 'LICENCIATURA EN ENSEÑANZA DE LENGUAS', 'PASANTE', 'INGLÉS', '16/10/2017', 85, '2411207027', '', 'edsa.495@gmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1748, 2419, '', 'TEFJ970225MVZMLN04', 'JUAN MARÍO TEMOLTZIN FLORES', 'IGNACION PICAZO No. 40-E', 'CENTRO', '', 90800, 0, 'SOLTERO', 'M', '25/02/1997', '30/10/2017', '', 'LICENCIATURA EN DERECHO', 'LICENCIATURA', 'PRIMER RESPONDIENTE; SEGURIDAD PÚBLICA', '16/10/2017', 85, '2411336290', '', 'jua_mariot.f@live.com.mx', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1749, 2420, '', '', 'EMILIO RENÉ PÉREZ CORONA', 'AV. SAN LORENZO nO. 236', '', 'XALTELULCO', 90660, 0, 'SOLTERO', 'M', '12/11/1990', '30/10/2017', '', 'INGENIERÍA INDUSTRIAL', 'INGENIERÍA', 'CALIDAD EN MANOFACTURA; INGENIERÍA INDUSTRIAL; TRABAJO INDUSTRIAL', '16/10/2017', 85, '2411336290', '', 'eprz.corona@gmail.com', '', '0', 9, '', '', '', '', '', '', '', '', '0'),
(1750, 2421, '', 'ROPS841111MTLDRN06', 'SONIA RODRÍGUEZ PÉREZ', 'CALLE 3 MAZ 4 EDIF. 40; DEPTO B', '', 'ACUITLAPILCO', 90110, 0, 'UNIÓN LIBRE', 'F', '11/11/1984', '30/10/2017', '', 'LICENCIATURA EN TELESECUNDARIA', 'LICENCIATURA', 'ARTESANÍAS CON FIBRAS TEXTILES; PINTURA; MANUALIDADES; METODOS PARA LA CAPACITACIÓN ARTESANAL', '16/10/2017', 85, '2461082781', '', 'sony84_11@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1751, 2325, '', 'TESA920217MTLMLN06', 'ANGELICA TEOMITZI SOLIS', '5 DE FEBRERO EDIF. 20 DEPTO. 7', 'EL LLANITO  ', 'GUADALUPE IXCOTLA', 90800, 0, 'SOLTERA', 'F', '17/02/1992', '20/06/2017', '', 'LICENCIADO EN CIENCIAS DE LA EDUCACIÓN', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '20/06/2017', 92, '7571515041 ', '2461442354', 'angelicateomitzisolis@gmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1752, 2327, '', 'MUGG830703MPLXVB01', 'GABRIELA MUÑOZ GUEVARA', 'SIQUEIROS NO.25 FRACC. JEAN CHARLOT', '', 'TZOMPANTEPEC', 90491, 0, 'SOLTERA', 'F', '03/07/1983', '28/06/2017', '', 'TÉCNICO EN INGLÉS  ', 'TÉCNICO', 'INGLÉS', '20/06/2017', 87, '2411124486 ', '', 'gaviota8303@gmail.com  ', '', '0', 38, '', '', '', '', '', '', '', '', '0'),
(1753, 2340, '', 'MICC920530MPLJRN09', 'CINDY MIJARES CRUZ', 'JESÚS CARRANZA #1909', 'EL CARMEN ', '', 90316, 0, 'CASADA', 'F', '30/05/1992', '19/07/2017', '', 'TÉCNICO', 'TÉCNICO', 'ESTILÍSMO Y BIENESTAR PERSONAL', '11/07/2017', 87, '241-108-75-21       ', '', 'cindymijares64@gmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1754, 2296, '', 'PEDG930730MTLRLB05', 'GABRIELA  PÉREZ DELGADO', 'MARCOS HERNÁNDEZ NO. 134  ', 'COLONIA INDECO', '', 90350, 0, 'DIVORCIADA', 'F', '30/07/1993', '06/04/2017', '', 'TÉCNICO EN INGLÉS  ', 'TÉCNICO', 'INGLÉS', '28/03/2017', 85, '2411565037 ', '2461915838 ', 'gaby.delgado11@outlook.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1755, 2422, '', 'LOQG440509MTLPRR03', 'MARÍA GREGORIA. LÓPEZ QUIROZ', 'TRINIDAD SANTOS No. 3', 'AGUA ESCONDIDA', '', 90120, 0, 'CASADA', 'F', '09/05/1944', '15/11/2017', '', 'ARTESANÍAS FAMILIARES', 'TÉCNICO', 'ARTESANÍAS CON FIBRAS TEXTILES', '08/11/2017', 86, '2227399482', '', '', '', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1756, 1754, '', 'HEGF780823HTLRVL05', 'JOSÉ FELIPE HERNÁNDEZ GUEVARA', 'CALLE MAXIMO ROJAS 7 ', 'BARRIO LA TRINIDAD', '', 90796, 0, 'CASADO', 'M', '23/08/1978', '16/11/2017', '', 'TÉCNICO', 'SECUNDARIA', 'INSTALACIONES HIDRAÚLICAS Y DE GAS', '27/11/2012', 80, '22 21 21 38 15', '22 22 79 42 39   ', '', '', '0', 41, '', '', '', '', '', '', '', '', '0'),
(1757, 2423, '', 'OIMI950528HTLCXR02', 'IRVING OCLICA  MUÑOZ', 'BENITO JUÁREZ No. 11', 'BARRIO MÉXICO', '', 90135, 0, 'SOLTERO', 'M', '28/05/1995', '16/11/2017', '', 'INGENIERÍA MECATRÓNICA', 'INGENIERÍA', 'MECATRÓNICA; IMPRESIÓN 3D; HIDRAULICA; NEUMATICA; PROGRAMACION PLC; DIBUJO CAD; ELECTRICIDAD 220', '07/11/2017', 87, '2462181697', '', 'irving.oclica230@gmail.com', 'PROGRAMACION C', '0', 56, '', '', '', '', '', '', '', '', '0'),
(1758, 2424, '', 'MEEE761109MTLLSR05', 'ERNESTINA MELENDEZ ESCOBAR', 'UNIÓN SUR No. 57', 'CENTRO', '', 90800, 0, 'SOLTERA', 'F', '09/11/1976', '16/11/2017', '', 'TÉCNICO EN ESTILÍSMO Y BIEN ESTAR PERSONAL', 'TÉCNICO', 'ESTILÍSMO Y BIENESTAR PERSONAL', '07/11/2017', 86, '2461486535', '', '', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1759, 2425, '', 'HECL950311HTLRTS07', 'LUIS ÁNGEL HERNÁNDEZ CUATECONTZI', 'FRANCISCO I. MADERO No. 100', 'CENTRO', '', 90620, 0, 'SOLTERO', 'M', '11/03/1995', '16/11/2017', '', 'LICENCIATURA EN EDUCACIÓN ESPECIAL', 'LICENCIATURA', 'ASISENCIA EDUCATIVA; LENGUAJE A SEÑAS MEXICANA', '07/11/2017', 86, '', '2464612168', '1ghernandezcuatecontzi@gmail.com', '', '0', 1, '', '', '', '', '', '', '', '', '0'),
(1760, 2426, '', 'SAOM940609MDFNRR05', 'MIRIAM BERENICE SAINOS ORTÍZ', 'AV. HIDALGO No. 5', '', 'SANTIAGO; TLACOCHCALCO', 90180, 0, 'SOLTERA', 'F', '09/06/1994', '16/11/2017', '', 'INGENIERIA MECATRONICA', 'INGENIERIA', 'MECATRÓNICA', '07/11/2017', 86, '2461794462', '', 'mbsainos@gmail.com', '', '0', 29, '', '', '', '', '', '', '', '', '0'),
(1761, 2427, '', 'SATC930731MTLLCR08', 'CRISTINA SALAMANCA TECPANECATL', 'EMILIANO ZAPATA nO. 39', 'SECCIÓN SEXTA', '', 90850, 0, 'SOLTERA', 'F', '31/07/1993', '16/11/2017', '', 'LICENCIATURA EN CONTADURÍA PÚBLICA', 'LICENCIATURA', 'CONTABILIDAD', '07/11/2017', 86, '2464633764', '', 'fersa_safer@hotmail.com', '', '0', 28, '', '', '', '', '', '', '', '', '0'),
(1762, 2428, '', 'PEMA910925HTLLCR08', 'ABEL PÉREZ MUÑOZ', 'SAN JOSÉ No. 23-A', 'BARRIO PUEBLA', '', 90135, 0, 'SOLTERO', 'F', '25/09/1991', '16/11/2017', '', 'INGENIERÍA MECATRÓNICA', 'INGENIERÍA; DIPLOMADO EN IDIOMAS', 'MECATRÓNICA; INGLÉS; ALEMÁN; JAPONES; EMSABLADO DE DRONES; ELECTRICIDAD; DIBUJO CAD', '07/11/2017', 86, '2461228517', '', 'aing.mec@gmail.com', '', '0', 56, '', '', '', '', '', '', '', '', '0'),
(1763, 2429, '', 'FUCL840912HTLNRN07', 'LEONARDO DANIEL FUENTES CORDERO', 'PRIV. LARDIZABAL No.2743', 'LA CAÑADA', '', 90300, 0, 'UNIÓN LIBRE', 'M', '12/09/1984', '16/11/2017', '', 'LICENCIATURA EN MÉDICO VETERINARIO ZOOTECNISTA', 'PASANTE', 'DISEÑO Y ELABORACIÓN DE MUEBLES DE MADERA; EBANISTA; BARNIZ PARA EXTERIOR BASE POLIURETANO; MVZ', '07/11/2017', 90, '2411173852', '2414170831', 'mvzleodanfuco@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1764, 2247, '', 'SANE500916MTLNVG04', 'EUGENIA SANTACRUZ NAVA', '', '', '', 0, 0, '', 'M', '', '', '', '', '', '', '', 0, '', '', '', '', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1765, 2430, '', 'MIGD940606HTLMR607', 'JOSÉ DIEGO MIMIENTZI GRACIA', 'VICENTE GUERRERO No. 4', '', 'GUADALUPE IXCOTLA', 90810, 0, 'SOLTERO', 'M', '06/06/1994', '2211/2017', '', 'INGENIERIA MECATRONICA', 'INGENIERIA', 'MECATRÓNICA', '07/11/2017', 89, '2461959968', '', 'diego.mimientzi.58@gmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1766, 2431, '', 'VAVJ760110HDFZZN00', 'JUAN CARLOS VÁZQUEZ VÁZQUEZ', 'PASO DE ÁGUILAS No1', 'SECCIÓN TERCERA', '', 90620, 0, 'SOLTERO', 'M', '10/01/1976', '27/11/2017', '', 'LICENCIATURA EN DERECHO', 'LICENCIATURA', 'DERECHO', '07/11/2017', 92, '2464578710', '2464612405', 'jcvazvaz@hotmail.com', '', '0', 1, '', '', '', '', '', '', '', '', '0'),
(1767, 2432, '', 'EUBA781118MPLSLN06', 'MARÍA DE LOS ÁNGELES  ESQUIVEL BALDOMINO', 'BLVD. REVOLUCIÓN No. 5-B', '', 'ATEMPAN', 90100, 0, 'UNIÓN LIBRE', 'F', '18/11/1978', '27/11/2017', '', 'LICENCIATURA EN INFORMÁTICA', 'PASANTE', 'INFORMÁTICA', '07/11/2017', 90, '2462221889', '', 'angles_esquivel_bal@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1768, 2433, '', 'PAAS800811MTLLVS06', 'SUSANA PALACIOS ÁVILA', 'JAZMÍNES No. 1', '', 'OCOTLAN TEPATLAXCO', 90670, 0, 'CASADA', 'F', '11/08/1980', '27/11/2017', '', 'LICENCIATURA EN CIENCIAS DE LA EDUCACIÓN', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '07/11/2017', 88, '2411275506', '0414155136', '', '', '0', 18, '', '', '', '', '', '', '', '', '0'),
(1769, 2434, '', 'ROMJ940430HTLMXM08', 'JOSÉ JAIME ROMERO MUÑOZ', 'ADOLFO LÓPEZ MATEOS No. 10-B', 'PUEBLA', '', 90124, 0, 'SOLTERO', 'M', '30/04/1994', '27/11/2017', '', 'INGENIERIA MECATRONICA', 'INGENIERIA', 'MECATRÓNICA; ELECTRÓNICA; HIDRAÚLICA; NEUMÁTICA; IMPRESIÓN 3D; DISEÑO INDUSTRIAL; ELECTRICIDAD', '07/11/2017', 88, '2461472290', '', 'jaime.r.m.498@gmail.com', 'FABRICIACIÓN DE DRONES; DIBUJO CAD', '0', 56, '', '', '', '', '', '', '', '', '0'),
(1770, 2435, '', 'CAMM740107MPLRRR08', 'MARÍA MARILÚ CARMONA  MORALES', 'ARBOLEDAS No. 53-A', 'ARBOLEDAS', '', 90500, 0, 'CASADA', 'F', '07/01/1974', '27/11/2017', '', 'TÉCNICO EN CORTE Y CONFECCIÓN', 'TÉCNICO', 'CONFECCIÓN INDUSTRIAL DE ROPA', '07/11/2017', 87, '2474715964', '2474726592', 'marylucarmorales@yahoo.com', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1771, 2436, '', 'MEML61/03/20MTLLDN01', 'LEONILA ALEJANDRA MELLADO MADRID', 'AV. CUAUHTÉMOC; No. 68', 'CENTRO', '', 90235, 0, 'CASADA', 'F', '20/03/1961', '28/11/2017', '', 'TÉCNICO EN ARTESANÍAS FAMILIARES', 'SECUNDARIA TERMINADA', 'ARTESANÍAS CON FIBRAS TEXTILES', '07/11/2017', 87, '7491014187', '7487663077', '', '', '0', 45, '', '', '', '', '', '', '', '', '0'),
(1772, 2437, '', 'BORR770914MTLDSS09', 'MA. DEL ROSARIO RODRÍGUEZ  DE LA ROSA', 'AV. PUEBLA PONIENTE No. 13', '', 'SAN ANTONIO TIZOSTOC', 90121, 0, 'CASADA', 'F', '14/09/1977', '28/11/2017', '', 'INGEIERÍA EN AGRONOMÍA', 'INGENIERÍA', 'PRODUCCIÓN INDUSTRIAL DE ALIMENTOS; AGRICULTURA CON ENFOQUE ORGÁNICO; ALIMENTOS ORGÁNICOS', '07/11/2017', 86, '2481294720', '', 'rorodero_2@hotmail.com', '', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1773, 2438, '', 'CURM710708TLPYR03', 'MARISELA CUAPIO REYES', 'ABALSOLO No. 12-6', '', 'SAN ANTONIO CUAUHTLA', 90117, 0, 'SOLTERA', 'F', '08/07/1971', '28/11/2017', '', 'LICCENCIATURA EN CRIMINOLOGÍA; CRIMINALÍSTICA Y TPECNICAS PERICIALES', 'PASANTE', 'DERECHO; CRIMINOLOGÍA; PRIMER RESPONDIENTE', '07/11/2017', 86, '22266332593', '2461448085', 'crimicuapio@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1774, 2439, '', 'GULV801209MPLTMV04', 'VIVIANA GUTIÉRREZ  LIMA', 'HOGARES FFCC; EDIF. 5 DEPTO. 202', '', '', 90329, 0, 'CASADA', 'M', '09/12/1980', '28/11/2017', '', 'INGENIERÍA INDUSTRIAL', 'PASANTE', 'HABILIDADES BLANDAS; INGENIERÍA INDUSTRIAL', '07/11/2017', 86, '2411302113', '', 'lavivis9@hotmail.com', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1775, 2440, '', 'MOXA830718MTLLLL01', 'ALICIA MOLINA XOLOCOTZI', '5 DE MAYO No. 13', '', 'BELÉN ATZIZIMITITLAN', 90605, 0, 'CASADA', 'F', '18/07/1983', '28/112017', '', 'INGENIERÍA INDUSTRIAL', 'INGENIERÍA', 'INGENIERÍA INDUSTRIAL Y QUÍMICA; LIDERAZGO; TRABAJO EN ALTURAS; ESPACIOS CONFINADOS', '07/11/2017', 85, '2461815316', '2461209587', 'chikismol@hotmail.com', '', '0', 2, '', '', '', '', '', '', '', '', '0'),
(1776, 2441, '', 'SACM880319MTLLYR05', 'MARICELA SALAZAR COYOMATZI', 'JOSEFA ORTÍZ DE DOMÍNGUEZ S/N', '', 'JESÚS XOLALPAN', 90840, 0, 'CASADA', 'F', '19/03/1988', '28/11/2017', '', 'TÉCNICO EN INFORMÁTICA AGROPECUARIA', 'TÉCNICO', 'ARTSANÍAS DE HOJAS DE MAÍZ; ARTESANÍAS CON FIBRAS TEXTILES; FIGURAS DE GLOBO', '07/11/2017', 85, '2462180170', '', 'aleciram.sc@hotmail.com', '', '0', 50, '', '', '', '', '', '', '', '', '0'),
(1777, 2442, '', 'GAMG950103MTLRNN08', 'GENOVEVA GARZA MENDOZA', 'FRANCISCO I. MADERO No. 51', '', 'XOLALPAN', 90840, 0, 'CASADA', 'F', '19/03/1988', '28/11/2017', '', 'TÉCNICO EN INFORMÁTICA', 'TÉCNICO', 'GLOBOFLEXIA', '07/11/2017', 85, '2461225359', '', 'geno_1234@live-com.mx', '', '0', 50, '', '', '', '', '', '', '', '', '0'),
(1778, 2443, '', 'CAPO830113HTLSRS08', 'OSVALDO CASTILLO PÉREZ', 'PORFIRIO DÍAZ No. 6', '', 'VILLA ALTA', 90700, 0, 'UNIÓN LIBRE', 'M', '13/01/1983', '28/11/2017', '', 'TÉCNICO EN INFORMÁTICA', 'TÉCNICO', 'INFORMÁTICA (WORD; POWER POINT)', '07/110/2017', 85, '2461250386', '', 'casper_01zorr@hotmail.com', '', '0', 19, '', '', '', '', '', '', '', '', '0'),
(1779, 2444, '', 'PMAC841227HTLCNR01', 'CARLOS GILBERTO PACHECO MONTIEL', 'PRIV. SANTA CRUZ No. 26-B', 'FRACC. ESMERALDA', '', 90105, 0, 'CASADO', 'M', '27/12/1984', '02/11/2017', '', 'INGENIERÍA EN AGRONOMÍA CON ORIENTACIÓN EN FITOTECNIA', 'INGENIERÍA', 'AGRONOMÍA;GERMOPLASMA; MEDIOS DE CRECIMIENTO; SISTEMAS DE RIEGO; FORMULACION DE NUTRIENTES; FITOSANI', '16/11/2017', 93, '2411118910', '', 'srchagoras@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1780, 2445, '', 'FEBJ711216HSRLLL03', 'JULIÁN ROBERTO FÉLIX BALDERRAMA', 'PRIV. REVOLUCIÓN No. 43', '', 'LA SIERRA', 90480, 0, 'CASADO', 'M', '16/12/1971', '28/11/2017', '', 'TÉCNICO EN SISTEMAS COMPUTACIONALES', 'TÉCNICO', 'INFORMÁTICA; ECOTÉCNIAS', '16/11/2017', 91, '2411166712', '', 'grupoemisa@hotmail.com', '', '0', 39, '', '', '', '', '', '', '', '', '0'),
(1781, 2446, '', 'LILG800525HTLMNS07', 'GASTÓN LIMA LUNA', '6 ORIENTE No. 17', 'BARRIO SANTIAGO', '', 90550, 0, 'CASADO', 'M', '25/05/1980', '29/11/2017', '', 'INGENIERÍA EN AGRONOMÍA CON ORIENTACIÓN EN FITOTÉCNIA', 'INGENIERÍA', 'ADMINISTRACIÓN; PLANEACIÓN ESTRATEGICA ENFOCADA AL SECTOR RURAL', '16/11/2017', 87, '2761039889', '', 'limalunagaston@gmail.com', '', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1782, 2447, '', 'ROHL890821MTLSRZ01', 'LUZ DE LA ROSA HERNÁNDEZ', 'AVENIDA MÉXICO nO. 115', '', 'SAN ANTONIO TIZOSTOC', 90127, 0, 'CASADA', 'F', '21/08/1989', '29//11/2017', '', 'LICENCIATURA EN COMUNICACIÓN E INNOVACIÓN EDUCATIVA', 'LICENCIATURA', 'ASISTENCIA EDUCATIVA', '16/11/2017', 86, '2461134816', '', '', '', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1783, 2448, '', 'CULG771112MMCRNB04', 'MARÍA GABRIELA CRUZ LUNA', 'PALO HUECO S/N', '', 'A. DEL PEÑON', 90255, 0, 'CASADA', 'F', '12/11/2017', '29/11/2017', '', 'TÉCNICO EN CORTE Y CONFECCIÓN', 'TÉCNICO', 'CONFECCIÓN INDUSTRIAL DE ROPA', '16/11/2017', 85, '2411639729', '2414124121', '', '', '0', 34, '', '', '', '', '', '', '', '', '0'),
(1784, 2449, '', 'HECE850127MHGRMD05', 'EDITH AHITÉ HERNÁNDEZ CAMPERO', 'UNIDAD HABITACIONAL SAN JOSÉ BUENAVISTA EDIF. 114 DEPTO. 6', '', '', 90120, 0, 'SOLTERA', 'F', '27/01/1985', '29/11/2017', '', 'LICENCIATURA EN BIOLOGÍA', 'LICENCIATURA', 'CULTIVO PRÁCTICO DE HONGO SETA; ECOTURISMO; AGRICULTURA ORGANICA; ENERGÍAS RENOVABLES', '16/11/2017', 85, '7715661928', '', 'ahitesol@hotmail.com', '', '0', 15, '', '', '', '', '', '', '', '', '0'),
(1785, 1523, '', 'HESY760226MTLRRN02', 'YENY  HERNÁNDEZ SARABIA', 'INSURGENTES #24  ', '', 'ATZINCO      ', 90160, 0, 'CASADA', 'F', '26/02/1976', '17/04/2012', '', 'INFORMÁTICA', 'TÉCNICO', 'INFORMÁTICA; LIDERAZGO; MOTIVIACIÓN PERSONAL; MANEJO DE CIRIS', '12/04/2012', 85, '2461232676', '', '', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1786, 2330, '', 'FEFG660708HTLRLL02', 'GILDARDO ADRIAN   FERNANDEZ FLORES', 'C. REPUBLICA DEL SALVADOR NO.105 ', 'COLONIA VENUSTIANO CARRANZA', '', 90460, 0, 'CASADO', 'M', '08/07/1966', '28/06/2017', '', 'ADMINISTRACÍÓN ', 'TÉCNICO EN OPERACIÓN DE MONTACARGAS', 'MONTACARGAS', '19/06/2017', 87, '2411632172', '2414130117', 'afefo1966@gmail.com', '', '0', 39, '', '', '', '', '', '', '', '', '0'),
(1787, 2450, '', 'CAZA850420MTLRMD08', 'ADALYD CARRO ZAMORA', 'MELCHOR OCAMPO #36', '5TO BARRIO', 'PANOTLA', 90140, 0, 'SOLTERA', 'F', '20/04/1985', '29/01/2018', '', 'LICENCIATURA EN EDUCACIÓN PREESCOLAR', 'LICENCIATURA EN EDUCACIÓN PREESCOLAR', 'EDUCACIÓN PREESCOLAR; TÉCNICO EN ANÁLISIS Y TECNOLOGÍA DE ALIMENTOS', '03/11/2017', 9326, '2461888206', '2464625470', 'adakrroza@gmail.com', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1788, 2451, '', 'TAIL860621HTLLLS04', 'LUIS ESDUARDO TLACHI ILHUICATZI', 'ROSALES #36', 'FRACC. RINCON DE LAS ROSAS', 'OCOTLAN', 90100, 0, 'CASADO', 'M', '21/06/1986', '29/01/2018', '', 'PASANTE ING MECATRONICA', 'TECNICO SUPERIOR EN ELECTROCNICA', 'MECATRONICA Y ELECTRONICA', '31/11/2017', 9032, '2461711752', '', 'luistlachi@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1789, 2452, '', 'AALA820422HTLNLL00', 'ALEJANDRO ANAYA LLANOS', '2 SUR; #8', 'BARRIO SAN ANTONIO', 'ALTZAYANCA', 90550, 0, 'CASADO', 'M', '22/04/1982', '29/11/2017', '', 'METALICA Y AUTOPARTES', 'INGENIERIA EN ELECTROMECÁNICA', 'ELECTROMECÁNICA', '30/11/2017', 9032, '2761112614', '', 'inge_anaya@hotmail.com', '', '0', 4, '', '', '', '', '', '', '', '', '0'),
(1790, 2453, '', 'LEAR860408HTLMRM08', 'RAMIRO ALBERTO LEMUS ARELLANO', 'IXTAZIHUATL # 8', 'VOLCANES', 'TLAXCALA', 90000, 0, 'CASADO', 'M', '09/04/1986', '29/01/2018', '', 'LICENCIATURA EN GASTRONOMIA', 'LICENCIATURA', 'ALIMENTOS Y BEBIDAS', '30/11/2017', 9016, '2464627078', '2464594042', 'arellanochef80@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1791, 2454, '', 'PETY851109MTLRPR08', 'YARELI PÉREZ TEPEPA', 'IGNACIO ZARAGOZA #16', 'BARRIO PUEBLA', 'NOPALUCAN', 90139, 0, 'SOLTERA', 'F', '09/11/1985', '29/01/2018', '', 'TÉCNICO EN INGLES', 'TECNICO EN INGLES', 'INGLES', '30/11/2017', 8910, '2461253561', '', 'cold_caty@hotmail.com', '', '0', 56, '', '', '', '', '', '', '', '', '0'),
(1792, 2455, '', 'AUNA740622MTLGVL08', 'ALICIA AGUILAR NAVA', 'EMILIO SANCHEZ PIEDRAS #44', 'CENTRO', 'CHIAUTEMPAN', 90800, 0, 'CASADA', 'F', '30/11/2017', '29/01/2018', '', 'INGENIERIA EN COMPUTACION', 'MAESTRÍA EN CIENCIAS DE LA CALIDAD', 'INFORMATICA', '30/11/2017', 8900, '2461505960', '2464646071', 'aliciaaguilar02@prodigy.net.mx', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1793, 2456, '', 'PIZE890202HTLLPR06', 'ERIC PILOTZI ZAPATA', 'EMILIANO ZAPATA; S/N;', 'SAN PEDRO XOCHITEOTLA', 'CHIAUTEMPAN', 90820, 0, 'SOLTERO', 'M', '02/02/1989', '29/01/2018', '', 'LICENCIATURA EN EDUCACIÓN SECUNDARIA', 'MAESTRÍA EN EDUCACIÓN BÁSICA', 'EDUCACION SECUNDARIA', '30/11/2017', 8896, '2461210738', '2464155044', 'ciprez89@gmail.com', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1794, 2457, '', 'BEXA690207MTLRCL01', 'ALTAGRACIA BERRUECOS XICOHTENCATL', 'PROLONGACION INDEPENDENCIA #76', 'EL CARMEN', 'PAPALOTLA', 90790, 0, 'CASADA', 'F', '07/02/1969', '29/01/2018', '', 'LICENCIATURA', 'LICENCIATURA EN INFORMATICA', 'INFORMÁTICA', '30/11/2017', 8784, '2223002712', '2222631445', 'altagracia.berruecos@gmail.com', '', '0', 41, '', '', '', '', '', '', '', '', '0'),
(1795, 2458, '', 'LAGP720529HHGGRT03', 'MARIA PATRICIA LAGUNAS GARCIA', 'JUAREZ #112', 'CENTRO', 'APIZACO', 90300, 0, 'UNION LIBRE', 'F', '29/05/1972', '29/01/2018', '', 'TÉCNICO PROFESIONAL EN CONTABILIDAD', 'TÉCNICO PROFESIONAL', 'CONTABILIDAD', '30/11/2017', 8768, '2411039483', '', 'liclagunas29@live.com.mx', '', '0', 5, '', '', '', '', '', '', '', '', '0'),
(1796, 2459, '', 'GOTU590418HTLNPR04', 'URIEL GONGORA TAPIA', 'EDIF. 101-4', 'TLAPANCALCO', 'LOMA BONITA', 90090, 0, 'CASADO', 'M', '18/04/1959', '29/01/2018', '', 'PASANTE DE LICENCIATURA DE MEDICO VETERINARIO ZOOTECNISTA', 'PASANTE', 'HORTICULTURA; TRASPATIO URBANA; AVES ( POSTURA; ENGORDA; CORDONIZ; OVICAPRINOS', '30/11/2017', 8596, '2464769878', '', 'mxgongorita@gmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1797, 2460, '', 'LACN790208MTLRNN03', 'NANCY LARA  CANO', 'FRANCISCO I. MADERO # 9', 'CENTRO', 'PANOTLA', 90140, 0, 'SOLTERA', 'F', '08/02/1979', '29/01/2018', '', 'TECNICO EN ASISTENCIA EDUCATIVA / LICENCIATURA EN IDIOMAS EUROPEOS', 'LICENCIATURA', 'INGLES Y ASISTENCIA EDUCATIVA', '30/11/2017', 8596, '2224872942', '2464625432', 'sispiestrellac@hotmail.com', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1798, 2461, '', 'MESC631028MPLZSN06', 'MARÍA CONCEPCIÓN HERMELINDA MEZA SOSA', '9 NORTE # 6407', 'GUADALUPE VICTORIA', 'PUEBLA', 72230, 0, 'SOLTERA', 'F', '28/10/1963', '29/01/2018', '', 'TÉCNICO PROFESIONAL EN TRABAJO SOCIAL', 'TECNICO PROFESIONAL', 'TRABAJO SOCIAL; PREVENCION DE VIOLENCIA Y SERVICIO INSTITUCIONAL', '30/11/2017', 8528, '2223276811', '', 'innovar2013@hotmail.com', '', '0', 22, '', '', '', '', '', '', '', '', '0'),
(1799, 2462, '', 'LOAS600918MTLPLF06', 'SOFIA LOPEZ ALVA', 'INDEPENDENCI #25', 'STA CRUZ TECHACHALCO', 'PANOTLA', 90145, 0, 'CASADA', 'M', '18/09/1960', '29/01/2018', '', 'TEJIDO; MANUALIDADES', 'PREPARATORIA', 'ARTESANIAS', '30/11/2017', 85, '2461354300', '', '', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1800, 2463, '', 'PEME790920MDFRNL06', 'ELVIA KARELY PEREZ MENDOZA', 'PRIVADA SANTA MARIA #24', 'SAN GABRIEL CUAUHTLA', 'SAN GABRIEL CUAUHTLA', 90117, 0, 'SOLTERA', 'F', '20/09/1979', '29/01/2018', '', 'MAESTRÍA EN DESARROLLO ORGANIZACIÓNAL', 'MAESTRÍA', 'PROGRAMACION NEUROLINGÜISTICA; DESARROLLO HUMANO; DESARROLLO DIRECTIVO; ATENCION Y SERVICIO AL CLIEN', '15/01/2018', 9568, '5543506967', '', 'ekarely@yahoo.com.mx', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1801, 2464, '', 'VIVR960119MTLYZS02', 'ROSA ISELA VIEYRA VAZQUEZ', 'MANUEL ÁVILA CAMACHO #12', 'SANTA MARTHA', 'XALOZTOC', 0, 0, 'SOLTERA', 'M', '19/01/1996', '29/01/2018', '', 'TÉCNICO EN PUERICULTURA', 'TECNICO', 'ASISTENCIA EDUCATIVA', '15/01/2018', 8968, '2411260371', '2414130612', 'rosa.viey19@hotmail.com', '', '0', 39, '', '', '', '', '', '', '', '', '0'),
(1802, 2465, '', 'NAPA600117MDFRZN02', 'ANTONIA NARES VAZQUEZ', 'PROLONGACION JUAREZ #2', 'ALEJANDRO ESPEJEL; ', 'CALPULALPAN', 90208, 0, 'SOLTERA', 'F', '17/01/1960', '29/01/2018', '', 'MAESTRIA EN EDUCACION', 'MAESTRIA', 'ASISTENCIA EDUCATIVA; CIENCIAS DE LA EDUCACIÓN', '15/01/2018', 8896, '5532487105', '7499182209', 'antonia_nares@hotmail.com', '', '0', 6, '', '', '', '', '', '', '', '', '0'),
(1803, 2466, '', 'TIMA811021MTLZNN03', 'ANA LAURA TIZAPAN MENDOZA', 'PRIV. ALLENDE PTE #80-7', 'CENTRO', 'CHIAUTEMPAN', 90800, 0, 'SOLTERA', 'F', '21/10/1981', '29/01/2018', '', 'LICENCIATURA EN NEGOCIOS', 'LICENCIATURA', 'ADMINISTRACION', '15/01/2018', 8764, '2461282786', '2464640190', 'tucur203@yahoo.com.mx', '', '0', 10, '', '', '', '', '', '', '', '', '0'),
(1804, 2467, '', 'SAMF941031HTLNRR09', 'JOSE FERNANDO DE LOS SANTOS MORENO', 'FERROC. MEX #14', ' FRACC LA ESTACION', 'EMILIANO ZAPATA', 90506, 0, 'SOLTERO', 'M', '31/10/1994', '15/01/2018', '', 'TUTORIAS EN INGLES', 'PREPARATORIA', 'INGLES', '15/01/2018', 8764, '2411242001', '', 'Caralbert_morena@hotmail.com', '', '0', 13, '', '', '', '', '', '', '', '', '0'),
(1805, 2468, '', 'SOSY820121MDFLLR06', 'YURI SOLIS SALDAÑA', 'AVENIDA GUADALUPE #202', 'TOPILCO DE JUAREZ', 'XALTOCAN', 90440, 0, 'DIVORCIADA', 'F', '21/01/1982', '29/01/2018', '', 'LICENCIATURA EN PSICOLOGIA SOCIAL', 'LICENCIATUTA', 'PSICOLOGIA Y HABILIDADES BLANDAS', '15/01/2018', 87038, '2411490362', '', '82yuriss@gmail.com', '', '0', 40, '', '', '', '', '', '', '', '', '0'),
(1806, 2469, '', 'VASL640523MTLSNL05', 'LILIA VÁZQUEZ SÁNCHEZ', 'MATAMOROS #5', 'TIZATLAN', 'TIZATLAN', 90100, 0, 'CASADA', 'F', '23/05/1964', '29/01/2018', '', 'SECRETARIADO EJECUTIVO', 'TECNICO EN SECRETARIADO EJECUTIVO', 'SECRETARIA EJECUTIVA', '15/01/2018', 8738, '2461619141', '24661306', 'lilis_hojademaiz@hotmail.com', '', '0', 33, '', '', '', '', '', '', '', '', '0'),
(1807, 2470, '', 'ZACE940914MTLMLD02', 'EDITH ZAMORA CALVA', 'MIGUEL HIDALGO S/N ', 'TOLUCA DE GUADALUPE', 'TERRENATE', 90544, 0, 'SOLTERA', 'F', '14/09/1994', '29/01/2018', '', 'INGENIERIA EN TECNOLOGIAS DE LA INFORMACION', 'INGENIERIA', 'INFORMATICA', '15/01/2018', 8732, '2411690626', '', 'edithzamorzcalva.ti@gmail.com', '', '0', 30, '', '', '', '', '', '', '', '', '0'),
(1808, 2471, '', 'MECA930701MTLZRR05', 'ARIANA MEZA CARRO', 'ALDAMA #9', 'PRIMER BARRIO', 'PANOTLA', 90140, 0, 'SOLTERA', 'F', '01/07/1993', '29/01/2018', '', 'LICENCIATURA EN ADMINISTRACIÓN GASTRONÓMICA', 'LICENCIATURA', 'ADMINISTRACION GASTRONOMICA', '15/01/2018', 8664, '2464028388', '2464662175', 'arimich_lucabujo@hotlmail.com', '', '0', 24, '', '', '', '', '', '', '', '', '0'),
(1809, 2472, '', 'COGI501228MTLRRN04', 'INOCENCIA ARACELI CORDERO GARCÍA', 'HIDALGO S/N', 'SAN PEDRO ECATEPEC', 'ATLANGA', 90415, 0, 'VIUDA', 'F', '28/12/1950', '29/01/2018', '', 'PRIMARIA', 'PRIMARIA', 'ARTESANIAS DE OCOXAL', '15/01/2018', 8554, '2411617391', '2415961002', '', '', 'BUENO', 3, '', '', '', '', '', '', '', '', '0'),
(1810, 2473, '', 'IASI740325MHGSNR02', 'IRMA ISLAS SÁNCHEZ', 'COLIMA #50', 'LA CONCHITA', 'APAN; HIDALGO', 43905, 0, 'UNION LIBRE', 'F', '25/04/1974', '29/01/2018', '', 'SECUNDARIA', 'SECUNDARIA', 'GLOBOFLEXIA; CERTIFICACION IMPARTICION DE CURSOS DE FORMACION DE CAPITAL HUMANO DE MANERA PRESENCIAL', '15/01/2018', 853, '7751622829', '', '', '', 'REGULAR', 22, '', '', '', '', '', '', '', '', '0'),
(1811, 2342, '', 'PEMS730627MTLRNC01', 'MA. SOCORRO', 'ROMERO #6   ', 'Contla', 'MONTERREY', 90731, 0, 'SOLTERA', 'F', '2018-06-24', '', '', 'TÉCNICO', 'TÉCNICO', 'ESTILÍSMO Y BIENESTAR PERSONAL', '', 87, '2461883205', '2461345205', 'brayan@gmail.com', '', 'MALO', 49, '', '', '', '', '', '', '', '', '0'),
(1812, 777, '777_30703796_1981140821960620_2971525191626129408_n.jpg', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'CASADO', 'MASCULINO', '1998-10-29', '2018-06-25', '', 'Programador', 'Técnico', 'Programación', '2018-06-24', 10000, '2464624064', '2461345205', 'aldo.luria@gmail.com', 'Ninguna', 'REGULAR', 33, '', '', '', '', '', '', '', '', '0'),
(1816, 2474, '2474_Captura de pantalla (3).png', 'lopez123456', 'luis mario hernandez flores', 'Calle Tulipanes', 'del toro', 'SAN DIEGO', 91212, 12, 'CASADO', 'FEMENINO', '2018-07-03', '2018-07-27', '2018-07-25', 'Tester', '6', '12', '2018-07-01', 12, '2464624064', '2461345205', 'aldo.luria@gmail.com', 'Ninguna', 'REGULAR', 7, '', '', '', '', '', '', '', '', '0'),
(1817, NULL, '2475_Captura de pantalla (3).png', 'lopez123456', 'luis mario hernandez flores', 'Calle Tulipanes', 'del toro', 'SAN DIEGO', 91212, 12, 'CASADO', 'FEMENINO', '2018-07-03', '2018-07-27', '2018-07-25', 'Tester', '6', '12', '2018-07-01', 12, '2464624064', '2461345205', 'aldo.luria@gmail.com', 'Ninguna', 'REGULAR', 5, '', '', '', '', '', '', '', '', '0'),
(1818, 2475, '2475_Captura de pantalla (4).png', 'LUGA981029HTLRRL09', 'asd', 'asd', 'GOEVILLAS', 'asd', 12, 12, 'soltero', 'MASCULINO', '2018-07-06', '2018-07-06', '', 'Tester', 'Tecnico', 'Programación', '2018-07-06', 123, '2464624064', '2461345205', 'brayan@gmail.com', 'Ninguna', 'BUENO', 4, 'No Competente', '', '', '', '', '', '', '', '0'),
(1821, 2477, '2477_Captura de pantalla (5).png', 'lopez123456', 'brayan lopez gonzalez', 'del brayan', 'del toro', 'huamantla', 666, 555, 'CASADO', 'MASCULINO', '1998-09-23', '2018-07-07', '', 'Tester', 'Tecnico', 'Programación', '2018-07-06', 1000, '2464624064', '2461345205', 'brayan@gmail.com', 'No', 'REGULAR', 13, '', '', '', '', '', '', '', '', '0'),
(1822, 2478, '2478_Captura de pantalla (4).png', 'LUGA981029HTLRRL09', 'brayan lopez gonzalez', 'del brayan', 'GOEVILLAS', 'huamantla', 666, 12, 'CASADO', 'MASCULINO', '2018-07-06', '2018-07-06', '', 'griego', '112e', 'Progranación', '2018-07-06', 1, '2464624064', '2461345205', 'brayan@gmail.com', '10', 'BUENO', 12, '', '', '', '', '', '', '', '', '0'),
(1823, 2479, '2479_531696.png', 'lopez123456', 'brayan lopez gonzalez', 'del brayan', 'del toro', 'huamantla', 666, 666, 'CASADO', 'FEMENINO', '1198-09-23', '2018-07-10', '', 'griegoxd', 'Tecnico', 'Ninguna', '2018-07-10', 777, '2464624064', '2461345205', 'brayan@gmail.com', 'Ninguna', 'MALO', 13, '', '', '', '', '', '', '', '', '0'),
(1824, 2480, '2480_ICATLAX.png', 'JUANPCURP123', 'brayan lopez gonzalez', 'del brayan', 'del brayan', 'huamantla', 666, 666, 'CASADO', 'MASCULINO', '2018-07-09', '2018-07-16', '', 'griego', 'Tecnico', 'Ninguna', '2018-07-18', 1, '2461345205', '2461345205', 'brayan@gmail.com', 'Ninguna otra vez xd', 'Sin calidad', 13, '', '', '', '', '', '', '', '', '0'),
(1825, 2481, '354369.png', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'SOLTERO', 'MASCULINO', '1998-10-29', '2018-08-12', '', 'Programador', 'Técnico', 'Programación web.', '2018-08-11', 100, '2461345205', '2464624064', 'aldo.luria@gmail.com', 'Ninguna.', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1826, 2482, '111704.png', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'SOLTERO', 'MASCULINO', '1998-10-29', '2018-08-12', '', 'Programador', 'Técnico', 'Programación web.', '2018-08-11', 100, '2461345205', '2464624064', 'aldo.luria@gmail.com', 'Ninguna.', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1827, 2483, '775102.png', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'SOLTERO', 'MASCULINO', '1998-10-29', '2018-08-12', '', 'Programador', 'Técnico', 'Programación web.', '2018-08-11', 100, '2461345205', '2464624064', 'aldo.luria@gmail.com', 'Ninguna.', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1828, 2484, '953939.jpg', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'SOLTERO', 'MASCULINO', '1998-10-29', '', '', 'Programador', 'Técnico', 'Programación web y ya xd', '2018-08-11', 100, '2461345205', '2464624064', 'aldo.luria@gmail.com', '', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1829, 2485, '2485_34466.jpg', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'SOLTERO', 'MASCULINO', '1998-10-29', '', '', 'Programador', 'Técnico', 'Programación web y ya xd', '2018-08-11', 100, '2461345205', '2464624064', 'aldo.luria@gmail.com', 'Creo no hay gg', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1830, 2486, '2486_906705.jpg', 'LUGA981029HTLRRL09', 'ALDO GIOVANNI LURIA GARCIA', 'Calle Tulipanes', 'GOEVILLAS', 'SAN DIEGO', 90110, 33, 'CASADO', 'MASCULINO', '1998-10-29', '2018-08-12', '2018-08-13', 'Programador', 'Técnico', 'Hola', '2018-08-11', 99, '2461345205', '2464624064', 'aldo.luria@gmail.com', 'Hola 2', 'Sin calidad', 33, '', '', '', '', '', '', '', '', ''),
(1831, 2487, '2487_957979.jpg', 'MITZI00112222PUREB3', 'LUIS FRANCISCO LURIA GARCIA GG', 'Calle Tulipanes gg', 'GOEVILLAS gg', 'SAN DIEGO gg', 90112, 332, 'SOLTERA gg', 'MASCULINO', '2002-03-28', '2018-08-23', '2018-08-24', 'Diseñadora gg', 'Preparatoria gg', 'Costura y sastrería gg.', '2018-08-22', 92, '2461234562', '2464624062', 'mitzi2@correo.com', 'No tiene titulo gg.', 'Sin calidad', 33, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modalidad_cursos`
--

CREATE TABLE `modalidad_cursos` (
  `id` int(11) NOT NULL,
  `Modalidad` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modalidad_cursos`
--

INSERT INTO `modalidad_cursos` (`id`, `Modalidad`) VALUES
(1, 'EBC'),
(2, 'Extensión'),
(3, 'CAE'),
(4, 'ROCO'),
(5, 'Regular'),
(6, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulosespecialidades`
--

CREATE TABLE `modulosespecialidades` (
  `id_especialidad` int(11) NOT NULL,
  `especialidad` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cveEspecialidad` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_modulo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `modulosespecialidades`
--

INSERT INTO `modulosespecialidades` (`id_especialidad`, `especialidad`, `cveEspecialidad`, `id_modulo`) VALUES
(1, 'Producción Industrial de Alimentos', '01-PIA-2017A', 1),
(2, 'Electricidad', '04-E-2017A', 4),
(3, 'Electrónica', '05-E-2017A', 5),
(4, 'Mecatrónica', '06-M-2017A', 6),
(5, 'Mantenimiento Industrial', '07-MI-2017A', 7),
(6, 'Mantenimiento de Máquinas de Costura', '07-MMC-2017A', 7),
(7, 'Refrigeración y Aire Acondicionado', '07-RAA-2017A', 7),
(8, 'Mecánica Automotriz', '08-MA-2017A', 8),
(9, 'Electrónica Automotriz', '08-EA-2017A', 8),
(10, 'Diseño e Imagen de la Carrocería', '08-DIC-2017A', 8),
(11, 'Mecánica Diesel', '08-MD-2017A', 8),
(12, 'Operación de Autotransporte', '08-OA-2017A', 8),
(13, 'Mantenimiento de Equipos y Sistemas Computacionales', '11-MESC-2017A', 11),
(14, 'Diseño y Decoración de Interiores', '12-DDI-2017A', 12),
(15, 'Instalación de Recubrimientos Cerámicos', '12-IRC-2017A', 12),
(16, 'Dibujo Insdustrial y Arquitectónico', '12-DIA-2017A', 12),
(17, 'Instalaciones Hidráulicas  y de Gas', '12-IHG-2017A', 12),
(18, 'Planeación, Programación y Presupuestación de la Construcción', '12-PPPC-2017A', 12),
(19, 'Diseño de Modas', '13-DM-2017A', 13),
(20, 'Confección Industrial de Ropa', '13-CIR-2017A', 13),
(21, 'Sastrería', '13-S-2017A', 13),
(22, 'Elaboración y Restauración de Artesanías de Madera', '14-ERAM-2017A', 14),
(23, 'Artesanias Metálicas', '14-AM-17A', 14),
(24, 'Artesanías con Fibras Textiles', '14-AFT-2017A', 14),
(25, 'Artesanías con Pastas, Pinturas y Acabados', '14-APPA-2017A', 14),
(26, 'Diseño  y Elaboración de Cerámica', '14-DEC-2017A', 14),
(27, 'Floristería', '14-F-2017A', 14),
(28, 'Diseño y Elaboración de Joyería y Orfebrería', '14-DEJO-2017A', 14),
(29, 'Diseño y Fabricación de Muebles de Madera', '16-DFMM-2017A', 16),
(30, 'Tapicería', '16-T-2017A', 16),
(31, 'Elaboración de Calzado y Artículos de Piel y de Cuero', '16-ECAPC-2017A', 16),
(32, 'Moldeado de Plástico', '17-MP-2017A', 17),
(33, 'Prótesis Dental', '18-PD-2017A', 18),
(34, 'Prótesis y Órtesis', '18-PO-2017A', 18),
(35, 'Salud Visual', '18-SV-2017A', 18),
(36, 'Máquinas-Herramienta', '19-MH-2017A', 19),
(37, 'Metrología Dimensional', '19-MD-2017A', 19),
(38, 'Soldadura y Pailería', '19-SP-2017A', 19),
(39, 'Inglés', '22-I-2017A', 22),
(40, 'Francés', '22-F-2017A', 22),
(41, 'Producción de Radio y Televisión', '22-PRT-2017', 22),
(42, 'Doblaje, Locución y Conducción en Radio, Cine y Televisión', '22-DLCRCT-2017A', 22),
(43, 'Fotografía', '22-FO-2017A', 22),
(44, 'Expresión Gráfica Digital', '22-EGD-2017A', 22),
(45, 'Programación y Aplicaciones de WEB y Móviles', '23-PAWM-2017A', 23),
(46, 'Artes Gráficas', '24-AG-2017A', 24),
(47, 'Admistración', '25-A-2017A', 25),
(48, 'Aplicación de Normas y Procedimientos Contables y Fiscales de una Entidad Económica', '25-ANPCFEE-2017A', 25),
(49, 'Asistencia Ejecutiva', '25-AE-2017A', 25),
(50, 'Archivística', '25-AR-2017A', 25),
(51, 'Asistencia Educativa', '26-AE-2017A', 26),
(52, 'Auxiliar de Enfermería', '27-AE-2017A', 27),
(53, 'Cuidados Complementarios para el Bienestar Personal', '27-CCBP-2017A', 27),
(54, 'Servicios Prehospitalarios de Urgencias Médicas', '27-SPUM-2017C', 27),
(55, 'Atención Integral a Personas Adultas Mayores', '28-AIPAM-2017A', 28),
(56, 'Seguridad e Higiene', '28-SH-2017A', 28),
(57, 'Seguridad Pública', '28-SP-2017A', 28),
(58, 'Estilismo y Diseño de Imagen', '29-EDI-2017A', 29),
(59, 'Cosmetología', '29-C-2017C', 29),
(61, 'Alimentos y Bebidas', '30-AB-2017A', 30),
(62, 'Hotelería', '30-H-2017A', 30),
(63, 'Gestión y Venta de Servicios Turísticos', '30-GVST-2017A', 30),
(64, 'Tratamiento de Aguas', '31-TA-2017A', 31),
(65, 'Aprovechamiento de Energía Solar', '31-AES-2017A', 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulosformacion`
--

CREATE TABLE `modulosformacion` (
  `idmodulo` int(11) NOT NULL,
  `modulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `modulosformacion`
--

INSERT INTO `modulosformacion` (`idmodulo`, `modulo`) VALUES
(1, 'Agropecuario '),
(4, 'Electricidad '),
(5, 'Electrónica '),
(6, 'Mecatrónica '),
(7, 'Mantenimiento Industrial '),
(8, 'Automotor '),
(11, 'Equipos y Sistemas '),
(12, 'Construcción '),
(13, 'Vestido y Textil'),
(14, 'Artesanal'),
(16, 'Procesos de Producción Industrial'),
(17, 'Plásticos '),
(18, 'Produccion de Prótesis  Y  Órtesis'),
(19, 'Metalmecánica '),
(22, 'Comunicación '),
(23, 'Tecnologías de la Información'),
(24, 'Sistema de Impresión '),
(25, 'Administración '),
(26, 'Educación '),
(27, 'Salud '),
(28, 'Asistencia Social '),
(29, 'Imagen  y Bienestar Personal'),
(30, 'Turismo '),
(31, 'Medio Ambiente ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `folio_mun` int(11) NOT NULL,
  `nombre_mun` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`folio_mun`, `nombre_mun`) VALUES
(1, 'Amaxac de Guerrero'),
(2, 'Apetatitlán de Antonio Carvajal'),
(3, 'Atlangatepec'),
(4, 'Altzayanca'),
(5, 'Apizaco'),
(6, 'Calpulalpan'),
(7, 'El Carmen Tequexquitla'),
(8, 'Cuapiaxtla'),
(9, 'Cuaxomulco'),
(10, 'Chiautempan'),
(11, 'Muñoz de Domingo Arenas'),
(12, 'Españita'),
(13, 'Huamantla'),
(14, 'Hueyotlipan'),
(15, 'Ixtacuixtla de Mariano Matamoros'),
(16, 'Ixtenco'),
(17, 'Mazatecochco de José María Morelos'),
(18, 'Contla de Juan Cuamatzi'),
(19, 'Tepetitla de Lardizábal'),
(20, 'Sanctórum de Lázaro Cárdenas'),
(21, 'Nanacamilpa de Mariano Arista'),
(22, 'Acuamanala de Miguel Hidalgo'),
(23, 'Natívitas'),
(24, 'Panotla'),
(25, 'San Pablo del Monte'),
(26, 'Santa Cruz Tlaxcala'),
(27, 'Tenancingo'),
(28, 'Teolocholco'),
(29, 'Tepeyanco'),
(30, 'Terrenate'),
(31, 'Tetla de la Solidaridad'),
(32, 'Tetlatlahuca'),
(33, 'Tlaxcala'),
(34, 'Tlaxco'),
(35, 'Tocatlán'),
(36, 'Totolac'),
(37, 'Zitlaltepec de Trinidad Sánchez Santos'),
(38, 'Tzompantepec'),
(39, 'Xalostoc'),
(40, 'Xaltocan'),
(41, 'Papalotla de Xicohténcatl'),
(42, 'Xicohtzinco'),
(43, 'Yauhquemecan'),
(44, 'Zacatelco'),
(45, 'Benito Juárez'),
(46, 'Emiliano Zapata'),
(47, 'Lázaro Cárdenas'),
(48, 'La Magdalena Tlaltelulco'),
(49, 'San Damián Texoloc'),
(50, 'San Francisco Tetlanohcan'),
(51, 'San Jerónimo Zacualpan'),
(52, 'San José Teacalco'),
(53, 'San Juan Huactzinco'),
(54, 'San Lorenzo Axocomanitla'),
(55, 'San Lucas Tecopilco'),
(56, 'Santa Ana Nopalucan'),
(57, 'Santa Apolonia Teacalco'),
(58, 'Santa Catarina Ayometla'),
(59, 'Santa Cruz Quilehtla'),
(60, 'Santa Isabel Xiloxoxtla'),
(61, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta`
--

CREATE TABLE `oferta` (
  `id` int(11) NOT NULL,
  `nombreModulo` varchar(250) COLLATE utf32_spanish_ci DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `cveModulo` varchar(25) COLLATE utf32_spanish_ci DEFAULT NULL,
  `idEspecialidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `oferta`
--

INSERT INTO `oferta` (`id`, `nombreModulo`, `duracion`, `cveModulo`, `idEspecialidad`) VALUES
(1, 'Preparación y Conservación de Alimentos de Origen Animal', 360, '01-PIA-2000A-CCO-01', 1),
(2, 'Elaboración de Mezclas para Conservas Alimenticias de Origen Vegetal', 195, '01-PIA-2013A-EBC-02', 1),
(3, 'Técnicas Especializadas de Carnicería', 200, '01-PIA-2009C-EBC-03', 1),
(4, 'Instalación del Sistema Eléctrico Residencial', 240, '04-E-2003T-EBC-01', 2),
(5, 'Instalación del Sistema Eléctrico Industrial', 240, '04-E-2003T-EBC-02', 2),
(6, 'Mantenimiento a Motores Eléctricos', 300, '04-E-2003T-EBC-03', 2),
(7, 'Mantenimiento de Aparatos Domésticos', 200, '04-E-2003T-EBC-05', 2),
(8, 'Construcción de Redes de Telecomunicaciones', 135, '04-E-2006C-EBC-07', 2),
(9, 'Construcción de Redes de Telecomunicaciones', 135, '04-E-2006C-EBC-07', 2),
(10, 'Instalación de Sistemas Electrónicos de Seguridad', 200, '04-E-2007C-EBC-08', 2),
(11, 'Instalación y Reparación de Sistemas de Comunicación', 200, '04-E-2007C-EBC-09', 2),
(12, 'Mantenimiento Preventivo y Correctivo de Circuitos Electrónicos Analógicos y Digitales', 360, '05-E-2013A-EBC-01', 3),
(13, 'Mantenimiento de Equipos Electrónicos de Audiofrecuencia', 360, '05-E-2008-EBC-02', 3),
(14, 'Mantenimiento de Equipos Receptores de Televisión', 360, '05-E-2008T-EBC-03', 3),
(15, 'Sistemas de Control Industrial de Motores Eléctricos', 300, '06-M-2000C-CCO-01', 4),
(16, 'Mantenimiento de Sistemas Neumáticos', 100, '06-M-2000C-CCO-02', 4),
(17, 'Dispositivos de Estado Sólido Aplicado al Control Industrial', 180, '06-M-2000A-CCO-03', 4),
(18, 'Mantenimiento de Sistemas Electroneumáticos', 160, '06-M-2001C-CCO-04', 4),
(19, 'Electrónica Industrial', 300, '06-M-2001C-CCO-05', 4),
(20, 'Mantenimiento de Sistemas Hidráulicos', 100, '06-M-2001A-CCO-06', 4),
(21, 'Mantenimiento de Sistemas Electrohidráulicos', 120, '06-M-2001A-CCO-07', 4),
(22, 'Mantenimiento Mecánico Industrial', 300, '07-MI-2000A-CCO-01', 5),
(23, 'Mantenimiento a Sistemas de Transmisión de Potencia Mecánica', 140, '07-MI-2008C-EBC-02', 5),
(24, 'Mantenimiento a Bombas', 200, '07-MI-2008T-EBC-03', 5),
(25, 'Mantenimiento a Reductores y Motoreductores de Velocidad', 240, '07-MI-2009C-EBC-04', 5),
(26, 'Mantenimiento a Compresores de Aire Comprimido de uso Comercial e Industral', 230, '07-MI-2009C-EBC-05', 5),
(27, 'Instalación, Mantenimiento y Pruebas a Interruptores y Arrancadores', 60, '07-MI-2016C-FAC-01', 5),
(28, 'Mantenimiento de Máquinas de Costura Recta', 180, '07-MMC-2000A-CCO-01', 6),
(29, 'Mantenimiento de Máquinas de Costura Zig Zag', 180, '07-MMC-2000A-CCO-02', 6),
(30, 'Mantenimiento de Máquinas de Costura Especiales', 180, '07-MMC-2000C-CCO-03', 6),
(31, 'Reparación de Equipos Industriales de Refrigeración', 200, '07-RAA-1985C-SCP-01', 7),
(32, 'Mantenimiento de Sitemas de Aire Acondicionado y Refrigeración', 200, '07-RAA-2003T-EBC-02', 7),
(33, 'Reparación de Motores a Gasolina', 450, '08-MA-2002T-EBC-01', 8),
(34, 'Reparación del Sistema de Suspensión con Alineación y Balanceo', 240, '08-MA-2003T-EBC-03', 8),
(35, 'Servicio y Reparación al Sistema de Dirección', 120, '08-MA-2003T-EBC-04', 8),
(36, 'Reparación del Sistema de Transmisión  Manual', 200, '08-MA-2003T-EBC-05', 8),
(37, 'Reparación del Sistema de Transmisión  Automática', 270, '08-MA-2003T-EBC-06', 8),
(38, 'Reparación del Sistema de Embrague', 180, '08-MA-2003T-EBC-07', 8),
(39, 'Reparación del Sistema de Frenos Básicos', 280, '08-MA-2003T-EBC-08', 8),
(40, 'Reparación al Sistema de Control de Emisión de Gases Contaminantes', 240, '08-EA-2003T-EBC-01', 9),
(41, 'Afinación de Motores a Gasolina con Sistema de Inyección de Combustible', 130, '08-EA-2005T-EBC-02', 9),
(42, 'Servicio Eléctrico', 420, '08-EA-1999T-EBC-03', 9),
(43, 'Autotrónica', 360, '08-EA-1999T-EBC-04', 9),
(44, 'Reparación del Sistema de Frenos ABS', 220, '08-EA-2003T-EBC-05', 9),
(45, 'Repintado de los Componentes de la Carrocería del Vehículo Automotriz', 220, '08-DIC-2003T-EBC-01', 10),
(46, 'Reparación del Sistema de Inyección de los Vehículos de Rango Medio y Servicio Pesado', 150, '08-MD-2003C-EBC-01', 11),
(47, 'Reparación del Motor a Diesel de los Vehículos de Rango Medio y Servicio Pesado', 250, '08-MD-2008C-EBC-02', 11),
(48, 'Aspirantes sin Experiencia para la Obtención de la Licencia Federal Tipo B del Autotransporte Federal y Transporte Privado de Carga General en Tractocamión Quinta Rueda', 196, '08-OA-2017A-SCP-01', 12),
(49, 'Operación de Transporte Público de Pasajeros', 215, '08-OA-2008C-EBC-02', 12),
(50, 'Operación de Montacargas', 100, '08-OA-2009C-EBC-03', 12),
(51, 'Programación y Control del Traslado de la Carga', 190, '08-OA-2009C-EBC-04', 12),
(52, 'Aspirantes sin Experiencia para la Obtención de la Licencia Federal Tipo B del Autotransporte Federal y Transporte Privado de Carga General en Tractocamión Quinta Rueda', 96, '08-OA-2017C-SCP-05', 12),
(53, 'Aspirantes sin Experiencia para la Obtención de la Licencia Federal Tipo C y Transporte Privado de Carga General en Camión Unitario', 180, '08-OA-2017C-SCP-06', 12),
(54, 'Aspirantes sin Experiencia para la Obtención de la Licencia Federal Tipo C de Autotransporte Federal y Transporte Privado de Carga General en Camión Unitario', 88, '08-OA-2017C-SCP-07', 12),
(55, 'Mantenimiento a Monitores', 150, '11-MESC-2007T-EBC-01', 13),
(56, 'Mantenimiento a Impresoras', 150, '11-MESC-2007T-EBC-02', 13),
(57, 'Mantenimiento a PC y Portátiles', 410, '11-MESC-2013A-EBC-03', 13),
(58, 'Mantenimiento a Redes de Área Local (LAN)', 245, '11-MESC-2008T-EBC-04', 13),
(59, 'Detección de Necesidades Decorativas y  Aplicación de Materiales', 90, '12-DDI-2008C-EBC-01', 14),
(60, 'Diseño y Asesoría de Ambientes Decorativos', 90, '12-DDI-2008C-EBC-02', 14),
(61, 'Dibujo y Supervisión Tecnológica', 90, '12-DDI-2008C-EBC-03', 14),
(62, 'Diseño Computarizado de Interiores y Cálculo de Iluminación', 90, '12-DDI-2008C-EBC-04', 14),
(63, 'Cálculo de Costo y Diseño de Proyecto', 90, '12-DDI-2008C-EBC-05', 14),
(64, 'Diseño de Proyecto Integral', 90, '12-DDI-2008C-EBC-06', 14),
(65, 'Instalación de Recubrimientos Cerámicos', 40, '12-IRC-2013A-EBC-01', 15),
(66, 'Dibujo Técnico Asistido por Computadora', 400, '12-DIA-2000A-CCO-01', 16),
(67, 'Dibujo Mecánico Asistido  por Computadora', 450, '12-DIA-2000A-CCO-02', 16),
(68, 'Dibujo Arquitectónico Asistido por Computadora', 450, '12-DIA-2000A-CCO-03', 16),
(69, 'Dibujo Asistido por Computadora en 3D', 180, '12-DIA-2000C-CCO-04', 16),
(70, 'Dibujo Técnico Asistido por Computadora', 180, '12-DIA-2007T-EBC-05', 16),
(71, 'Desarrollar Dibujos Industriales', 320, '12-DIA-2007T-EBC-05', 16),
(72, 'Instalación y Reparación del Sistema Hidrosanitario Residencial', 300, '12-IHG-2002T-EBC-01', 17),
(73, 'Instalación del Sistema de Gas', 150, '12-IHG-2004T-EBC-02', 17),
(74, 'Elaboración de Presupuesto de Construcción', 150, '12-PPPC-2011C-EPI-01', 18),
(75, 'Diseño de Modas', 360, '13-DM-2000A-CCO-01', 19),
(76, 'Diseño', 210, '13-DM-2000A-CCO-02', 19),
(77, 'Modelado', 270, '13-DM-2000A-CCO-03', 19),
(78, 'Patronaje', 450, '13-DM-2000A-CCO-04', 19),
(79, 'Graduacion de Patrones', 120, '13-DM-2014T-EBC-05', 19),
(80, 'Costura , Confección y Bordado', 165, '13-DM-2000A-CCO-06', 19),
(81, 'Alta Costura', 350, '13-CIR-2009T-EBC-01', 20),
(82, 'Confección de Prendas para Dama y Niña', 350, '13-CIR-2009T-EBC-02', 20),
(83, 'Confección de Prendas para Caballero  y Niño', 300, '13-CIR-2009T-EBC-03', 20),
(84, 'Patronaje y Graduación', 200, '13-CIR-2000T-EBC-04', 20),
(85, 'Confección de Saco Sastre', 310, '13-S-2005T-EBC-01', 21),
(86, 'Confección de Chaleco y Falda Sastre', 175, '13-S-2006T-EBC-02', 21),
(87, 'Confección de Pantalón Sastre', 210, '13-S-2006T-EBC-03', 21),
(88, 'Cestería', 240, '14-ERAM-2000A-CCO-01', 22),
(89, 'Administración Artesanal', 120, '14-ERAM-2000A-CCO-02', 22),
(90, 'Óleo Aplicado', 300, '14-ERAM-2000A-CCO-03', 22),
(91, 'Restauración Artística', 240, '14-ERAM-2003A-CCO-04', 22),
(92, 'Talla en Madera', 240, '14-ERAM-2000A-CCO-05', 22),
(93, 'Pirograbado', 300, '14-ERAM-2000A-CCO-06', 22),
(94, 'Resinas y Moldes', 150, '14-ERAM-2000A-CCO-07', 22),
(95, 'Repujado en Metal', 200, '14-AM-2000A-CCO-01', 23),
(96, 'Vidrio Artístico', 240, '14-AM-2001A-CCO-02', 23),
(97, 'Esmalte a Fuego', 240, '14-AM-2003A-CCO-03', 23),
(98, 'Herrería Artística', 240, '14-AM-2003A-CCO-04', 23),
(99, 'Tejido a Mano', 300, '14-AFT-2000A-CCO-01', 24),
(100, 'Bordado en Tela', 240, '14-AFT-2002A-CCO-02', 24),
(101, 'Chaquira', 200, '14-AFT-2001A-CCO-03', 24),
(102, 'Macramé', 300, '14-AFT-2001A-CCO-05', 24),
(103, 'Pintura Textil', 120, '14-APPA-2002A-CCO1', 25),
(104, 'Migajón', 240, '14-APPA-2002A-CCO2', 25),
(105, 'Pintura  en Cerámica', 300, '14-APPA-2002A-CCO3', 25),
(106, 'Cerámica Artificial', 300, '14-APPA-2000A-CCO4', 25),
(107, 'Poliéster', 120, '14-ANA-2003A-CCO5', 25),
(108, 'Trabajos en Papel y Cartón', 240, '14-APPA-2003A-CCO6', 25),
(109, 'Formado de Piezas por el Método de Torneado', 600, '14-DEC-2000A-CCO-02', 26),
(110, 'Pintura en Cerámica Bajo Esmalte', 400, '14-DEC-2000A-CCO-03', 26),
(111, 'Pintura en Cerámica Tipo Talavera', 300, '14-DEC-2000A-CCO-04', 26),
(112, 'Producción de Piezas Cerámicas por el Proceso de Vaciado', 300, '14-DEC-2002T-EBC-05', 26),
(113, 'Proparación de Pastas Cerámicas para Procesos de Formado', 180, '14-DEC-2005T-EBC-06', 26),
(114, 'Diseños Florales de Ocasión', 250, '14-F-2013A-EBC-01', 27),
(115, 'Ornamento Floral', 240, '14-F-2000A-CCO-02', 27),
(116, 'Joyería', 260, '14-DEJO-2000A-CCO-01', 28),
(117, 'Fundido de Metales para Joyería', 295, '14-DEJO-2006C-EBC-02', 28),
(118, 'Laminado y Trefilado para Joyería', 190, '14-DEJO-2007C-EBC-03', 28),
(119, 'Vaciado de Metales en Joyería', 200, '14-DEJO-2009C-EBC-04', 28),
(120, 'Orfebrería', 260, '14-DEJO-2000A-CCO-05', 28),
(121, 'Carpintería de Puertas, Ventanas y Armarios', 300, '16-DFMM-1994A-SCP-01', 29),
(122, 'Preparación de Materia Prima en la Elaboración de Muebles de Madera', 175, '16-DFMM-2013A-EBC-02', 29),
(123, 'Pintado de Piezas, Componentes y Muebles de Madera', 260, '16-DFMM-2003C-EBC-03', 29),
(124, 'Elaboración de Piezas Torneadas', 180, '16-DFMM-2003C-EBC-04', 29),
(125, 'Terminado de Muebles de Madera', 230, '16-DFMM-2003C-EBC-05', 29),
(126, 'Restauración de Muebles', 160, '16-DFMM-2005C-EBC-06', 29),
(127, 'Rauteado de Piezas para Muebles de Madera', 310, '16-DFMM-2008C-EBC-07', 29),
(128, 'Cortinas y Cojines', 150, '16-T-2000A-CCO-01', 30),
(129, 'Tapizado de Muebles', 300, '16-T-2005T-EBC-03', 30),
(130, 'Corte de Calzado', 280, '16-ECAPC-2014A-EBC-01', 31),
(131, 'Pespunte de Calzado', 280, '16-ECAPC-2014A-EBC-02', 31),
(132, 'Montado de Calzado', 170, '16-ECAPC-2012C-EBC-03', 31),
(133, 'Ensuelado y Terminado de Calzado', 210, '16-ECAPC-2015C-EBC-04', 31),
(134, 'Moldeado de Plástico por Inyección', 180, '17-MP-2001C-SCP-01', 32),
(135, 'Prostodoncia Total', 240, '18-PD-1994C-SCP-01', 33),
(136, 'Elaboración de Prótesis Fija', 230, '18-PD-2000T-EBC-02', 33),
(137, 'Elaboración de Prótesis Dental Parcial Removible', 365, '18-PD-2006T-EBC-03', 33),
(138, 'Elaboración de la Aparatología de la Ortodoncia y Ortopedia Dental Fija y Removible', 300, '18-PD-2009C-EBC-04', 33),
(139, 'Prótesis Debajo de Rodilla', 240, '18-PO-2000A-CCO-01', 34),
(140, 'Prótesis Arriba de Rodilla', 240, '18-PO-2000A-CCO-02', 34),
(141, 'Prótesis Arriba y Debajo de Codo', 300, '18-PO-2000C-CCO-03', 34),
(142, 'Práctica de Exámenes de Refracción', 450, '18-SV-2014A-EBC-01', 35),
(143, 'Producción de Lentes y Anteojos', 320, '18-SV-2006T-EBC-02', 35),
(144, 'Matricería', 400, '19-MH-2001A-SCP-01', 36),
(145, 'Maquinado de Piezas por Control Numérico Computarizado', 310, '19-MH-2007T-EBC-02', 36),
(146, 'Torneado de Piezas', 360, '19-MH-2009T-EBC-03', 36),
(147, 'Fresado de Piezas', 410, '19-MH-2009T-EBC-04', 36),
(148, 'Rectificado de Piezas', 220, '19-MH-2009T-EBC-05', 36),
(149, 'Metrología de Taller', 180, '19-MD-2000A-CCO-01', 37),
(150, 'Laboratorio de Metrología', 180, '19-MD-2001A-CCO-02', 37),
(151, 'Soldadura Oxigas del Acero en Posiciones', 120, '19-SP-2002A-CCO-01', 38),
(152, 'Soldadura en Procesos Especiales TIG Y MIG Computarizado', 180, '19-SP-2002A-CCO-02', 38),
(153, 'Soldadura de Ventanería', 180, '19-SP-2002A-SCP-03', 38),
(154, 'Aplicación de Soldadura por Arco Metálico Protegida con Gas (GMAW)', 200, '19-SP-2003C-EBC-04', 38),
(155, 'Pailería Industrial', 350, '19-SP-2003T-EBC-05', 38),
(156, 'Aplicación de Soldadura por Arco con  Electrodo Metálico Revestido', 300, '19-SP-2014A-EBC-06', 38),
(157, 'Soldadura por Arco con Tungsteno y Gas (GTAW)', 200, '19-SP-2009C-EBC-07', 38),
(158, 'Inglés Comunicativo Básico Inicial', 180, '22-I-1999A-CCO-01', 39),
(159, 'Inglés Comunicativo Básico Superior', 180, '22-I-1999A-CCO-02', 39),
(160, 'Inglés Comunicativo Preintermedio', 180, '22-I-1999A-CCO-03', 39),
(161, 'Inglés Comunicativo Intermedio', 180, '22-I-2000A-CCO-04', 39),
(162, 'Francés Comunicativo Básico Inicial', 180, '22-F-2000A-CCO-01', 40),
(163, 'Francés Comunicativo Básico Superior', 180, '22-F-2000A-CCO-02', 40),
(164, 'Francés Comunicativo Preintermiedio', 180, '22-F-2000A-CCO-03', 40),
(165, 'Edición', 130, '22-PRT-2000A-CCO-01', 41),
(166, 'Registro de Imagen con Cámara Televisiva', 195, '22-PRT-2005T-EBC-03', 41),
(167, 'Iluminación Televisiva', 160, '22-PRT-2005T-EBC-04', 41),
(168, 'Registro de Audio', 160, '22-PRT-2006C-EBC-06', 41),
(169, 'Produccion de T.V.', 325, '22-PRT-2008C-EBC-07', 41),
(170, 'Elaboración de Guiones para Radio y T.V.', 170, '22-PRT-2009C-EBC-08', 41),
(171, 'Elaboración de Guiones para Televisión', 175, '22-PRT-2017C-EBC-09', 41),
(172, 'Locución y Conducción para Radio, Cine y TV.', 130, '22-DLCRCT-2014A-EBC-01', 42),
(173, 'Doblaje para Cine, TV. y Publicidad', 100, '22-DLCRCT-2014A-EBC-02', 42),
(174, 'Fotografía en Blanco y Negro', 120, '22-FO-2000C-CCO-01', 43),
(175, 'Fotografía de Retrato en Color', 195, '22-FO-2011C-CCO-02', 43),
(176, 'Fotografía de Producto', 120, '22-FO-2012C-CCO-03', 43),
(177, 'Presentaciones Electrónicas', 120, '22-EGD-2005A-CCO-01', 44),
(178, 'Presentaciones Gráficas', 120, '22-EGD-2005A-CCO-02', 44),
(179, 'Presentaciones Fotográficas', 120, '22-EGD-2005A-CCO-03', 44),
(180, 'Operación de Base de Datos', 120, '23-PAWN-2000A-CCO-01', 45),
(181, 'Windows e Internet', 120, '23-PAWM-2000T-EBC-02', 45),
(182, 'Elaboración de Textos', 120, '23-PAWN-2012A-EBC-03', 45),
(183, 'Elaboración de Presentaciones Electrónicas', 100, '23-PAWN-2013A-EBC-04', 45),
(184, 'Elaboración de Hojas de Cálculo', 100, '23-IF-2013A-EBC-05', 45),
(185, 'Encuadernación', 200, '24-AG-1985C-SCP-01', 46),
(186, 'Dibujo Publicitario', 400, '24-AG-1985C-SCP-03', 46),
(187, 'Operación Offset', 140, '24-AG-1999T-EBC-04', 46),
(188, 'Obtención de Impresos en Serigrafía', 270, '24-AG-2013A-EBC-05', 46),
(189, 'Mercadotecnia en la Micro y Pequeña Empresa', 60, '25-A-2013A-EBC-01', 47),
(190, 'Administración  en la Micro y Pequeña Empresa', 120, '25-A-2005C-EBC-02', 47),
(191, 'Servicio y Comunicación con el Cliente', 165, '25-A-2008C-EBC-03', 47),
(192, 'Servicios de Atención Telefónica y Telemercadeo', 100, '25-A-2009C-EBC-04', 47),
(193, 'Manejo de Herramientas para Auditoría', 240, '25-A-2009C-EBC-05', 47),
(194, 'Tráfico de Mercancías y Tramitación Aduanal', 45, '25-A-2014C-EPI-02', 47),
(195, 'Asesoría en comercialización de Bienes Inmuebles', 45, '25A-2014C-EPI-02', 47),
(196, 'Diseño de la Investigación de Mercados', 100, '25-A-2017C-EPI-03', 47),
(197, 'Contabilidad General con Paquete Contable', 340, '25-ANPCFEE-2008T-EBC-01', 48),
(198, 'Contabilidad de Costos Asistida por Computadora', 320, '25-ANPCFEE-2008T-EBC-02', 48),
(199, 'Determinación de Obligaciones Fiscales', 300, '25-ANPCFEE-2011A-EBC-03', 48),
(200, 'Taquigrafía', 300, '25-AE-1999A-CCO-01', 49),
(201, 'Servicios Secretariales', 400, '25-AE-2013A-EBC-02', 49),
(202, 'Mecanografía Asistida por Computadora', 350, '25-AE-2005A-CCO-03', 49),
(203, 'Administración de Archivos de Trámite', 80, '25-AR-2008C-EPI-01', 50),
(204, 'Administración de Archivos de Concentración', 80, '25-AR-2008C-EPI-02', 50),
(205, 'Administración de Archivos Históricos', 80, '25-AR-2008C-EPI-03', 50),
(206, 'Cuidado de Niños Lactantes en Centros de Atención Infantil', 220, '26-AE-2014A-EBC-01', 51),
(207, 'Cuidado de Niños Maternales en Centros de Atención Infantil', 200, '26-AE-2010T-EBC-02', 51),
(208, 'Cuidado de Niños Preescolares en Centros de Atención Infantil', 250, '26-AE-2010T-EBC-03', 51),
(209, 'Cuidados Básicos y Orientación al Paciente', 275, '27-AE-2015C-EBC-06', 52),
(210, 'Manejo de Material de Consumo en Unidadades de Atención Médica', 160, '27-AE-2016C-EBC-07', 52),
(211, 'Uso y Manejo del Equipo Médico e Instrumental en Unidades de Atención Médica', 160, '27-AE-2016C-EBC-08', 52),
(212, 'Asesoría en Herbolaria Básica', 220, '27-CCBP-2009C-EPI-02', 53),
(213, 'Asesoría en Herbolaria Avanzada', 230, '27-CCBP-2009C-EPI-03', 53),
(214, 'Servicios de Atención Médica Prehospitalaria', 230, '27-SPUM-2017C-EPI-01', 54),
(215, 'Orientación Familiar y Escolar para Personas con Discapacidad', 200, '28-AIPAM-2014A-EBC-01', 55),
(216, 'Atención a Personas con Discapacidad Auditiva', 120, '28-AIPAM-2009C-EBC-02', 55),
(217, 'Atención a Personas con Discapacidad Visual', 200, '28-AIPAM-2009C-EBC-03', 55),
(218, 'Habilitación Física Funcional', 250, '28-AIPAM-2009C-EBC-04', 55),
(219, 'Cuidados Básicos de la Salud para Personas con Discapacidad', 270, '28-AIPAM-2009C-EBC-05', 55),
(220, 'Atención a Personas Adultas Mayores', 220, '28-AIPAM-2009C-EBC-06', 55),
(221, 'Servicios Contra Incendios', 235, '28-SH-2007C-EPI-01', 56),
(222, 'Intervención Policial en el Nuevo Sistema Penal  Acusatorio con Cadena de Custodia', 110, '28-SP-2015C-EPI-01', 57),
(223, 'Corte y Peinado del Cabello', 200, '29-EDI-2013A-EBC-01', 58),
(224, 'Color y Trasformación en el Cabello', 220, '29-EDI-2013A-EBC-02', 58),
(225, 'Maquillaje del Rostro', 60, '29-EDI-2005C-BC-03', 58),
(226, 'Cuidados Faciales y Corporales', 120, '29-C-2005T-EBC-01', 59),
(227, 'Cuidado de Manos y Pies', 120, '29-C-2005T-EBC-02', 59),
(228, 'Servicio a Comensales', 216, '30-AB-2014A-EBC-01', 61),
(229, 'Preparación de Bebidas', 200, '30-AB-2013A-EBC-02', 61),
(230, 'Control de Costos de Alimentos y Bebidas', 200, '30-AB-2003C-EBC-03', 61),
(231, 'Preparación de Alimentos', 360, '30-AB-2003T-EBC-04', 61),
(232, 'Atención del Cliente del Vino en el Lugar de Consumo', 200, '30-AB-2009C-EBC-05', 61),
(233, 'Resguardo del Vino', 180, '30-AB-2010C-EBC-06', 61),
(234, 'Pastelería y Dulces Finos', 285, '30-AB-2009C-EBC-08', 61),
(235, 'Manejo Higiénico de Alimentos y Bebidas', 60, '30-AB-2014A-EBC-09', 61),
(236, 'Reservación de Servicios Hoteleros', 120, '30-H-1999T-EBC-01', 62),
(237, 'Recepción del Huésped', 190, '30-H-1999T-EBC-02', 62),
(238, 'Registro y Control de la Cuenta del Huésped', 200, '30-H-1999T-EBC-03', 62),
(239, 'Conserjería y de Comunicación Telefónica en Hotelería', 185, '30-H-2009C-EBC-04', 62),
(240, 'Servicio de Atención al Huésped', 200, '30-H-2009C-EBC-05', 62),
(241, 'Servicio de Habitaciones', 110, '30-H-1999T-EBC-06', 62),
(242, 'Supervisión de Habitaciones y Áreas Comunes', 165, '30-H-1999C-EBC-07', 62),
(243, 'Coordinación de los Servicios de Hospedaje', 165, '30-H-2009C-EBC-08', 62),
(244, 'Organización de Eventos de Negocios, Sociales y Culturales', 180, '30-H-2009C-EBC-09', 62),
(245, 'Supervisor Coach', 288, '30-H-2016C-FAC-01', 62),
(246, 'Front Desk', 288, '30-H-2016C-FAC-02', 62),
(247, 'Técnico en Mantimiento', 288, '30-H-2016C-FAC-03', 62),
(248, 'Venta de Servicios de Viajes', 120, '30-GVST-1999T-EBC-01', 63),
(249, 'Análisis de Laboratorio para Tratamiento de Aguas', 360, '31-TA-2000A-CCO-01', 64),
(250, 'Operación de Plantas de Potabilización y Tratamiento de Aguas Residuales  (Proceso Manual y Semiautomático)', 300, '31-TA-2002T-EBC-02', 64),
(251, 'Instalación y Manteniento de Calentadores Solares de Agua', 360, '31-AES-2012C-EBC-01', 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ofertaacademica`
--

CREATE TABLE `ofertaacademica` (
  `id` int(11) NOT NULL,
  `id_plantel` int(11) DEFAULT NULL,
  `id_especialidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ofertaacademica`
--

INSERT INTO `ofertaacademica` (`id`, `id_plantel`, `id_especialidad`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 1, 4),
(4, 1, 5),
(5, 1, 6),
(6, 1, 8),
(7, 1, 9),
(8, 1, 22),
(9, 1, 2),
(10, 1, 25),
(11, 1, 26),
(12, 1, 16),
(13, 1, 14),
(14, 1, 13),
(15, 1, 15),
(16, 1, 29),
(17, 1, 30),
(18, 1, 31),
(19, 1, 32),
(20, 2, 1),
(21, 2, 3),
(22, 2, 4),
(23, 2, 5),
(24, 2, 6),
(25, 2, 7),
(26, 2, 8),
(27, 2, 9),
(28, 2, 22),
(29, 2, 2),
(30, 2, 16),
(31, 2, 10),
(32, 2, 12),
(33, 2, 14),
(34, 2, 13),
(35, 2, 24),
(36, 2, 11),
(37, 2, 21),
(38, 2, 32),
(39, 3, 1),
(40, 3, 3),
(41, 3, 22),
(42, 3, 2),
(43, 3, 26),
(44, 3, 16),
(45, 3, 12),
(46, 3, 14),
(47, 3, 13),
(48, 3, 15),
(49, 3, 29),
(50, 3, 30),
(51, 3, 31),
(52, 3, 24),
(53, 3, 11),
(54, 3, 21),
(55, 4, 1),
(56, 4, 17),
(57, 4, 25),
(58, 4, 14),
(59, 4, 13),
(60, 5, 1),
(61, 5, 3),
(62, 5, 4),
(63, 5, 5),
(64, 5, 6),
(65, 5, 7),
(66, 5, 8),
(67, 5, 22),
(68, 5, 2),
(69, 5, 18),
(70, 5, 16),
(71, 5, 14),
(72, 5, 13),
(73, 5, 11),
(74, 5, 32),
(75, 6, 1),
(76, 6, 3),
(77, 6, 5),
(78, 6, 6),
(79, 6, 7),
(80, 6, 8),
(81, 6, 9),
(82, 6, 22),
(83, 6, 19),
(84, 6, 17),
(85, 6, 25),
(86, 6, 26),
(87, 6, 16),
(88, 6, 23),
(89, 6, 10),
(90, 6, 14),
(91, 6, 13),
(92, 6, 29),
(93, 6, 30),
(94, 6, 31),
(95, 6, 11),
(96, 6, 32),
(97, 7, 1),
(98, 7, 3),
(99, 7, 4),
(100, 7, 6),
(101, 7, 8),
(102, 7, 9),
(103, 7, 22),
(104, 7, 2),
(105, 7, 19),
(106, 7, 20),
(107, 7, 26),
(108, 7, 16),
(109, 7, 10),
(110, 7, 12),
(111, 7, 14),
(112, 7, 13),
(113, 7, 28),
(114, 7, 30),
(115, 7, 31),
(116, 7, 24),
(117, 7, 11),
(118, 7, 21),
(119, 8, 1),
(120, 8, 3),
(121, 8, 4),
(122, 8, 6),
(123, 8, 8),
(124, 8, 22),
(125, 8, 2),
(126, 8, 20),
(127, 8, 17),
(128, 8, 25),
(129, 8, 16),
(130, 8, 23),
(131, 8, 14),
(132, 8, 13),
(133, 8, 27),
(134, 8, 15),
(135, 8, 29),
(136, 9, 1),
(137, 9, 3),
(138, 9, 4),
(139, 9, 5),
(140, 9, 6),
(141, 9, 7),
(142, 9, 9),
(143, 9, 22),
(144, 9, 2),
(145, 9, 17),
(146, 9, 16),
(147, 9, 10),
(148, 9, 12),
(149, 9, 14),
(150, 9, 13),
(151, 9, 15),
(152, 9, 11),
(153, 9, 32),
(154, 10, 1),
(155, 10, 3),
(156, 10, 4),
(157, 10, 5),
(158, 10, 6),
(159, 10, 7),
(160, 10, 8),
(161, 10, 9),
(162, 10, 16),
(163, 10, 14),
(164, 10, 13),
(165, 10, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantel`
--

CREATE TABLE `plantel` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) DEFAULT NULL,
  `calle` varchar(50) DEFAULT NULL,
  `numero` varchar(11) DEFAULT NULL,
  `colonia` varchar(40) DEFAULT NULL,
  `municipio` varchar(40) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `plantel`
--

INSERT INTO `plantel` (`id`, `nombre`, `calle`, `numero`, `colonia`, `municipio`, `telefono`, `email`) VALUES
(1, 'Calpulalpan', 'Carretera México-Veracruz Kilometro 76', 's/n', 'Calpulalpan', 'Calpulalpan', '4981421', 'calpulalpan@icatlax.edu.mx'),
(2, 'Chiautempan', 'Vicente Guerrero', 's/n', 'Guadalupe Ixcotla', 'Santa Ana Chiahutempan', '2464646227', 'chiautempan@icatlax.edu.mx'),
(3, 'Huamantla', 'Carretera Huamantla-Benito Juárez Km 2', 's/n', 'Huamantla', 'Huamantla', '2474722830', 'huamantla@icatlax.edu.mx'),
(4, 'Acción móvil Papalotla', 'Maria Amparo Viderique de Shein', 's/n', 'Papalotla', 'Papalotla', '2222630236', 'papalotla@icatlax.edu.mx'),
(5, 'San Pablo del Monte', 'Via corta Santa Ana - Puebla Km 7', 's/n', 'Villa Vicente Guerrero', 'San Pablo del Monte', '2464623878', 'sanpablo@icatlax.edu.mx'),
(6, 'Tepetitla', 'Canal San Lucas', 's/n', 'San Carlos', 'Tepetitla de Lardizabal', '2484870335', 'tepetitla@icatlax.edu.mx'),
(7, 'Tetla', 'Coaxamalucan Esq. Piedras Negras', 's/n', 'Ciudad Industrial Xicotencatl', 'Tetla de la Solidaridad', '2414127280', 'tetla@icatlax.edu.mx'),
(8, 'Tetlanohcan', 'Zaragoza', 's/n', 'Barrio de Santa Cruz', 'San Francisco Tetlanohcan', '2464166412', 'tetlanohcan@icatlax.edu.mx'),
(9, 'Tlaxco', 'Máximo Rojas esq. 5 de Mayo', 's/n', 'Tlaxco', 'Tlaxco', '2414960347', 'tlaxco@icatlax.edu.mx'),
(10, 'Zitlaltepec', 'Reforma', '18', 'San Andrés Ahuashuatepec', 'Tzompantepec', '2414152087', 'zitlaltepec@icatlax.edu.mx');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantel_especialidad`
--

CREATE TABLE `plantel_especialidad` (
  `id` int(11) NOT NULL,
  `Id_plantel` int(11) NOT NULL,
  `Plantel` varchar(50) DEFAULT NULL,
  `id_especialidad` int(11) NOT NULL,
  `Especialidad` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `plantel_especialidad`
--

INSERT INTO `plantel_especialidad` (`id`, `Id_plantel`, `Plantel`, `id_especialidad`, `Especialidad`) VALUES
(1, 1, 'Calpulalpan', 1, 'Administración'),
(2, 1, 'Calpulalpan', 3, 'Alimentos y bebidas'),
(3, 1, 'Calpulalpan', 4, 'Artesanías con Fibras Textiles'),
(4, 1, 'Calpulalpan', 5, 'Artesanías de alta precisión'),
(5, 1, 'Calpulalpan', 6, 'Artesanías con pastas, pinturas y acabados'),
(6, 1, 'Calpulalpan', 8, 'Asistencia Educativa'),
(7, 1, 'Calpulalpan', 9, 'Asistencia Ejecutiva'),
(8, 1, 'Calpulalpan', 22, 'Confección Industrial de Ropa'),
(9, 1, 'Calpulalpan', 2, 'Contabilidad'),
(10, 1, 'Calpulalpan', 25, 'Electricidad'),
(11, 1, 'Calpulalpan', 26, 'Electrónica Automotriz'),
(12, 1, 'Calpulalpan', 16, 'Estilismo y Bienestar personal'),
(13, 1, 'Calpulalpan', 14, 'Informática'),
(14, 1, 'Calpulalpan', 13, 'Inglés'),
(15, 1, 'Calpulalpan', 15, 'Mantenimiento de Equipos y Sistemas Computacionales'),
(16, 1, 'Calpulalpan', 29, 'Mantenimiento de Maquinas de Costura'),
(17, 1, 'Calpulalpan', 30, 'Mecánica Automotriz'),
(18, 1, 'Calpulalpan', 31, 'Mecánica Diésel'),
(19, 1, 'Calpulalpan', 32, 'Soldadura y Palería'),
(20, 2, 'Chiautempan', 1, 'Administración'),
(21, 2, 'Chiautempan', 3, 'Alimentos y bebidas'),
(22, 2, 'Chiautempan', 4, 'Artesanías con Fibras Textiles'),
(23, 2, 'Chiautempan', 5, 'Artesanías de alta precisión'),
(24, 2, 'Chiautempan', 6, 'Artesanías con pastas, pinturas y acabados'),
(25, 2, 'Chiautempan', 7, 'Artesanías Metálicas'),
(26, 2, 'Chiautempan', 8, 'Asistencia Educativa'),
(27, 2, 'Chiautempan', 9, 'Asistencia Ejecutiva'),
(28, 2, 'Chiautempan', 22, 'Confección Industrial de Ropa'),
(29, 2, 'Chiautempan', 2, 'Contabilidad'),
(30, 2, 'Chiautempan', 16, 'Estilismo y Bienestar personal'),
(31, 2, 'Chiautempan', 10, 'Gestión Y venta de Servicios Turísticos'),
(32, 2, 'Chiautempan', 12, 'Hotelería'),
(33, 2, 'Chiautempan', 14, 'Informática'),
(34, 2, 'Chiautempan', 13, 'Inglés'),
(35, 2, 'Chiautempan', 24, 'Mecatrónica'),
(36, 2, 'Chiautempan', 11, 'Producción Industrial de Alimentos'),
(37, 2, 'Chiautempan', 21, 'Sastrería'),
(38, 2, 'Chiautempan', 32, 'Soldadura y Palería'),
(39, 3, 'Huamantla', 1, 'Administración'),
(40, 3, 'Huamantla', 3, 'Alimentos y bebidas'),
(41, 3, 'Huamantla', 22, 'Confección Industrial de Ropa'),
(42, 3, 'Huamantla', 2, 'Contabilidad'),
(43, 3, 'Huamantla', 26, 'Electrónica Automotriz'),
(44, 3, 'Huamantla', 16, 'Estilismo y Bienestar personal'),
(45, 3, 'Huamantla', 12, 'Hotelería'),
(46, 3, 'Huamantla', 14, 'Informática'),
(47, 3, 'Huamantla', 13, 'Inglés'),
(48, 3, 'Huamantla', 15, 'Mantenimiento de Equipos y Sistemas Computacionales'),
(49, 3, 'Huamantla', 29, 'Mantenimiento de Maquinas de Costura'),
(50, 3, 'Huamantla', 30, 'Mecánica Automotriz'),
(51, 3, 'Huamantla', 31, 'Mecánica Diésel'),
(52, 3, 'Huamantla', 24, 'Mecatrónica'),
(53, 3, 'Huamantla', 11, 'Producción Industrial de Alimentos'),
(54, 3, 'Huamantla', 21, 'Sastrería'),
(55, 4, 'Acción móvil Papalotla', 1, 'Administración'),
(56, 4, 'Acción móvil Papalotla', 17, 'Diseño y Fabricación de Muebles de Madera'),
(57, 4, 'Acción móvil Papalotla', 25, 'Electricidad'),
(58, 4, 'Acción móvil Papalotla', 14, 'Informática'),
(59, 4, 'Acción móvil Papalotla', 13, 'Inglés'),
(60, 5, 'San Pablo del Monte', 1, 'Administración'),
(61, 5, 'San Pablo del Monte', 3, 'Alimentos y bebidas'),
(62, 5, 'San Pablo del Monte', 4, 'Artesanías con Fibras Textiles'),
(63, 5, 'San Pablo del Monte', 5, 'Artesanías de alta precisión'),
(64, 5, 'San Pablo del Monte', 6, 'Artesanías con pastas, pinturas y acabados'),
(65, 5, 'San Pablo del Monte', 7, 'Artesanías Metálicas'),
(66, 5, 'San Pablo del Monte', 8, 'Asistencia Educativa'),
(67, 5, 'San Pablo del Monte', 22, 'Confección Industrial de Ropa'),
(68, 5, 'San Pablo del Monte', 2, 'Contabilidad'),
(69, 5, 'San Pablo del Monte', 18, 'Diseño y Elaboración de cerámica'),
(70, 5, 'San Pablo del Monte', 16, 'Estilismo y Bienestar personal'),
(71, 5, 'San Pablo del Monte', 14, 'Informática'),
(72, 5, 'San Pablo del Monte', 13, 'Inglés'),
(73, 5, 'San Pablo del Monte', 11, 'Producción Industrial de Alimentos'),
(74, 5, 'San Pablo del Monte', 32, 'Soldadura y Palería'),
(75, 6, 'Tepetitla', 1, 'Administración'),
(76, 6, 'Tepetitla', 3, 'Alimentos y bebidas'),
(77, 6, 'Tepetitla', 5, 'Artesanías de alta precisión'),
(78, 6, 'Tepetitla', 6, 'Artesanías con pastas, pinturas y acabados'),
(79, 6, 'Tepetitla', 7, 'Artesanías Metálicas'),
(80, 6, 'Tepetitla', 8, 'Asistencia Educativa'),
(81, 6, 'Tepetitla', 9, 'Asistencia Ejecutiva'),
(82, 6, 'Tepetitla', 22, 'Confección Industrial de Ropa'),
(83, 6, 'Tepetitla', 19, 'Diseño e imagen de la carrocería'),
(84, 6, 'Tepetitla', 17, 'Diseño y Fabricación de Muebles de Madera'),
(85, 6, 'Tepetitla', 25, 'Electricidad'),
(86, 6, 'Tepetitla', 26, 'Electrónica Automotriz'),
(87, 6, 'Tepetitla', 16, 'Estilismo y Bienestar personal'),
(88, 6, 'Tepetitla', 23, 'Floristería'),
(89, 6, 'Tepetitla', 10, 'Gestión Y venta de Servicios Turísticos'),
(90, 6, 'Tepetitla', 14, 'Informática'),
(91, 6, 'Tepetitla', 13, 'Inglés'),
(92, 6, 'Tepetitla', 29, 'Mantenimiento de Maquinas de Costura'),
(93, 6, 'Tepetitla', 30, 'Mecánica Automotriz'),
(94, 6, 'Tepetitla', 31, 'Mecánica Diésel'),
(95, 6, 'Tepetitla', 11, 'Producción Industrial de Alimentos'),
(96, 6, 'Tepetitla', 32, 'Soldadura y Palería'),
(97, 7, 'Tetla', 1, 'Administración'),
(98, 7, 'Tetla', 3, 'Alimentos y bebidas'),
(99, 7, 'Tetla', 4, 'Artesanías con Fibras Textiles'),
(100, 7, 'Tetla', 6, 'Artesanías con pastas, pinturas y acabados'),
(101, 7, 'Tetla', 8, 'Asistencia Educativa'),
(102, 7, 'Tetla', 9, 'Asistencia Ejecutiva'),
(103, 7, 'Tetla', 22, 'Confección Industrial de Ropa'),
(104, 7, 'Tetla', 2, 'Contabilidad'),
(105, 7, 'Tetla', 19, 'Diseño e imagen de la carrocería'),
(106, 7, 'Tetla', 20, 'Diseño de Modas'),
(107, 7, 'Tetla', 26, 'Electrónica Automotriz'),
(108, 7, 'Tetla', 16, 'Estilismo y Bienestar personal'),
(109, 7, 'Tetla', 10, 'Gestión Y venta de Servicios Turísticos'),
(110, 7, 'Tetla', 12, 'Hotelería'),
(111, 7, 'Tetla', 14, 'Informática'),
(112, 7, 'Tetla', 13, 'Inglés'),
(113, 7, 'Tetla', 28, 'Mantenimiento Industrial'),
(114, 7, 'Tetla', 30, 'Mecánica Automotriz'),
(115, 7, 'Tetla', 31, 'Mecánica Diésel'),
(116, 7, 'Tetla', 24, 'Mecatrónica'),
(117, 7, 'Tetla', 11, 'Producción Industrial de Alimentos'),
(118, 7, 'Tetla', 21, 'Sastrería'),
(119, 8, 'Tetlanohcan', 1, 'Administración'),
(120, 8, 'Tetlanohcan', 3, 'Alimentos y bebidas'),
(121, 8, 'Tetlanohcan', 4, 'Artesanías con Fibras Textiles'),
(122, 8, 'Tetlanohcan', 6, 'Artesanías con pastas, pinturas y acabados'),
(123, 8, 'Tetlanohcan', 8, 'Asistencia Educativa'),
(124, 8, 'Tetlanohcan', 22, 'Confección Industrial de Ropa'),
(125, 8, 'Tetlanohcan', 2, 'Contabilidad'),
(126, 8, 'Tetlanohcan', 20, 'Diseño de Modas'),
(127, 8, 'Tetlanohcan', 17, 'Diseño y Fabricación de Muebles de Madera'),
(128, 8, 'Tetlanohcan', 25, 'Electricidad'),
(129, 8, 'Tetlanohcan', 16, 'Estilismo y Bienestar personal'),
(130, 8, 'Tetlanohcan', 23, 'Floristería'),
(131, 8, 'Tetlanohcan', 14, 'Informática'),
(132, 8, 'Tetlanohcan', 13, 'Inglés'),
(133, 8, 'Tetlanohcan', 27, 'Instalaciones Hidráulicas y de Gas'),
(134, 8, 'Tetlanohcan', 15, 'Mantenimiento de Equipos y Sistemas Computacionales'),
(135, 8, 'Tetlanohcan', 29, 'Mantenimiento de Maquinas de Costura'),
(136, 9, 'Tlaxco', 1, 'Administración'),
(137, 9, 'Tlaxco', 3, 'Alimentos y bebidas'),
(138, 9, 'Tlaxco', 4, 'Artesanías con Fibras Textiles'),
(139, 9, 'Tlaxco', 5, 'Artesanías de alta precisión'),
(140, 9, 'Tlaxco', 6, 'Artesanías con pastas, pinturas y acabados'),
(141, 9, 'Tlaxco', 7, 'Artesanías Metálicas'),
(142, 9, 'Tlaxco', 9, 'Asistencia Ejecutiva'),
(143, 9, 'Tlaxco', 22, 'Confección Industrial de Ropa'),
(144, 9, 'Tlaxco', 2, 'Contabilidad'),
(145, 9, 'Tlaxco', 17, 'Diseño y Fabricación de Muebles de Madera'),
(146, 9, 'Tlaxco', 16, 'Estilismo y Bienestar personal'),
(147, 9, 'Tlaxco', 10, 'Gestión Y venta de Servicios Turísticos'),
(148, 9, 'Tlaxco', 12, 'Hotelería'),
(149, 9, 'Tlaxco', 14, 'Informática'),
(150, 9, 'Tlaxco', 13, 'Inglés'),
(151, 9, 'Tlaxco', 15, 'Mantenimiento de Equipos y Sistemas Computacionales'),
(152, 9, 'Tlaxco', 11, 'Producción Industrial de Alimentos'),
(153, 9, 'Tlaxco', 32, 'Soldadura y Palería'),
(154, 10, 'Zitlaltepec', 1, 'Administración'),
(155, 10, 'Zitlaltepec', 3, 'Alimentos y bebidas'),
(156, 10, 'Zitlaltepec', 4, 'Artesanías con Fibras Textiles'),
(157, 10, 'Zitlaltepec', 5, 'Artesanías de alta precisión'),
(158, 10, 'Zitlaltepec', 6, 'Artesanías con pastas, pinturas y acabados'),
(159, 10, 'Zitlaltepec', 7, 'Artesanías Metálicas'),
(160, 10, 'Zitlaltepec', 8, 'Asistencia Educativa'),
(161, 10, 'Zitlaltepec', 9, 'Asistencia Ejecutiva'),
(162, 10, 'Zitlaltepec', 16, 'Estilismo y Bienestar personal'),
(163, 10, 'Zitlaltepec', 14, 'Informática'),
(164, 10, 'Zitlaltepec', 13, 'Inglés'),
(165, 10, 'Zitlaltepec', 11, 'Producción Industrial de Alimentos'),
(166, 1, 'Calpulalpan', 33, 'N/A'),
(167, 2, 'Chiautempan', 33, 'N/A'),
(168, 3, 'Huamantla', 33, 'N/A'),
(169, 4, 'Acción móvil Papalotla', 33, 'N/A'),
(170, 5, 'San Pablo del Monte', 33, 'N/A'),
(171, 6, 'Tepetitla', 33, 'N/A'),
(172, 7, 'Tetla', 33, 'N/A'),
(173, 8, 'Tetlanohcan', 33, 'N/A'),
(174, 9, 'Tlaxco', 33, 'N/A'),
(175, 10, 'Zitlaltepec', 33, 'N/A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_apps`
--

CREATE TABLE `sec_apps` (
  `app_name` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `app_type` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_groups`
--

CREATE TABLE `sec_groups` (
  `group_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_groups_apps`
--

CREATE TABLE `sec_groups_apps` (
  `group_id` int(11) NOT NULL,
  `app_name` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `priv_access` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_insert` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_delete` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_update` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_export` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_print` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_users`
--

CREATE TABLE `sec_users` (
  `login` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pswd` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `active` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activation_code` varchar(32) COLLATE utf8_spanish_ci DEFAULT NULL,
  `priv_admin` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_users_groups`
--

CREATE TABLE `sec_users_groups` (
  `login` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_curso`
--

CREATE TABLE `status_curso` (
  `id` int(11) NOT NULL,
  `status_curso` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status_curso`
--

INSERT INTO `status_curso` (`id`, `status_curso`) VALUES
(1, 'Aprobado'),
(2, 'Rechazado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usr` int(11) NOT NULL,
  `Nombre_usr` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usr`, `Nombre_usr`, `username`, `pass`, `tipo`) VALUES
(1, 'Administrador', 'admin', '202cb962ac59075b964b07152d234b70', 'ADMIN'),
(2, 'DTA', 'dta', '202cb962ac59075b964b07152d234b70', 'DTA'),
(3, 'Planeación', 'planea', '202cb962ac59075b964b07152d234b70', 'PLANEA'),
(4, 'Coordinación', 'coo', '202cb962ac59075b964b07152d234b70', 'COO'),
(5, 'Calpulalpan', 'placal', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(6, 'Chiautempan', 'plachi', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(7, 'Huamantla', 'plahua', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(8, 'Acción móvil Papalotla', 'plaacc', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(9, 'San Pablo del Monte', 'plasan', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(10, 'Tepetitla', 'platep', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(11, 'Tetla', 'platetla', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(12, 'Tetlanohcan', 'platet', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(13, 'Tlaxco', 'platlaxco', '202cb962ac59075b964b07152d234b70', 'PLANT'),
(14, 'Zitlaltepec', 'plazit', '202cb962ac59075b964b07152d234b70', 'PLANT');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alta_alumnos`
--
ALTER TABLE `alta_alumnos`
  ADD PRIMARY KEY (`id_alumno`),
  ADD KEY `municipio` (`municipio`);

--
-- Indices de la tabla `alumno_curso2018cae`
--
ALTER TABLE `alumno_curso2018cae`
  ADD PRIMARY KEY (`id_ac`),
  ADD KEY `id_alumno` (`id_alumno_c`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `alumno_curso2018ebc`
--
ALTER TABLE `alumno_curso2018ebc`
  ADD PRIMARY KEY (`id_ac`),
  ADD KEY `id_alumno` (`id_alumno_c`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `alumno_curso2018ext`
--
ALTER TABLE `alumno_curso2018ext`
  ADD PRIMARY KEY (`id_ac`),
  ADD KEY `id_alumno` (`id_alumno_c`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `alumno_curso2018reg`
--
ALTER TABLE `alumno_curso2018reg`
  ADD PRIMARY KEY (`id_ac`),
  ADD KEY `id_alumno` (`id_alumno_c`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `alumno_curso2018roco`
--
ALTER TABLE `alumno_curso2018roco`
  ADD PRIMARY KEY (`id_ac`),
  ADD KEY `id_alumno` (`id_alumno_c`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `asignacion_curso`
--
ALTER TABLE `asignacion_curso`
  ADD PRIMARY KEY (`Folio`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_especialidad` (`id_especialidad`);

--
-- Indices de la tabla `cursos2018_cae`
--
ALTER TABLE `cursos2018_cae`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `id_instructor` (`id_instructor`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_mun_c` (`id_mun_c`);

--
-- Indices de la tabla `cursos2018_ebc`
--
ALTER TABLE `cursos2018_ebc`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `id_instructor` (`id_instructor`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_modulo` (`id_modulo_c`),
  ADD KEY `id_mun_c` (`id_mun_c`);

--
-- Indices de la tabla `cursos2018_extension`
--
ALTER TABLE `cursos2018_extension`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `id_instructor` (`id_instructor`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_mun_c` (`id_mun_c`);

--
-- Indices de la tabla `cursos2018_regular`
--
ALTER TABLE `cursos2018_regular`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `id_instructor` (`id_instructor`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_modulo` (`id_modulo_c`),
  ADD KEY `id_mun_c` (`id_mun_c`);

--
-- Indices de la tabla `cursos2018_roco`
--
ALTER TABLE `cursos2018_roco`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `id_instructor` (`id_instructor`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_modulo` (`id_modulo_c`),
  ADD KEY `id_mun_c` (`id_mun_c`);

--
-- Indices de la tabla `distritos`
--
ALTER TABLE `distritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_municipio` (`id_municipio`);

--
-- Indices de la tabla `especialidades`
--
ALTER TABLE `especialidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `instructores`
--
ALTER TABLE `instructores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_municipios` (`id_municipios`);

--
-- Indices de la tabla `modalidad_cursos`
--
ALTER TABLE `modalidad_cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulosespecialidades`
--
ALTER TABLE `modulosespecialidades`
  ADD PRIMARY KEY (`id_especialidad`),
  ADD KEY `id_modulo` (`id_modulo`);

--
-- Indices de la tabla `modulosformacion`
--
ALTER TABLE `modulosformacion`
  ADD PRIMARY KEY (`idmodulo`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`folio_mun`);

--
-- Indices de la tabla `oferta`
--
ALTER TABLE `oferta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEspecialidad` (`idEspecialidad`);

--
-- Indices de la tabla `ofertaacademica`
--
ALTER TABLE `ofertaacademica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_plantel` (`id_plantel`),
  ADD KEY `id_especialidad` (`id_especialidad`);

--
-- Indices de la tabla `plantel`
--
ALTER TABLE `plantel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plantel_especialidad`
--
ALTER TABLE `plantel_especialidad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_plantel` (`Id_plantel`),
  ADD KEY `id_especialidad` (`id_especialidad`);

--
-- Indices de la tabla `sec_apps`
--
ALTER TABLE `sec_apps`
  ADD PRIMARY KEY (`app_name`);

--
-- Indices de la tabla `sec_groups`
--
ALTER TABLE `sec_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indices de la tabla `sec_groups_apps`
--
ALTER TABLE `sec_groups_apps`
  ADD PRIMARY KEY (`group_id`,`app_name`),
  ADD KEY `sec_groups_apps_ibfk_2` (`app_name`);

--
-- Indices de la tabla `sec_users`
--
ALTER TABLE `sec_users`
  ADD PRIMARY KEY (`login`);

--
-- Indices de la tabla `sec_users_groups`
--
ALTER TABLE `sec_users_groups`
  ADD PRIMARY KEY (`login`,`group_id`),
  ADD KEY `sec_users_groups_ibfk_2` (`group_id`);

--
-- Indices de la tabla `status_curso`
--
ALTER TABLE `status_curso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usr`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alta_alumnos`
--
ALTER TABLE `alta_alumnos`
  MODIFY `id_alumno` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `alumno_curso2018cae`
--
ALTER TABLE `alumno_curso2018cae`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `alumno_curso2018ebc`
--
ALTER TABLE `alumno_curso2018ebc`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `alumno_curso2018ext`
--
ALTER TABLE `alumno_curso2018ext`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `alumno_curso2018reg`
--
ALTER TABLE `alumno_curso2018reg`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `alumno_curso2018roco`
--
ALTER TABLE `alumno_curso2018roco`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `asignacion_curso`
--
ALTER TABLE `asignacion_curso`
  MODIFY `Folio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT de la tabla `cursos2018_cae`
--
ALTER TABLE `cursos2018_cae`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cursos2018_ebc`
--
ALTER TABLE `cursos2018_ebc`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cursos2018_extension`
--
ALTER TABLE `cursos2018_extension`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cursos2018_regular`
--
ALTER TABLE `cursos2018_regular`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `cursos2018_roco`
--
ALTER TABLE `cursos2018_roco`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `distritos`
--
ALTER TABLE `distritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de la tabla `especialidades`
--
ALTER TABLE `especialidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `instructores`
--
ALTER TABLE `instructores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1832;

--
-- AUTO_INCREMENT de la tabla `modalidad_cursos`
--
ALTER TABLE `modalidad_cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `folio_mun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `oferta`
--
ALTER TABLE `oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT de la tabla `ofertaacademica`
--
ALTER TABLE `ofertaacademica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT de la tabla `plantel`
--
ALTER TABLE `plantel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `plantel_especialidad`
--
ALTER TABLE `plantel_especialidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT de la tabla `sec_groups`
--
ALTER TABLE `sec_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `status_curso`
--
ALTER TABLE `status_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alta_alumnos`
--
ALTER TABLE `alta_alumnos`
  ADD CONSTRAINT `alta_alumnos_ibfk_1` FOREIGN KEY (`municipio`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `alumno_curso2018cae`
--
ALTER TABLE `alumno_curso2018cae`
  ADD CONSTRAINT `alumno_curso2018cae_ibfk_1` FOREIGN KEY (`id_alumno_c`) REFERENCES `alta_alumnos` (`id_alumno`),
  ADD CONSTRAINT `alumno_curso2018cae_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos2018_cae` (`folio`);

--
-- Filtros para la tabla `alumno_curso2018ebc`
--
ALTER TABLE `alumno_curso2018ebc`
  ADD CONSTRAINT `alumno_curso2018ebc_ibfk_1` FOREIGN KEY (`id_alumno_c`) REFERENCES `alta_alumnos` (`id_alumno`),
  ADD CONSTRAINT `alumno_curso2018ebc_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos2018_ebc` (`folio`);

--
-- Filtros para la tabla `alumno_curso2018ext`
--
ALTER TABLE `alumno_curso2018ext`
  ADD CONSTRAINT `alumno_curso2018ext_ibfk_1` FOREIGN KEY (`id_alumno_c`) REFERENCES `alta_alumnos` (`id_alumno`),
  ADD CONSTRAINT `alumno_curso2018ext_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos2018_extension` (`folio`);

--
-- Filtros para la tabla `alumno_curso2018reg`
--
ALTER TABLE `alumno_curso2018reg`
  ADD CONSTRAINT `alumno_curso2018reg_ibfk_1` FOREIGN KEY (`id_alumno_c`) REFERENCES `alta_alumnos` (`id_alumno`),
  ADD CONSTRAINT `alumno_curso2018reg_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos2018_regular` (`folio`);

--
-- Filtros para la tabla `alumno_curso2018roco`
--
ALTER TABLE `alumno_curso2018roco`
  ADD CONSTRAINT `alumno_curso2018roco_ibfk_1` FOREIGN KEY (`id_alumno_c`) REFERENCES `alta_alumnos` (`id_alumno`),
  ADD CONSTRAINT `alumno_curso2018roco_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos2018_roco` (`folio`);

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidades` (`id`);

--
-- Filtros para la tabla `cursos2018_cae`
--
ALTER TABLE `cursos2018_cae`
  ADD CONSTRAINT `cursos2018_cae_ibfk_1` FOREIGN KEY (`id_instructor`) REFERENCES `instructores` (`id`),
  ADD CONSTRAINT `cursos2018_cae_ibfk_2` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `cursos2018_cae_ibfk_3` FOREIGN KEY (`id_mun_c`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `cursos2018_ebc`
--
ALTER TABLE `cursos2018_ebc`
  ADD CONSTRAINT `cursos2018_ebc_ibfk_1` FOREIGN KEY (`id_instructor`) REFERENCES `instructores` (`id`),
  ADD CONSTRAINT `cursos2018_ebc_ibfk_2` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `cursos2018_ebc_ibfk_4` FOREIGN KEY (`id_modulo_c`) REFERENCES `oferta` (`id`),
  ADD CONSTRAINT `cursos2018_ebc_ibfk_5` FOREIGN KEY (`id_mun_c`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `cursos2018_extension`
--
ALTER TABLE `cursos2018_extension`
  ADD CONSTRAINT `cursos2018_extension_ibfk_1` FOREIGN KEY (`id_instructor`) REFERENCES `instructores` (`id`),
  ADD CONSTRAINT `cursos2018_extension_ibfk_2` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `cursos2018_extension_ibfk_3` FOREIGN KEY (`id_mun_c`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `cursos2018_regular`
--
ALTER TABLE `cursos2018_regular`
  ADD CONSTRAINT `cursos2018_regular_ibfk_1` FOREIGN KEY (`id_instructor`) REFERENCES `instructores` (`id`),
  ADD CONSTRAINT `cursos2018_regular_ibfk_2` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `cursos2018_regular_ibfk_4` FOREIGN KEY (`id_modulo_c`) REFERENCES `oferta` (`id`),
  ADD CONSTRAINT `cursos2018_regular_ibfk_5` FOREIGN KEY (`id_mun_c`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `cursos2018_roco`
--
ALTER TABLE `cursos2018_roco`
  ADD CONSTRAINT `cursos2018_roco_ibfk_1` FOREIGN KEY (`id_instructor`) REFERENCES `instructores` (`id`),
  ADD CONSTRAINT `cursos2018_roco_ibfk_2` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `cursos2018_roco_ibfk_4` FOREIGN KEY (`id_modulo_c`) REFERENCES `oferta` (`id`),
  ADD CONSTRAINT `cursos2018_roco_ibfk_5` FOREIGN KEY (`id_mun_c`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `instructores`
--
ALTER TABLE `instructores`
  ADD CONSTRAINT `instructores_ibfk_1` FOREIGN KEY (`id_municipios`) REFERENCES `municipios` (`folio_mun`);

--
-- Filtros para la tabla `modulosespecialidades`
--
ALTER TABLE `modulosespecialidades`
  ADD CONSTRAINT `modulosespecialidades_ibfk_1` FOREIGN KEY (`id_modulo`) REFERENCES `modulosformacion` (`idmodulo`);

--
-- Filtros para la tabla `oferta`
--
ALTER TABLE `oferta`
  ADD CONSTRAINT `oferta_ibfk_1` FOREIGN KEY (`idEspecialidad`) REFERENCES `modulosespecialidades` (`id_especialidad`);

--
-- Filtros para la tabla `ofertaacademica`
--
ALTER TABLE `ofertaacademica`
  ADD CONSTRAINT `ofertaacademica_ibfk_1` FOREIGN KEY (`id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `ofertaacademica_ibfk_2` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidades` (`id`);

--
-- Filtros para la tabla `plantel_especialidad`
--
ALTER TABLE `plantel_especialidad`
  ADD CONSTRAINT `plantel_especialidad_ibfk_1` FOREIGN KEY (`Id_plantel`) REFERENCES `plantel` (`id`),
  ADD CONSTRAINT `plantel_especialidad_ibfk_2` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidades` (`id`);

--
-- Filtros para la tabla `sec_groups_apps`
--
ALTER TABLE `sec_groups_apps`
  ADD CONSTRAINT `sec_groups_apps_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `sec_groups` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sec_groups_apps_ibfk_2` FOREIGN KEY (`app_name`) REFERENCES `sec_apps` (`app_name`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sec_users_groups`
--
ALTER TABLE `sec_users_groups`
  ADD CONSTRAINT `sec_users_groups_ibfk_1` FOREIGN KEY (`login`) REFERENCES `sec_users` (`login`) ON DELETE CASCADE,
  ADD CONSTRAINT `sec_users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `sec_groups` (`group_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
